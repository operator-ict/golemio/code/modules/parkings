import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { models } from "#og/models";
import { ParkingsMeasurementsModels } from "#og/models/ParkingsMeasurementsModels";

chai.use(chaiAsPromised);

describe("ParkingMeasurementsModels", () => {
    const parkingMeasurementsModel: ParkingsMeasurementsModels = models.ParkingsMeasurementsModel;
    const parkingMeasurementsActualModel: ParkingsMeasurementsModels = models.ParkingsMeasurementsActualModel;

    it("should instantiate", () => {
        expect(parkingMeasurementsModel).not.to.be.undefined;
        expect(parkingMeasurementsActualModel).not.to.be.undefined;
    });

    describe("parkingMeasurementsModel", () => {
        describe("GetAll", async () => {
            describe("When called without params", () => {
                it("should return all items", async () => {
                    const result = await parkingMeasurementsModel.GetAll({});
                    expect(result).to.be.an.instanceOf(Array);
                    expect(result.length).to.be.at.least(30);
                });
            });

            describe("When called with from/to params", () => {
                it("should return correct subset of items", async () => {
                    const measureFrom = new Date("2021-02-26T12:01:00.000Z").toISOString();
                    const measureTo = new Date("2021-02-26T12:04:00.000Z").toISOString();

                    const result = await parkingMeasurementsModel.GetAll({
                        measureFrom,
                        measureTo,
                    });

                    for (const el of result) {
                        expect(new Date(el.date_modified) >= new Date(measureFrom)).to.be.true;
                    }

                    expect(result).to.be.an.instanceOf(Array);
                    expect(result.length).to.eql(3);
                });
            });

            describe("When called with source and sourceId params", () => {
                it("should return correct subset of items", async () => {
                    const result = await parkingMeasurementsModel.GetAll({
                        source: ["korid"],
                        sourceId: "22",
                    });

                    expect(result).to.be.an.instanceOf(Array);
                    expect(result.length).to.eql(1);
                });
            });

            describe("When called with sources array", () => {
                it("should return correct subset of items", async () => {
                    const sources = ["tsk", "korid"];
                    const result = await parkingMeasurementsModel.GetAll({
                        source: sources,
                    });

                    expect(result).to.be.an.instanceOf(Array);
                    for (const res of result) {
                        expect(sources).to.include(res.source);
                    }
                });
            });

            describe("When called with minutesBefore", () => {
                it("should return correct items", async () => {
                    const result = await parkingMeasurementsModel.GetAll({
                        minutesBefore: 15,
                    });

                    expect(result).to.be.an.instanceOf(Array);
                    expect(result.length).to.eql(1);
                    expect(result[0].source_id).to.eql("38");
                });
            });

            describe("When called with updatedSince", () => {
                it("should return correct items", async () => {
                    const now = Date.now();
                    const miliseconds = 3 * 60 * 60 * 1000; // 3 hours ago
                    const updatedSince = new Date(now - miliseconds);

                    const result = await parkingMeasurementsModel.GetAll({
                        updatedSince,
                    });

                    expect(result).to.be.an.instanceOf(Array);
                    expect(result.length).to.eql(1);
                    expect(result[0].source_id).to.eql("38");
                });
            });
        });
    });

    describe("parkingMeasurementsActualModel", () => {
        describe("GetAll", async () => {
            describe("When called without params", () => {
                it("should return all items", async () => {
                    const result = await parkingMeasurementsActualModel.GetAll({});
                    expect(result).to.be.an.instanceOf(Array);
                    expect(result.length).to.eql(4);
                });
            });

            describe("When called with from/to params", () => {
                it("should return correct subset of items", async () => {
                    const measureFrom = new Date("2021-02-28T00:00:00.000Z").toISOString();
                    const measureTo = new Date("2021-02-29T00:00:00.000Z").toISOString();

                    const result = await parkingMeasurementsActualModel.GetAll({
                        measureFrom,
                        measureTo,
                    });

                    for (const el of result) {
                        expect(new Date(el.date_modified) >= new Date(measureFrom)).to.be.true;
                    }

                    expect(result).to.be.an.instanceOf(Array);
                    expect(result.length).to.eql(1);
                });
            });

            describe("When called with source and sourceId params", () => {
                it("should return correct subset of items", async () => {
                    const result = await parkingMeasurementsActualModel.GetAll({
                        source: ["korid"],
                        sourceId: "21",
                    });

                    expect(result).to.be.an.instanceOf(Array);
                    expect(result.length).to.eql(1);
                });
            });

            describe("When called with minutesBefore", () => {
                it("should return correct items", async () => {
                    const result = await parkingMeasurementsActualModel.GetAll({
                        minutesBefore: 15,
                    });

                    expect(result).to.be.an.instanceOf(Array);
                    expect(result.length).to.eql(1);
                    expect(result[0].source_id).to.eql("52");
                });
            });

            describe("When called with updatedSince", () => {
                it("should return correct items", async () => {
                    const now = Date.now();
                    const miliseconds = 3 * 60 * 60 * 1000; // 3 hours ago
                    const updatedSince = new Date(now - miliseconds);

                    const result = await parkingMeasurementsActualModel.GetAll({
                        updatedSince,
                    });

                    expect(result).to.be.an.instanceOf(Array);
                    expect(result.length).to.eql(1);
                    expect(result[0].source_id).to.eql("52");
                });
            });
        });
    });
});

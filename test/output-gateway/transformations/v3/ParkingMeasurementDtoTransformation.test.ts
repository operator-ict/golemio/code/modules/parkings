import { ParkingMeasurementDtoTransformation } from "#og/transformations/v3/ParkingMeasurementDtoTransformation";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { v3MeasurementData } from "../../data/v3-parking-measurement-data";
import { v3MeasurementDataTransformed } from "../../data/v3-parking-measurement-data-transformed";

chai.use(chaiAsPromised);

describe("V3 Parking Measurement transformation", () => {
    let transformation: ParkingMeasurementDtoTransformation;

    before(() => {
        transformation = new ParkingMeasurementDtoTransformation();
    });

    it("should transform input data", async () => {
        const result = transformation.transformArray(v3MeasurementData);
        expect(result).to.deep.equal(v3MeasurementDataTransformed);
    });
});

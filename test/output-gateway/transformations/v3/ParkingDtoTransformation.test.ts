import { ParkingDtoTransformation } from "#og/transformations/v3/ParkingDtoTransformation";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import path from "path";
import { ILogger } from "@golemio/core/dist/helpers";

chai.use(chaiAsPromised);

describe("V3 Parking transformation", () => {
    let transformation: ParkingDtoTransformation;
    let inputData: any;
    let expectedResult: any;

    before(() => {
        const fakeLogger = { log: { error: () => {} } } as any as ILogger;
        transformation = new ParkingDtoTransformation(
            {
                tsk_v2: {
                    payment: null,
                    reservation: {
                        type: "possible",
                        web_url: null,
                        android_url: "https://ke-utc.appspot.com/static/onstreet.html?shortname={name}",
                        ios_url: null,
                        discovery_url: null,
                    },
                },
            },
            fakeLogger
        );
        inputData = JSON.parse(fs.readFileSync(path.join(__dirname + "/../../data/tskv2_test_parking_multiple.json"), "utf8"));
        expectedResult = fs.readFileSync(
            path.join(__dirname + "/../../data/tskv2_test_parking_multiple_transformed.json"),
            "utf8"
        );
    });

    it("should transform parking input data", async () => {
        const result = transformation.transformToFeatureCollection(inputData);
        expect(result).to.deep.equal(JSON.parse(expectedResult));
    });

    it("should transform average occupancy data", async () => {
        expect(
            transformation.transformAverageOccupancyArray([
                {
                    parking_id: "tsk2-P1-0586",
                    source: "tsk_v2",
                    source_id: "c66bb668-eab6-49da-bc84-002263e1dce0",
                    day_of_week: 2,
                    hour: "12",
                    average_occupancy: 64,
                    record_count: 1,
                    last_updated: new Date("2024-01-23T11:04:00.000Z"),
                },
                {
                    parking_id: "tsk2-P1-0586",
                    source: "tsk_v2",
                    source_id: "c66bb668-eab6-49da-bc84-002263e1dce0",
                    day_of_week: 2,
                    hour: "11",
                    average_occupancy: 65,
                    record_count: 3,
                    last_updated: new Date("2024-01-23T10:04:00.000Z"),
                },
            ])
        ).to.deep.equal({ "2": { "11": 65, "12": 64 } });
    });
});

import { ParkingMachinesDtoTransformation } from "#og/transformations/v3/ParkingMachinesDtoTransformation";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { parkingMachines } from "../../data/tskv2_test_parkingMachine_multiple";
import { parkingMachinesTransformed } from "../../data/tskv2_test_parkingMachine_multiple_transformed";

chai.use(chaiAsPromised);

describe("V3 Parking Machines transformation", () => {
    let transformation: ParkingMachinesDtoTransformation;

    before(() => {
        transformation = new ParkingMachinesDtoTransformation();
    });

    it("should transform input data", async () => {
        const result = transformation.transformToFeatureCollection(parkingMachines);
        expect(result).to.deep.equal(parkingMachinesTransformed);
    });
});

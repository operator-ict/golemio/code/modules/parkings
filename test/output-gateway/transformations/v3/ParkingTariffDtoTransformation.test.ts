import { ParkingTariffDtoTransformation } from "#og/transformations/v3/ParkingTariffDtoTransformation";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import path from "path";

chai.use(chaiAsPromised);

describe("V3 Parking Tariff Transformation", () => {
    let transformation: ParkingTariffDtoTransformation;
    let inputData: any;
    let expectedResult: any;

    before(() => {
        transformation = new ParkingTariffDtoTransformation();
        inputData = JSON.parse(fs.readFileSync(path.join(__dirname + "/../../data/tskv2_test_tariff_multiple.json"), "utf8"));

        inputData[0].parking_tariffs = inputData[0].parking_tariffs.map((element: any) => {
            return {
                ...element,
                valid_from: new Date(element.valid_from),
                valid_to: new Date(element.valid_to),
            };
        });
        expectedResult = JSON.parse(
            fs.readFileSync(path.join(__dirname + "/../../data/tskv2_test_tariff_multiple_transformed.json"), "utf8")
        );
    });

    it("should transform input data", async () => {
        expect(transformation.transformElement(inputData)).to.deep.equal(expectedResult);
    });
});

import { IParkingsMeasurement } from "#og/models/ParkingsMeasurementsModels";

export const measurementActualData: IParkingsMeasurement[] = [
    {
        available_spot_number: 20,
        closed_spot_number: 0,
        date_modified: "2022-07-27T10:37:06.563Z",
        occupied_spot_number: 82,
        source: "korid",
        source_id: "52",
        parking_id: "korid-52",
        total_spot_number: 102,
    },
    {
        source: "korid",
        source_id: "54",
        parking_id: "korid-54",
        available_spot_number: 4,
        closed_spot_number: 0,
        occupied_spot_number: 9,
        total_spot_number: 13,
        date_modified: "2021-02-28T23:19:47.384Z",
    },
    {
        source: "korid",
        source_id: "21",
        parking_id: "korid-21",
        available_spot_number: 6,
        closed_spot_number: 0,
        occupied_spot_number: 12,
        total_spot_number: 18,
        date_modified: "2021-02-26T11:58:42.311Z",
    },
];

import { IParkingsFeature } from "#og/transformations/interfaces/IParkingFeature";
import { GeoCoordinatesType } from "@golemio/core/dist/output-gateway";

export const featureItemDataId5: IParkingsFeature = {
    geometry: {
        coordinates: [
            [
                [15.074568223409903, 50.77590827454969],
                [15.074585370561389, 50.77586811775529],
                [15.074947701396539, 50.775930320848644],
                [15.07493055452721, 50.7759704777025],
                [15.074568223409903, 50.77590827454969],
            ],
        ],
        type: GeoCoordinatesType.Polygon,
    },
    properties: {
        id: "korid-54",
        source: "korid",
        source_id: "54",
        data_provider: "www.korid.cz",
        name: "Masarykova ulice",
        category: "park_paid_private",
        date_modified: "2021-02-24T16:30:06.236Z",
        address_formatted: null,
        address: null,
        area_served: null,
        web_app_payment_url: null,
        android_app_payment_url: null,
        ios_app_payment_url: null,
        total_spot_number: 10,
        tariff_id: null,
        valid_from: null,
        valid_to: null,
        parking_type: "on_street",
        zone_type: null,
        available_spots_last_updated: "2021-03-01T00:19:47+01:00",
        available_spots_number: 4,
        centroid: {
            type: GeoCoordinatesType.Point,
            coordinates: [15.05929990174349, 50.77035456801465],
        },
    },
    type: "Feature",
};

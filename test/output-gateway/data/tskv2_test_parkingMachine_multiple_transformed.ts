export const parkingMachinesTransformed = {
    features: [
        {
            geometry: {
                coordinates: [14.4626206, 50.0963113],
                type: "Point",
            },
            properties: {
                id: "osm-n_7770679009",
                primary_source: "osm",
                primary_source_id: "n_7770679009",
                code: null,
                machine_type: "payment_machine",
                valid_from: new Date("2024-06-12T09:17:52.422Z"),
                tariff_id: null,
            },
            type: "Feature",
        },
        {
            geometry: {
                coordinates: [14.463211568160052, 50.09726448399468],
                type: "Point",
            },
            properties: {
                id: "tsk_v2-a0e25b4f-83b4-4990-ad29-85f2636da799",
                primary_source: "tsk_v2",
                primary_source_id: "a0e25b4f-83b4-4990-ad29-85f2636da799",
                code: "8000069",
                machine_type: "info_box",
                valid_from: new Date("2023-03-27T12:22:55.370Z"),
                tariff_id: "3c2178d7-f3c6-4e5d-bbf8-89e9c9af9ee0",
            },
            type: "Feature",
        },
        {
            geometry: {
                coordinates: [14.463479479257808, 50.09713673615595],
                type: "Point",
            },
            properties: {
                id: "tsk_v2-cbacd85a-9257-4cb1-bc24-4d251e293cd6",
                primary_source: "tsk_v2",
                primary_source_id: "cbacd85a-9257-4cb1-bc24-4d251e293cd6",
                code: "8000068",
                machine_type: "payment_machine",
                valid_from: new Date("2023-03-27T12:22:55.370Z"),
                tariff_id: "3c2178d7-f3c6-4e5d-bbf8-89e9c9af9ee0",
            },
            type: "Feature",
        },
        {
            geometry: {
                coordinates: [14.462620643288227, 50.096311253724274],
                type: "Point",
            },
            properties: {
                id: "tsk_v2-ed44bcaa-3efb-411d-867e-51c22c094af0",
                primary_source: "tsk_v2",
                primary_source_id: "ed44bcaa-3efb-411d-867e-51c22c094af0",
                code: "8000034",
                machine_type: "payment_machine",
                valid_from: new Date("2023-03-27T12:22:55.370Z"),
                tariff_id: "b51b71ad-87ac-41e5-b2ce-a92a66ad45a3",
            },
            type: "Feature",
        },
        {
            geometry: {
                coordinates: [14.464719680196637, 50.0969080143518],
                type: "Point",
            },
            properties: {
                id: "tsk_v2-fb7f8377-ad5d-4583-b420-f2039022a776",
                primary_source: "tsk_v2",
                primary_source_id: "fb7f8377-ad5d-4583-b420-f2039022a776",
                code: "8000035",
                machine_type: "payment_machine",
                valid_from: new Date("2023-04-19T16:13:06.277Z"),
                tariff_id: "3c2178d7-f3c6-4e5d-bbf8-89e9c9af9ee0",
            },
            type: "Feature",
        },
        {
            geometry: {
                coordinates: [14.509560372775391, 50.08336146182989],
                type: "Point",
            },
            properties: {
                id: "tsk_v2-4521afac-66db-4680-abc5-0006837d977a",
                primary_source: "tsk_v2",
                primary_source_id: "4521afac-66db-4680-abc5-0006837d977a",
                code: "10000088",
                machine_type: "info_box",
                valid_from: new Date("2023-03-27T16:22:55.370Z"),
                tariff_id: "bdd68c37-2462-4825-befa-1eeab518dfbd",
            },
            type: "Feature",
        },
    ],
    type: "FeatureCollection",
};

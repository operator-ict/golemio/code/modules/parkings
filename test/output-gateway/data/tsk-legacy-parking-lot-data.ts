import { ITskParkingLotFeature } from "#og/data-access/interfaces/ITskParkingLotFeature";
import { GeoCoordinatesType, IGeoJSONFeatureCollection } from "@golemio/core/dist/output-gateway/Geo";

export const tskLegacyParkingsFeature: ITskParkingLotFeature = {
    geometry: {
        coordinates: [14.517897, 50.07622],
        type: GeoCoordinatesType.Point,
    },
    properties: {
        parking_type: {
            description: "P+R parkoviště",
            id: 1,
        },
        id: 534015,
        last_updated: 1674985504000,
        name: "Depo Hostivař",
        num_of_free_places: 98,
        num_of_taken_places: 6,
        updated_at: "2023-01-29T10:50:01.539Z",
        total_num_of_places: 104,
        address: {
            address_country: "Česko",
            address_formatted: "Černokostelecká, 10000 Hlavní město Praha Strašnice, Česko",
            address_locality: "Hlavní město Praha",
            address_region: "Strašnice",
            postal_code: "10000",
            street_address: "Černokostelecká",
        },
        average_occupancy: {
            "00": {
                "09": 5,
            },
            "01": {
                "09": 65,
            },
            "02": {
                "09": 77,
            },
            "03": {
                "09": 71,
            },
            "04": {
                "09": 75,
            },
            "05": {
                "09": 68,
            },
            "06": {
                "09": 12,
            },
        },
        district: "praha-10",
        payment_link: "https://ke-utc.appspot.com/static/select_offstreet.html?shortname=122",
        payment_shortname: "122",
    },
    type: "Feature",
};

export const tskLegacyParkingsData: IGeoJSONFeatureCollection = {
    features: [tskLegacyParkingsFeature],
    type: "FeatureCollection",
};

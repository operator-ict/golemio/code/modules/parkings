import { IParkingsMeasurement } from "#og/models/ParkingsMeasurementsModels";

export const measurementData: IParkingsMeasurement[] = [
    {
        source: "korid",
        source_id: "20",
        parking_id: "korid-20",
        available_spot_number: 5,
        closed_spot_number: 0,
        occupied_spot_number: 8,
        total_spot_number: 13,
        date_modified: "2021-02-26T12:02:46.188Z",
    },
    {
        source: "korid",
        source_id: "20",
        parking_id: "korid-20",
        available_spot_number: 4,
        closed_spot_number: 0,
        occupied_spot_number: 9,
        total_spot_number: 13,
        date_modified: "2021-02-26T12:01:46.188Z",
    },
];

import { IParkingMachine } from "#sch/models/interfaces/IParkingMachine";
import { Geometry } from "@golemio/core/dist/shared/geojson";

export const parkingMachines: IParkingMachine[] = [
    {
        id: "osm-n_7770679009",
        source: "osm",
        sourceId: "n_7770679009",
        code: null,
        type: "payment_machine",
        location: {
            crs: {
                type: "name",
                properties: {
                    name: "EPSG:4326",
                },
            },
            type: "Point",
            coordinates: [14.4626206, 50.0963113],
        } as Geometry,
        validFrom: new Date("2024-06-12T09:17:52.422Z"),
        tariffId: null,
    },
    {
        id: "tsk_v2-a0e25b4f-83b4-4990-ad29-85f2636da799",
        source: "tsk_v2",
        sourceId: "a0e25b4f-83b4-4990-ad29-85f2636da799",
        code: "8000069",
        type: "info_box",
        location: {
            crs: {
                type: "name",
                properties: {
                    name: "EPSG:4326",
                },
            },
            type: "Point",
            coordinates: [14.463211568160052, 50.09726448399468],
        } as Geometry,
        validFrom: new Date("2023-03-27T12:22:55.370Z"),
        tariffId: "3c2178d7-f3c6-4e5d-bbf8-89e9c9af9ee0",
    },
    {
        id: "tsk_v2-cbacd85a-9257-4cb1-bc24-4d251e293cd6",
        source: "tsk_v2",
        sourceId: "cbacd85a-9257-4cb1-bc24-4d251e293cd6",
        code: "8000068",
        type: "payment_machine",
        location: {
            crs: {
                type: "name",
                properties: {
                    name: "EPSG:4326",
                },
            },
            type: "Point",
            coordinates: [14.463479479257808, 50.09713673615595],
        } as Geometry,
        validFrom: new Date("2023-03-27T12:22:55.370Z"),
        tariffId: "3c2178d7-f3c6-4e5d-bbf8-89e9c9af9ee0",
    },
    {
        id: "tsk_v2-ed44bcaa-3efb-411d-867e-51c22c094af0",
        source: "tsk_v2",
        sourceId: "ed44bcaa-3efb-411d-867e-51c22c094af0",
        code: "8000034",
        type: "payment_machine",
        location: {
            crs: {
                type: "name",
                properties: {
                    name: "EPSG:4326",
                },
            },
            type: "Point",
            coordinates: [14.462620643288227, 50.096311253724274],
        } as Geometry,
        validFrom: new Date("2023-03-27T12:22:55.370Z"),
        tariffId: "b51b71ad-87ac-41e5-b2ce-a92a66ad45a3",
    },
    {
        id: "tsk_v2-fb7f8377-ad5d-4583-b420-f2039022a776",
        source: "tsk_v2",
        sourceId: "fb7f8377-ad5d-4583-b420-f2039022a776",
        code: "8000035",
        type: "payment_machine",
        location: {
            crs: {
                type: "name",
                properties: {
                    name: "EPSG:4326",
                },
            },
            type: "Point",
            coordinates: [14.464719680196637, 50.0969080143518],
        } as Geometry,
        validFrom: new Date("2023-04-19T16:13:06.277Z"),
        tariffId: "3c2178d7-f3c6-4e5d-bbf8-89e9c9af9ee0",
    },
    {
        id: "tsk_v2-4521afac-66db-4680-abc5-0006837d977a",
        source: "tsk_v2",
        sourceId: "4521afac-66db-4680-abc5-0006837d977a",
        code: "10000088",
        type: "info_box",
        location: {
            crs: {
                type: "name",
                properties: {
                    name: "EPSG:4326",
                },
            },
            type: "Point",
            coordinates: [14.509560372775391, 50.08336146182989],
        } as Geometry,
        validFrom: new Date("2023-03-27T16:22:55.370Z"),
        tariffId: "bdd68c37-2462-4825-befa-1eeab518dfbd",
    },
];

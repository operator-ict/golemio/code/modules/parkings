import { IParkingLatestMeasurements } from "#og/data-access/interfaces/IParkingMeasurementsDto";

export const v3MeasurementData: IParkingLatestMeasurements[] = [
    {
        source: "korid",
        source_id: "1",
        parking_id: "korid-1",
        available_spot_number: 2,
        closed_spot_number: 0,
        occupied_spot_number: 12,
        total_spot_number: 14,
        date_modified: new Date("2024-02-09T07:36:37.361Z"),
        updated_at: new Date("2024-02-09T07:36:37.110Z"),
    },
];

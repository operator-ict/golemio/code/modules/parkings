import { IV3ParkingMeasurementsDto } from "#og/transformations/interfaces/v3/IV3ParkingMeasurementsDto";

export const v3MeasurementDataTransformed: IV3ParkingMeasurementsDto[] = [
    {
        parking_id: "korid-1",
        primary_source: "korid",
        primary_source_id: "1",
        has_free_spots: true,
        total_spot_number: 14,
        free_spot_number: 2,
        closed_spot_number: 0,
        occupied_spot_number: 12,
        last_updated: new Date("2024-02-09T07:36:37.361Z"),
    },
];

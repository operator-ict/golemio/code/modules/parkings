import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { models } from "#og/models";
import { ParkingsModel } from "#og/models/ParkingsModel";
import sinon from "sinon";

chai.use(chaiAsPromised);

describe("ParkingModel", () => {
    const parkingModel: ParkingsModel = models.ParkingsModel;

    it("should instantiate", () => {
        expect(parkingModel).not.to.be.undefined;
    });

    describe("GetAll", async () => {
        describe("When called without params", () => {
            it("should return all items", async () => {
                const result = await parkingModel.GetAll();
                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(16);
            });
        });

        describe("When called with lng, lat and range param", () => {
            it("should return correct subset of items", async () => {
                const result = await parkingModel.GetAll({
                    lat: 50.7628457146645,
                    lng: 15.048722595067447,
                    range: 1300,
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(8);

                expect(result[0].distance).to.eql(0);
                expect(result[1].distance).to.eql(639.13698415);
                expect(result[2].distance).to.eql(716.00305254);
            });
        });

        describe("When called with source and sourceId params", () => {
            const source = "korid";

            it("should return correct item", async () => {
                const result = await parkingModel.GetAll({
                    source,
                    sourceId: "21",
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(1);
                expect(result[0].id).to.eql("korid-21");
            });

            it("should return correct subset of items", async () => {
                const result = await parkingModel.GetAll({
                    source,
                    limit: 2,
                    offset: 1,
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(2);
                expect(result[0].source_id).to.eql("10");
                expect(result[1].source_id).to.eql("11");
            });
        });

        describe("When called with minutesBefore", () => {
            it("should return correct items", async () => {
                const result = await parkingModel.GetAll({
                    minutesBefore: 15,
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(1);
                expect(result[0].id).to.eql("61122");
            });
        });

        describe("When called with updatedSince", () => {
            it("should return correct items", async () => {
                const now = Date.now();
                const miliseconds = 3 * 60 * 60 * 1000; // 3 hours ago
                const updatedSince = new Date(now - miliseconds);

                const result = await parkingModel.GetAll({
                    updatedSince,
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(1);
                expect(result[0].id).to.eql("61122");
            });
        });
    });

    describe("GetOne", async () => {
        it("should return single item", async () => {
            const id = "korid-54";
            const result = await parkingModel.GetOne(id);
            expect(result).not.to.be.empty;
            expect(result).to.be.an.instanceOf(Object);
            expect(result).to.have.property("id", id);
        });

        it("should return no item", async () => {
            const result = await parkingModel.GetOne("88");
            expect(result).to.be.null;
        });
    });

    describe("GetAllDetail", async () => {
        describe("When called without params", () => {
            it("should return all items", async () => {
                const result = await parkingModel.GetAllDetail();
                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(19);
            });
        });

        describe("When called with lng, lat and range param", () => {
            it("should return correct subset of items", async () => {
                const result = await parkingModel.GetAllDetail({
                    lat: 50.066266757446,
                    lng: 14.443355698017,
                    range: 500,
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(3);

                expect(result[0].distance).to.eql(8.13685803);
                expect(result[1].distance).to.eql(28.85684963);
                expect(result[2].distance).to.eql(61.94294786);
            });
        });

        describe("When called with source and sourceId params", () => {
            const source = "tsk";

            it("should return correct items", async () => {
                const result = await parkingModel.GetAllDetail({
                    source,
                    sourceId: "P10-0101",
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(3);
            });

            it("should return correct subset of items", async () => {
                const result = await parkingModel.GetAllDetail({
                    source,
                    limit: 2,
                    offset: 1,
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(2);
                for (const row of result) {
                    expect(row.id).to.include("tsk-p10-0101_");
                }
            });
        });

        describe("When called with minutesBefore", () => {
            it("should return correct items", async () => {
                let clock = sinon.useFakeTimers(new Date("2021-02-24T16:50:06.236Z"));

                const result = await parkingModel.GetAllDetail({
                    minutesBefore: 15,
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(10);
                expect(result[0].id).to.eql("61122");

                clock.restore();
            });
        });

        describe("When called with zoneType", () => {
            it("should return correct subset of items", async () => {
                const result = await parkingModel.GetAllDetail({
                    zoneType: ["zone_mixed", "zone_other"],
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(4);
            });
        });

        describe("When called with parkingType", () => {
            it("should return correct subset of items", async () => {
                const result = await parkingModel.GetAllDetail({
                    parkingType: ["on_street"],
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(10);
            });
        });

        describe("When called with zoneType and parkingType", () => {
            it("should return correct subset of items", async () => {
                const result = await parkingModel.GetAllDetail({
                    zoneType: ["zone_mixed"],
                    parkingType: ["on_street"],
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(3);
            });
        });

        describe("When called with updatedSince", () => {
            it("should return correct items", async () => {
                const now = new Date("2022-07-25T02:00:00.124Z").getTime();
                const miliseconds = 3 * 60 * 60 * 1000; // 3 hours ago
                const updatedSince = new Date(now - miliseconds);

                const result = await parkingModel.GetAllDetail({
                    updatedSince,
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(10);
                expect(result[0].id).to.eql("61122");
            });
        });
    });

    describe("GetOneDetail", async () => {
        it("should return single item", async () => {
            const id = "tsk-p10-0101_16425";
            const result = await parkingModel.GetOneDetail(id);
            expect(result).not.to.be.empty;
            expect(result).to.be.an.instanceOf(Object);
            expect(result).to.have.property("id", id);
        });

        it("should return no item", async () => {
            const result = await parkingModel.GetOneDetail("88");
            expect(result).to.be.null;
        });
    });
});

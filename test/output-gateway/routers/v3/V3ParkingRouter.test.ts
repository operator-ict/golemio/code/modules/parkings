import { log } from "@golemio/core/dist/output-gateway";
import express, { Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, match } from "sinon";
import request from "supertest";
import { v3ParkingRouter } from "#og";
import OpenDataHelper from "#og/helpers/OpenDataHelper";
import { SourceEnum } from "#helpers/constants/SourceEnum";

chai.use(chaiAsPromised);

describe("V3ParkingRouter", () => {
    const app = express();
    let sandbox: SinonSandbox;

    before(() => {
        app.use(v3ParkingRouter.getPath(), v3ParkingRouter.getRouter());
        app.use((err: any, req: Request, res: Response) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("it should find 1 entry with 2 opening dates valid from 2024-05-31T00:00:00Z", async () => {
        const response = await request(app)
            .get("/v3/parking?primarySource=isphk&openingHoursValidFrom=2024-05-31T00:00:00Z")
            .set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.body.features).to.have.lengthOf(4);

        const isphkObject005 = response.body.features.find((item: any) => item.properties.id === "isphk-hk_isp_005");
        const isphkObject002 = response.body.features.find((item: any) => item.properties.id === "isphk-hk_isp_002");
        const isphkObject003 = response.body.features.find((item: any) => item.properties.id === "isphk-hk_isp_003");

        expect(isphkObject005.properties.opening_hours).to.have.lengthOf(2);
        expect(isphkObject002.properties.opening_hours).to.have.lengthOf(0);
        expect(isphkObject003.properties.opening_hours).to.have.lengthOf(0);
    });

    it("it should find 1 entry with 1 opening date that matches the following criteria", async () => {
        const response = await request(app)
            .get(
                // eslint-disable-next-line max-len
                "/v3/parking?primarySource=isphk&openingHoursValidFrom=2024-06-25T12:01:00.000Z&openingHoursValidTo=2024-06-26T12:01:00.000Z"
            )
            .set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.body.features).to.have.lengthOf(4);

        const isphkObject005 = response.body.features.find((item: any) => item.properties.id === "isphk-hk_isp_005");
        const isphkObject002 = response.body.features.find((item: any) => item.properties.id === "isphk-hk_isp_002");
        const isphkObject003 = response.body.features.find((item: any) => item.properties.id === "isphk-hk_isp_003");

        expect(isphkObject005.properties.opening_hours).to.have.lengthOf(1);
        expect(isphkObject002.properties.opening_hours).to.have.lengthOf(0);
        expect(isphkObject003.properties.opening_hours).to.have.lengthOf(0);
    });

    it("it should find 3 entries with 2,1,1 opening dates respectively valid from 2021-03-26T12:01:00.000Z", async () => {
        const response = await request(app)
            .get("/v3/parking?primarySource=isphk&openingHoursValidFrom=2021-03-26T12:01:00.000Z")
            .set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.body.features).to.have.lengthOf(4);

        const isphkObject005 = response.body.features.find((item: any) => item.properties.id === "isphk-hk_isp_005");
        const isphkObject002 = response.body.features.find((item: any) => item.properties.id === "isphk-hk_isp_002");
        const isphkObject003 = response.body.features.find((item: any) => item.properties.id === "isphk-hk_isp_003");

        expect(isphkObject005.properties.opening_hours).to.have.lengthOf(2);
        expect(isphkObject002.properties.opening_hours).to.have.lengthOf(1);
        expect(isphkObject003.properties.opening_hours).to.have.lengthOf(1);
    });

    it("it should find the id tsk2-P1-0586", async () => {
        const response = await request(app)
            .get("/v3/parking/tsk2-P1-0586?isRestrictedToOpenData=false")
            .set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.properties.id).to.equal("tsk2-P1-0586");
    });

    it("it should find 4 entries with the source isphk", async () => {
        const response = await request(app).get("/v3/parking?primarySource=isphk").set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features).to.have.lengthOf(4);
    });

    it("it should find all entries from a secondary source", async () => {
        const response = await request(app)
            .get("/v3/parking?primarySource[]=osm&isRestrictedToOpenData=false")
            .set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features).to.be.an("array").of.length(12);
        expect(response.body.features).to.satisfy((features: unknown[]) =>
            // @ts-expect-error ts(2339)
            features.every((ft: unknown) => ft?.properties?.primary_source === "osm")
        );
    });

    it("it should include entries from primary and non-overlapping secondary sources when requested all entries", async () => {
        const secondarySourceParkingsIdsWithPrimarySourceOverlap = [
            "osm-w_478696407",
            "osm-r_6296601",
            "osm-w_478800278",
            "osm-w_841366329",
            "osm-w_841393012",
            "osm-w_1213412035",
            "osm-w_669661031",
            "osm-w_1213412034",
            "osm-w_51788842",
            "osm-w_841362302",
        ];

        const response = await request(app).get("/v3/parking?isRestrictedToOpenData=false").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features).to.be.an("array").that.is.not.empty;
        expect(
            response.body.features.filter(
                // @ts-expect-error ts(2339)
                (ft: unknown) => ft?.properties?.primary_source !== SourceEnum.OSM
            )
        ).to.not.be.empty;
        const secondarySourceResults = response.body.features.filter(
            // @ts-expect-error ts(2339)
            (ft: unknown) => ft?.properties?.primary_source === SourceEnum.OSM
        );
        expect(secondarySourceResults).to.not.be.empty;
        // @ts-expect-error ts(2339)
        const resultsIdsFromSecondarySource = secondarySourceResults.map((it: unknown) => it?.id);
        for (const id of secondarySourceParkingsIdsWithPrimarySourceOverlap) {
            expect(resultsIdsFromSecondarySource).to.not.include(id);
        }
    });

    it("it should filter by parking policy", async () => {
        const response = await request(app).get("/v3/parking?parkingPolicy=commercial").set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features).to.have.lengthOf(4);
    });

    it("should filter by source", async () => {
        const response = await request(app).get("/v3/parking?primarySource[]=korid").set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features).to.have.lengthOf(7);
    });

    it("it should filter active parkings", async () => {
        const response = await request(app).get("/v3/parking?activeOnly=true").set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features).to.have.lengthOf(11);
    });

    it("it should sanitize valid query param 'isRestrictedToOpenData' of value 'true' for route '/'", async () => {
        const openDataHelperParseOpenDataParamsSpy = sandbox.spy(OpenDataHelper, "parseOpenDataParam");
        const response = await request(app).get("/v3/parking?isRestrictedToOpenData=true").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(openDataHelperParseOpenDataParamsSpy.calledOnce).to.be.true;
        expect(
            openDataHelperParseOpenDataParamsSpy.calledWithMatch(match.hasNested("query.isRestrictedToOpenData", "true") as any)
        ).to.be.true;
    });

    it("it should sanitize valid query param 'isRestrictedToOpenData' of value 'false' for route '/'", async () => {
        const openDataHelperParseOpenDataParamsSpy = sandbox.spy(OpenDataHelper, "parseOpenDataParam");
        const response = await request(app)
            .get("/v3/parking?isRestrictedToOpenData=false&parkingPolicy=commercial")
            .set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(openDataHelperParseOpenDataParamsSpy.calledOnce).to.be.true;
        expect(
            openDataHelperParseOpenDataParamsSpy.calledWithMatch(match.hasNested("query.isRestrictedToOpenData", "false") as any)
        ).to.be.true;
    });

    it("it should sanitize invalid query param 'isRestrictedToOpenData' for route '/'", async () => {
        const response = await request(app).get("/v3/parking?isRestrictedToOpenData=kek").set("Accept", "application/json");
        expect(response.status).to.equal(400);
    });

    it("it should sanitize valid query param 'isRestrictedToOpenData' of value '1' for route '/'", async () => {
        const openDataHelperParseOpenDataParamsSpy = sandbox.spy(OpenDataHelper, "parseOpenDataParam");
        const response = await request(app).get("/v3/parking?isRestrictedToOpenData=1").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(openDataHelperParseOpenDataParamsSpy.calledOnce).to.be.true;
        expect(openDataHelperParseOpenDataParamsSpy.calledWithMatch(match.hasNested("query.isRestrictedToOpenData", "1") as any))
            .to.be.true;
    });

    it("it should sanitize valid query param 'isRestrictedToOpenData' of value '0' for route '/'", async () => {
        const openDataHelperParseOpenDataParamsSpy = sandbox.spy(OpenDataHelper, "parseOpenDataParam");
        const response = await request(app).get("/v3/parking?isRestrictedToOpenData=0").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(openDataHelperParseOpenDataParamsSpy.calledOnce).to.be.true;
        expect(openDataHelperParseOpenDataParamsSpy.calledWithMatch(match.hasNested("query.isRestrictedToOpenData", "0") as any))
            .to.be.true;
    });

    it("it should sanitize valid undefined query param 'isRestrictedToOpenData' for route '/'", async () => {
        const openDataHelperParseOpenDataParamsStub = sandbox.stub(OpenDataHelper, "parseOpenDataParam");
        let openDataHelperParseOpenDataParamsStubReq: any = {};
        openDataHelperParseOpenDataParamsStub.callsFake((req: Request) => {
            openDataHelperParseOpenDataParamsStubReq = req;
            return openDataHelperParseOpenDataParamsStub.wrappedMethod(req);
        });
        const response = await request(app).get("/v3/parking").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(openDataHelperParseOpenDataParamsStub.calledOnce).to.be.true;
        expect(openDataHelperParseOpenDataParamsStubReq?.query)
            .to.be.an("object")
            .and.to.not.have.property("isRestrictedToOpenData");
    });

    it("it should sanitize valid query param 'isRestrictedToOpenData' of value 'true' for route '/:id'", async () => {
        const openDataHelperParseOpenDataParamsSpy = sandbox.spy(OpenDataHelper, "parseOpenDataParam");
        const response = await request(app)
            .get("/v3/parking/tsk2-P1-0586?isRestrictedToOpenData=true")
            .set("Accept", "application/json");

        expect(response.status).to.equal(404);
        expect(openDataHelperParseOpenDataParamsSpy.calledOnce).to.be.true;
        expect(
            openDataHelperParseOpenDataParamsSpy.calledWithMatch(match.hasNested("query.isRestrictedToOpenData", "true") as any)
        ).to.be.true;
    });

    it("it should sanitize valid query param 'isRestrictedToOpenData' of value 'false' for route '/:id'", async () => {
        const openDataHelperParseOpenDataParamsSpy = sandbox.spy(OpenDataHelper, "parseOpenDataParam");
        const response = await request(app)
            .get("/v3/parking/isphk-hk_isp_001?isRestrictedToOpenData=false")
            .set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");

        expect(openDataHelperParseOpenDataParamsSpy.calledOnce).is.true;
        expect(
            openDataHelperParseOpenDataParamsSpy.calledWithMatch(match.hasNested("query.isRestrictedToOpenData", "false") as any)
        ).to.be.true;
    });

    it("it should sanitize invalid query param 'isRestrictedToOpenData' for route '/:id'", async () => {
        const response = await request(app)
            .get("/v3/parking/tsk2-P1-0586?isRestrictedToOpenData=kek")
            .set("Accept", "application/json");
        expect(response.status).to.equal(400);
    });

    it.skip("it should sanitize valid undefined query param 'isRestrictedToOpenData' for route '/:id'", async () => {
        const openDataHelperParseOpenDataParamsStub = sandbox.stub(OpenDataHelper, "parseOpenDataParam");
        let openDataHelperParseOpenDataParamsStubReq: any = {};
        openDataHelperParseOpenDataParamsStub.callsFake((req: Request) => {
            openDataHelperParseOpenDataParamsStubReq = req;
            return openDataHelperParseOpenDataParamsStub.wrappedMethod(req);
        });
        const response = await request(app).get("/v3/parking/tsk2-P1-0586").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        // TODO: the stub does not get called for some reason and so the test fails
        expect(openDataHelperParseOpenDataParamsStub.called).to.be.true;
        expect(openDataHelperParseOpenDataParamsStubReq?.query)
            .to.be.an("object")
            .and.to.not.have.property("isRestrictedToOpenData");
    });

    it(
        "it should include entries from primary and non-overlapping secondary sources when requested select primary and" +
            " secondary sources",
        async () => {
            const secondarySourceParkingsIdsWithPrimarySourceOverlap = [
                "osm-w_478696407",
                "osm-r_6296601",
                "osm-w_478800278",
                "osm-w_841366329",
                "osm-w_841393012",
                "osm-w_1213412035",
                "osm-w_669661031",
                "osm-w_1213412034",
                "osm-w_51788842",
                "osm-w_841362302",
            ];

            const response = await request(app)
                .get("/v3/parking?primarySource[]=osm&primarySource[]=korid&isRestrictedToOpenData=false")
                .set("Accept", "application/json");

            expect(response.status).to.equal(200);
            expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
            expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
            expect(response.body.features).to.be.an("array").that.is.not.empty;
            expect(
                response.body.features.filter(
                    // @ts-expect-error ts(2339)
                    (ft: unknown) => ft?.properties?.primary_source === SourceEnum.Korid
                )
            ).to.not.be.empty;
            const osmResults = response.body.features.filter(
                // @ts-expect-error ts(2339)
                (ft: unknown) => ft?.properties?.primary_source === SourceEnum.OSM
            );
            expect(osmResults).to.not.be.empty;
            // @ts-expect-error ts(2339)
            const resultsIdsFromSecondarySource = osmResults.map((it: unknown) => it?.id);
            for (const id of secondarySourceParkingsIdsWithPrimarySourceOverlap) {
                expect(resultsIdsFromSecondarySource).to.not.include(id);
            }
            expect(
                response.body.features.filter(
                    // @ts-expect-error ts(2339)
                    (ft: unknown) => ![SourceEnum.Korid, SourceEnum.OSM].includes(ft?.properties?.primary_source)
                )
            ).to.be.empty;
        }
    );

    it("it should find all isphk parkings including inactive", async () => {
        const response = await request(app)
            .get("/v3/parking?primarySource=isphk&activeOnly=false")
            .set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features).to.have.lengthOf(5);
        expect(response.body.features[0].properties.id).to.eql("isphk-hk_isp_001");
    });
});

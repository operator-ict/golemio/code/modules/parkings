import { v3ParkingMachinesRouter } from "#og";
import { log } from "@golemio/core/dist/output-gateway";
import express, { Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import request from "supertest";

chai.use(chaiAsPromised);

describe("V3ParkingMachinesRouter", () => {
    const app = express();
    let sandbox: SinonSandbox;

    before(() => {
        app.use(v3ParkingMachinesRouter.getPath(), v3ParkingMachinesRouter.getRouter());
        app.use((err: any, req: Request, res: Response) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("it should return one object out of two (1 filtered out with the region of interest)", async () => {
        const response = await request(app).get("/v3/parking-machines").set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features).to.have.lengthOf(1);
    });
});

import { v3ParkingSourcesRouter } from "#og/routers/v3/V3ParkingSourcesRouter";
import { log } from "@golemio/core/dist/output-gateway";
import express, { Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import request from "supertest";

chai.use(chaiAsPromised);

describe("V3ParkingSourcesRouter", () => {
    const app = express();
    let sandbox: SinonSandbox;

    before(() => {
        app.use(v3ParkingSourcesRouter.getPath(), v3ParkingSourcesRouter.getRouter());
        app.use((err: any, req: Request, res: Response) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("it should find entries for admin", async () => {
        const response = await request(app)
            .get("/v3/parking-sources?isRestrictedToOpenData=false")
            .set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=43200, stale-while-revalidate=60");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.have.lengthOf(8);
    });

    it("it should find entries for opendatar", async () => {
        const response = await request(app)
            .get("/v3/parking-sources?isRestrictedToOpenData=true")
            .set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=43200, stale-while-revalidate=60");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.have.lengthOf(5);
    });
});

import { ITskParkingLotFeature } from "#og/data-access/interfaces/ITskParkingLotFeature";
import { v1ParkingRouter } from "#og/routers/v1/V1ParkingRouter";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import { expect } from "chai";
import sinon, { SinonFakeTimers } from "sinon";
import request from "supertest";
import { tskLegacyParkingsData } from "../../data/tsk-legacy-parking-lot-data";

describe("V1ParkingRouter", () => {
    const app = express();
    let clock: SinonFakeTimers;

    before(() => {
        app.use(v1ParkingRouter.getPath(), v1ParkingRouter.getRouter());
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    beforeEach(() => {
        clock = sinon.useFakeTimers({
            now: new Date("2023-01-29T11:00:00+01:00").getTime(),
            shouldAdvanceTime: true,
        });
    });

    afterEach(() => {
        clock?.restore();
    });

    it("should respond with json to GET /v1/parkings (district=praha-10)", (done) => {
        request(app)
            .get("/v1/parkings?limit=1&district=praha-10")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body.features[0].properties.average_occupancy).to.have.all.keys([
                    "00",
                    "01",
                    "02",
                    "03",
                    "04",
                    "05",
                    "06",
                ]);

                (tskLegacyParkingsData.features[0] as ITskParkingLotFeature).properties.average_occupancy =
                    res.body.features[0].properties.average_occupancy;

                expect(res.body).to.deep.equal(tskLegacyParkingsData);
                done();
            });
    });

    it("should respond with json to GET /v1/parkings (latlng=14.517897,50.07622)", (done) => {
        request(app)
            .get("/v1/parkings?limit=1&latlng=14.517897,50.07622")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body.features[0].properties.average_occupancy).to.include.all.keys([
                    "00",
                    "01",
                    "02",
                    "03",
                    "04",
                    "05",
                    "06",
                ]);

                (tskLegacyParkingsData.features[0] as ITskParkingLotFeature).properties.average_occupancy =
                    res.body.features[0].properties.average_occupancy;

                expect(res.body).to.deep.equal(tskLegacyParkingsData);
                done();
            });
    });

    it("should respond with json to GET /v1/parkings/history", (done) => {
        request(app)
            .get("/v1/parkings/history?sensorId=534015")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body).to.have.length(23);
                expect(res.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
                for (const record of res.body) {
                    expect(record).to.have.all.keys([
                        "id",
                        "num_of_free_places",
                        "num_of_taken_places",
                        "updated_at",
                        "total_num_of_places",
                        "last_updated",
                    ]);
                }

                done();
            });
    });
});

import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import express, { Request, Response } from "@golemio/core/dist/shared/express";
import { v2ParkingRouter } from "#og/routers/v2/V2ParkingRouter";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import { log } from "@golemio/core/dist/output-gateway";
import request from "supertest";

import { featureItemDataId5 } from "../../data/parking-data-01";
import { featureItemDataId3 } from "../../data/parking-data-02";
import { measurementData } from "../../data/parking-measurement-data";
import { tariffData } from "../../data/parking-tariff-data";
import { featureLocationItemData16424, featureLocationItemData16425 } from "../../data/parking-location-data-01";
import { measurementActualData } from "../../data/parking-measurement-actual-data";
import sinon from "sinon";

chai.use(chaiAsPromised);

describe("V2ParkingRouter", () => {
    const app = express();

    before(() => {
        app.use(v2ParkingRouter.getPath(), v2ParkingRouter.getRouter());
        app.use((err: any, req: Request, res: Response) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond correctly to GET /v2/parking", async () => {
        const response = await request(app)
            .get(
                // eslint-disable-next-line max-len
                "/v2/parking?latlng=50.7759704777025,15.07493055452721&range=1000&source=korid&category[]=park_and_ride&category[]=park_paid_private&offset=0&limit=2"
            )
            .set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.deep.include({
            features: [featureItemDataId5, featureItemDataId3],
            type: "FeatureCollection",
        });
    });

    it("should respond with 400 on GET /v2/parking?source=isphk", async () => {
        const response = await request(app).get("/v2/parking?source=isphk").set("Accept", "application/json");

        expect(response.status).to.equal(400);
    });

    it("should respond correctly to GET /v2/parking with minutesBefore parameter", async () => {
        let clock = sinon.useFakeTimers(new Date("2021-02-24T16:30:06.236Z"));
        const response = await request(app).get("/v2/parking?minutesBefore=15").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features.length).to.equal(1);
        clock.restore();
    });

    it("should respond correctly to GET /v2/parking with updatedSince parameter", async () => {
        const now = new Date("2022-07-25T02:00:00.124Z").getTime();
        const miliseconds = 3 * 60 * 60 * 1000; // 3 hours ago
        const updatedSince = new Date(now - miliseconds);
        const url = `/v2/parking?updatedSince=${encodeURIComponent(updatedSince.toISOString())}`;

        const response = await request(app).get(url).set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features.length).to.equal(9);
    });

    it("should respond correctly to GET /v2/parking/:id", async () => {
        const response = await request(app).get("/v2/parking/korid-54").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.deep.include(featureItemDataId5);
    });

    it("should respond correctly to GET /v2/parking/detail", async () => {
        const response = await request(app)
            .get(
                // eslint-disable-next-line max-len
                "/v2/parking/detail?latlng=50.066196729,14.442636742&range=1000&source=TSK&category[]=zone_mixed&offset=0&limit=2"
            )
            .set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.deep.include({
            features: [featureLocationItemData16424, featureLocationItemData16425],
            type: "FeatureCollection",
        });
    });

    it("should respond correctly to GET /v2/parking/detail with minutesBefore parameter", async () => {
        let clock = sinon.useFakeTimers(new Date("2021-02-24T16:50:06.236Z"));
        const response = await request(app).get("/v2/parking/detail?minutesBefore=15").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features.length).to.equal(10);
        clock.restore();
    });

    it("should respond correctly to GET /v2/parking/detail with updatedSince parameter", async () => {
        const now = new Date("2022-07-25T02:00:00.124Z").getTime();
        const miliseconds = 3 * 60 * 60 * 1000; // 3 hours ago
        const updatedSince = new Date(now - miliseconds);
        const url = `/v2/parking/detail?updatedSince=${encodeURIComponent(updatedSince.toISOString())}`;

        const response = await request(app).get(url).set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features.length).to.equal(10);
    });

    it("should respond correctly to GET /v2/parking/detail/:id", async () => {
        const response = await request(app).get("/v2/parking/detail/tsk-p10-0101_16424").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.deep.include(featureLocationItemData16424);
    });

    it("should respond correctly to GET /v2/parking/measurements", async () => {
        const response = await request(app)
            .get("/v2/parking/measurements?source=korid&sourceId=20&from=2021-02-26T12:01:00.000Z&to=2021-02-26T12:03:00.000Z")
            .set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.have.deep.members(measurementData);
    });

    it("should respond correctly to GET /v2/parking/measurements with 'latest' query param", async () => {
        const response = await request(app)
            .get("/v2/parking/measurements?latest=true&source=korid")
            .set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.have.deep.members(measurementActualData);
    });

    it("should respond with correct header to GET /v2/parking/measurements", async () => {
        const response = await request(app)
            .get("/v2/parking/measurements?limit=4&offset=4&source=korid")
            .set("Accept", "application/json");

        expect(response.header["link"]).to.equal(
            // eslint-disable-next-line max-len
            "</parking/measurements?limit=4&source=korid&offset=0>; rel=previous, </parking/measurements?limit=4&source=korid&offset=8>; rel=next"
        );
    });

    it("should respond correctly to GET /v2/parking/measurements with minutesBefore parameter", async () => {
        const response = await request(app).get("/v2/parking/measurements?minutesBefore=15").set("Accept", "application/json");

        const expectedResult = {
            source_id: "38",
            available_spot_number: 14,
        };

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body[0].source_id).to.eq(expectedResult.source_id);
        expect(response.body[0].available_spot_number).to.eq(expectedResult.available_spot_number);
    });

    it("should respond correctly to GET /v2/parking/measurements with updatedSince parameter", async () => {
        const now = Date.now();
        const milliseconds = 3 * 60 * 60 * 1000; // 3 hours ago
        const updatedSince = new Date(now - milliseconds);
        const url = `/v2/parking/measurements?updatedSince=${encodeURIComponent(updatedSince.toISOString())}`;

        const response = await request(app).get(url).set("Accept", "application/json");

        const expectedResult = {
            source_id: "38",
            available_spot_number: 14,
        };

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body[0].source_id).to.eq(expectedResult.source_id);
        expect(response.body[0].available_spot_number).to.eq(expectedResult.available_spot_number);
    });

    it("should respond correctly to GET /v2/parking/measurements with minutesBefore and latest parameter", async () => {
        const response = await request(app)
            .get("/v2/parking/measurements?minutesBefore=15&latest=true")
            .set("Accept", "application/json");

        const expectedResult = {
            source_id: "52",
        };

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body[0].source_id).to.eq(expectedResult.source_id);
    });

    it("should respond correctly to GET /v2/parking/measurements with updatedSince and latest parameter", async () => {
        const now = Date.now();
        const miliseconds = 3 * 60 * 60 * 1000; // 3 hours ago
        const updatedSince = new Date(now - miliseconds);
        const url = `/v2/parking/measurements?latest=true&updatedSince=${encodeURIComponent(updatedSince.toISOString())}`;

        const response = await request(app).get(url).set("Accept", "application/json");

        const expectedResult = {
            source_id: "52",
        };

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body[0].source_id).to.eq(expectedResult.source_id);
    });

    it("should respond correctly to GET /v2/parking/tariffs", async () => {
        const response = await request(app).get("/v2/parking/tariffs?source=korid").set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=43200, stale-while-revalidate=60");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.eql(tariffData);
    });

    it("should respond correctly to GET /v2/parking/tariffs/:tariffId", async () => {
        const response = await request(app)
            .get("/v2/parking/tariffs/b184865e-5a5e-5465-abc1-4fab6bc56a77")
            .set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=43200, stale-while-revalidate=60");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body).to.deep.include(tariffData[0]);
    });

    it("should respond correctly to GET /v2/parking/detail with zoneType parameter", async () => {
        const url = `/v2/parking/detail?zoneType[]=zone_mixed`;

        const response = await request(app).get(url).set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features.length).to.equal(3);
        for (const parking of response.body.features) {
            expect(parking.properties.zone_type).to.equal("zone_mixed");
        }
    });

    it("should respond correctly to GET /v2/parking/detail with parkingType parameter", async () => {
        const url = `/v2/parking/detail?parkingType[]=on_street`;

        const response = await request(app).get(url).set("Accept", "application/json");

        expect(response.status).to.equal(200);
        expect(response.headers["cache-control"]).to.eq("public, s-maxage=300, stale-while-revalidate=5");
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.body.features.length).to.equal(10);
        for (const parking of response.body.features) {
            expect(parking.properties.parking_type).to.equal("on_street");
        }
    });
});

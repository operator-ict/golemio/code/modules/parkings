import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { models } from "#og/models";
import { ParkingsLocationModel } from "#og/models/ParkingsLocationModel";

chai.use(chaiAsPromised);

describe("ParkingsLocationModel", () => {
    const parkingLocationModel: ParkingsLocationModel = models.ParkingsLocationModel;

    it("should instantiate", () => {
        expect(parkingLocationModel).not.to.be.undefined;
    });
});

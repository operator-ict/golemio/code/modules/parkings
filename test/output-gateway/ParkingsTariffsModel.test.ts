import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { models } from "#og/models";
import { V2ParkingTariffsRepository } from "#og/data-access/v2/V2ParkingTariffsRepository";

chai.use(chaiAsPromised);

describe("ParkingTariffsModel", () => {
    const parkingTariffsModel: V2ParkingTariffsRepository = models.ParkingsTariffsModel;

    it("should instantiate", () => {
        expect(parkingTariffsModel).not.to.be.undefined;
    });

    describe("GetAll", async () => {
        describe("When called without params", () => {
            it("should return all items", async () => {
                const result = await parkingTariffsModel.GetAll();
                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(6);
            });
        });

        describe("When called with params", () => {
            it("should return correct subset of items", async () => {
                const result = await parkingTariffsModel.GetAll({
                    source: "korid",
                });

                expect(result).to.be.an.instanceOf(Array);
                expect(result.length).to.eql(6);
            });
        });
    });

    describe("GetOne", async () => {
        it("should return single item", async () => {
            const tariffId = "b184865e-5a5e-5465-abc1-4fab6bc56a77";
            const result = await parkingTariffsModel.GetOne(tariffId);
            expect(result).not.to.be.empty;
            expect(result).to.be.an.instanceOf(Array);
            expect(result.length).to.eql(4);

            result.every((el) => expect(el.tariff_id).to.equal(tariffId));
            expect(result.map((el) => el.charge_order_index)).to.eql([0, 1, 2, 3]);
        });

        it("should return no item", async () => {
            const result = await parkingTariffsModel.GetOne("nope-5a5e-5465-abc1-4fab6bc56a77");
            expect(result).to.be.an.instanceOf(Array);
            expect(result.length).to.eql(0);
        });
    });
});

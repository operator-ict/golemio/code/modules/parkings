import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import PaginationHelper from "#og/helpers/PaginationHelper";
import { IGetMeasurementsQuery } from "#og/routers/v2/V2ParkingRouter";

chai.use(chaiAsPromised);

describe("PaginationHelper", () => {
    const filterParams: IGetMeasurementsQuery = {
        source: "korid",
        sourceId: "e345",
    };

    describe("generatePagingUrl", async () => {
        it("should return correct paging Links Definition ~ prev, next", async () => {
            const result = PaginationHelper.generatePagingUrl(filterParams, 2, 10, 2);

            expect(result).to.eql(
                // eslint-disable-next-line max-len
                "</parking/measurements?limit=2&source=korid&sourceId=e345&offset=8>; rel=previous, </parking/measurements?limit=2&source=korid&sourceId=e345&offset=12>; rel=next"
            );
        });

        it("should return correct paging Links Definition ~ prev", async () => {
            const result = PaginationHelper.generatePagingUrl(filterParams, 1, 10, 2);

            expect(result).to.eql(
                // eslint-disable-next-line max-len
                "</parking/measurements?limit=2&source=korid&sourceId=e345&offset=8>; rel=previous"
            );
        });

        it("should return correct paging Links Definition ~ next", async () => {
            const result = PaginationHelper.generatePagingUrl(filterParams, 100, 0, 100);

            expect(result).to.eql(
                // eslint-disable-next-line max-len
                "</parking/measurements?limit=100&source=korid&sourceId=e345&offset=100>; rel=next"
            );
        });

        it("should return empty paging Links Definition", async () => {
            const result = PaginationHelper.generatePagingUrl(filterParams, 0, 0, 2);

            expect(result.length).to.eql(0);
        });
    });
});

import { SourceEnum } from "#helpers/constants/SourceEnum";
import { ParkingMachinesRepository } from "#og/data-access/ParkingMachinesRepository";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { IParkingMachine } from "#sch/models/interfaces/IParkingMachine";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { parkingMachines } from "../data/tskv2_test_parkingMachine_multiple";

chai.use(chaiAsPromised);

describe("ParkingMachinesRepository", () => {
    let repo: ParkingMachinesRepository;
    let connector: IDatabaseConnector;

    before(async () => {
        connector = OutputGatewayContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
        repo = ParkingsContainer.resolve<ParkingMachinesRepository>(ModuleContainerToken.ParkingMachinesRepository);
    });

    it("should return correct getAll results and filter out secondary source", async () => {
        const result = await repo.GetAll({ primarySource: ["tsk_v2", "osm"] });

        expect(result).to.be.an("array").that.has.length(5);
        expect(result).to.satisfy((res: IParkingMachine[]) => res.every((it) => it.source === SourceEnum.TSK_V2));
    });

    it("should return correct getAll results with applied filters", async () => {
        const result = await repo.GetAll({
            primarySource: ["tsk_v2"],
            validFrom: "2023-03-27T16:22:55.370+0200",
            type: ["info_box"],
            boundingBox: [50.017, 14.243, 50.123, 14.573],
            codeMask: "100%",
            limit: 10,
            offset: 0,
        });
        expect(result).to.deep.equal(parkingMachines.filter((el) => el.code?.startsWith("100")));
    });

    it("should return correct getOne results", async () => {
        const result = await repo.GetOne("osm-n_7770679009");

        expect(result).to.deep.equal(parkingMachines[0]);
    });
});

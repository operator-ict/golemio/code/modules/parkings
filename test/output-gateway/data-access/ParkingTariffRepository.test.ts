import { ParkingTariffsRepository } from "#og/data-access/ParkingTariffsRepository";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import path from "path";

chai.use(chaiAsPromised);

describe("ParkingTariff", () => {
    let repo: ParkingTariffsRepository;
    let connector: IDatabaseConnector;
    let expectedResultMultiple: any;

    before(async () => {
        connector = OutputGatewayContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
        repo = ParkingsContainer.resolve<ParkingTariffsRepository>(ModuleContainerToken.ParkingTariffRepository);
        expectedResultMultiple = JSON.parse(
            fs.readFileSync(path.join(__dirname + "/../data/tskv2_test_tariff_multiple.json"), "utf8")
        );
    });

    it("should return correct getAll results", async () => {
        const result = await repo.GetAll({
            primarySource: ["tsk_v2"],
            limit: 10,
            offset: 0,
        });
        expect(JSON.parse(JSON.stringify(result))).to.deep.equal(expectedResultMultiple);
    });

    it("should return correct getOne results", async () => {
        const result = await repo.GetOne("0639c53f-c555-49ac-b5e0-bb3d50acf588", ["tsk_v2"]);

        expect(JSON.parse(JSON.stringify(result))).to.deep.equal(expectedResultMultiple[0].parking_tariffs);
    });
});

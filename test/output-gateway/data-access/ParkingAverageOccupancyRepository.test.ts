import { ParkingAverageOccupancyRepository } from "#og/data-access/ParkingAverageOccupancyRepository";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import path from "path";

chai.use(chaiAsPromised);

describe("ParkingAverageOccupancyRepository", () => {
    let repo: ParkingAverageOccupancyRepository;
    let connector: IDatabaseConnector;
    let expectedResult: any[];

    before(async () => {
        connector = ParkingsContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
        repo = ParkingsContainer.resolve<ParkingAverageOccupancyRepository>(
            ModuleContainerToken.ParkingAverageOccupancyRepository
        );
        expectedResult = JSON.parse(
            fs.readFileSync(path.join(__dirname + "/../data/tskv2_test_parkingAverageOccupancy_multiple.json"), "utf8")
        );
    });

    it("should return correct getOne results", async () => {
        const result = await repo.GetOccupancy("tsk2-P1-0586", ["tsk_v2"]);
        expect(JSON.parse(JSON.stringify(result))).to.deep.equal(expectedResult);
    });
});

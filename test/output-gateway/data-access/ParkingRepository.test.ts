import { SourceEnum } from "#helpers/constants/SourceEnum";
import { ParkingRepository } from "#og/data-access/ParkingRepository";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { IParkingWithLocationAndOccupancyInfo } from "#sch/models/interfaces/IParking";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import path from "path";

chai.use(chaiAsPromised);

describe("ParkingRepository", () => {
    let repo: ParkingRepository;
    let connector: IDatabaseConnector;
    let expectedResultMultiple: any;

    before(async () => {
        connector = ParkingsContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
        repo = ParkingsContainer.resolve<ParkingRepository>(ModuleContainerToken.ParkingRepository);
        expectedResultMultiple = fs.readFileSync(path.join(__dirname + "/../data/tskv2_test_parking_multiple.json"), "utf8");
    });

    describe("GetAll", () => {
        it("should return correct result from a primary source", async () => {
            const result = await repo.GetAll({
                primarySource: ["tsk_v2"],
                validFrom: "2023-03-27T16:22:55.370+0200",
                boundingBox: [50.017, 14.243, 50.123, 14.573],
                accessDedicatedTo: undefined, //["disabled"],
                minutesBefore: undefined,
                updatedSince: undefined,
                limit: 1,
                offset: 0,
                isRestrictedToOpenData: false,
            });

            expect(JSON.parse(JSON.stringify(result))).to.deep.equal(JSON.parse(expectedResultMultiple));
        });

        it("should return results from a secondary source", async () => {
            const result = await repo.GetAll({
                primarySource: ["osm"],
                isRestrictedToOpenData: false,
            });

            expect(result).to.be.an("array").that.is.not.empty;
            expect(result).to.satisfy((res: IParkingWithLocationAndOccupancyInfo[]) => res.every((it) => it.source === "osm"));
        });

        it(
            "should return results from a secondary source, including those with an overlap with locations of primary source" +
                " parkings",
            async () => {
                const secondarySourceParkingsIdsWithPrimarySourceOverlap = [
                    "osm-w_478696407",
                    "osm-r_6296601",
                    "osm-w_478800278",
                    "osm-w_841366329",
                    "osm-w_841393012",
                    "osm-w_1213412035",
                    "osm-w_669661031",
                    "osm-w_1213412034",
                    "osm-w_51788842",
                    "osm-w_841362302",
                ];

                const result = await repo.GetAll({ primarySource: ["osm"], isRestrictedToOpenData: false });

                expect(result).to.be.an("array").that.is.not.empty;
                const secondarySourceResults = result.filter((it) => it.source === SourceEnum.OSM);
                expect(secondarySourceResults).to.be.of.length.greaterThan(
                    secondarySourceParkingsIdsWithPrimarySourceOverlap.length
                );
                const resultsIdsFromSecondarySource = secondarySourceResults.map((it) => it.id);
                for (const id of secondarySourceParkingsIdsWithPrimarySourceOverlap) {
                    expect(resultsIdsFromSecondarySource).to.include(id);
                }
                expect(secondarySourceResults).to.be.of.length(12);
            }
        );
    });

    describe("GetOne", () => {
        it("should return correct getOne results", async () => {
            const result = await repo.GetOne("tsk2-P1-0586", ["tsk_v2"]);
            expect(JSON.parse(JSON.stringify(result))).to.deep.equal(JSON.parse(expectedResultMultiple)[0]);
        });
    });
});

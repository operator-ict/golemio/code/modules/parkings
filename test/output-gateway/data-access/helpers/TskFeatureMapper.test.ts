import { ParkingTypeEnum, TskFeatureMapper } from "#og/data-access/helpers/TskFeatureMapper";
import { expect } from "chai";
import { tskLegacyParkingsFeature } from "../../data/tsk-legacy-parking-lot-data";

describe("TskFeatureMapper", () => {
    describe("formatOutput", () => {
        it("should return formatted feature", () => {
            const result = TskFeatureMapper.formatOutput({
                get: () => ({
                    centroid: {
                        crs: { type: "name", properties: { name: "EPSG:4326" } },
                        type: "Point",
                        coordinates: [14.517897, 50.07622],
                    },
                    parking_type: ParkingTypeEnum.ParkAndRide,
                    id: 534015,
                    last_updated: 1674985504000,
                    payment_link: "https://ke-utc.appspot.com/static/select_offstreet.html?shortname=122",
                    name: "Depo Hostivař",
                    num_of_free_places: 98,
                    num_of_taken_places: 6,
                    updated_at: "2023-01-29T10:50:01.539Z",
                    total_num_of_places: 104,
                    address: {
                        address_country: "Česko",
                        address_formatted: "Černokostelecká, 10000 Hlavní město Praha Strašnice, Česko",
                        address_locality: "Hlavní město Praha",
                        address_region: "Strašnice",
                        postal_code: "10000",
                        street_address: "Černokostelecká",
                    },
                    average_occupancy: {
                        "00": {
                            "09": 5,
                        },
                        "01": {
                            "09": 65,
                        },
                        "02": {
                            "09": 77,
                        },
                        "03": {
                            "09": 71,
                        },
                        "04": {
                            "09": 75,
                        },
                        "05": {
                            "09": 68,
                        },
                        "06": {
                            "09": 12,
                        },
                    },
                    district: "praha-10",
                }),
            } as any);
            expect(result).to.deep.equal(tskLegacyParkingsFeature);
        });
    });

    describe("createParkingTypeObject", () => {
        it("shoud map parking type to object (park_and_ride)", () => {
            expect(TskFeatureMapper["createParkingTypeObject"](ParkingTypeEnum.ParkAndRide)).to.deep.equal({
                description: "P+R parkoviště",
                id: 1,
            });
        });

        it("shoud map parking type to object (park_paid_private)", () => {
            expect(TskFeatureMapper["createParkingTypeObject"](ParkingTypeEnum.ParkPaidPrivate)).to.deep.equal({
                description: "placené parkoviště",
                id: 2,
            });
        });
    });

    describe("getPaymentAttributes", () => {
        it("shoud map payment link to object", () => {
            expect(
                TskFeatureMapper["getPaymentAttributes"]("https://ke-utc.appspot.com/static/select_offstreet.html?shortname=122")
            ).to.deep.equal({
                payment_link: "https://ke-utc.appspot.com/static/select_offstreet.html?shortname=122",
                payment_shortname: "122",
            });
        });

        it("shoud return empty object", () => {
            expect(TskFeatureMapper["getPaymentAttributes"](null)).to.deep.equal({});
        });
    });
});

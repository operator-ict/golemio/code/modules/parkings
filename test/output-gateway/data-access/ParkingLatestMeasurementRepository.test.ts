import { ParkingLatestMeasurementRepository } from "#og/data-access/ParkingLatestMeasurementRepository";
import { ParkingMeasurementRepository } from "#og/data-access/ParkingMeasurementRepository";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { Parkings } from "#sch";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import path from "path";
import { QueryTypes } from "@golemio/core/dist/shared/sequelize";

chai.use(chaiAsPromised);

describe("ParkingLatestMeasurementRepository", () => {
    let latestMeasurementRepository: ParkingLatestMeasurementRepository;
    let measurementRepository: ParkingMeasurementRepository;
    let connector: IDatabaseConnector;
    let expectedResult: any;
    const now = new Date().toISOString();

    before(async () => {
        connector = ParkingsContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
        latestMeasurementRepository = ParkingsContainer.resolve<ParkingLatestMeasurementRepository>(
            ModuleContainerToken.ParkingLatestMeasurementRepository
        );
        measurementRepository = ParkingsContainer.resolve<ParkingMeasurementRepository>(
            ModuleContainerToken.ParkingMeasurementRepository
        );
        expectedResult = JSON.parse(
            fs.readFileSync(path.join(__dirname + "/../data/tskv2_test_parkingLatestMeasurements.json"), "utf8")
        );
        expectedResult[0].date_modified = now;
        expectedResult[0].updated_at = now;

        await measurementRepository["sequelizeModel"].sequelize!.query(
            `CALL ${Parkings.pgSchema}.calculate_average_occupancy()`,
            {
                plain: true,
                type: QueryTypes.SELECT,
            }
        );

        await measurementRepository["sequelizeModel"].create({
            source: "tsk_v2",
            source_id: "c66bb668-eab6-49da-bc84-002263e1dce0",
            parking_id: "tsk2-P1-0586",
            available_spot_number: 50,
            closed_spot_number: null,
            occupied_spot_number: 54,
            total_spot_number: 104,
            date_modified: now,
            updated_at: now,
        });
    });

    after(async () => {
        await measurementRepository["sequelizeModel"].destroy({
            where: {
                source: "tsk_v2",
                source_id: "c66bb668-eab6-49da-bc84-002263e1dce0",
                date_modified: now,
            },
        });
    });

    it("should return correct getAll results", async () => {
        const result = await latestMeasurementRepository.GetAll({
            primarySource: ["tsk_v2"],
            parkingId: ["tsk2-P1-0586"],
            limit: 10,
            offset: 0,
        });
        expect(JSON.parse(JSON.stringify(result))).to.deep.equal(expectedResult);
    });
});

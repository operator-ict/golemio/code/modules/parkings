import { SourceEnum } from "#helpers/constants/SourceEnum";
import { ParkingMachinesFilteredRepository } from "#og/data-access/ParkingMachinesFilteredRepository";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { IParkingMachine } from "#sch/models/interfaces/IParkingMachine";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("ParkingMachinesFilteredRepository", () => {
    let repo: ParkingMachinesFilteredRepository;
    let connector: IDatabaseConnector;

    before(async () => {
        connector = OutputGatewayContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
        repo = ParkingsContainer.resolve<ParkingMachinesFilteredRepository>(
            ModuleContainerToken.ParkingMachinesFilteredRepository
        );
    });

    describe("GetAll", () => {
        it("should return all results from a primary source", async () => {
            const result = await repo.GetAll({ primarySource: ["tsk_v2"] });

            expect(result).to.be.an("array").that.has.length(5);
            expect(result).to.satisfy((res: IParkingMachine[]) => res.every((it) => it.source === SourceEnum.TSK_V2));
        });

        it("should return filtered results from a secondary source", async () => {
            const result = await repo.GetAll({ primarySource: ["osm"] });

            expect(result).to.be.an("array").that.has.length(1);
            expect(result).to.satisfy((res: IParkingMachine[]) => res.every((it) => it.source === SourceEnum.OSM));
        });

        it("should return results from both primary and filtered secondary sources", async () => {
            const result = await repo.GetAll({ primarySource: ["osm", "tsk_v2"] });

            expect(result).to.be.an("array").that.has.length(6);
            expect(result.filter((it) => it.source === SourceEnum.OSM)).to.not.be.empty;
            expect(result.filter((it) => it.source === SourceEnum.TSK_V2)).to.not.be.empty;
        });
    });
});

import { ParkingSourcesRepository } from "#og/data-access/ParkingSourcesRepository";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("ParkingSourcesRepository", () => {
    let repo: ParkingSourcesRepository;
    let connector: IDatabaseConnector;

    before(async () => {
        connector = OutputGatewayContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
        repo = ParkingsContainer.resolve<ParkingSourcesRepository>(ModuleContainerToken.ParkingSourcesRepository);
    });

    it("should return correct getAll results", async () => {
        const result = await repo.GetAll({ primarySource: ["tsk_v2", "osm"], isRestrictedToOpenData: false });

        expect(result).to.be.an("array").that.has.length(2);
        expect(result).to.eql([
            {
                source: "osm",
                name: null,
                contact: null,
            },
            {
                source: "tsk_v2",
                name: "Technická správa komunikací hl. m. Prahy",
                contact: {
                    email: "parking@tsk-praha.cz",
                    phone: "+420 257 015 257",
                    web_url: "https://parking.praha.eu/",
                    term_of_use_url: "https://parking.praha.eu/cs/moznosti-parkovani-v-praze/parkovani-v-zonach/",
                },
            },
        ]);
    });
});

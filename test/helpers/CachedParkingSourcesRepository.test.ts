import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { IParkingSource } from "#sch/models/interfaces/IParkingSource";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { sourceLookupTestData } from "./data/test_source_lookup";

chai.use(chaiAsPromised);

describe("CachedParkingSourcesRepository", () => {
    let connector: IDatabaseConnector;
    let cachedRepo: CachedParkingSourcesRepository;
    let sandbox: SinonSandbox;

    before(async () => {
        connector = OutputGatewayContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
        cachedRepo = new CachedParkingSourcesRepository(connector, log);
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should return lookup table", async () => {
        const sourceLookupTestDataSorted = structuredClone(sourceLookupTestData).sort((a, b) => a.source.localeCompare(b.source));
        const result = await cachedRepo.getAll();
        result.sort((a, b) => a.source.localeCompare(b.source));

        expect(result).to.be.deep.equal(sourceLookupTestDataSorted);
    });

    it("should return the entire lookup table when access is unrestricted", async () => {
        const openDataSources = ["ipr", "tsk_v2", "isphk", "pmdp", "korid", "mr_parkit", "smart4city"];
        const stub = sandbox.stub(cachedRepo, "getAll");
        stub.callsFake(async () => {
            const table = await stub.wrappedMethod();
            for (const item of table) {
                if (!openDataSources.includes(item.source)) continue;
                item.open_data = true;
            }
            return table;
        });

        const result = await cachedRepo["getAllAllowed"](false);

        const expectedData = structuredClone(sourceLookupTestData);
        for (const item of expectedData) {
            if (!openDataSources.includes(item.source)) continue;
            item.open_data = true;
        }

        expect(result).to.be.deep.equal(expectedData);
    });

    it("should return lookup table with only open data when restricted", async () => {
        const openDataSources = ["tsk_v2", "ipr", "isphk", "pmdp", "korid", "mr_parkit", "smart4city"];
        const stub = sandbox.stub(cachedRepo, "getAll");
        stub.callsFake(async () => {
            const table = await stub.wrappedMethod();
            for (const item of table) {
                if (!openDataSources.includes(item.source)) continue;
                item.open_data = true;
            }
            return table;
        });

        const result = await cachedRepo["getAllAllowed"](true);

        const expectedData: IParkingSource[] = [];
        for (const item of structuredClone(sourceLookupTestData)) {
            if (!openDataSources.includes(item.source)) continue;
            item.open_data = true;
            expectedData.push(item);
        }
        expect(result).to.be.deep.equal(expectedData);
    });

    it("should return v3 sources", async () => {
        const result = await cachedRepo.getV3Sources({ isRestrictedToOpenData: false });
        expect(result).to.have.members(["isphk", "pmdp", "test_cases", "osm", "korid", "mr_parkit", "smart4city", "tsk_v2"]);
    });

    it("should return legacy sources", async () => {
        const result = await cachedRepo.getLegacySources({ isRestrictedToOpenData: false });
        expect(result).to.have.members(["tsk", "manual", "korid", "ipr"]);
    });

    it("should return sources with parking spaces url specified", async () => {
        const result = await cachedRepo.getParkingSpaceDataSourceSources({ isRestrictedToOpenData: false });
        expect(result).to.have.members(["isphk", "pmdp", "test_cases"]);
    });
});

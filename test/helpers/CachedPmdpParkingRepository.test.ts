import { CachedPmdpParkingRepository } from "#helpers/data-access/CachedPmdpParkingRepository";
import { PmdpIdList } from "#ie/datasources/pmdp/PmdpMeasurementsDataSource";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { pmdpParkingLookupTestData } from "./data/pmdp_parkings_lookup";

chai.use(chaiAsPromised);

describe("CachedPmdpParkingRepository", () => {
    let connector: IDatabaseConnector;
    let cachedRepo: CachedPmdpParkingRepository;
    let sandbox: SinonSandbox;

    before(async () => {
        connector = OutputGatewayContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
        cachedRepo = new CachedPmdpParkingRepository(connector, log);
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should return pmdp parking lookup table", async () => {
        const result = await cachedRepo.getAll();
        expect(result).to.be.deep.equal(pmdpParkingLookupTestData);
    });

    it("should return pmdp record by id", async () => {
        const result = await cachedRepo.getPmdpParking(PmdpIdList.pmdp_001);
        expect(result).to.eql(pmdpParkingLookupTestData[0]);
    });
});

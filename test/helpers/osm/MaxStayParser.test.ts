import { MaxStayParser } from "#helpers/osm/MaxStayParser";
import { expect } from "chai";

describe("MaxStayParser", () => {
    // Tests for parse method
    it("should return null for empty string", () => {
        const result = MaxStayParser.parse("");
        expect(result).to.be.null;
    });

    it("should return null for 'unlimited', 'none', 'no'", () => {
        expect(MaxStayParser.parse("unlimited")).to.be.undefined;
        expect(MaxStayParser.parse("none")).to.be.undefined;
        expect(MaxStayParser.parse("no")).to.be.undefined;
    });

    it("should return correct value for minutes", () => {
        const result = MaxStayParser.parse("90 minutes");
        expect(result).to.equal(90);
    });

    it("should return correct value for hours", () => {
        expect(MaxStayParser.parse("1 hour")).to.equal(60);
        expect(MaxStayParser.parse("5 hours")).to.equal(300);
        expect(MaxStayParser.parse("1.5 hours")).to.equal(90);
    });

    it("should return correct value for days", () => {
        expect(MaxStayParser.parse("1 day")).to.equal(1440);
        expect(MaxStayParser.parse("2 days")).to.equal(2880);
    });

    it("should return null for invalid format", () => {
        const result = MaxStayParser.parse("1 week");
        expect(result).to.be.null;
    });
});

import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { OpeningHoursParser } from "#helpers/osm/OpeningHoursParser";

chai.use(chaiAsPromised);

describe("OpeningHoursParser", () => {
    let openingHoursParser: OpeningHoursParser;

    before(async () => {
        openingHoursParser = new OpeningHoursParser();
    });

    it("should parse 24/7", async () => {
        const result = openingHoursParser.parse("24/7");
        expect(result).to.be.an("array");
        expect(result).to.have.lengthOf(14);
    });

    it("should parse 09:00-22:00", async () => {
        const result = openingHoursParser.parse("09:00-22:00");
        expect(result).to.be.an("array");
        expect(result).to.have.lengthOf(14);
    });

    it("should parse Mo-Sa 09:00-19:00; Su 10:00-18:00", async () => {
        const result = openingHoursParser.parse("Mo-Sa 09:00-19:00; Su 10:00-18:00");

        expect(result).to.be.an("array");
        expect(result).to.have.lengthOf(14);
        expect(result![0].day_in_week).to.equal("Mo");
        expect(result![0].start).to.equal("09:00");
        expect(result![0].end).to.equal("19:00");
        expect(result![13].day_in_week).to.equal("Su");
        expect(result![13].start).to.equal("10:00");
        expect(result![13].end).to.equal("18:00");
    });

    it("should parse Mo-Su 07:00-01:00", async () => {
        const result = openingHoursParser.parse("Mo-Su 07:00-01:00");

        expect(result).to.be.an("array");
        expect(result).to.have.lengthOf(28);
    });

    it("should return periods for given weekdays and times", async () => {
        const result = openingHoursParser.parse("Mo,Tu,We 09:00-18:00");
        expect(result).to.be.an("array");
        expect(result).to.have.lengthOf(6);
    });

    it("should return periods for single weekday and time", async () => {
        const result = openingHoursParser.parse("Mo 09:00-18:00");
        expect(result).to.be.an("array");
        expect(result).to.have.lengthOf(2);
    });

    it("should return null for invalid string", async () => {
        const result = openingHoursParser.parse("Mo-Fr 07:00-01:00; 24/7");
        expect(result).to.is.null;
    });
});

import { GeoConfigHelper } from "#og/helpers/GeoConfigHelper";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
chai.use(chaiAsPromised);

describe("GeoConfigHelper", () => {
    let geoconfigHelper: GeoConfigHelper;

    beforeEach(() => {
        geoconfigHelper = ParkingsContainer.resolve<GeoConfigHelper>(ModuleContainerToken.GeoConfigHelper);
    });

    it("should return geo coordinates", async () => {
        const result = geoconfigHelper.returnGeoCoordinates();
        expect(result.type).to.equal("MultiPolygon");
    });
});

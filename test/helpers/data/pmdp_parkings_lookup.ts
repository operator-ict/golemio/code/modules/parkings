import { PmdpIdList } from "#ie/datasources/pmdp/PmdpMeasurementsDataSource";
import { IParking } from "#sch/models/interfaces/IParking";
import { IGeoCoordinatesPoint, IGeoCoordinatesPolygon } from "@golemio/core/dist/output-gateway/Geo";

export const pmdpParkingLookupTestData: IParking[] = [
    {
        id: PmdpIdList.pmdp_001,
        source: "pmdp",
        source_id: "pmdp_001",
        data_provider: "manual",
        name: "Parkovací dům Rychtářka",
        category: null,
        date_modified: new Date("2024-07-08T06:50:01.593Z"),
        address: null,
        address_updated_at: null,
        location: {
            crs: {
                type: "name",
                properties: {
                    name: "EPSG:4326",
                },
            },
            type: "Polygon",
            coordinates: [
                [
                    [13.3804006, 49.7501638],
                    [13.3802544, 49.7498325],
                    [13.3804147, 49.7498033],
                    [13.3804332, 49.7497999],
                    [13.3804487, 49.7497971],
                    [13.3813665, 49.7496298],
                    [13.3814128, 49.7497355],
                    [13.3814221, 49.7497341],
                    [13.3814381, 49.7497707],
                    [13.3814897, 49.7497616],
                    [13.3815738, 49.7499517],
                    [13.3812636, 49.7500078],
                    [13.3804436, 49.750156],
                    [13.3804006, 49.7501638],
                ],
            ],
        } as IGeoCoordinatesPolygon,
        area_served: null,
        area: 3300,
        total_spot_number: null,
        tariff_id: null,
        valid_from: null,
        valid_to: null,
        parking_type: "multi_storey",
        zone_type: null,
        centroid: {
            crs: {
                type: "name",
                properties: {
                    name: "EPSG:4326",
                },
            },
            type: "Point",
            coordinates: [13.3809141, 49.7498968],
        } as IGeoCoordinatesPoint,
        security: true,
        max_vehicle_dimensions: null,
        covered: true,
        contact: null,
        parking_policy: "commercial",
        sanitized_location: {
            crs: {
                type: "name",
                properties: {
                    name: "EPSG:4326",
                },
            },
            type: "Polygon",
            coordinates: [
                [
                    [13.3804006, 49.7501638],
                    [13.3802544, 49.7498325],
                    [13.3804147, 49.7498033],
                    [13.3804332, 49.7497999],
                    [13.3804487, 49.7497971],
                    [13.3813665, 49.7496298],
                    [13.3814128, 49.7497355],
                    [13.3814221, 49.7497341],
                    [13.3814381, 49.7497707],
                    [13.3814897, 49.7497616],
                    [13.3815738, 49.7499517],
                    [13.3812636, 49.7500078],
                    [13.3804436, 49.750156],
                    [13.3804006, 49.7501638],
                ],
            ],
        } as IGeoCoordinatesPolygon,
        active: true,
    } as IParking,
    {
        id: "pmdp-pmdp_002",
        source: "pmdp",
        source_id: "pmdp_002",
        data_provider: "manual",
        name: "Parkovací dům Nové divadlo",
        category: null,
        date_modified: new Date("2024-07-08T06:50:01.593Z"),
        address: null,
        address_updated_at: null,
        location: {
            crs: {
                type: "name",
                properties: {
                    name: "EPSG:4326",
                },
            },
            type: "Polygon",
            coordinates: [
                [
                    [13.3726268, 49.7498881],
                    [13.3726043, 49.7496604],
                    [13.3735115, 49.7495817],
                    [13.3735396, 49.7498409],
                    [13.3726268, 49.7498881],
                ],
            ],
        } as IGeoCoordinatesPolygon,
        area_served: null,
        area: 1789,
        total_spot_number: null,
        tariff_id: null,
        valid_from: null,
        valid_to: null,
        parking_type: "multi_storey",
        zone_type: null,
        centroid: {
            crs: {
                type: "name",
                properties: {
                    name: "EPSG:4326",
                },
            },
            type: "Point",
            coordinates: [13.37307195, 49.7497349],
        } as IGeoCoordinatesPoint,
        security: true,
        max_vehicle_dimensions: null,
        covered: true,
        contact: null,
        parking_policy: "commercial",
        sanitized_location: {
            crs: {
                type: "name",
                properties: {
                    name: "EPSG:4326",
                },
            },
            type: "Polygon",
            coordinates: [
                [
                    [13.3726268, 49.7498881],
                    [13.3726043, 49.7496604],
                    [13.3735115, 49.7495817],
                    [13.3735396, 49.7498409],
                    [13.3726268, 49.7498881],
                ],
            ],
        } as IGeoCoordinatesPolygon,
        active: true,
    } as IParking,
];

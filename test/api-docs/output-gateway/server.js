// Load reflection lib
require("@golemio/core/dist/shared/_global");

const path = require("path");
require("ts-node").register({ project: path.resolve(process.cwd(), "tsconfig.json") });
require("tsconfig-paths").register();

const http = require("http");
const express = require("@golemio/core/dist/shared/express");
const { ContainerToken, OutputGatewayContainer } = require("@golemio/core/dist/output-gateway/ioc");

const app = express();
const server = http.createServer(app);
const postgresConnector = OutputGatewayContainer.resolve(ContainerToken.PostgresDatabase);

const start = async () => {
    await postgresConnector.connect();

    const { v1ParkingRouter } = require("#og/routers/v1/V1ParkingRouter");
    const { v2ParkingRouter } = require("#og/routers/v2/V2ParkingRouter");
    const { v3ParkingRouter } = require("#og/routers/v3/V3ParkingRouter");
    const { v3ParkingMachinesRouter } = require("#og/routers/v3/V3ParkingMachinesRouter");
    const { v3ParkingTariffRouter } = require("#og/routers/v3/V3ParkingTariffsRouter");
    const { v3ParkingMeasurementsRouter } = require("#og/routers/v3/V3ParkingMeasurementsRouter");
    const { v3ParkingSourcesRouter } = require("#og/routers/v3/V3ParkingSourcesRouter");

    app.use(v1ParkingRouter.getPath(), v1ParkingRouter.getRouter());
    app.use(v2ParkingRouter.getPath(), v2ParkingRouter.getRouter());
    app.use(v3ParkingRouter.getPath(), v3ParkingRouter.getRouter());
    app.use(v3ParkingMachinesRouter.getPath(), v3ParkingMachinesRouter.getRouter());
    app.use(v3ParkingTariffRouter.getPath(), v3ParkingTariffRouter.getRouter());
    app.use(v3ParkingMeasurementsRouter.getPath(), v3ParkingMeasurementsRouter.getRouter());
    app.use(v3ParkingSourcesRouter.getPath(), v3ParkingSourcesRouter.getRouter());

    return new Promise((resolve) => {
        server.listen(3011, () => {
            resolve();
        });
    });
};

const stop = async () => {
    await postgresConnector.disconnect();
    server.close();
};

module.exports = {
    start,
    stop,
};

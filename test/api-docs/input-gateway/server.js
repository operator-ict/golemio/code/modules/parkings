// Load reflection lib
require("@golemio/core/dist/shared/_global");

const path = require("path");
require("ts-node").register({ project: path.resolve(process.cwd(), "tsconfig.json") });
require("tsconfig-paths").register();

const http = require("http");
const express = require("@golemio/core/dist/shared/express");
const { AMQPConnector } = require("@golemio/core/dist/input-gateway/connectors");

const app = express();
const server = http.createServer(app);

const bodyParser = require("body-parser");

const start = async () => {
    await AMQPConnector.connect();

    app.use(bodyParser.json());

    const { KoridParkingsRouter } = require("#ig/korid-parkings/KoridParkingsRouter");

    const { isphkParkingRouter } = require("#ig/index");

    app.use("/koridparkings", new KoridParkingsRouter().router);

    app.use("/isphkparkings", isphkParkingRouter);

    return new Promise((resolve) => {
        server.listen(3011, () => {
            resolve();
        });
    });
};

const stop = async () => {
    await AMQPConnector.disconnect();
    server.close();
};

module.exports = {
    start,
    stop,
};

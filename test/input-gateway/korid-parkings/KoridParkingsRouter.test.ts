import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import express, { Request, Response, NextFunction } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import { AMQPConnector } from "@golemio/core/dist/input-gateway/connectors";
import { log } from "@golemio/core/dist/output-gateway";
import { parkingsRouter } from "#ig/korid-parkings";

const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);

chai.use(chaiAsPromised);

describe("KoridParkingsRouter", () => {
    // Create clean express instance
    const app = express();
    app.use(
        bodyParser.json({
            limit: "2MB", // Reject payload bigger than limit
        })
    );

    const testKoridData = JSON.stringify(require("./data/korid-data.json"));
    const testKoridConfig = JSON.stringify(require("./data/korid-config.json"));
    const malformedKoridData = JSON.stringify(require("./data/korid-malformed.json"));

    before(async () => {
        // Connect to MQ
        await AMQPConnector.connect();
        // Mount the tested router to the express instance
        app.use("/koridparkings", parkingsRouter);
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
    });

    describe("KoridParkingConfig", () => {
        it("should respond with 204 to POST /koridparkings/config", (done) => {
            request(app)
                .post("/koridparkings/config")
                .send(testKoridConfig)
                .set("Content-Type", "application/json")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /koridparkings/config with bad content type", (done) => {
            request(app)
                .post("/koridparkings/config")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /koridparkings/config with no content type", (done) => {
            request(app)
                .post("/koridparkings/config")
                .send(testKoridData)
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /koridparkings/config with invalid data", (done) => {
            request(app)
                .post("/koridparkings/config")
                .send(malformedKoridData)
                .set("Content-Type", "application/json")
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });

    describe("KoridParkingData", () => {
        it("should respond with 204 to POST /koridparkings/data", (done) => {
            request(app)
                .post("/koridparkings/data")
                .send(testKoridData)
                .set("Content-Type", "application/json")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /koridparkings/data with bad content type", (done) => {
            request(app)
                .post("/koridparkings/data")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /koridparkings/data with no content type", (done) => {
            request(app)
                .post("/koridparkings/data")
                .send(testKoridData)
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /koridparkings/data with invalid data", (done) => {
            request(app)
                .post("/koridparkings/data")
                .send(malformedKoridData)
                .set("Content-Type", "application/json")
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });
});

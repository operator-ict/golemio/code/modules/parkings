import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { KoridParkingsConfigController } from "#ig/korid-parkings/KoridParkingsConfigController";

chai.use(chaiAsPromised);

describe("KoridParkingsConfigController", () => {
    let sandbox: SinonSandbox;
    let controller: KoridParkingsConfigController;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        controller = new KoridParkingsConfigController();

        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        sandbox.stub(controller, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should has name", () => {
        expect(controller.name).not.to.be.undefined;
    });

    it("should has processData method", () => {
        expect(controller.processData).not.to.be.undefined;
    });

    it("should properly process data", async () => {
        const data = require("./data/korid-config.json");
        await controller.processData(data);
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
    });
});

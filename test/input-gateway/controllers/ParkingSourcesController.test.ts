import { ParkingSourcesController } from "#ig/controllers/ParkingSourcesController";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

describe("ParkingSourcesController", () => {
    let sandbox: SinonSandbox;
    let controller: ParkingSourcesController;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        controller = new ParkingSourcesController();
        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        sandbox.stub(controller, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("calls correct methods", async () => {
        const jsonData = require("../data/parkingSourceInput.json");
        await controller.processData(jsonData);
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
    });
});

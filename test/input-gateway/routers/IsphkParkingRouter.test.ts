import express from "@golemio/core/dist/shared/express";
import bodyParser from "body-parser";
import { isphkParkingRouter } from "#ig/index";
import request from "supertest";

describe("IsphkParkingRouter: POST ", () => {
    const app = express();
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(
        bodyParser.json({
            limit: "2MB", // Reject payload bigger than limit
        })
    );

    before(() => {
        app.use("/isphkparkings", isphkParkingRouter);
    });

    it("responds correctly to invalid data sent to the isphk endpoint", (done) => {
        const jsonData = require("../data/parkingIsphkInvalidInput.json");

        request(app)
            .post("/isphkparkings/measurements")
            .send(jsonData)
            .set("Content-Type", "application/json")
            .expect(422)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });
    it("responds with 204 given correct data sent to the isphk endpoint", (done) => {
        const jsonData = require("../data/parkingIsphkInput.json");
        request(app)
            .post("/isphkparkings/measurements")
            .send(jsonData)
            .set("Content-Type", "application/json")
            .expect(204)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });
});

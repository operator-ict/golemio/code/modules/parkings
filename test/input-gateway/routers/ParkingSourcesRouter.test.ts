import express from "@golemio/core/dist/shared/express";
import bodyParser from "body-parser";
import { parkingSourcesRouter } from "#ig/index";
import request from "supertest";

describe("ParkingSourcesRouter: POST on /source", () => {
    const app = express();
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(
        bodyParser.json({
            limit: "2MB", // Reject payload bigger than limit
        })
    );

    before(() => {
        app.use("/parkings", parkingSourcesRouter);
    });

    it("responds with 204 given correct data", (done) => {
        const jsonData = require("../data/parkingSourceInput.json");
        request(app)
            .post("/parkings/source")
            .send(jsonData)
            .set("Content-Type", "application/json")
            .expect(204)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("responds correctly to invalid data", (done) => {
        const jsonData = require("../data/parkingSourceInvalidInput.json");

        request(app)
            .post("/parkings/source")
            .send(jsonData)
            .set("Content-Type", "application/json")
            .expect(422)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });
});

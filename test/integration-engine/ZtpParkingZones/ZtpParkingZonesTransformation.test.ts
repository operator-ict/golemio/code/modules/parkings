import { ZtpParkingZonesTransformation } from "#ie/ZtpParkingZones/ZtpParkingZonesTransformation";
import { ZtpParkingJsonFeatureFixture } from "../data/ZtpParkingJsonFeatureFixture";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("ZtpParkingZonesTransformation", async () => {
    const transformationDate = new Date();
    const transformation = new ZtpParkingZonesTransformation(transformationDate);
    let sandbox: SinonSandbox;

    const transformedParking = [
        {
            id: "ipr-ztp-ztp-1",
            source: "ipr",
            source_id: "ztp-1",
            data_provider: "opendata.iprpraha.cz",
            date_modified: transformationDate.toISOString(),
            location: {
                coordinates: [14.497054645000048, 50.03949779900006],
                type: "Point",
            },
            name: "Parkoviště ZTP",
            total_spot_number: 1,
            parking_type: "disabled_parking",
            centroid: {
                coordinates: [14.497054645000048, 50.03949779900006],
                type: "Point",
            },
            active: true,
        },
        {
            id: "ipr-ztp-ztp-2",
            source: "ipr",
            source_id: "ztp-2",
            data_provider: "opendata.iprpraha.cz",
            date_modified: transformationDate.toISOString(),
            location: {
                coordinates: [14.492410407000023, 50.03617044500004],
                type: "Point",
            },
            name: "Parkoviště ZTP",
            total_spot_number: 3,
            parking_type: "disabled_parking",
            centroid: {
                coordinates: [14.492410407000023, 50.03617044500004],
                type: "Point",
            },
            active: true,
        },
    ];

    before(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
    });

    after(() => {
        sandbox.restore();
    });

    it("transforms single ZTP parking correctly", async () => {
        const actual = await transformation.transformElement(ZtpParkingJsonFeatureFixture[0]);
        expect(actual).deep.eq(transformedParking[0]);
    });

    it("transforms multiple ZTP parkings correctly", async () => {
        const actual = await transformation.transform(ZtpParkingJsonFeatureFixture);
        expect(actual).deep.eq(transformedParking);
    });
});

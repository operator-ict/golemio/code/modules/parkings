export const iptOictTariffsRelationDataTransformed = [
    {
        id: "isphk-hk_isp_001",
        source: "isphk",
        source_id: "hk_isp_001",
        tariff_id: "d2e559f4-344e-57f4-a261-e057f89c0eb4",
    },
    {
        id: "isphk-hk_isp_002",
        source: "isphk",
        source_id: "hk_isp_002",
        tariff_id: "d2e559f4-344e-57f4-a261-e057f89c0eb4",
    },
    {
        id: "isphk-hk_isp_004",
        source: "isphk",
        source_id: "hk_isp_004",
        tariff_id: "42a71bfa-b00c-5229-ad46-4eb97d818055",
    },
];

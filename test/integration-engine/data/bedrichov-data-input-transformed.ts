import { IParkingMeasurements } from "#ie/ParkingInterface";

type OmitDateModified = Omit<IParkingMeasurements, "date_modified">;

export const bedrichovInputDataTransformed: OmitDateModified[] = [
    {
        source: "bedrichov",
        source_id: "656da0247e188300012f6512",
        parking_id: "bedrichov-656da0247e188300012f6512",
        total_spot_number: 25,
        available_spot_number: 25,
        occupied_spot_number: 0,
    },
    {
        source: "bedrichov",
        source_id: "656da0497e188300012f651a",
        parking_id: "bedrichov-656da0497e188300012f651a",
        total_spot_number: 100,
        available_spot_number: 100,
        occupied_spot_number: 0,
    },
    {
        source: "bedrichov",
        source_id: "656da07b7e188300012f6524",
        parking_id: "bedrichov-656da07b7e188300012f6524",
        total_spot_number: 120,
        available_spot_number: 119,
        occupied_spot_number: 1,
    },
    {
        source: "bedrichov",
        source_id: "656da0a77e188300012f652e",
        parking_id: "bedrichov-656da0a77e188300012f652e",
        total_spot_number: 200,
        available_spot_number: 193,
        occupied_spot_number: 7,
    },
    {
        source: "bedrichov",
        source_id: "656da0f57e188300012f653c",
        parking_id: "bedrichov-656da0f57e188300012f653c",
        total_spot_number: 150,
        available_spot_number: 147,
        occupied_spot_number: 3,
    },
    {
        source: "bedrichov",
        source_id: "657727df9e01fb0001621c0a",
        parking_id: "bedrichov-657727df9e01fb0001621c0a",
        total_spot_number: 150,
        available_spot_number: 150,
        occupied_spot_number: 0,
    },
];

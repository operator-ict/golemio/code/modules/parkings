import { IMrParkitGarageWithTariff } from "#sch/datasources/interfaces/IMrParkitGarage";

export const MrParkitInputDataFixture: Array<IMrParkitGarageWithTariff & Record<string, any>> = [
    {
        id: "463253c6-852d-41d4-8082-96532a97eeac",
        name: "Anděl Park - Radlická 14",
        image: "https://s3-eu-west-1.amazonaws.com/image1.png",
        free: true,
        minReservationDuration: 12,
        maxReservationDuration: 720,
        pricePerHour: 1660,
        pricePerDay: 34900,
        pricePerWeekend: 69800,
        address: "Radlická 14, Praha 5",
        latitude: 50.0705288,
        longitude: 14.4009956,
        maxVehicleDimensions: {
            length: 530,
            width: 230,
            height: 190,
        },
        properties: {
            lpgCngAllowed: false,
            motorbikesAllowed: true,
            benefits: ["nonstop_service", "nonstop_reception", "open_gate_by_phone", "5_minutes_form_city_center"],
        },
    },
    {
        id: "1e9f986f-79d2-4fc7-94d6-32400e76b1ed",
        name: "Anglická 20",
        image: "https://s3-eu-west-1.amazonaws.com/image2.png",
        free: false,
        minReservationDuration: 12,
        maxReservationDuration: 720,
        pricePerHour: 2490,
        pricePerDay: 54900,
        pricePerWeekend: 109800,
        address: "Anglická 20, Praha 2",
        latitude: 50.0762453,
        longitude: 14.4337195,
        maxVehicleDimensions: {
            length: 550,
            width: 220,
            height: 190,
        },
        properties: {
            lpgCngAllowed: false,
            motorbikesAllowed: true,
            benefits: ["5_minutes_form_city_center", "cctv_at_entrance", "subway_availability", "security_system"],
        },
    },
];

import { IParkingMeasurements } from "#ie/ParkingInterface";

export const smart4CityMeasurementTransformed: IParkingMeasurements[] = [
    {
        source: "smart4city",
        source_id: "4663aa2e-4cbe-539f-a53a-4930af118497",
        parking_id: "smart4city-4663aa2e-4cbe-539f-a53a-4930af118497",
        total_spot_number: 39,
        available_spot_number: 2,
        occupied_spot_number: 37,
        date_modified: "2024-01-04T06:15:00.000Z",
    },
    {
        source: "smart4city",
        source_id: "152d86fb-6b25-5639-a949-805b95cd36ee",
        parking_id: "smart4city-152d86fb-6b25-5639-a949-805b95cd36ee",
        total_spot_number: 350,
        available_spot_number: 0,
        occupied_spot_number: 350,
        date_modified: "2024-01-04T06:15:00.000Z",
    },
];

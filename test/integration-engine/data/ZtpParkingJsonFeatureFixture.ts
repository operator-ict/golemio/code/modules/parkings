import { IZtpParkingJsonFeature } from "#sch/datasources/interfaces/IZtpParkingJsonFeature";
import { GeoCoordinatesType } from "@golemio/core/dist/output-gateway";

export const ZtpParkingJsonFeatureFixture: IZtpParkingJsonFeature[] = [
    {
        type: "Feature",
        id: 123,
        geometry: {
            type: GeoCoordinatesType.Point,
            coordinates: [14.497054645000048, 50.03949779900006],
        },
        properties: {
            OBJECTID: 1,
            POCET_PS: 1,
            ROZM_DELKA: 5,
            ROZM_SIRKA: 3.4000001,
            TYP_PS: 3,
            KOLME_PS: 1,
            POD_SKLON: 0,
            PRIC_SKLON: 0,
            TYP_POVRCH: 1,
            POSK_POVRC: 2,
            PES_ZONA: 2,
            MAT_POVRCH: 1,
            GLOBALID: "f31486be-91e3-4d8c-ba16-06f1cae9134c",
        },
    },
    {
        type: "Feature",
        id: 124,
        geometry: {
            type: GeoCoordinatesType.Point,
            coordinates: [14.492410407000023, 50.03617044500004],
        },
        properties: {
            OBJECTID: 2,
            POCET_PS: 3,
            ROZM_DELKA: 6,
            ROZM_SIRKA: 3.5,
            TYP_PS: 2,
            KOLME_PS: 2,
            POD_SKLON: 0,
            PRIC_SKLON: 0,
            TYP_POVRCH: 2,
            POSK_POVRC: 2,
            PES_ZONA: 2,
            MAT_POVRCH: 2,
            GLOBALID: "6003a87d-43e9-4896-a27d-8eb449f7e4bd",
        },
    },
];

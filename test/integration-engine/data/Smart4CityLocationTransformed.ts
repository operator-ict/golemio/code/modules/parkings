import { ISmart4CityLocationCollectionTransformed } from "#ie/transformations/Smart4CityLocationTransformation";
import { GeoCoordinatesType } from "@golemio/core/dist/output-gateway";

export const smart4CityLocationTransformed: ISmart4CityLocationCollectionTransformed = {
    parking: [
        {
            id: "smart4city-4663aa2e-4cbe-539f-a53a-4930af118497",
            source: "smart4city",
            source_id: "4663aa2e-4cbe-539f-a53a-4930af118497",
            data_provider: "www.smart4city.cz-benesov",
            name: "ulice Tyršova",
            location: {
                type: GeoCoordinatesType.Point,
                coordinates: [14.688302, 49.78222],
            },
            centroid: {
                type: GeoCoordinatesType.Point,
                coordinates: [14.688302, 49.78222],
            },
            total_spot_number: 39,
            date_modified: "2024-01-04T06:15:00.000Z",
            valid_from: "2024-01-04T06:15:00.000Z",
            parking_type: "surface",
            active: true,
        },
        {
            id: "smart4city-152d86fb-6b25-5639-a949-805b95cd36ee",
            source: "smart4city",
            source_id: "152d86fb-6b25-5639-a949-805b95cd36ee",
            data_provider: "www.smart4city.cz-benesov",
            name: "Konopiště",
            location: {
                type: GeoCoordinatesType.Point,
                coordinates: [14.659354, 49.783124],
            },
            centroid: {
                type: GeoCoordinatesType.Point,
                coordinates: [14.659354, 49.783124],
            },
            total_spot_number: 350,
            date_modified: "2024-01-04T06:15:00.000Z",
            valid_from: "2024-01-04T06:15:00.000Z",
            parking_type: "on_street",
            active: true,
        },
        {
            id: "smart4city-352ac774-98f3-5469-bfb8-cb8de7ed8244",
            source: "smart4city",
            source_id: "352ac774-98f3-5469-bfb8-cb8de7ed8244",
            data_provider: "www.smart4city.cz-benesov",
            name: "ulice Poštovní",
            location: {
                type: GeoCoordinatesType.Point,
                coordinates: [14.690396, 49.781623],
            },
            centroid: {
                type: GeoCoordinatesType.Point,
                coordinates: [14.690396, 49.781623],
            },
            total_spot_number: null,
            date_modified: "2024-01-04T06:15:00.000Z",
            valid_from: "2024-01-04T06:15:00.000Z",
            parking_type: "surface",
            active: true,
        },
    ],
    parkingLocation: [
        {
            id: "smart4city-4663aa2e-4cbe-539f-a53a-4930af118497",
            source: "smart4city",
            source_id: "4663aa2e-4cbe-539f-a53a-4930af118497",
            data_provider: "www.smart4city.cz-benesov",
            location: {
                type: GeoCoordinatesType.Point,
                coordinates: [14.688302, 49.78222],
            },
            centroid: {
                type: GeoCoordinatesType.Point,
                coordinates: [14.688302, 49.78222],
            },
            total_spot_number: 39,
        },
        {
            id: "smart4city-152d86fb-6b25-5639-a949-805b95cd36ee",
            source: "smart4city",
            source_id: "152d86fb-6b25-5639-a949-805b95cd36ee",
            data_provider: "www.smart4city.cz-benesov",
            location: {
                type: GeoCoordinatesType.Point,
                coordinates: [14.659354, 49.783124],
            },
            centroid: {
                type: GeoCoordinatesType.Point,
                coordinates: [14.659354, 49.783124],
            },
            total_spot_number: 350,
        },
        {
            id: "smart4city-352ac774-98f3-5469-bfb8-cb8de7ed8244",
            source: "smart4city",
            source_id: "352ac774-98f3-5469-bfb8-cb8de7ed8244",
            data_provider: "www.smart4city.cz-benesov",
            location: {
                type: GeoCoordinatesType.Point,
                coordinates: [14.690396, 49.781623],
            },
            centroid: {
                type: GeoCoordinatesType.Point,
                coordinates: [14.690396, 49.781623],
            },
            total_spot_number: null,
        },
    ],
};

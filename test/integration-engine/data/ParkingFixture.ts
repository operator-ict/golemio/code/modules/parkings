import { IParking } from "#ie/ParkingInterface";
import { GeoCoordinatesType } from "@golemio/core/dist/output-gateway/Geo";

export const ParkingFixture: IParking = {
    id: "tsk-534004",
    source: "tsk",
    source_id: "534004",
    data_provider: "www.tsk-praha.cz",
    date_modified: "2020-02-19T15:37:22.000Z",
    location: {
        coordinates: [14.406528, 50.028584],
        type: GeoCoordinatesType.Point,
    },
    total_spot_number: 110,
    name: "Braník",
    category: "park_and_ride",
    parking_type: "park_and_ride",
    centroid: {
        coordinates: [14.406528, 50.028584],
        type: GeoCoordinatesType.Point,
    },
    active: true,
};

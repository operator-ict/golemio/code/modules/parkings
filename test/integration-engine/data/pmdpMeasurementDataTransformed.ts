import { PmdpIdList } from "#ie/datasources/pmdp/PmdpMeasurementsDataSource";
import { IParkingMeasurements } from "#ie/ParkingInterface";

export const pmdpMeasurementDataTransformed: IParkingMeasurements = {
    source: "pmdp",
    source_id: "pmdp_001",
    parking_id: PmdpIdList.pmdp_001,
    total_spot_number: 1000,
    available_spot_number: 127,
    occupied_spot_number: 873,
    date_modified: "2024-08-06T08:00:00.000Z",
};

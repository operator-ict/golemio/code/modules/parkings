import { IPmdpMeasurement } from "#sch/datasources/pmdp/interfaces/IPmdpMeasurement";

export const pmdpMeasurementDataFixture: IPmdpMeasurement = {
    kapacita: "265",
    kp: "138",
    dp_bez_rezervace: "0",
    dp_rezervace: "0",
    volno: "127",
    volno_bez_prepoctu: "127",
    datum_aktualizace: "1720534494",
};

import { IParkingTariff } from "#sch/models/interfaces/IParkingTariff";

export const StaticTariffsFixture: IParkingTariff[] = [
    {
        tariff_id: "park_and_ride_zone_tsk",
        source: "manual",
        last_updated: "2022-12-23T20:00+01:00",
        payment_mode: "",
        payment_additional_description: "Parkování Praha",
        free_of_charge: false,
        url_link_address: undefined,
        charge_band_name: "0",
        accepts_litacka: true,
        accepts_cash: false,
        accepts_payment_card: false,
        accepts_mobile_payment: false,
        charge_currency: "CZK",
        allowed_vehicle_type: undefined,
        allowed_fuel_type: undefined,
        charge: 0,
        charge_type: "other",
        charge_order_index: 0,
        charge_interval: 3600,
        max_iterations_of_charge: 12,
        min_iterations_of_charge: undefined,
        start_time_of_period: "Mo-Su 00:00",
        end_time_of_period: "Mo-Su 23:59",
    },
];

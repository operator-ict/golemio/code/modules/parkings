const transformationDate = "2022-12-21T16:30:00+02:00";

export default {
    parking: [
        {
            id: "tsk-p10-0101",
            source: "tsk",
            source_id: "P10-0101",
            data_provider: "www.tsk-praha.cz",
            date_modified: transformationDate,
            location: {
                type: "Polygon",
                coordinates: [
                    [
                        [14.444951887424557, 50.06635611888514],
                        [14.445311792896923, 50.06624137249515],
                        [14.445089213514812, 50.06617981932712],
                        [14.444314784415674, 50.066282419968395],
                        [14.443200416961972, 50.06626796918713],
                        [14.441588523837021, 50.0657549636314],
                        [14.441223821761263, 50.06573762250265],
                        [14.44127785169841, 50.065809877164384],
                        [14.442801946175493, 50.066340223049934],
                        [14.443639410201307, 50.0664428233481],
                        [14.444951887424557, 50.06635611888514],
                    ],
                ],
            },
            total_spot_number: 49,
            name: "Vršovická",
            category: "zone_mixed",
            tariff_id: "bfcfc515-369f-5093-b779-7091c520edd3",
            valid_from: "2020-06-26T22:47:07.213",
            valid_to: "2999-12-31T23:59:59.999",
            parking_type: "on_street",
            zone_type: "zone_mixed",
            active: true,
        },
        {
            id: "tsk-p10-0102",
            source: "tsk",
            source_id: "P10-0102",
            data_provider: "www.tsk-praha.cz",
            date_modified: transformationDate,
            location: {
                type: "Polygon",
                coordinates: [
                    [
                        [14.442801946175493, 50.06634022304992],
                        [14.44127785169841, 50.06580987716439],
                        [14.44112251562911, 50.06586334554399],
                        [14.441390414067472, 50.06605698728481],
                        [14.442446249089263, 50.06644282334811],
                        [14.442783936196443, 50.066415366951794],
                        [14.442801946175493, 50.06634022304992],
                    ],
                ],
            },
            total_spot_number: 14,
            name: "Otakarova",
            category: "zone_mixed",
            tariff_id: "bfcfc515-369f-5093-b779-7091c520edd3",
            valid_from: "2020-06-26T22:48:02.573",
            valid_to: "2999-12-31T23:59:59.999",
            parking_type: "on_street",
            zone_type: "zone_mixed",
            active: true,
        },
    ],
    payment: [
        {
            parking_id: "tsk-p10-0101",
            source: "tsk",
            payment_web_url: "https://ke-utc.appspot.com/static/onstreet.html?shortname=P10-0101",
        },
        {
            parking_id: "tsk-p10-0102",
            source: "tsk",
            payment_web_url: "https://ke-utc.appspot.com/static/onstreet.html?shortname=P10-0102",
        },
    ],
};

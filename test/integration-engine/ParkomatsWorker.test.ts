import { ParkomatsWorker } from "#ie/workers/ParkomatsWorker";
import { Parkomats } from "#sch";
import { ParkomatInputSchema } from "#sch/parkomats/datasources/ParkomatInputSchema";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("ParkomatsWorker", () => {
    let worker: ParkomatsWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub(),
            })
        );

        worker = new ParkomatsWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("getQueuePrefix", () => {
        it("should have correct queue prefix set", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".parkomats");
        });
    });

    describe("getQueueDefinition", () => {
        it("should return correct queue definition", () => {
            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("Parkomats");
            expect(result.queues.length).to.equal(1);
        });
    });

    it("should validate input properly", async () => {
        const validator = new JSONSchemaValidator(Parkomats.name + "DataSource", ParkomatInputSchema);
        const testingData = require("./data/parkomats-input.json").data;
        const result = await validator.Validate(testingData);
        expect(result).to.be.true;
    });
});

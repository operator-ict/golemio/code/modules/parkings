import { assert, expect } from "chai";
import { DataSourceIntegrationChecker } from "#ie/businessRules/DataSourceIntegrationChecker";
import { IParkingSource } from "#sch/models/interfaces/IParkingSource";

describe("DatasourceIntegrationChecker", () => {
    const dataSourceIntegrationChecker: DataSourceIntegrationChecker = new DataSourceIntegrationChecker();

    it("checks all allowed sources correctly", () => {
        const allowedAllInput: IParkingSource = {
            source: "manual",
            name: null,
            open_data: true,
            api_v3_allowed: true,
            legacy_api_allowed: false,
            payment: null,
            contact: null,
            reservation: null,
            datasource_prohibitions: "url",
            datasource_entrances: "url",
            datasource_locations: "url",
            datasource_parking: "url",
            datasource_payments: "url",
            datasource_tariffs: null,
        };
        assert.doesNotThrow(() => dataSourceIntegrationChecker.checkAllowedDataSources(allowedAllInput));
    });

    it("checks valid limited sources correctly", () => {
        const iprCorrectInput: IParkingSource = {
            source: "ipr",
            name: null,
            open_data: true,
            api_v3_allowed: true,
            legacy_api_allowed: false,
            payment: null,
            contact: null,
            reservation: null,
            datasource_prohibitions: "url",
            datasource_entrances: "url",
            datasource_locations: null,
            datasource_parking: null,
            datasource_payments: "url",
            datasource_tariffs: "url",
        };
        assert.doesNotThrow(() => dataSourceIntegrationChecker.checkAllowedDataSources(iprCorrectInput));
    });

    it("checks invalid limited sources correctly", () => {
        const iprIncorrectInput: IParkingSource = {
            source: "ipr",
            name: null,
            open_data: true,
            api_v3_allowed: true,
            legacy_api_allowed: false,
            payment: null,
            contact: null,
            reservation: null,
            datasource_prohibitions: "url",
            datasource_entrances: "url",
            datasource_locations: null,
            datasource_parking: "url",
            datasource_payments: "url",
            datasource_tariffs: null,
        };
        assert.throws(() => dataSourceIntegrationChecker.checkAllowedDataSources(iprIncorrectInput));
    });
});

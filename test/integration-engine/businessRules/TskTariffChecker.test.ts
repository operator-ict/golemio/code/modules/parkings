import { TskTariffChecker } from "#ie/businessRules/TskTariffChecker";
import { ITariffInfo } from "#ie/businessRules/interfaces/ITariffInfo";
import { expect } from "chai";

describe("TSK TariffChecker", () => {
    let tskTariffChecker: TskTariffChecker;

    beforeEach(() => {
        tskTariffChecker = new TskTariffChecker();
    });

    it("invalid tariff ~ MaxChargeIterationsRule", async () => {
        const tariff: ITariffInfo = {
            cTariff: 80,
            pricePerHour: 20,
            maxParkingTime: 4,
            maxPrice: 800,
            minPrice: 50,
        };
        const result = tskTariffChecker.isTariffValid(tariff);

        expect(result.isValid).to.be.false;
        expect(result.failedRule).equals("MaxChargeIterationsRule");
    });

    it("invalid tariff ~ MaxPriceRule", async () => {
        const tariff: ITariffInfo = {
            cTariff: 80,
            pricePerHour: 200,
            maxParkingTime: 4,
            maxPrice: 1000,
            minPrice: 50,
        };
        const result = tskTariffChecker.isTariffValid(tariff);

        expect(result.isValid).to.be.false;
        expect(result.failedRule).equals("MaxPriceRule");
    });

    it("valid tariff", async () => {
        const tariff: ITariffInfo = {
            cTariff: 80,
            pricePerHour: 200,
            maxParkingTime: 4,
            maxPrice: 800,
            minPrice: 50,
        };

        expect(tskTariffChecker.isTariffValid(tariff).isValid).to.be.true;
    });

    it("tariff minPrice 0 & pricePerHour 200 is valid", async () => {
        const tariff: ITariffInfo = {
            cTariff: 80,
            pricePerHour: 200,
            maxParkingTime: 4,
            maxPrice: 800,
            minPrice: 0,
        };

        expect(tskTariffChecker.isTariffValid(tariff).isValid).to.be.true;
    });

    it("tariff minPrice 0 & pricePerHour 0 & maxPrice 0 is valid", async () => {
        const tariff: ITariffInfo = {
            cTariff: 80,
            pricePerHour: 0,
            maxParkingTime: 4,
            maxPrice: 0,
            minPrice: 0,
        };
        const result = tskTariffChecker.isTariffValid(tariff);
        expect(result.isValid).to.be.true;
    });

    it("tariff maxParkingTime 0 is invalid", async () => {
        const tariff: ITariffInfo = {
            cTariff: 80,
            pricePerHour: 200,
            maxParkingTime: 0,
            maxPrice: 800,
            minPrice: 50,
        };
        const result = tskTariffChecker.isTariffValid(tariff);

        expect(result.isValid).to.be.false;
        expect(result.failedRule).equals("ZeroMaxParkingTime");
    });

    it("tariff all values NaN is invalid", async () => {
        const tariff: ITariffInfo = {
            cTariff: NaN,
            pricePerHour: NaN,
            maxParkingTime: NaN,
            maxPrice: NaN,
            minPrice: NaN,
        };
        const result = tskTariffChecker.isTariffValid(tariff);

        expect(result.isValid).to.be.false;
    });

    it("tariff all values undefined is invalid", async () => {
        const tariff: ITariffInfo = {
            cTariff: undefined,
            pricePerHour: undefined,
            maxParkingTime: undefined,
            maxPrice: undefined,
            minPrice: undefined,
        };
        const result = tskTariffChecker.isTariffValid(tariff);

        expect(result.isValid).to.be.false;
    });
});

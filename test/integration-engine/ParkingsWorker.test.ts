import { IParkingMeasurements } from "#ie/ParkingInterface";
import { ParkingsWorker } from "#ie/ParkingsWorker";
import { StaticParkingLotsGeoDataSourceFactory } from "#ie/datasources/StaticParkingLotsGeoDataSourceFactory";
import { IParkingLotsTransform } from "#ie/transformations/ParkingLotsTransformation";
import { IStaticParkingLotsGeo } from "#sch/datasources/interfaces/IStaticParkingLotsGeo";
import { DataSource, QueueManager } from "@golemio/core/dist/integration-engine";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy, SinonStub } from "sinon";
import parkingLotsDataTransformed from "./data/parking-lots-data-transformed";

chai.use(chaiAsPromised);
describe("ParkingsWorker", () => {
    let worker: ParkingsWorker;
    let sandbox: SinonSandbox;
    let testData: number[];
    let testTransformedData: number[];

    let parkingLotsData: Record<string, any>;

    let staticGeoGetAllStub: SinonStub;
    let staticParkingLotsGeoData: IStaticParkingLotsGeo;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.useFakeTimers(new Date("2022-12-21 16:30:00"));

        testData = [1, 2];
        testTransformedData = [1, 2];

        parkingLotsData = require("./data/parking-lots-data-input.json");
        staticParkingLotsGeoData = require("./data/StaticParkingLotsGeoData.json");
        sandbox.stub(config, "datasources").value({
            TSKFTP: {
                host: "",
                port: 21,
                user: "",
                password: "",
            },
            TSKFTPParkingZonesPath: "/external/tsk/parking-zones",
        });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => ({})),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );
        worker = new ParkingsWorker();

        sandbox.stub(worker["dataSource"], "getAll").callsFake(() => Promise.resolve(parkingLotsData));
        sandbox.stub(worker, "sendMessageToExchange" as any);

        staticGeoGetAllStub = sandbox.stub().callsFake(() => Promise.resolve<IStaticParkingLotsGeo>(staticParkingLotsGeoData));
        sandbox
            .stub(StaticParkingLotsGeoDataSourceFactory, "getDataSource")
            .callsFake(() => ({ getAll: staticGeoGetAllStub } as any as DataSource));

        sandbox.stub(QueueManager, "sendMessageToExchange" as any);

        sandbox
            .stub(worker["koridParkingConfigTransformation"], "transform")
            .callsFake(() => Promise.resolve({ geo: testData as any, tariff: [], location: [] }));
        sandbox.stub(worker["parkingsModel"], "saveActiveParkingsWithoutAddress");
        sandbox.stub(worker["parkingsTariffsModel"], "bulkSave");
        sandbox.stub(worker["parkingsTariffsModel"], "deleteByTariffIds");
        sandbox.stub(worker["parkingsLocationRepository"], "saveWithoutAddress");
        sandbox
            .stub(worker["koridParkingDataTransformation"], "transform")
            .callsFake(() => Promise.resolve(testTransformedData as unknown as IParkingMeasurements[]));
        sandbox.stub(worker["parkingsMeasurementsModel"], "bulkSave");
        sandbox.stub(worker["parkingsMeasurementsActualModel"], "bulkSave");
        sandbox
            .stub(worker["parkingLotsTransformation"], "transform")
            .callsFake(() => Promise.resolve(parkingLotsDataTransformed as IParkingLotsTransform));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by saveKoridConfToDB method", async () => {
        await worker.saveKoridConfToDB({ content: Buffer.from(JSON.stringify(testData)) });
        sandbox.assert.calledOnce(worker["koridParkingConfigTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["parkingsModel"].saveActiveParkingsWithoutAddress as SinonSpy);
        sandbox.assert.calledOnce(worker["parkingsLocationRepository"].saveWithoutAddress as SinonSpy);
        sandbox.assert.callOrder(
            worker["koridParkingConfigTransformation"].transform as SinonSpy,
            worker["parkingsModel"].saveActiveParkingsWithoutAddress as SinonSpy
        );
    });

    it("should calls the correct methods by saveKoridDataToDB method", async () => {
        await worker.saveKoridDataToDB({ content: Buffer.from(JSON.stringify(testData)) });
        sandbox.assert.calledOnce(worker["koridParkingDataTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["parkingsMeasurementsModel"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(worker["parkingsMeasurementsActualModel"].bulkSave as SinonSpy);
        sandbox.assert.callOrder(
            worker["koridParkingDataTransformation"].transform as SinonSpy,
            worker["parkingsMeasurementsModel"].bulkSave as SinonSpy,
            worker["parkingsMeasurementsActualModel"].bulkSave as SinonSpy
        );
    });

    it("should call the correct methods by saveParkingLotsPrague method", async () => {
        await worker.saveParkingLotsPrague();
        sandbox.assert.calledOnce(worker["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(staticGeoGetAllStub as SinonSpy);
        sandbox.assert.calledOnce(worker["parkingLotsTransformation"].transform as SinonSpy);
        sandbox.assert.callCount(QueueManager.sendMessageToExchange as SinonSpy, 1);
        sandbox.assert.calledOnce(worker["parkingsModel"].saveActiveParkingsWithoutAddress as SinonSpy);
        sandbox.assert.calledWith(
            worker["parkingsModel"].saveActiveParkingsWithoutAddress as SinonSpy,
            parkingLotsDataTransformed.geo,
            "tsk"
        );
        sandbox.assert.callOrder(
            worker["dataSource"].getAll as SinonSpy,
            staticGeoGetAllStub as SinonSpy,
            worker["parkingLotsTransformation"].transform as SinonSpy,
            worker["parkingsModel"].saveActiveParkingsWithoutAddress as SinonSpy,
            QueueManager.sendMessageToExchange as SinonSpy
        );
    });

    it("should call the correct methods by saveParkingLotsMeasurementsPrague method", async () => {
        await worker.saveParkingLotsMeasurementsPrague();
        sandbox.assert.calledOnce(worker["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["parkingLotsTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["parkingsMeasurementsModel"].bulkSave as SinonSpy);
        sandbox.assert.callOrder(
            worker["dataSource"].getAll as SinonSpy,
            worker["parkingLotsTransformation"].transform as SinonSpy,
            worker["parkingsMeasurementsModel"].bulkSave as SinonSpy
        );
    });
});

import { IParking } from "#ie/ParkingInterface";
import { ParkingFixture } from "../data/ParkingFixture";

export class ParkingsModelFake {
    public GetOne = async (id: string): Promise<IParking | null> => {
        return ParkingFixture;
    };
}

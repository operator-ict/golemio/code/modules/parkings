import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IptOictOpeningHoursTransformation } from "#ie/transformations/iptoict/IptOictOpeningHoursTransformation";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import path from "path";

chai.use(chaiAsPromised);

describe("IptOictOpeningHoursTransformation", () => {
    let sourceDataParking: any;

    beforeEach(async () => {
        sourceDataParking = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../../data/isphk-parking-data-input.geojson"), "utf-8")
        ).features;
    });

    it("should transform opening hours", async () => {
        const expected = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../../data/isphk-openhours-data-transformed.json"), "utf-8")
        );
        const transformation = new IptOictOpeningHoursTransformation(SourceEnum.Isphk);
        const result = transformation.transformArray(sourceDataParking);

        expect(result).to.be.deep.equal(expected);
    });
});

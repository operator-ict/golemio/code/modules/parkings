import { expect } from "chai";
import { IsphkMeasurementTransformation } from "#ie/transformations/iptoict/IsphkMeasurementTransformation";
import { IIsphkMeasurementsItem } from "#sch/datasources/isphk/interfaces/IIsphkMeasurements";
import { IIsphkMeasurementSchema } from "#ie/transformations/iptoict/IIsphkMeasurementSchema";

describe("IsphkMeasurementTransformation", () => {
    it("should transform ISPHK measurments", async () => {
        const input: IIsphkMeasurementsItem = {
            id: "hk_isp_001",
            capacity: 60,
            free: 20,
            status: "nepoužíváme",
        };
        const expected: IIsphkMeasurementSchema = {
            source: "isphk",
            source_id: "hk_isp_001",
            parking_id: "isphk-hk_isp_001",
            total_spot_number: 60,
            available_spot_number: 20,
            occupied_spot_number: 40,
            date_modified: "2024-09-25T15:12:23.000Z",
        };
        const transformation = new IsphkMeasurementTransformation(new Date("2024-09-25T15:12:23.000Z"));
        const result = transformation.transformArray([input]);
        expect(result).to.be.deep.equal([expected]);
    });
});

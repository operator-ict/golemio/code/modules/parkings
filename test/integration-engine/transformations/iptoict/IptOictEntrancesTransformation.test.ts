import { IptOictEntrancesTransformation } from "#ie/transformations/iptoict/IptOictEntrancesTransformation";
import fs from "fs";
import path from "path";
import { IEntrance } from "#sch/models/interfaces/IEntrance";
import { TGeoCoordinates } from "@golemio/core/dist/output-gateway";
import { expect } from "chai";

describe("IptOictEntrancesTransformation", () => {
    const source = "isphk";
    const transformation = new IptOictEntrancesTransformation(source);

    it("transforms data correctly", () => {
        const entrances = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../../data/iptoict-entrances.geojson"), "utf-8")
        ).features;

        const expectedResult: IEntrance[] = [
            {
                entrance_id: "hk_isp_002_001",
                source: "isphk",
                parking_id: "isphk-hk_isp_002",
                location: {
                    type: "Point",
                    coordinates: [15.8326895, 50.2131171],
                } as TGeoCoordinates,
                entry: false,
                exit: true,
                entrance_type: ["car"],
                level: 0,
                max_height: 2.1,
                max_width: null,
                max_length: null,
                max_weight: 2500,
            },
            {
                entrance_id: "hk_isp_002_002",
                source: "isphk",
                parking_id: "isphk-hk_isp_002",
                location: {
                    type: "Point",
                    coordinates: [15.832548, 50.2130889],
                } as TGeoCoordinates,
                entry: true,
                exit: false,
                entrance_type: ["car"],
                level: 0,
                max_height: 2.1,
                max_width: null,
                max_length: null,
                max_weight: 2500,
            },
        ];

        const transformedResult = transformation.transformArray(entrances);
        expect(transformedResult).to.deep.eq(expectedResult);
    });
});

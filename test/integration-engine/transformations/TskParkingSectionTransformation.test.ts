import { expect } from "chai";
import { TskParkingSectionTransformation } from "#ie/transformations/TskParkingSectionTransformation";
import { ITskParkingSection } from "#sch/datasources/interfaces/ITskParkingSection";
import { IParking } from "#ie/ParkingInterface";
import { GeoCoordinatesType } from "@golemio/core/dist/output-gateway";

describe("TskParkingSectionTransformation", () => {
    const transformationDate = new Date("2023-09-25T11:17:07.297Z");
    const transformation = new TskParkingSectionTransformation(transformationDate);

    it("transforms data correctly", () => {
        const tskParkingMachines: ITskParkingSection[] = [
            {
                idSection: "9a84d143-39e8-4460-8cef-00c5e5980cf2",
                sectionCode: "P2-0616",
                sectionCat: 1,
                borderWKT:
                    "POLYGON ((14.440558278944026 50.074408916133315, 14.44075675813181 50.074252472117145, " +
                    "14.441330414492716 50.074205170614967, 14.441753182968831 50.074248010434417, " +
                    "14.442034093989282 50.0743658250624, 14.442412370753226 50.07424087071751, 14.442818447051611 " +
                    "50.074205169748751, 14.443124397859711 50.074221235269086, 14.443566641036918 50.0742355210572, " +
                    "14.443946291859783 50.074419378097012, 14.44203548683657 50.074406878843021, 14.440558278944026 " +
                    "50.074408916133315))",
                idTariff: "fb6ddc1e-0236-4e54-bc66-5bf031b02e94",
                projectedCapacity: 60,
                activeFrom: "2023-03-15T14:17:05.297",
            },
            {
                idSection: "e2469c84-d93e-46f1-8c49-00cc5ce5258c",
                sectionCode: "P8-2311",
                sectionCat: 7,
                borderWKT:
                    "POLYGON ((14.470466737966575 50.108095191181633, 14.470611657402745 50.108022831568192, " +
                    "14.470953502336846 50.108045984192749, 14.470999278776418 50.108129956971155, 14.47105182267978 " +
                    "50.108071321895636, 14.47209819077022 50.108134279829564, 14.47219735858898 50.108208931534641, " +
                    "14.470466737966575 50.108095191181633))",
                idTariff: null,
                projectedCapacity: 0,
                activeFrom: "2023-03-15T14:17:05.297",
            },
        ];
        const expectedOutput: IParking[] = [
            {
                id: "tsk2-P2-0616",
                source: "tsk_v2",
                source_id: "9a84d143-39e8-4460-8cef-00c5e5980cf2",
                data_provider: "www.tsk-praha.cz",
                location: {
                    type: GeoCoordinatesType.Polygon,
                    coordinates: [
                        [
                            [14.440558278944026, 50.074408916133315],
                            [14.44075675813181, 50.074252472117145],
                            [14.441330414492716, 50.074205170614967],
                            [14.441753182968831, 50.074248010434417],
                            [14.442034093989282, 50.0743658250624],
                            [14.442412370753226, 50.07424087071751],
                            [14.442818447051611, 50.074205169748751],
                            [14.443124397859711, 50.074221235269086],
                            [14.443566641036918, 50.0742355210572],
                            [14.443946291859783, 50.074419378097012],
                            [14.44203548683657, 50.074406878843021],
                            [14.440558278944026, 50.074408916133315],
                        ],
                    ],
                },
                name: "P2-0616",
                total_spot_number: 60,
                category: "blue",
                tariff_id: "fb6ddc1e-0236-4e54-bc66-5bf031b02e94",
                valid_from: "2023-03-15T14:17:05+01:00",
                parking_type: "on_street",
                zone_type: "zone",
                date_modified: "2023-09-25T11:17:07.297Z",
                centroid: {
                    type: GeoCoordinatesType.Point,
                    coordinates: [14.442252285401905, 50.074312273922885],
                },
                parking_policy: "zone",
                active: true,
            },
            {
                id: "tsk2-P8-2311",
                source: "tsk_v2",
                source_id: "e2469c84-d93e-46f1-8c49-00cc5ce5258c",
                data_provider: "www.tsk-praha.cz",
                location: {
                    type: GeoCoordinatesType.Polygon,
                    coordinates: [
                        [
                            [14.470466737966575, 50.10809519118163],
                            [14.470611657402745, 50.10802283156819],
                            [14.470953502336846, 50.10804598419275],
                            [14.470999278776418, 50.108129956971155],
                            [14.47105182267978, 50.108071321895636],
                            [14.47209819077022, 50.108134279829564],
                            [14.47219735858898, 50.10820893153464],
                            [14.470466737966575, 50.10809519118163],
                        ],
                    ],
                },
                name: "P8-2311",
                total_spot_number: 0,
                tariff_id: undefined,
                category: "other",
                valid_from: "2023-03-15T14:17:05+01:00",
                parking_type: "on_street",
                zone_type: "zone",
                date_modified: "2023-09-25T11:17:07.297Z",
                centroid: {
                    type: GeoCoordinatesType.Point,
                    coordinates: [14.471332048277777, 50.10811588155141],
                },
                parking_policy: "zone",
                active: true,
            },
        ];

        const result = transformation.transformArray(tskParkingMachines);
        expect(result).to.deep.eq(expectedOutput);
    });
});

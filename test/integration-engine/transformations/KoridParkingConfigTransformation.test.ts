import { KoridParkingConfigTransformation } from "#ie/transformations/KoridParkingConfigTransformation";
import { Parkings } from "#sch";
import { ParkingsDtoSchema } from "#sch/datasources/ParkingsDtoSchema";
import { ParkingTariffsModel } from "#sch/models/ParkingTariffsModel";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { promises as fs } from "fs";

chai.use(chaiAsPromised);

describe("KoridParkingConfigTransformation", () => {
    let transformation: KoridParkingConfigTransformation;
    let testSourceData: any[];
    let validatorGeo: JSONSchemaValidator;
    let validatorTariff: JSONSchemaValidator;
    let validatorLocation: JSONSchemaValidator;

    before(() => {
        validatorGeo = new JSONSchemaValidator(Parkings.name + "PgModelValidator", ParkingsDtoSchema);
        validatorTariff = new JSONSchemaValidator("ParkingTariffsValidator", ParkingTariffsModel.jsonSchema);
        validatorLocation = new JSONSchemaValidator(
            Parkings.location.name + "PgModelValidator",
            Parkings.location.outputJsonSchema
        );
    });

    beforeEach(async () => {
        transformation = new KoridParkingConfigTransformation();
        const buffer = await fs.readFile(__dirname + "/../data/korid-parkings-config-input.json");
        testSourceData = JSON.parse(buffer.toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("KoridParkingsConfig");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        await expect(validatorGeo.Validate(data.geo)).to.be.fulfilled;
        await expect(validatorTariff.Validate(data.tariff)).to.be.fulfilled;
        await expect(validatorLocation.Validate(data.location)).to.be.fulfilled;
    });
});

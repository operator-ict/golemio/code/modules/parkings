import { TskParkingZonesTariffsTransformation } from "#ie/transformations/TskParkingZonesTariffsTransformation";
import { expect } from "chai";
import { tskParkingZonesTariffListInput } from "../data/tsk-parking-zones-tariff-list-input";
import { tskParkingZonesTariffListTransformed } from "../data/tsk-parking-zones-tariff-list-transformed";

describe("TskParkingZonesTariffsTransformation", () => {
    const transformation = new TskParkingZonesTariffsTransformation(new Date("2023-09-20T12:00:00.000Z"));

    describe("getTariffPeriods", () => {
        it("should get charge periods with holiday", () => {
            const periods = transformation["getTariffPeriods"](
                tskParkingZonesTariffListInput[0].tariffGroups[0],
                tskParkingZonesTariffListInput[0].tariffGroups[0].tariffDefinitions[0],
                true
            );

            expect(periods.length).to.eq(1);
            expect(periods[0].length).to.eq(8);
            for (let i = 0; i < 4; i++) {
                expect(periods[0][i].ph).to.eq("PH_off");
            }
            for (let i = 4; i < 8; i++) {
                expect(periods[0][i].ph).to.eq("PH_only");
            }
        });

        it("should get charge periods without holiday", () => {
            const periods = transformation["getTariffPeriods"](
                tskParkingZonesTariffListInput[0].tariffGroups[0],
                tskParkingZonesTariffListInput[0].tariffGroups[0].tariffDefinitions[0],
                false
            );

            expect(periods.length).to.eq(1);
            expect(periods[0].length).to.eq(4);
            for (const item of periods[0]) {
                expect(item.ph).to.eq("PH_off");
            }
        });
    });

    it("transforms data correctly", () => {
        const result = transformation.transformArray(tskParkingZonesTariffListInput);

        expect(result.flat()).to.deep.eq(tskParkingZonesTariffListTransformed);
    });
});

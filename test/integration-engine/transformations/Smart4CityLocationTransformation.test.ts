import { Smart4CityLocationTransformation } from "#ie/transformations/Smart4CityLocationTransformation";
import { expect } from "chai";
import { smart4CityInputDataFixture } from "../data/Smart4CityInputDataFixture";
import { smart4CityLocationTransformed } from "../data/Smart4CityLocationTransformed";

describe("Smart4CityLocationTransformation", () => {
    it("should transform input data", async () => {
        const transformationDate = new Date("2024-01-04T06:15:00.000Z");
        const transformation = new Smart4CityLocationTransformation("benesov", transformationDate);
        const result = transformation.transformCollection(smart4CityInputDataFixture);

        expect(result).to.be.an("object").that.has.all.keys("parking", "parkingLocation");
        expect(result).to.deep.equal(smart4CityLocationTransformed);
    });
});

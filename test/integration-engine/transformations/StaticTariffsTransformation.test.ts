import fs from "fs";
import { StaticTariffsJson } from "#sch/datasources/StaticTariffsJsonSchema";
import { expect } from "chai";
import { StaticTariffsTransformation } from "#ie/transformations/StaticTariffsTransformation";
import { StaticTariffsFixture } from "../data/StaticTariffsFixture";

describe("StaticTariffsTransformation", () => {
    const transformation = new StaticTariffsTransformation();

    it("transforms single tariff", async () => {
        const rawData = fs.readFileSync(__dirname + "/../data/StaticParkingTariffsData.json") as unknown;
        const matchingData: StaticTariffsJson = JSON.parse(rawData as string);
        const result = await transformation.transform(matchingData.tariffs.slice(0, 1));

        expect(result[0]).to.deep.equal(StaticTariffsFixture[0]);
    });
});

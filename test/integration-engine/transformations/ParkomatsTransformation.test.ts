import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { readFile } from "fs/promises";
import { ParkomatsTransformation } from "#ie/transformations/ParkomatsTransformation";

chai.use(chaiAsPromised);

describe("ParkomatsTransformation", () => {
    let transformation: ParkomatsTransformation;
    let testSourceData: any[];

    beforeEach(async () => {
        transformation = new ParkomatsTransformation();
        const buffer = await readFile(__dirname + "/../data/parkomats-datasource.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("Parkomats");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element with object keys", async () => {
        const data = await transformation.transform(testSourceData);
        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("channel");
            expect(data[i]).to.have.property("parking_zone");
            expect(data[i]).to.have.property("price");
            expect(data[i]).to.have.property("ticket_bought");
            expect(data[i]).to.have.property("transaction_id");
            expect(data[i]).to.have.property("validity_from");
            expect(data[i]).to.have.property("validity_to");
        }
    });
});

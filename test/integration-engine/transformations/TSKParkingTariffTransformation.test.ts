import { TSKParkingTariffTransformation } from "#ie/transformations/TSKParkingTariffTransformation";
import { ParkingTariffsModel } from "#sch/models/ParkingTariffsModel";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import tskDataTransformed from "../data/tsk-parking-tariff-data-transformed";

chai.use(chaiAsPromised);

describe("TSKParkingTariffTransformation", () => {
    let transformation: TSKParkingTariffTransformation;
    let sourceData: { tsk: Array<Record<string, any>>; lastUpdated: Date };
    let validator: JSONSchemaValidator;

    before(() => {
        validator = new JSONSchemaValidator("ParkingTariffsValidator", ParkingTariffsModel.jsonSchema);
    });

    beforeEach(async () => {
        transformation = new TSKParkingTariffTransformation();
        sourceData = {
            tsk: require("../data/tsk-parking-tariff-data-input.json"),
            lastUpdated: new Date("2020-10-12T15:19:21+02:00"),
        };
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("TSKParkingData");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(sourceData);
        await expect(data).to.eql(tskDataTransformed);
        await expect(validator.Validate(data)).to.be.fulfilled;
    });
});

import { TSKParkingTransformation } from "#ie/transformations/TSKParkingTransformation";
import { Parkings } from "#sch";
import { ParkingsDtoSchema } from "#sch/datasources/ParkingsDtoSchema";
import { PaymentModel } from "#sch/models/PaymentModel";
import { config } from "@golemio/core/dist/integration-engine/config";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";
import tskDataTransformed from "../data/tsk-parking-data-transformed";

chai.use(chaiAsPromised);

describe("TSKParkingTransformation", () => {
    let transformation: TSKParkingTransformation;
    let sourceData: Array<Record<string, any>>;
    let validator: JSONSchemaValidator;
    let validatorPayment: JSONSchemaValidator;
    const transformationDate = "2022-12-21T16:30:00+02:00";

    before(() => {
        validator = new JSONSchemaValidator(Parkings.name + "PgModelValidator", ParkingsDtoSchema);
        validatorPayment = new JSONSchemaValidator(Parkings.name + "PaymentPgModelValidator", PaymentModel.jsonSchema);
    });

    beforeEach(async () => {
        transformation = new TSKParkingTransformation(transformationDate);
        sourceData = require("../data/tsk-parking-data-input.json").features;
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("TSKParkingData");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        sinon.stub(config, "PARKING_ZONES_PAYMENT_URL").value("https://ke-utc.appspot.com/static/onstreet.html");
        const data = await transformation.transform(sourceData);
        expect(data).to.eql(tskDataTransformed);
        await expect(validator.Validate(data.parking)).to.be.fulfilled;
        await expect(validatorPayment.Validate(data.payment)).to.be.fulfilled;
    });
});

import { IptOictTariffsTransformation } from "#ie/transformations/IptOictTariffsTransformation";
import { expect } from "chai";
import { iptOictTariffsData } from "../data/iptoict-tariffs-data";
import { iptOictTariffsDataTransformed } from "../data/iptoict-tariffs-data-transformed";

describe("IptOictTariffsTransformation", () => {
    const transformation = new IptOictTariffsTransformation("isphk", new Date());

    it("transforms data correctly", () => {
        const result = transformation.transformArray(iptOictTariffsData);
        expect(result).to.deep.eq(iptOictTariffsDataTransformed);
    });
});

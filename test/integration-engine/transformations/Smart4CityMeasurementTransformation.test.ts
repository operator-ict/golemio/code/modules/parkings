import { Smart4CityMeasurementTransformation } from "#ie/transformations/Smart4CityMeasurementTransformation";
import { expect } from "chai";
import { smart4CityInputDataFixture } from "../data/Smart4CityInputDataFixture";
import { smart4CityMeasurementTransformed } from "../data/Smart4CityMeasurementTransformed";

describe("Smart4CityMeasurementTransformation", () => {
    it("should transform input data", async () => {
        const transformationDate = new Date("2024-01-04T06:15:00.000Z");
        const transformation = new Smart4CityMeasurementTransformation(transformationDate);
        const result = transformation.transformArray(smart4CityInputDataFixture);

        expect(result).to.be.an("array").that.has.lengthOf(2);
        expect(result).to.deep.equal(smart4CityMeasurementTransformed);
    });
});

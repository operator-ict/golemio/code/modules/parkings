import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { BedrichovMeasurementsTransformation } from "#ie/transformations/BedrichovMeasurementsTransformation";
import { expect } from "chai";
import { bedrichovInputDataTransformed } from "../data/bedrichov-data-input-transformed";
import { bedrichovInputData } from "../data/bedrichov-input-data";

describe("BedrichovMeasurementsTransformation.test", () => {
    const transformation = ParkingsContainer.resolve<BedrichovMeasurementsTransformation>(
        ModuleContainerToken.BedrichovMeasurementsTransformation
    );

    it("transforms data correctly", () => {
        const result = transformation.transformArray(bedrichovInputData);
        const resultWithoutDateModified = result.map((r) => {
            const { date_modified, ...rest } = r;
            return rest;
        });

        expect(resultWithoutDateModified).to.deep.eq(bedrichovInputDataTransformed);
    });
});

import { KoridParkingDataTransformation } from "#ie/transformations/KoridParkingDataTransformation";
import { Parkings } from "#sch";
import { ParkingMeasurementsDtoSchema } from "#sch/datasources/ParkingMeasurementsDtoSchema";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { promises as fs } from "fs";

chai.use(chaiAsPromised);

describe("KoridParkingDataTransformation", () => {
    let transformation: KoridParkingDataTransformation;
    let testSourceData: any[];
    let validator: JSONSchemaValidator;

    before(() => {
        validator = new JSONSchemaValidator(Parkings.measurements.name + "PgModelValidator", ParkingMeasurementsDtoSchema);
    });

    beforeEach(async () => {
        transformation = new KoridParkingDataTransformation();
        const buffer = await fs.readFile(__dirname + "/../data/korid-parkings-data-input.json");
        testSourceData = JSON.parse(buffer.toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("KoridParkingsData");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        await expect(validator.Validate(data)).to.be.fulfilled;
    });
});

import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { IPRParkingTransformation } from "#ie/transformations/IPRParkingTransformation";
import iprDataTransformed from "../data/ipr-parking-data-transformed";
import { IIPRParkingJsonFeature } from "#sch/datasources/interfaces/IIPRParkingJsonSchema";

chai.use(chaiAsPromised);

describe("IPRParkingTransformation", () => {
    let transformation: IPRParkingTransformation;
    let sourceData: IIPRParkingJsonFeature[];

    beforeEach(async () => {
        transformation = new IPRParkingTransformation();
        sourceData = require("../data/ipr-parking-data-input.json").features;
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("IPRParkingData");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(sourceData);
        expect(data).to.eql(iprDataTransformed);
    });
});

import { StaticTariffsMatcher } from "#ie/transformations/StaticTariffsMatcher";
import fs from "fs";
import { StaticTariffsJson } from "#sch/datasources/StaticTariffsJsonSchema";
import { expect } from "chai";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { ParkingsModelFake } from "../models/ParkingsModel.fake";
import { ParkingFixture } from "../data/ParkingFixture";
import { IParking } from "#ie/ParkingInterface";

describe("StaticTariffsMatcher", () => {
    const parkingsModelFake = new ParkingsModelFake();

    const matcher = new StaticTariffsMatcher(parkingsModelFake as any as ParkingsRepository);

    it("should match tariffs", async () => {
        const rawData = fs.readFileSync(__dirname + "/../data/StaticParkingTariffsData.json") as unknown;
        const matchingData: StaticTariffsJson = JSON.parse(rawData as string);
        const result = await matcher.matchTariffRelations(matchingData.relations);

        const expectedResult: IParking = ParkingFixture;
        expectedResult.tariff_id = "park_and_ride_zone_1_tsk";

        expect(result).to.deep.equal([expectedResult]);
    });
});

import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { OsmOpeningHoursTransformation } from "#ie/transformations/osm/OsmOpeningHoursTransformation";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import path from "path";

chai.use(chaiAsPromised);

describe("OsmOpeningHoursTransformation", () => {
    let sourceDataParking: any;

    beforeEach(async () => {
        sourceDataParking = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../../data/osm-parkings-data-input.geojson"), "utf-8")
        ).features;
    });

    it("should transform opening hours", async () => {
        const expected = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../../data/osm-openhours-data-transformed.json"), "utf-8")
        );
        const tranformation = ParkingsContainer.resolve<OsmOpeningHoursTransformation>(
            ModuleContainerToken.OsmOpeningHoursTransformation
        );
        const result = tranformation.transformArray(sourceDataParking);
        expect(result).to.be.deep.equal(expected);
    });
});

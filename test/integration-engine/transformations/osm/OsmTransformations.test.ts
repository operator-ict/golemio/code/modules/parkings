import { IGenericFeature } from "#ie/datasources/interfaces/IGenericFeature";
import { OsmParkingSpaceTransformation } from "#ie/transformations/osm/OsmParkingSpaceTransformation";
import { OsmParkingTransformation } from "#ie/transformations/osm/OsmParkingTransformation";
import { OsmSchemaGenerator } from "#sch/datasources/osm/FeatureCollectionSchemaGenerator";
import { IOsmParkingProperties } from "#sch/datasources/osm/interfaces/IOsmParkingProperties";
import { IOsmParkingSpacesProperties } from "#sch/datasources/osm/interfaces/IOsmParkingSpacesProperties";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import path from "path";

chai.use(chaiAsPromised);

describe("OSMParkingTransformation", () => {
    let transformation: OsmParkingTransformation;
    let transformationSpaces: OsmParkingSpaceTransformation;
    let sourceDataParking: Array<IGenericFeature<IOsmParkingProperties>>;
    let sourceDataParkingLocations: Array<IGenericFeature<IOsmParkingSpacesProperties>>;
    let sourceDataParkingValid: Array<IGenericFeature<IOsmParkingProperties>>;
    let sourceDataParkingLocationsValid: Array<IGenericFeature<IOsmParkingSpacesProperties>>;

    beforeEach(async () => {
        transformation = new OsmParkingTransformation(new Date("2024-02-06T00:00:00.000Z"));
        transformationSpaces = new OsmParkingSpaceTransformation();
        sourceDataParking = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../../data/osm-parkings-data-input.geojson"), "utf-8")
        ).features;
        sourceDataParkingLocations = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../../data/osm-parking-spaces-data-input.geojson"), "utf-8")
        ).features;
        sourceDataParkingValid = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../../data/osm-parkings-data-input-valid.geojson"), "utf-8")
        ).features;
        sourceDataParkingLocationsValid = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../../data/osm-parking-spaces-data-input-valid.geojson"), "utf-8")
        ).features;
    });

    it("should validate parkings input", async () => {
        const validator = new JSONSchemaValidator(
            "ParkingOsmDataSourceValidator",
            OsmSchemaGenerator.getParkingSchema().properties.features
        );
        expect(await validator.Validate(sourceDataParkingValid)).to.be.true;
    });

    it("should validate parkings input and fail on invalid", async () => {
        const validator = new JSONSchemaValidator(
            "ParkingOsmDataSourceValidator",
            OsmSchemaGenerator.getParkingSchema().properties.features
        );
        await expect(validator.Validate(sourceDataParking)).to.be.rejected;
    });

    it("should validate parking spaces input", async () => {
        const validator = new JSONSchemaValidator(
            "ParkingSpacesOsmDataSourceValidator",
            OsmSchemaGenerator.getParkingSpacesSchema().properties.features
        );
        expect(await validator.Validate(sourceDataParkingLocationsValid)).to.be.true;
    });

    it("should validate parking spaces input and fail on invalid", async () => {
        const validator = new JSONSchemaValidator(
            "ParkingSpacesOsmDataSourceValidator",
            OsmSchemaGenerator.getParkingSpacesSchema().properties.features
        );
        await expect(validator.Validate(sourceDataParkingLocations)).to.be.rejected;
    });

    it("should transform parkings", async () => {
        const expectedResult = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../../data/osm-parkings-transformed.json"), "utf-8")
        );
        const result = transformation.transformArray(sourceDataParkingValid);
        expect(result).to.deep.equal(expectedResult);
    });

    it("should transform parking spaces", async () => {
        const result = transformationSpaces.transformArray(sourceDataParkingLocationsValid);
        const expectedResult = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../../data/osm-parking-spaces-transformed.json"), "utf-8")
        );
        expect(result).to.deep.equal(expectedResult);
    });
});

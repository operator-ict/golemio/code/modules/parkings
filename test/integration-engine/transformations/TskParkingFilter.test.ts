import { TskParkingFilter } from "#ie/transformations/TskParkingFilter";
import { expect } from "chai";

describe("TskParkingFilter", () => {
    const tskTariffFilter = new TskParkingFilter();

    it("does not filter unique parkings", () => {
        const correctParkings = require("../data/tsk-parking-data-input.json").features;
        const result = tskTariffFilter.filterUniqueParkings(correctParkings);
        expect(result).to.deep.eq(correctParkings);
    });

    it("filters out valid duplicate parkings", () => {
        const parkingsWithDuplicates = require("../data/tsk-parking-data-duplicates-input.json").features;
        const expected = require("../data/tsk-parking-data-filtered-duplicates.json").features;
        const result = tskTariffFilter.filterUniqueParkings(parkingsWithDuplicates);
        expect(result).to.deep.eq(expected);
    });

    it("deletes invalid duplicate parkings", () => {
        const invalidParkings = require("../data/tsk-parking-data-invalid-duplicates-input.json").features;
        const expected = require("../data/tsk-parking-data-input.json").features;
        const result = tskTariffFilter.filterUniqueParkings(invalidParkings);
        expect(result).to.deep.eq(expected);
    });
});

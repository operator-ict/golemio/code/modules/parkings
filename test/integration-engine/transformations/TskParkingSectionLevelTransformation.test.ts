import { expect } from "chai";
import { IParkingLocation } from "#ie/ParkingInterface";
import { GeoCoordinatesType } from "@golemio/core/dist/output-gateway";
import { TskParkingSectionLevelTransformation } from "#ie/transformations/TskParkingSectionLevelTransformation";
import { ITskParkingSectionLevel } from "#sch/datasources/interfaces/ITskParkingSectionLevel";

describe("TskParkingSectionLevelTransformation", () => {
    const transformation = new TskParkingSectionLevelTransformation("disabled");

    it("transforms data correctly", () => {
        const tskParkingMachines: ITskParkingSectionLevel[] = [
            {
                idSectionItem: "a489c153-7790-4c5e-b983-00016e6be95d",
                idSection: "3aa7fde0-e4e7-40db-91ac-7487a1691ea2",
                sectionItemCode: "P4-1710",
                streetNameValid: "Bartákova",
                sectionCat: 1,
                borderWKT:
                    "POLYGON ((14.438110030970504 50.04603388132039, 14.438114825700046 50.046015900051209, " +
                    "14.438141039567954 50.046022512862528, 14.438163987912334 50.046022092043408, 14.438198995902374 " +
                    "50.0460276403637, 14.438242198776376 50.046039439447796, 14.438285750482155 50.04605838460332, " +
                    "14.438304704336334 50.046068637955749, 14.438328959456543 50.046084097726755, 14.438371194260823 " +
                    "50.046131615994462, 14.43840379405991 50.046181229820625, 14.438428393613611 50.046229556866713, " +
                    "14.438441046461893 50.046273567885763, 14.438410386250395 50.04627736178589, 14.438407244261663 " +
                    "50.0462554924001, 14.438394539465151 50.046213836981934, 14.43837965760933 50.046188860007796, " +
                    "14.438347070249561 50.046141307366163, 14.4383079709766 50.046098513327117, 14.438290911030689 " +
                    "50.04608791017089, 14.438265013732437 50.0460708992924, 14.43823099167197 50.0460562363504, " +
                    "14.438184737510337 50.046045391193871, 14.438110030970504 50.04603388132039))",
                parkingType: 1,
                projectedCapacity: 7,
                activeFrom: "2023-03-11T22:36:43",
            },
            {
                idSectionItem: "505210ed-9868-4af5-a695-0004830504e5",
                idSection: "a91c7888-c23c-4349-b172-20bdf9db8b7f",
                sectionItemCode: "P18-0002",
                streetNameValid: "Kopřivnická",
                sectionCat: 2,
                borderWKT:
                    "POLYGON ((14.514873223130207 50.139546700189122, 14.514935115997211 50.139537404246639, " +
                    "14.515046564904521 50.139870302402564, 14.514974097377392 50.139853227495614, 14.514873223130207 " +
                    "50.139546700189122))",
                parkingType: 3,
                projectedCapacity: 11,
                activeFrom: "2023-02-25T11:54:25",
            },
        ];
        const expectedOutput: IParkingLocation[] = [
            {
                id: "tsk_v2-a489c153-7790-4c5e-b983-00016e6be95d",
                data_provider: "www.tsk-praha.cz",
                location: {
                    type: GeoCoordinatesType.Polygon,
                    coordinates: [
                        [
                            [14.438110030970504, 50.04603388132039],
                            [14.438114825700046, 50.046015900051209],
                            [14.438141039567954, 50.046022512862528],
                            [14.438163987912334, 50.046022092043408],
                            [14.438198995902374, 50.0460276403637],
                            [14.438242198776376, 50.046039439447796],
                            [14.438285750482155, 50.04605838460332],
                            [14.438304704336334, 50.046068637955749],
                            [14.438328959456543, 50.046084097726755],
                            [14.438371194260823, 50.046131615994462],
                            [14.43840379405991, 50.046181229820625],
                            [14.438428393613611, 50.046229556866713],
                            [14.438441046461893, 50.046273567885763],
                            [14.438410386250395, 50.04627736178589],
                            [14.438407244261663, 50.0462554924001],
                            [14.438394539465151, 50.046213836981934],
                            [14.43837965760933, 50.046188860007796],
                            [14.438347070249561, 50.046141307366163],
                            [14.4383079709766, 50.046098513327117],
                            [14.438290911030689, 50.04608791017089],
                            [14.438265013732437, 50.0460708992924],
                            [14.43823099167197, 50.0460562363504],
                            [14.438184737510337, 50.046045391193871],
                            [14.438110030970504, 50.04603388132039],
                        ],
                    ],
                },
                centroid: {
                    type: GeoCoordinatesType.Point,
                    coordinates: [14.438347070249561, 50.04614130736616],
                },
                total_spot_number: 7,
                source: "tsk_v2",
                source_id: "3aa7fde0-e4e7-40db-91ac-7487a1691ea2",
                special_access: ["disabled"],
            },
            {
                id: "tsk_v2-505210ed-9868-4af5-a695-0004830504e5",
                data_provider: "www.tsk-praha.cz",
                location: {
                    type: GeoCoordinatesType.Polygon,
                    coordinates: [
                        [
                            [14.514873223130207, 50.13954670018912],
                            [14.514935115997211, 50.13953740424664],
                            [14.515046564904521, 50.139870302402564],
                            [14.514974097377392, 50.139853227495614],
                            [14.514873223130207, 50.13954670018912],
                        ],
                    ],
                },
                centroid: {
                    type: GeoCoordinatesType.Point,
                    coordinates: [14.514959894017364, 50.139703853324605],
                },
                total_spot_number: 11,
                source: "tsk_v2",
                source_id: "a91c7888-c23c-4349-b172-20bdf9db8b7f",
                special_access: ["disabled"],
            },
        ];
        const result = transformation.transformArray(tskParkingMachines);
        expect(result).to.deep.equal(expectedOutput);
    });
});

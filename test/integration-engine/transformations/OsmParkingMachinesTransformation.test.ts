import { IParkingMachine } from "#sch/models/interfaces/IParkingMachine";
import { expect } from "chai";
import { OsmParkingMachinesTransformation } from "#ie/transformations/osm/OsmParkingMachinesTransformation";
import { IGenericFeature } from "#ie/datasources/interfaces/IGenericFeature";
import { IOsmParkingMachinesProperties } from "#sch/datasources/osm/interfaces/IOsmParkingMachinesProperties";

describe("OsmParkingMachinesTransformation", () => {
    const transformationDate = new Date("2024-04-04");
    const transformation = new OsmParkingMachinesTransformation(transformationDate);

    it("transforms data correctly", () => {
        const parkingMachines: Array<IGenericFeature<IOsmParkingMachinesProperties>> = [
            {
                type: "Feature",
                geometry: {
                    type: "Point",
                    coordinates: [14.5075317, 50.0696385],
                },
                properties: {
                    parking_id: "r_14864801",
                    osm_id: "n_7653409366",
                    name: null,
                },
            },
            {
                type: "Feature",
                geometry: {
                    type: "Point",
                    coordinates: [14.5075637, 50.0696419],
                },
                properties: {
                    parking_id: "r_14864801",
                    osm_id: "n_7653409375",
                    name: null,
                },
            },
        ];
        const expectedOutput: IParkingMachine[] = [
            {
                id: "osm-n_7653409366",
                source: "osm",
                sourceId: "n_7653409366",
                code: null,
                type: "payment_machine",
                location: {
                    type: "Point",
                    coordinates: [14.5075317, 50.0696385],
                },
                validFrom: transformationDate,
                tariffId: null,
            },
            {
                id: "osm-n_7653409375",
                source: "osm",
                sourceId: "n_7653409375",
                code: null,
                type: "payment_machine",
                location: {
                    type: "Point",
                    coordinates: [14.5075637, 50.0696419],
                },
                validFrom: transformationDate,
                tariffId: null,
            },
        ];

        const result = transformation.transformArray(parkingMachines);
        expect(result).to.deep.eq(expectedOutput);
    });
});

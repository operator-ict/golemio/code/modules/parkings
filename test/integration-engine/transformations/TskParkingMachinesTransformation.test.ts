import { ITskParkingMachine } from "#sch/datasources/interfaces/ITskParkingMachine";
import { IParkingMachine } from "#sch/models/interfaces/IParkingMachine";
import { TskParkingMachinesTransformation } from "#ie/transformations/TskParkingMachinesTransformation";
import { expect } from "chai";

describe("TskParkingMachinesTransformation", () => {
    const transformation = new TskParkingMachinesTransformation();

    it("transforms data correctly", () => {
        const tskParkingMachines: ITskParkingMachine[] = [
            {
                idParkMachine: "8b5d2d2b-b32e-4647-bf23-5851d833bf1c",
                code: "4000048",
                idStatus: 1,
                type: "STANDARD",
                idTariff: "2c6e4a09-9d3a-457d-a565-81f3d6f8f55f",
                positionWKT: "POINT (14.431023594709631 50.053807926707542)",
                activeFrom: "2023-03-27T14:22:55.37",
            },
            {
                idParkMachine: "881efe54-ee0b-40b7-af23-58b929a241ce",
                code: "5000088",
                idStatus: 5,
                type: "FAKE",
                idTariff: "2c6e4a09-9d3a-457d-a565-81f3d6f8f55f",
                positionWKT: "POINT (14.408580214942127 50.063672607107534)",
                activeFrom: "2023-03-27T14:22:55.37",
            },
        ];
        const expectedOutput: IParkingMachine[] = [
            {
                id: "tsk_v2-8b5d2d2b-b32e-4647-bf23-5851d833bf1c",
                source: "tsk_v2",
                sourceId: "8b5d2d2b-b32e-4647-bf23-5851d833bf1c",
                code: "4000048",
                type: "payment_machine",
                location: {
                    type: "Point",
                    coordinates: [14.431023594709631, 50.053807926707542],
                },
                validFrom: new Date("2023-03-27T14:22:55.37"),
                tariffId: "2c6e4a09-9d3a-457d-a565-81f3d6f8f55f",
            },
            {
                id: "tsk_v2-881efe54-ee0b-40b7-af23-58b929a241ce",
                source: "tsk_v2",
                sourceId: "881efe54-ee0b-40b7-af23-58b929a241ce",
                code: "5000088",
                type: "info_box",
                location: {
                    type: "Point",
                    coordinates: [14.408580214942127, 50.063672607107534],
                },
                validFrom: new Date("2023-03-27T14:22:55.37"),
                tariffId: "2c6e4a09-9d3a-457d-a565-81f3d6f8f55f",
            },
        ];

        const result = transformation.transformArray(tskParkingMachines);
        expect(result).to.deep.eq(expectedOutput);
    });
});

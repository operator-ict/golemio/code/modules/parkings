import { ParkingLotsTransformation } from "#ie/transformations/ParkingLotsTransformation";
import { Parkings } from "#sch";
import { config } from "@golemio/core/dist/integration-engine/config";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

import { ParkingMeasurementsDtoSchema } from "#sch/datasources/ParkingMeasurementsDtoSchema";
import { ParkingsDtoSchema } from "#sch/datasources/ParkingsDtoSchema";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import parkingLotsDataTransformed from "../data/parking-lots-data-transformed";
import parkingLotsDataTransformedEnhanced from "../data/parking-lots-data-transformed-enhanced";

chai.use(chaiAsPromised);

describe("ParkingLotsTransformation", () => {
    let transformation: ParkingLotsTransformation;
    let sourceData: Array<Record<string, any>>;
    let staticParkingLotsGeoData: Record<string, any>;
    let validatorGeo: JSONSchemaValidator;
    let validatorMeasurements: JSONSchemaValidator;
    let sandbox: SinonSandbox;

    before(() => {
        validatorGeo = new JSONSchemaValidator(Parkings.name + "PgModelValidator", ParkingsDtoSchema);
        validatorMeasurements = new JSONSchemaValidator(
            Parkings.measurements.name + "PgModelValidator",
            ParkingMeasurementsDtoSchema
        );
    });

    beforeEach(async () => {
        transformation = new ParkingLotsTransformation();
        sourceData = require("../data/parking-lots-data-input.json");
        staticParkingLotsGeoData = require("../data/StaticParkingLotsGeoData.json");
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("TSKParkingData");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        sandbox = sinon.createSandbox();
        sandbox.stub(config, "PARKINGS_PAYMENT_URL").value("https://payment-url.cz");

        const data = await transformation.transform(sourceData);
        await expect(data).to.eql(parkingLotsDataTransformed);
        await expect(validatorGeo.Validate(data.geo)).to.be.fulfilled;
        await expect(validatorMeasurements.Validate(data.measurements)).to.be.fulfilled;
    });

    it("should properly transform collection with additional custom geo", async () => {
        sandbox = sinon.createSandbox();
        sandbox.stub(config, "PARKINGS_PAYMENT_URL").value("https://payment-url.cz");

        const data = await transformation.transform(sourceData, staticParkingLotsGeoData.features);
        await expect(data).to.eql(parkingLotsDataTransformedEnhanced);
        await expect(validatorGeo.Validate(data.geo)).to.be.fulfilled;
        await expect(validatorMeasurements.Validate(data.measurements)).to.be.fulfilled;
    });
});

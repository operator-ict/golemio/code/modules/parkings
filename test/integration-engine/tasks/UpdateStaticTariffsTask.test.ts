import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { UpdateStaticTariffsTask } from "#ie/workers/tasks/UpdateStaticTariffsTask";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { StaticTariffsTransformation } from "#ie/transformations/StaticTariffsTransformation";
import { ParkingTariffsRepository } from "#ie/repositories/ParkingTariffsRepository";
import { StaticTariffsMatcher } from "#ie/transformations/StaticTariffsMatcher";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";

chai.use(chaiAsPromised);

describe("UpdateStaticTariffsTask", () => {
    let sandbox: SinonSandbox;
    let task: UpdateStaticTariffsTask;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));

        const dataSourceStub = {
            getAll: sandbox.stub().returns({
                tariffs: [],
                relations: [],
            }),
        } as any as DataSource;
        const transformer = { transform: sandbox.stub() } as any as StaticTariffsTransformation;
        const parkingTariffsModel = { save: sandbox.stub() } as any as ParkingTariffsRepository;
        const parkingsModel = { save: sandbox.stub(), getOne: sandbox.stub() } as any as ParkingsRepository;
        const staticTariffsMatcher = { matchTariffRelations: sandbox.stub() } as any as StaticTariffsMatcher;

        task = new UpdateStaticTariffsTask(
            "test.test",
            dataSourceStub,
            transformer,
            parkingTariffsModel,
            parkingsModel,
            staticTariffsMatcher
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call correct methods", async () => {
        await task.execute();
        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["transformer"].transform as SinonSpy);
        sandbox.assert.calledOnce(task["parkingsTariffsModel"].save as SinonSpy);
        sandbox.assert.calledOnce(task["matcher"].matchTariffRelations as SinonSpy);
        sandbox.assert.calledOnce(task["parkingsModel"].save as SinonSpy);
    });
});

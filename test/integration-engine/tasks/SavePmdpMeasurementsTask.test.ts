import { PmdpIdList } from "#ie/datasources/pmdp/PmdpMeasurementsDataSource";
import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { SavePmdpMeasurementsTask } from "#ie/workers/tasks/SavePmdpMeasurementsTask";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { pmdpMeasurementDataFixture } from "../data/pmdpMeasurementDataFixture";
import { pmdpMeasurementDataTransformed } from "../data/pmdpMeasurementDataTransformed";

describe("SavePmdpMeasurementsTask", () => {
    let sandbox: SinonSandbox;
    let task: SavePmdpMeasurementsTask;
    let getAllStub: SinonStub;
    let getPmdpParkingStub: SinonStub;

    before(async () => {
        sandbox = sinon.createSandbox({ useFakeTimers: { now: new Date("2024-08-06T08:00:00.000Z") } });
        getAllStub = sandbox.stub().resolves(pmdpMeasurementDataFixture);
        getPmdpParkingStub = sandbox
            .stub()
            .resolves({ id: PmdpIdList.pmdp_001, source: "pmdp", source_id: "pmdp_001", total_spot_number: 1000 });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));

        ParkingsContainer.registerSingleton(
            ModuleContainerToken.PmdpMeasurementsDataSourceFactory,
            class DummyFactory {
                getData(...args: any[]) {
                    return getAllStub(...args);
                }
            }
        ).registerSingleton(
            ModuleContainerToken.CachedPmdpParkingRepository,
            class DummyRepository {
                getPmdpParking = getPmdpParkingStub;
            }
        );
    });

    beforeEach(() => {
        task = ParkingsContainer.resolve(ModuleContainerToken.SavePmdpMeasurementsTask);
    });

    afterEach(() => {
        ParkingsContainer.clearInstances();
    });

    after(async () => {
        sandbox.restore();
    });

    it("should fetch measurement data", async () => {
        const saveMeasurementStub = sandbox.stub(task["parkingsMeasurementsRepository"], "bulkSave");
        const saveMeasurementActualStub = sandbox.stub(task["parkingsMeasurementsActualRepository"], "bulkSave");

        await task["execute"]({ code: PmdpIdList.pmdp_001 });

        sandbox.assert.calledOnce(getAllStub);
        sandbox.assert.calledWithExactly(getAllStub, PmdpIdList.pmdp_001);
        sandbox.assert.calledOnce(getAllStub);
        sandbox.assert.calledOnce(saveMeasurementStub);
        sandbox.assert.calledWith(saveMeasurementStub, [pmdpMeasurementDataTransformed]);
        sandbox.assert.calledOnce(saveMeasurementActualStub);
        sandbox.assert.calledWith(saveMeasurementActualStub, [pmdpMeasurementDataTransformed]);
    });
});

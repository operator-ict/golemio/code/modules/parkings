import { SaveTskParkingMachinesTask } from "#ie/workers/tasks/SaveTskParkingMachinesTask";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { DataSource, PostgresConnector } from "@golemio/core/dist/integration-engine";
import { TskParkingMachinesTransformation } from "#ie/transformations/TskParkingMachinesTransformation";
import { ParkingMachinesRepository } from "#ie/repositories/ParkingMachinesRepository";

describe("SaveTskParkingMachinesTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveTskParkingMachinesTask;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));

        const dataSourceStub = {
            getAll: sandbox.stub().returns([]),
        } as any as DataSource;
        const transformation = { transformArray: sandbox.stub() } as any as TskParkingMachinesTransformation;
        const repository = { bulkSave: sandbox.stub() } as any as ParkingMachinesRepository;

        task = new SaveTskParkingMachinesTask("testQuery", dataSourceStub, transformation, repository);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("calls correct methods", async () => {
        await task.consume(null);
        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["transformation"].transformArray as SinonSpy);
        sandbox.assert.calledOnce(task["repository"].bulkSave as SinonSpy);
    });
});

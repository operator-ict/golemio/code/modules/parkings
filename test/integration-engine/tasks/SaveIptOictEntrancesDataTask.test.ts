import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import fs from "fs";
import path from "path";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { SaveIptOictEntrancesDataTask } from "#ie/workers/tasks/SaveIptOictEntrancesDataTask";

describe("SaveIptOictEntrancesDataTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveIptOictEntrancesDataTask;
    let getEntrancesStub: SinonStub, bulkSaveStub: SinonStub, getDataSourceConfigStub: SinonStub;
    let getEntrances: any;
    let testContainer: DependencyContainer;

    before(async () => {
        const postgresConnector = ParkingsContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
        testContainer = ParkingsContainer.createChildContainer();
        sandbox = sinon.createSandbox();
        getEntrancesStub = sandbox.stub().callsFake(() => getEntrances);

        bulkSaveStub = sandbox.stub().resolves();
        getEntrances = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../data/manual-data-entrances.geojson"), "utf-8")
        ).features;
        getDataSourceConfigStub = sandbox.stub().resolves([{ source: "osm", datasource_entranecs: "http://osm.storage.url" }]);

        testContainer.registerSingleton(
            ModuleContainerToken.IptOictEntrancesDataSource,
            class DummyFactory {
                getEntrancesData = getEntrancesStub;
            }
        );
        testContainer.registerSingleton(
            ModuleContainerToken.ParkingEntrancesRepository,
            class DummyRepository {
                mergeEntrances = bulkSaveStub;
            }
        );
        testContainer.registerSingleton(
            ModuleContainerToken.CachedParkingSourcesRepository,
            class DummyRepository {
                getParkingEntrancesDataSourceConfig = getDataSourceConfigStub;
            }
        );
    });

    beforeEach(() => {
        task = testContainer.resolve(ModuleContainerToken.SaveIptOictEntrancesDataTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
    });

    after(() => {
        sandbox.restore();
    });

    it("should call correct methods", async () => {
        await task["execute"]({ source: "osm" });
        sandbox.assert.calledOnce(getDataSourceConfigStub);
        sandbox.assert.calledOnce(getEntrancesStub);
        sandbox.assert.calledOnce(bulkSaveStub);
    });
});

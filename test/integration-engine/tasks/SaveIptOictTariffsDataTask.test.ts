import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { SaveIptOictTariffsDataTask } from "#ie/workers/tasks/SaveIptOictTariffsDataTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { iptOictTariffsData } from "../data/iptoict-tariffs-data";
import { iptOictTariffsDataTransformed } from "../data/iptoict-tariffs-data-transformed";
import { iptOictTariffsRelationDataTransformed } from "../data/iptoict-tariffs-relation-data-transformed";

describe("SaveIptOictTariffsDataTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveIptOictTariffsDataTask;
    let getParkingTariffsStub: SinonStub;
    let replaceStub: SinonStub;
    let testContainer: DependencyContainer;

    before(async () => {
        const postgresConnector = ParkingsContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
        testContainer = ParkingsContainer.createChildContainer();
        sandbox = sinon.createSandbox();
        getParkingTariffsStub = sandbox.stub().callsFake(() => iptOictTariffsData);
        replaceStub = sandbox.stub().resolves();

        testContainer.registerSingleton(
            ModuleContainerToken.IptOictTariffsDataSource,
            class DummyFactory {
                getTariffsData = getParkingTariffsStub;
            }
        );
        testContainer.registerSingleton(
            ModuleContainerToken.ParkingTariffsRepository,
            class DummyRepository {
                replaceTariffs = replaceStub;
            }
        );
    });

    beforeEach(() => {
        task = testContainer.resolve(ModuleContainerToken.SaveIptOictTariffsDataTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
    });

    after(() => {
        sandbox.restore();
    });

    it("should refresh data", async () => {
        await task["execute"]({ source: "isphk" });
        sandbox.assert.calledOnce(getParkingTariffsStub);
        sandbox.assert.calledOnce(replaceStub);
        sandbox.assert.calledWith(
            replaceStub,
            iptOictTariffsDataTransformed.flat(),
            iptOictTariffsRelationDataTransformed,
            "isphk",
            sinon.match.any
        );
    });
});

import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { UpdateAllParkingAdresses } from "#ie/workers/tasks/UpdateAllParkingAdresses";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import SimpleConfig from "@golemio/core/dist/helpers/configuration/SimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors/QueueManager";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("UpdateAllParkingAdresses", () => {
    let sendMsgStub: SinonStub;
    let sandbox: SinonSandbox;
    let testContainer: DependencyContainer;
    let findStub: SinonStub;
    let updateParkingsTask: UpdateAllParkingAdresses;

    before(async () => {
        const postgresConnector = ParkingsContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();

        sandbox = sinon.createSandbox({ useFakeTimers: true });
        sendMsgStub = sandbox.stub(QueueManager, "sendMessageToExchange");
        findStub = sandbox.stub().resolves([]);
        testContainer = ParkingsContainer.createChildContainer();
        testContainer.register(
            ModuleContainerToken.ParkingsRepository,
            class DummyFactory {
                findWithOutdatedAddress = findStub;
            }
        );
        testContainer.registerInstance<ISimpleConfig>(
            CoreToken.SimpleConfig,
            new SimpleConfig({
                env: {
                    RABBIT_EXCHANGE_NAME: "test",
                },
                module: {
                    parking: {
                        addresses: {
                            batchSize: 10,
                            updateIntervalDays: 180,
                        },
                    },
                },
            })
        );
        updateParkingsTask = testContainer.resolve(ModuleContainerToken.UpdateAllParkingAdresses);
    });

    after(() => {
        sandbox.restore();
        testContainer.clearInstances();
    });

    it("should update photon addresses successfully", async () => {
        await updateParkingsTask.execute();
        sandbox.assert.calledOnce(findStub);
    });
});

import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { IUpdateAddressInput } from "#ie/workers/schemas/UpdateAddressSchema";
import { UpdateLocationAddressTask } from "#ie/workers/tasks/UpdateLocationAddressTask";
import { IPostalAddress } from "@golemio/core/dist/integration-engine/helpers/GeocodeApi";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("UpdateLocationAddressTask", () => {
    let updateAddressTask: UpdateLocationAddressTask;
    let getExtendedAddressFromPhoton: SinonStub;
    let repositoryUpdate: SinonStub;
    let sandbox: SinonSandbox;
    let testContainer: DependencyContainer;
    let keyExists: SinonStub;
    let address: IPostalAddress;

    before(async () => {
        testContainer = ParkingsContainer.createChildContainer();
        address = {
            street_address: "Nikola Tesla 1",
            address_region: "Brno",
            address_formatted: "Nikola Tesla 1, Brno",
            address_country: "Brno Republic",
        };

        getExtendedAddressFromPhoton = sinon.stub().resolves(address);
        repositoryUpdate = sinon.stub().resolves();

        keyExists = sinon.stub().resolves(false);
        IntegrationEngineContainer.registerSingleton(
            ContainerToken.GeocodeApi,
            class DummyFactory {
                getExtendedAddressFromPhoton = getExtendedAddressFromPhoton;
            }
        );

        testContainer.registerSingleton(
            ModuleContainerToken.ParkingsLocationRepository,
            class DummyFactory {
                update = repositoryUpdate;
            }
        );
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
    });

    afterEach(() => {
        sandbox.restore();
    });

    after(async () => {
        testContainer.clearInstances();
    });

    it("get address from photon and save data to repository", async () => {
        updateAddressTask = testContainer.resolve<UpdateLocationAddressTask>(ModuleContainerToken.UpdateLocationAddressTask);

        const data: IUpdateAddressInput = {
            id: "123",
            point: { type: "Point", coordinates: [1, 2] },
        };

        // called once
        await updateAddressTask.execute(data);
        sandbox.assert.calledOnce(getExtendedAddressFromPhoton);
        sandbox.assert.calledOnce(repositoryUpdate);
    });
});

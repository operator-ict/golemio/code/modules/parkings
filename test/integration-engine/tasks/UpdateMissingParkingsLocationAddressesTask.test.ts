import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { UpdateMissingParkingsLocationAddressesTask } from "#ie/workers/tasks/UpdateMissingParkingsLocationAddressesTask";

describe("UpdateMissingParkingsLocationAddressesTask", () => {
    let task: UpdateMissingParkingsLocationAddressesTask;
    let findWithoutAddress: SinonStub;
    let sandbox: SinonSandbox;
    let testContainer: DependencyContainer;
    let sendMsgStub: SinonStub;

    const parking = [
        {
            id: "tsk-3",
            centroid: { type: "Point", coordinates: [1, 2] },
        },
    ];

    before(async () => {
        testContainer = ParkingsContainer.createChildContainer();
        findWithoutAddress = sinon.stub().resolves(parking);
        testContainer.registerSingleton(
            ModuleContainerToken.ParkingsLocationRepository,
            class DummyFactory {
                findWithoutAddress = findWithoutAddress;
            }
        );
    });
    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        sendMsgStub = sandbox.stub(QueueManager, "sendMessageToExchange");
    });

    afterEach(() => {
        sandbox.restore();
    });

    after(async () => {
        testContainer.clearInstances();
    });

    it("send update address messages correctly", async () => {
        task = testContainer.resolve<UpdateMissingParkingsLocationAddressesTask>(
            ModuleContainerToken.UpdateMissingParkingsLocationAdressesTask
        );

        // called once
        await task.execute();
        sandbox.assert.calledOnce(findWithoutAddress);
        sandbox.assert.calledOnce(sendMsgStub);
        sandbox.assert.calledWith(sendMsgStub.getCall(0), sinon.match(/parkings$/), "updateLocationAddress", {
            id: parking[0].id,
            point: parking[0].centroid,
        });
    });
});

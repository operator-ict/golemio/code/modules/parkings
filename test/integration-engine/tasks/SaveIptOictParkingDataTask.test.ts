import { SourceEnum } from "#helpers/constants/SourceEnum";
import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { SaveIptOictParkingDataTask } from "#ie/workers/tasks/SaveIptOictParkingDataTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import fs from "fs";
import path from "path";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("SaveIptOictParkingDataTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveIptOictParkingDataTask;
    let getParkingStub: SinonStub, getParkingSpacesStub: SinonStub, bulkSaveStub: SinonStub;
    let sourceDataParking: any;
    let sourceDataParkingLocations: any;
    let testContainer: DependencyContainer;

    before(async () => {
        const postgresConnector = ParkingsContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
        testContainer = ParkingsContainer.createChildContainer();
        sandbox = sinon.createSandbox();
        getParkingStub = sandbox.stub().callsFake(() => sourceDataParking);
        getParkingSpacesStub = sandbox.stub().callsFake(() => sourceDataParkingLocations);
        bulkSaveStub = sandbox.stub().resolves();
        sourceDataParking = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../data/isphk-parking-data-input.geojson"), "utf-8")
        ).features;
        sourceDataParkingLocations = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../data/isphk-parking-space-data-input.geojson"), "utf-8")
        ).features;

        testContainer.registerSingleton(
            ModuleContainerToken.IptOictDataSource,
            class DummyFactory {
                getParkingData = getParkingStub;
                getParkingLocationData = getParkingSpacesStub;
            }
        );
        testContainer.registerSingleton(
            ModuleContainerToken.ParkingsRepository,
            class DummyRepository {
                saveActiveParkingsWithoutAddress = bulkSaveStub;
            }
        );
        testContainer.registerSingleton(
            ModuleContainerToken.ParkingsLocationRepository,

            class DummyRepository {
                saveWithoutAddress = bulkSaveStub;
            }
        );
        testContainer.registerSingleton(
            ModuleContainerToken.OpenHoursRepository,

            class DummyRepository {
                mergeOpenHours = bulkSaveStub;
            }
        );
    });

    beforeEach(() => {
        task = testContainer.resolve(ModuleContainerToken.SaveIptOictParkingDataTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
    });

    after(() => {
        sandbox.restore();
    });

    it("should refresh data", async () => {
        await task["execute"]({ source: SourceEnum.Isphk });
        sandbox.assert.calledThrice(bulkSaveStub);
    });
});

import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { SaveMrParkitDataTask } from "#ie/workers/tasks/SaveMrParkitDataTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { MrParkitInputDataFixture } from "../data/MrParkitInputDataFixture";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import SimpleConfig from "@golemio/core/dist/helpers/configuration/SimpleConfig";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { QueueManager } from "@golemio/core/dist/integration-engine";

describe("NewParkingsWorker - SaveMrParkitDataTask", () => {
    let sendMsgStub: SinonStub;
    let sandbox: SinonSandbox;
    let task: SaveMrParkitDataTask;
    let getAllStub: SinonStub;
    let testContainer: DependencyContainer;

    before(async () => {
        sandbox = sinon.createSandbox();
        getAllStub = sandbox.stub().resolves([MrParkitInputDataFixture[0]]);

        testContainer = ParkingsContainer.createChildContainer();

        const postgresConnector = testContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();

        testContainer.registerSingleton(
            ModuleContainerToken.ParkingProviderDataSourceFactory,
            class DummyFactory {
                getDataSource() {
                    return {
                        getAll: getAllStub,
                    };
                }
            }
        );
        testContainer.registerInstance<ISimpleConfig>(
            CoreToken.SimpleConfig,
            new SimpleConfig({
                env: {
                    RABBIT_EXCHANGE_NAME: "test",
                },
            })
        );

        sendMsgStub = sandbox.stub(QueueManager, "sendMessageToExchange");
    });

    beforeEach(() => {
        task = testContainer.resolve(ModuleContainerToken.SaveMrParkitDataTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
    });

    after(async () => {
        await task["parkingRepository"]["sequelizeModel"].destroy({
            where: { source: "mr_parkit" },
            force: true,
        });

        await task["parkingLocationRepository"]["sequelizeModel"].destroy({
            where: { source: "mr_parkit" },
            force: true,
        });

        await task["measurementsRepository"]["sequelizeModel"].destroy({
            where: { source: "mr_parkit" },
            force: true,
        });

        sandbox.restore();
    });

    it("should refresh data", async () => {
        await task["execute"]();

        const parkings = await task["parkingRepository"]["sequelizeModel"].findAll({
            where: { source: "mr_parkit" },
            raw: true,
        });
        expect(parkings).to.be.an("array").that.has.lengthOf(1);
        expect(parkings[0]).to.be.an("object").that.have.property("id", "mr_parkit-463253c6-852d-41d4-8082-96532a97eeac");

        const locations = await task["parkingLocationRepository"]["sequelizeModel"].findAll({
            where: { source: "mr_parkit" },
            raw: true,
        });
        expect(locations).to.be.an("array").that.has.lengthOf(1);
        expect(locations[0]).to.be.an("object").that.have.property("id", "mr_parkit-463253c6-852d-41d4-8082-96532a97eeac");

        const measurements = await task["measurementsRepository"]["sequelizeModel"].findAll({
            where: { parking_id: parkings[0].id },
            raw: true,
        });
        expect(measurements).to.be.an("array").that.has.lengthOf(1);
        expect(measurements[0])
            .to.be.an("object")
            .that.have.property("parking_id", "mr_parkit-463253c6-852d-41d4-8082-96532a97eeac");
    });
});

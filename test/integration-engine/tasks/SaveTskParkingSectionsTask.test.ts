import sinon, { SinonSandbox, SinonSpy, SinonStub } from "sinon";
import { DataSource, PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import { SaveTskParkingSectionsTask } from "#ie/workers/tasks/SaveTskParkingSectionsTask";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { TskParkingSectionTransformation } from "#ie/transformations/TskParkingSectionTransformation";
import { IParking } from "#ie/ParkingInterface";
import { ITskParkingSection } from "#sch/datasources/interfaces/ITskParkingSection";

describe("SaveTskParkingSectionsTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveTskParkingSectionsTask;
    let transformation: TskParkingSectionTransformation;
    let sendMsgStub: SinonStub;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));

        const dataSourceStub = {
            getAll: sandbox.stub().returns([
                {
                    idSection: "9a84d143-39e8-4460-8cef-00c5e5980cf2",
                    sectionCode: "P2-0616",
                    sectionCat: 1,
                    borderWKT:
                        "POLYGON ((14.514873223130207 50.139546700189122, 14.514935115997211 50.139537404246639, " +
                        "14.515046564904521 50.139870302402564, 14.514974097377392 50.139853227495614, 14.514873223130207 " +
                        "50.139546700189122))",
                    idTariff: "fb6ddc1e-0236-4e54-bc66-5bf031b02e94",
                    projectedCapacity: 60,
                    activeFrom: "2023-03-15T14:17:05.297",
                },
                {
                    idSection: "e2469c84-d93e-46f1-8c49-00cc5ce5258c",
                    sectionCode: "P8-2311",
                    sectionCat: 7,
                    borderWKT:
                        "POLYGON ((14.514873223130207 50.139546700189122, 14.514935115997211 50.139537404246639, " +
                        "14.515046564904521 50.139870302402564, 14.514974097377392 50.139853227495614, 14.514873223130207 " +
                        "50.139546700189122))",
                    idTariff: null,
                    projectedCapacity: 0,
                    activeFrom: "2023-03-15T14:17:05.297",
                },
            ] as ITskParkingSection[]),
        } as any as DataSource;

        const repository = { saveActiveParkingsWithoutAddress: sandbox.stub() } as any as ParkingsRepository;
        const config = { getValue: sandbox.stub() } as any as ISimpleConfig;

        sendMsgStub = sandbox.stub(QueueManager, "sendMessageToExchange");
        task = new SaveTskParkingSectionsTask("testQuery", dataSourceStub, repository, config);
        transformation = new TskParkingSectionTransformation(new Date());
        sandbox.stub(transformation, "transformArray").returns([
            { id: 1, name: "parking1" },
            { id: 2, name: "parking2" },
        ] as any as IParking[]);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("calls correct methods", async () => {
        await task.consume(null);
        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["parkingsRepository"].saveActiveParkingsWithoutAddress as SinonSpy);
        sandbox.assert.calledOnce(task["config"].getValue as SinonSpy);
    });
});

import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import fs from "fs";
import path from "path";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { SaveOsmEntrancesDataTask } from "#ie/workers/tasks/SaveOsmEntrancesDataTask";

describe("SaveOsmEntrancesDataTask.test", () => {
    let sandbox: SinonSandbox;
    let task: SaveOsmEntrancesDataTask;
    let getOsmEntrancesStub: SinonStub, bulkSaveStub: SinonStub;
    let getOsmEntrances: any;
    let testContainer: DependencyContainer;

    before(async () => {
        const postgresConnector = ParkingsContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
        testContainer = ParkingsContainer.createChildContainer();
        sandbox = sinon.createSandbox();
        getOsmEntrancesStub = sandbox.stub().callsFake(() => getOsmEntrances);

        bulkSaveStub = sandbox.stub().resolves();
        getOsmEntrances = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../data/osm-data-entrances.geojson"), "utf-8")
        ).features;

        testContainer.registerSingleton(
            ModuleContainerToken.OsmEntrancesDataSource,
            class DummyFactory {
                getOsmEntrances = getOsmEntrancesStub;
            }
        );
        testContainer.registerSingleton(
            ModuleContainerToken.ParkingEntrancesRepository,
            class DummyRepository {
                mergeEntrances = bulkSaveStub;
            }
        );
    });

    beforeEach(() => {
        task = testContainer.resolve(ModuleContainerToken.SaveOsmEntrancesDataTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
    });

    after(() => {
        sandbox.restore();
    });

    it("should refresh data", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(bulkSaveStub);
    });
});

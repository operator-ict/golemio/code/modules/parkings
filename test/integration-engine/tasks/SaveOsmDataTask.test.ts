import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { SaveOsmDataTask } from "#ie/workers/tasks/SaveOsmDataTask";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import fs from "fs";
import path from "path";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("NewParkingsWorker - SaveOsmDataTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveOsmDataTask;
    let getParkingsStub: SinonStub, getParkingSpacesStub: SinonStub, bulkSaveStub: SinonStub;
    let sourceDataParking: any;
    let sourceDataParkingLocations: any;
    let testContainer: DependencyContainer;

    before(async () => {
        testContainer = ParkingsContainer.createChildContainer();
        sandbox = sinon.createSandbox();
        getParkingsStub = sandbox.stub().callsFake(() => sourceDataParking);
        getParkingSpacesStub = sandbox.stub().callsFake(() => sourceDataParkingLocations);
        bulkSaveStub = sandbox.stub().resolves();
        sourceDataParking = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../data/osm-parkings-data-input.geojson"), "utf-8")
        ).features;
        sourceDataParkingLocations = JSON.parse(
            fs.readFileSync(path.join(__dirname, "../data/osm-parking-spaces-data-input.geojson"), "utf-8")
        ).features;

        testContainer.registerSingleton(
            ModuleContainerToken.OsmDataSource,
            class DummyFactory {
                getParkingsData = getParkingsStub;
                getParkingsLocationData = getParkingSpacesStub;
            }
        );
        testContainer.registerSingleton(
            ModuleContainerToken.ParkingsSecondaryRepository,
            class DummyRepository {
                saveActiveParkingsWithoutAddress = bulkSaveStub;
            }
        );
        testContainer.registerSingleton(
            ModuleContainerToken.ParkingsLocationRepository,
            class DummyRepository {
                bulkSave = bulkSaveStub;
            }
        );
    });

    beforeEach(() => {
        task = testContainer.resolve(ModuleContainerToken.SaveOsmDataTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
    });

    after(() => {
        sandbox.restore();
    });

    it("should refresh data", async () => {
        await task["execute"]();
        sandbox.assert.calledTwice(bulkSaveStub);
    });
});

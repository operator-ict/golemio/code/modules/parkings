import sinon, { SinonSandbox, SinonSpy, SinonStub } from "sinon";
import { DataSource, PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import { SaveTskParkingSectionLevelTask } from "#ie/workers/tasks/SaveTskParkingSectionLevelTask";
import { TskParkingSectionLevelTransformation } from "#ie/transformations/TskParkingSectionLevelTransformation";
import { ParkingsLocationRepository } from "#ie/repositories/ParkingsLocationRepository";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";

describe("SaveTskParkingSectionLevelTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveTskParkingSectionLevelTask;
    let sendMsgStub: SinonStub;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));
        sendMsgStub = sandbox.stub(QueueManager, "sendMessageToExchange");
        const dataSourceStub = {
            getAll: sandbox.stub().returns([]),
        } as any as DataSource;
        const transformation = { transformArray: sandbox.stub().returns([1, 2]) } as any as TskParkingSectionLevelTransformation;
        const repository = { saveWithoutAddress: sandbox.stub() } as any as ParkingsLocationRepository;
        const config = { getValue: sandbox.stub() } as any as ISimpleConfig;

        task = new SaveTskParkingSectionLevelTask(
            "sectionLevelQueueName",
            "testQuery",
            dataSourceStub,
            transformation,
            repository,
            config
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("calls correct methods", async () => {
        await task.consume(null);
        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["transformation"].transformArray as SinonSpy);
        sandbox.assert.calledOnce(task["parkingsLocationRepository"].saveWithoutAddress as SinonSpy);
        sandbox.assert.callCount(sendMsgStub, 1);
        sandbox.assert.calledWith(sendMsgStub.getCall(0), sinon.match(/parkings$/), "updateMissingParkingsLocationAddresses", {});
        sandbox.assert.calledOnce(task["config"].getValue as SinonSpy);
    });
});

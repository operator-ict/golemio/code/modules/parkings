import { ParkingProvider } from "#ie/datasources/helpers/ParkingProviderEnum";
import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { UpdateSmart4CityMeasurementsTask } from "#ie/workers/tasks/UpdateSmart4CityMeasurementsTask";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { smart4CityInputDataFixture } from "../data/Smart4CityInputDataFixture";
import { smart4CityMeasurementTransformed } from "../data/Smart4CityMeasurementTransformed";

describe("UpdateSmart4CityMeasurementsTask", () => {
    let sandbox: SinonSandbox;
    let task: UpdateSmart4CityMeasurementsTask;
    let getDatasourceStub: SinonStub;
    let getAllStub: SinonStub;

    before(async () => {
        sandbox = sinon.createSandbox({ useFakeTimers: { now: new Date("2024-01-04T06:15:00.000Z") } });
        getAllStub = sandbox.stub().resolves(smart4CityInputDataFixture);
        getDatasourceStub = sandbox.stub().returns({ getAll: getAllStub });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));

        ParkingsContainer.registerSingleton(
            ModuleContainerToken.ParkingProviderDataSourceFactory,
            class DummyFactory {
                getDataSource(...args: any[]) {
                    return getDatasourceStub(...args);
                }
            }
        );
    });

    beforeEach(() => {
        task = ParkingsContainer.resolve(ModuleContainerToken.UpdateSmart4CityMeasurementsTask);
    });

    afterEach(() => {
        ParkingsContainer.clearInstances();
    });

    after(async () => {
        sandbox.restore();
    });

    it("should fetch city data", async () => {
        const saveMeasurementStub = sandbox.stub(task["measurementsRepository"], "bulkSave");

        await task["execute"]({ code: "benesov" });

        sandbox.assert.calledOnce(getDatasourceStub);
        sandbox.assert.calledWithExactly(getDatasourceStub, ParkingProvider.Smart4CityLocation, "benesov");
        sandbox.assert.calledOnce(getAllStub);
        sandbox.assert.calledOnce(saveMeasurementStub);
        sandbox.assert.calledWith(saveMeasurementStub, smart4CityMeasurementTransformed);
    });
});

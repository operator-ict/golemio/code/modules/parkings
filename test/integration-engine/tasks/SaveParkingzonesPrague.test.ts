import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IParkingTariff } from "#sch/models/interfaces/IParkingTariff";
import { StaticParkingLotsGeoDataSourceFactory } from "#ie/datasources/StaticParkingLotsGeoDataSourceFactory";
import { IIPRParkingTransformed } from "#ie/transformations/IPRParkingTransformation";
import { SaveParkingZonesPrague } from "#ie/workers/tasks/SaveParkingZonesPrague";
import { Parkings } from "#sch";
import { IStaticParkingLotsGeo } from "#sch/datasources/interfaces/IStaticParkingLotsGeo";
import { DataSource, QueueManager } from "@golemio/core/dist/integration-engine";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy, SinonStub } from "sinon";
import iprDataTransformed from "./../data/ipr-parking-data-transformed";
import datasetsMerged from "./../data/parking-data-merged";
import tskDataTransformed from "./../data/tsk-parking-data-transformed";
import tskTariffDataTransformed from "./../data/tsk-parking-tariff-data-transformed";
import { IIPRParkingJsonFeature } from "#sch/datasources/interfaces/IIPRParkingJsonSchema";

chai.use(chaiAsPromised);

describe("SaveParkingZonesPrague Task", () => {
    let task: SaveParkingZonesPrague;
    let sandbox: SinonSandbox;
    let tskTestData: Record<string, any>;
    let tskTariffTestData: Record<string, any>;
    let iprTestData: IIPRParkingJsonFeature[];
    let staticGeoGetAllStub: SinonStub;
    let staticParkingLotsGeoData: IStaticParkingLotsGeo;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.useFakeTimers(new Date("2022-12-21T15:30:00.000Z"));
        staticParkingLotsGeoData = require("./../data/StaticParkingLotsGeoData.json");
        tskTestData = {
            data: require("./../data/tsk-parking-data-input.json").features,
        };
        iprTestData = require("./../data/ipr-parking-data-input.json").features;
        tskTariffTestData = {
            data: require("./../data/tsk-parking-tariff-data-input-some-invalid.json"),
            lastUpdated: "2020-10-12T15:19:21+02:00",
        };

        sandbox.stub(config, "datasources").value({
            TSKFTP: {
                host: "",
                port: 21,
                user: "",
                password: "",
            },
            TSKFTPParkingZonesPath: "/external/tsk/parking-zones",
            ParkingZones: "http://ipr-test.cz",
        });

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => ({})),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );

        task = new SaveParkingZonesPrague("test");

        staticGeoGetAllStub = sandbox.stub().callsFake(() => Promise.resolve<IStaticParkingLotsGeo>(staticParkingLotsGeoData));
        sandbox
            .stub(StaticParkingLotsGeoDataSourceFactory, "getDataSource")
            .callsFake(() => ({ getAll: staticGeoGetAllStub } as any as DataSource));

        sandbox.stub(QueueManager, "sendMessageToExchange" as any);

        sandbox.stub(task["parkingsModel"], "saveActiveParkingsWithoutAddress");
        sandbox.stub(task["parkingsLocationModel"], "saveWithoutAddress");
        sandbox.stub(task["parkingPaymentRepository"], "updatePaymentsBySource");
        sandbox.stub(task["parkingsTariffsModel"], "bulkSave");
        sandbox.stub(task["parkingsTariffsModel"], "deleteByTariffIds");
        sandbox.stub(task["parkingsBusinessErrorsRepository"], "bulkSave");

        sandbox.stub(task["dataSourceTSK"], "getAll").callsFake(() => Promise.resolve(tskTestData));
        sandbox.stub(task["dataSourceIPR"], "getAll").callsFake(() => Promise.resolve(iprTestData));
        sandbox.stub(task["dataSourceTariffTSK"], "getAll").callsFake(() => Promise.resolve(tskTariffTestData));
        sandbox.stub(task, <any>"transformTskData").callsFake(() => Promise.resolve(tskDataTransformed));
        sandbox
            .stub(task["tskTariffTransformation"], "transform")
            .callsFake(() => Promise.resolve(tskTariffDataTransformed as IParkingTariff[]));
        sandbox
            .stub(task["iprTransformation"], "transform")
            .callsFake(() => Promise.resolve(iprDataTransformed as IIPRParkingTransformed));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should validate parking zones input properly", async () => {
        const iprValidator = new JSONSchemaValidator(Parkings.ipr.name + "DataSource", Parkings.ipr.datasourceJsonSchema);
        expect(await iprValidator.Validate(iprTestData)).to.be.true;
        const tskValidator = new JSONSchemaValidator(Parkings.tsk.name + "DataSource", Parkings.tsk.datasourceGeoJsonSchema);
        expect(await tskValidator.Validate(tskTestData.data)).to.be.true;
    });

    it("should call the correct methods by saveParkingZonesPrague method", async () => {
        await task.consume(null);
        sandbox.assert.calledOnce(task["dataSourceTSK"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["dataSourceTariffTSK"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["transformTskData"] as SinonSpy);
        sandbox.assert.calledOnce(task["tskTariffTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(task["dataSourceIPR"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["iprTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(task["parkingsModel"].saveActiveParkingsWithoutAddress as SinonSpy);
        sandbox.assert.calledOnce(task["parkingsTariffsModel"].bulkSave as SinonSpy);
        sandbox.assert.calledWith(
            task["parkingsModel"].saveActiveParkingsWithoutAddress as SinonSpy,
            datasetsMerged.parking,
            "tsk"
        );
        sandbox.assert.calledWith(task["parkingsLocationModel"].saveWithoutAddress as SinonSpy, datasetsMerged.locations);
        sandbox.assert.calledWith(
            task["parkingPaymentRepository"].updatePaymentsBySource as SinonSpy,
            tskDataTransformed.payment,
            SourceEnum.TSK,
            new Date("2022-12-21T15:30:00.000Z")
        );
        sandbox.assert.calledWith(task["parkingsTariffsModel"].deleteByTariffIds as SinonSpy);
        sandbox.assert.calledWith(task["parkingsTariffsModel"].bulkSave as SinonSpy, tskTariffDataTransformed);
        sandbox.assert.calledWith(task["parkingsBusinessErrorsRepository"].bulkSave as SinonSpy);
        sandbox.assert.callOrder(
            task["dataSourceTSK"].getAll as SinonSpy,
            task["transformTskData"] as SinonSpy,
            task["dataSourceIPR"].getAll as SinonSpy,
            task["iprTransformation"].transform as SinonSpy,
            task["parkingsModel"].saveActiveParkingsWithoutAddress as SinonSpy,
            task["parkingsLocationModel"].saveWithoutAddress as SinonSpy,
            task["dataSourceTariffTSK"].getAll as SinonSpy,
            task["parkingsTariffsModel"].bulkSave as SinonSpy,
            task["parkingsBusinessErrorsRepository"].bulkSave as SinonSpy
        );
    });

    it("should return correctly separated valid and invalid tariffs", () => {
        const { validTariffs, invalidResults } = task["checkBusinessRules"](tskTariffTestData.data);
        expect(tskTariffTestData.data.length).to.be.equal(48);
        // 3 tariffs are removed because of invalid data, two of them share same CTARIFF
        expect(validTariffs.length).to.be.equal(45);
        expect(invalidResults.length).to.be.equal(2);
        expect(invalidResults[0].failedRule).to.be.equal("MaxChargeIterationsRule");
        expect(invalidResults[1].failedRule).to.be.equal("MaxPriceRule");
    });
});

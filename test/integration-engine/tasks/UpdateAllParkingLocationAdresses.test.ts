import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { UpdateAllParkingsLocationAdresses } from "#ie/workers/tasks/UpdateAllParkingsLocationAdresses";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import SimpleConfig from "@golemio/core/dist/helpers/configuration/SimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors/QueueManager";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("UpdateAllParkingLocationAdresses", () => {
    let sendMsgStub: SinonStub;
    let sinonSandbox: SinonSandbox;
    let testParkingsContainer: DependencyContainer;
    let findDataStub: SinonStub;
    let task: UpdateAllParkingsLocationAdresses;

    before(async () => {
        const postgresConnector = ParkingsContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
        testParkingsContainer = ParkingsContainer.createChildContainer();
        sinonSandbox = sinon.createSandbox({ useFakeTimers: true });
        sendMsgStub = sinonSandbox.stub(QueueManager, "sendMessageToExchange");
        findDataStub = sinonSandbox.stub().resolves([]);
        testParkingsContainer.register(
            ModuleContainerToken.ParkingsLocationRepository,
            class DummyFactory {
                findWithOutdatedAddress = findDataStub;
            }
        );
        testParkingsContainer.registerInstance<ISimpleConfig>(
            CoreToken.SimpleConfig,
            new SimpleConfig({
                env: {
                    RABBIT_EXCHANGE_NAME: "test",
                },
                module: {
                    parking: {
                        addresses: {
                            batchSize: 10,
                            updateIntervalDays: 180,
                        },
                    },
                },
            })
        );
        task = testParkingsContainer.resolve(ModuleContainerToken.UpdateAllParkingsLocationAdresses);
    });

    after(() => {
        sinonSandbox.restore();
        testParkingsContainer.clearInstances();
    });

    it("should update addresses successfully", async () => {
        await task.execute();
        sinonSandbox.assert.calledOnce(findDataStub);
    });
});

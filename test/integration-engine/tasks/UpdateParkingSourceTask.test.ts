import { UpdateParkingSourceTask } from "#ie/workers/tasks/UpdateParkingSourceTask";
import { ParkingSourcesRepository } from "#ie/repositories/ParkingSourcesRepository";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { ILogger } from "@golemio/core/dist/helpers";
import { IParkingSource } from "#sch/models/interfaces/IParkingSource";
import { DataSourceIntegrationChecker } from "#ie/businessRules/DataSourceIntegrationChecker";

describe("UpdateParkingSourceTask", () => {
    let sandbox: SinonSandbox;
    let task: UpdateParkingSourceTask;

    before(() => {
        sandbox = sinon.createSandbox();
        const loggerStub = { log: { error: sandbox.stub() } } as any as ILogger;
        const repositoryStub = { save: sandbox.stub() } as any as ParkingSourcesRepository;
        const checkerStup = { checkAllowedDataSources: sandbox.stub() } as any as DataSourceIntegrationChecker;
        task = new UpdateParkingSourceTask("queuePrefix", loggerStub, repositoryStub, checkerStup);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("calls correct methods", async () => {
        await task["execute"]({} as IParkingSource);
        sandbox.assert.calledOnce(task["integrationChecker"].checkAllowedDataSources as SinonSpy);
        sandbox.assert.calledOnce(task["repository"].save as SinonSpy);
    });
});

import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { AverageOccupancyRepository } from "#ie/repositories/AverageOccupancyRepository";
import { CalculateAverageOccupancyTask } from "#ie/workers/tasks/CalculateAverageOccupancyTask";
import { Parkings } from "#sch";
import { ParkingMeasurementsDtoSchema } from "#sch/datasources/ParkingMeasurementsDtoSchema";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { expect } from "chai";
import { PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

describe.skip("NewParkingsWorker - CalculateAverageOccupancyTask", () => {
    let task: CalculateAverageOccupancyTask;
    let averageOccupancyRepository: AverageOccupancyRepository;
    let measurementsRepository: PostgresModel;

    before(async () => {
        const postgresConnector = ParkingsContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
    });

    beforeEach(() => {
        task = ParkingsContainer.resolve(ModuleContainerToken.CalculateAverageOccupancyTask);
        averageOccupancyRepository = ParkingsContainer.resolve(ModuleContainerToken.AverageOccupancyRepository);
        measurementsRepository = new PostgresModel(
            Parkings.measurements.name + "Model",
            {
                outputSequelizeAttributes: Parkings.measurements.outputSequelizeAttributes,
                pgSchema: Parkings.pgSchema,
                pgTableName: Parkings.measurements.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Parkings.measurements.name + "PgModelValidator", ParkingMeasurementsDtoSchema)
        );
    });

    afterEach(() => {
        ParkingsContainer.clearInstances();
    });

    it("should populate parkings_average_occupancy table", async () => {
        const averageOccupancyBefore = await averageOccupancyRepository["sequelizeModel"].findAll();
        expect(averageOccupancyBefore).to.be.an("array").that.has.lengthOf(12);

        await measurementsRepository["sequelizeModel"].create({
            source: "tsk_v2",
            source_id: "d66bb668-eab6-49da-bc84-002263e1dce1",
            parking_id: "tsk2-P1-0000",
            available_spot_number: 20,
            closed_spot_number: null,
            occupied_spot_number: 30,
            total_spot_number: 50,
            date_modified: new Date().toISOString(),
            updated_at: new Date().toISOString(),
        });

        await task["execute"]();

        await measurementsRepository["sequelizeModel"].destroy({
            where: {
                parking_id: "tsk2-P1-0000",
            },
        });

        const averageOccupancy = await averageOccupancyRepository["sequelizeModel"].findAll();
        expect(averageOccupancy).to.be.an("array").that.has.lengthOf(12);
    });
});

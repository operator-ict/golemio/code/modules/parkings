import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { DataSource } from "@golemio/core/dist/integration-engine";
import { SaveTskParkingZonesTariffsTask } from "#ie/workers/tasks/SaveTskParkingZonesTariffsTask";
import { ParkingTariffsRepository } from "#ie/repositories/ParkingTariffsRepository";

describe("SaveTskParkingZonesTariffsTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveTskParkingZonesTariffsTask;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        const dataSourceStub = { getAll: sandbox.stub().resolves([]) } as any as DataSource;
        const repositoryStub = { merge: sandbox.stub() } as any as ParkingTariffsRepository;

        task = new SaveTskParkingZonesTariffsTask("queuePrefix", dataSourceStub, repositoryStub);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("calls correct methods", async () => {
        await task.consume(null);
        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["repository"].merge as SinonSpy);
    });
});

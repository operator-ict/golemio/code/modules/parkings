import { ParkingProvider } from "#ie/datasources/helpers/ParkingProviderEnum";
import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { UpdateSmart4CityLocationsTask } from "#ie/workers/tasks/UpdateSmart4CityLocationsTask";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import { smart4CityInputDataFixture } from "../data/Smart4CityInputDataFixture";
import { smart4CityLocationTransformed } from "../data/Smart4CityLocationTransformed";

describe("UpdateSmart4CityLocationsTask", () => {
    let sandbox: SinonSandbox;
    let task: UpdateSmart4CityLocationsTask;
    let getDatasourceStub: SinonStub;
    let getAllStub: SinonStub;
    let sendMsgStub: SinonStub;

    before(async () => {
        sandbox = sinon.createSandbox({ useFakeTimers: { now: new Date("2024-01-04T06:15:00.000Z") } });
        getAllStub = sandbox.stub().resolves(smart4CityInputDataFixture);
        getDatasourceStub = sandbox.stub().returns({ getAll: getAllStub });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));
        sendMsgStub = sandbox.stub(QueueManager, "sendMessageToExchange");

        ParkingsContainer.registerSingleton(
            ModuleContainerToken.ParkingProviderDataSourceFactory,
            class DummyFactory {
                getDataSource(...args: any[]) {
                    return getDatasourceStub(...args);
                }
            }
        );
    });

    beforeEach(() => {
        task = ParkingsContainer.resolve(ModuleContainerToken.UpdateSmart4CityLocationsTask);
    });

    afterEach(() => {
        ParkingsContainer.clearInstances();
    });

    after(async () => {
        sandbox.restore();
    });

    it("should fetch city data", async () => {
        const saveParkingStub = sandbox.stub(task["parkingRepository"], "saveActiveParkingsWithoutAddress");
        const saveParkingLocationStub = sandbox.stub(task["parkingLocationRepository"], "bulkSave");

        await task["execute"]({ code: "benesov" });

        sandbox.assert.calledOnce(getDatasourceStub);
        sandbox.assert.calledWithExactly(getDatasourceStub, ParkingProvider.Smart4CityLocation, "benesov");
        sandbox.assert.calledOnce(getAllStub);
        sandbox.assert.calledOnce(saveParkingStub);
        sandbox.assert.calledWith(saveParkingStub, smart4CityLocationTransformed.parking, "smart4city");
        sandbox.assert.calledOnce(saveParkingLocationStub);
        sandbox.assert.calledWith(saveParkingLocationStub, smart4CityLocationTransformed.parkingLocation);

        sandbox.assert.callCount(sendMsgStub, 2);
        sandbox.assert.calledWith(sendMsgStub.getCall(0), sinon.match(/parkings$/), "updateMissingParkingsAddresses", {});
        sandbox.assert.calledWith(sendMsgStub.getCall(1), sinon.match(/parkings$/), "updateMissingParkingsLocationAddresses", {});
    });
});

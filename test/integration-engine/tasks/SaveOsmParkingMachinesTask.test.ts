import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { ParkingMachinesRepository } from "#ie/repositories/ParkingMachinesRepository";
import { SaveOsmParkingMachinesTask } from "#ie/workers/tasks/SaveOsmParkingMachinesTask";
import { OsmParkingMachinesDataSource } from "#ie/datasources/osm/OsmParkingMachinesDataSource";

describe("SaveOsmParkingMachinesTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveOsmParkingMachinesTask;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));

        const parkingMachine = {
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: [14.5075317, 50.0696385],
            },
            properties: {
                parking_id: "r_14864801",
                osm_id: "n_7653409366",
                name: null,
            },
        };

        const dataSourceStub = {
            getOsmParkingMachines: sandbox.stub().returns([parkingMachine]),
        } as any as OsmParkingMachinesDataSource;
        const repository = { bulkSave: sandbox.stub() } as any as ParkingMachinesRepository;

        task = new SaveOsmParkingMachinesTask(dataSourceStub, repository);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("calls correct methods", async () => {
        await task.consume(null);
        sandbox.assert.calledOnce(task["dataSource"].getOsmParkingMachines as SinonSpy);
        sandbox.assert.calledOnce(task["repository"].bulkSave as SinonSpy);
    });
});

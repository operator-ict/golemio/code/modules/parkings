import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { IUpdateAddressAndNameInput } from "#ie/workers/schemas/UpdateAddressAndNameSchema";
import { UpdateAddressAndNameTask } from "#ie/workers/tasks/UpdateAddressAndNameTask";
import { IPostalAddress } from "@golemio/core/dist/integration-engine";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("UpdateAddressAndNameTask", () => {
    let updateAddressTask: UpdateAddressAndNameTask;
    let getExtendedAddressFromPhoton: SinonStub;
    let repositoryUpdate: SinonStub;
    let keyExists: SinonStub;
    let sandbox: SinonSandbox;
    let testContainer: DependencyContainer;
    let address: IPostalAddress;

    before(async () => {
        testContainer = ParkingsContainer.createChildContainer();
        address = {
            street_address: "Nikola Tesla 1",
            address_region: "Brno",
            address_formatted: "Nikola Tesla 1, Brno",
            address_country: "Brno Republic",
        };

        getExtendedAddressFromPhoton = sinon.stub().resolves(address);
        repositoryUpdate = sinon.stub().resolves();

        keyExists = sinon.stub().resolves(false);

        IntegrationEngineContainer.registerSingleton(
            ContainerToken.GeocodeApi,
            class DummyFactory {
                getExtendedAddressFromPhoton = getExtendedAddressFromPhoton;
            }
        );

        testContainer.registerSingleton(
            ModuleContainerToken.ParkingsRepository,
            class DummyFactory {
                update = repositoryUpdate;
            }
        );
    });
    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
    });

    afterEach(() => {
        sandbox.restore();
    });

    after(async () => {
        testContainer.clearInstances();
    });

    it("call photon api and save data to database", async () => {
        updateAddressTask = testContainer.resolve<UpdateAddressAndNameTask>(ModuleContainerToken.UpdateAddressAndNameTask);

        const data: IUpdateAddressAndNameInput = {
            id: "123",
            name: "Test",
            point: { type: "Point", coordinates: [1, 2] },
        };

        // called once
        await updateAddressTask.execute(data);
        sandbox.assert.calledOnce(getExtendedAddressFromPhoton);
        sandbox.assert.calledOnce(repositoryUpdate);
    });
});

import { SourceEnum } from "#helpers/constants/SourceEnum";
import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { GenerateIptOictParkingJobsTask } from "#ie/workers/tasks/GenerateIptOictParkingJobsTask";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { QueueManager } from "@golemio/core/dist/integration-engine";

describe("GenerateIptOictPaymentJobsTask", () => {
    let sandbox: SinonSandbox;
    let connector: IDatabaseConnector;
    let task: GenerateIptOictParkingJobsTask;
    let sendMsgStub: SinonStub;
    let testContainer: DependencyContainer;

    before(async () => {
        testContainer = ParkingsContainer.createChildContainer();
        connector = testContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sendMsgStub = sandbox.stub(QueueManager, "sendMessageToExchange");
        task = ParkingsContainer.resolve(ModuleContainerToken.GenerateIptOictPaymentJobsTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
        sandbox.restore();
    });

    it("should fetch parking payments datasource list and generate tasks", async () => {
        await task["execute"]();

        sandbox.assert.callCount(sendMsgStub, 1);
        sandbox.assert.calledWith(sendMsgStub.getCall(0), sinon.match(/parkings$/), "saveIptOictPaymentsData", {
            source: SourceEnum.OSM,
        });
    });
});

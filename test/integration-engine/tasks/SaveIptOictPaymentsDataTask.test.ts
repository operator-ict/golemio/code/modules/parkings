import { SourceEnum } from "#helpers/constants/SourceEnum";
import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { SaveIptOictPaymentsDataTask } from "#ie/workers/tasks/SaveIptOictPaymentsDataTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import fs from "fs";
import path from "path";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("SaveIptOictPaymentsDataTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveIptOictPaymentsDataTask;
    let getPaymentsStub: SinonStub, updateStub: SinonStub;
    let sourceDataPayments: any;
    let testContainer: DependencyContainer;

    before(async () => {
        const postgresConnector = ParkingsContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
        sandbox = sinon.createSandbox();
        sourceDataPayments = JSON.parse(fs.readFileSync(path.join(__dirname, "../data/osm-payments-data-input.json"), "utf-8"));
        getPaymentsStub = sandbox.stub().callsFake(() => sourceDataPayments);
        updateStub = sandbox.stub().resolves();

        testContainer = ParkingsContainer.createChildContainer();
        testContainer
            .registerSingleton(
                ModuleContainerToken.IptOictPaymentsDataSource,
                class DummyFactory {
                    getPaymentsData = getPaymentsStub;
                }
            )
            .registerSingleton(
                ModuleContainerToken.ParkingPaymentsRepository,
                class DummyRepository {
                    updatePaymentsBySource = updateStub;
                }
            );
    });

    beforeEach(() => {
        task = testContainer.resolve(ModuleContainerToken.SaveIptOictPaymentsDataTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
    });

    after(() => {
        sandbox.restore();
    });

    it("should refresh data", async () => {
        await task["execute"]({ source: SourceEnum.OSM });
        sandbox.assert.calledOnce(updateStub);
    });
});

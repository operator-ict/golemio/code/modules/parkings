import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { UpdateMissingParkingsAddressesTask } from "#ie/workers/tasks/UpdateMissingParkingsAddressesTask";
import { QueueManager } from "@golemio/core/dist/integration-engine";

describe("UpdateMissingParkingsAddressesTask", () => {
    let task: UpdateMissingParkingsAddressesTask;
    let findWithoutAddress: SinonStub;
    let sandbox: SinonSandbox;
    let testContainer: DependencyContainer;
    let sendMsgStub: SinonStub;

    const parking = [
        {
            id: "ipr-1",
            source: "ipr",
            centroid: { type: "Point", coordinates: [1, 2] },
            name: "ZTP 1",
        },
        {
            id: "tsk-2",
            source: "tsk",
            centroid: { type: "Point", coordinates: [3, 4] },
            name: "TSK 2",
        },
    ];

    before(async () => {
        testContainer = ParkingsContainer.createChildContainer();
        findWithoutAddress = sinon.stub().resolves(parking);
        testContainer.registerSingleton(
            ModuleContainerToken.ParkingsRepository,
            class DummyFactory {
                findWithoutAddress = findWithoutAddress;
            }
        );
    });
    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        sendMsgStub = sandbox.stub(QueueManager, "sendMessageToExchange");
    });

    afterEach(() => {
        sandbox.restore();
    });

    after(async () => {
        testContainer.clearInstances();
    });

    it("send update address messages correctly", async () => {
        task = testContainer.resolve<UpdateMissingParkingsAddressesTask>(ModuleContainerToken.UpdateMissingParkingAdressesTask);

        // called once
        await task.execute();
        sandbox.assert.calledOnce(findWithoutAddress);
        sandbox.assert.calledTwice(sendMsgStub);
        sandbox.assert.calledWith(sendMsgStub.getCall(0), sinon.match(/parkings$/), "updateAddressAndName", {
            id: parking[0].id,
            point: parking[0].centroid,
            name: parking[0].name,
        });
        sandbox.assert.calledWith(sendMsgStub.getCall(1), sinon.match(/parkings$/), "updateAddress", {
            id: parking[1].id,
            point: parking[1].centroid,
        });
    });
});

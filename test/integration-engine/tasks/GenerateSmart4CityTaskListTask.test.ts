import { ParkingProvider } from "#ie/datasources/helpers/ParkingProviderEnum";
import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { GenerateSmart4CityTaskListTask, Smart4CityTaskType } from "#ie/workers/tasks/GenerateSmart4CityTaskListTask";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { smart4CityListInputDataFixture } from "../data/Smart4CityListInputDataFixture";

describe("GenerateSmart4CityTaskListTask", () => {
    let sandbox: SinonSandbox;
    let task: GenerateSmart4CityTaskListTask;
    let getAllStub: SinonStub;
    let getDatasourceStub: SinonStub;
    let sendMsgStub: SinonStub;

    before(async () => {
        ParkingsContainer.registerSingleton(
            ModuleContainerToken.ParkingProviderDataSourceFactory,
            class DummyFactory {
                getDataSource(...args: any[]) {
                    return getDatasourceStub(...args);
                }
            }
        );
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        getAllStub = sandbox.stub().resolves(smart4CityListInputDataFixture);
        getDatasourceStub = sandbox.stub().returns({ getAll: getAllStub });
        sendMsgStub = sandbox.stub(QueueManager, "sendMessageToExchange");
        task = ParkingsContainer.resolve(ModuleContainerToken.GenerateSmart4CityTaskListTask);
    });

    afterEach(() => {
        ParkingsContainer.clearInstances();
        sandbox.restore();
    });

    it("should fetch city list and generate location tasks", async () => {
        await task["execute"]({ type: Smart4CityTaskType.locations });

        sandbox.assert.calledOnce(getDatasourceStub);
        sandbox.assert.calledWithExactly(getDatasourceStub, ParkingProvider.Smart4CityList);
        sandbox.assert.calledOnce(getAllStub);
        sandbox.assert.callCount(sendMsgStub, 12);
        for (let i = 0; i < 12; i++) {
            sandbox.assert.calledWith(sendMsgStub.getCall(i), sinon.match(/parkings$/), "updateSmart4CityLocations", {
                code: sinon.match.string,
            });
        }
    });

    it("should fetch city list and generate measurement tasks", async () => {
        await task["execute"]({ type: Smart4CityTaskType.measurements });

        sandbox.assert.calledOnce(getDatasourceStub);
        sandbox.assert.calledWithExactly(getDatasourceStub, ParkingProvider.Smart4CityList);
        sandbox.assert.calledOnce(getAllStub);
        sandbox.assert.callCount(sendMsgStub, 12);
        for (let i = 0; i < 12; i++) {
            sandbox.assert.calledWith(sendMsgStub.getCall(i), sinon.match(/parkings$/), "updateSmart4CityMeasurements", {
                code: sinon.match.string,
            });
        }
    });
});

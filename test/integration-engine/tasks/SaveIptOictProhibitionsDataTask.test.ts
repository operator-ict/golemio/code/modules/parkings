import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { SaveIptOictProhibitionsDataTask } from "#ie/workers/tasks/SaveIptOictProhibitionsDataTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import fs from "fs";
import path from "path";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("SaveIptOictProhibitionsDataTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveIptOictProhibitionsDataTask;
    let getSourceMethodStub: SinonStub, bulkSaveStub: SinonStub, getDataSourceConfigStub: SinonStub;
    let testContainer: DependencyContainer;

    before(async () => {
        const postgresConnector = ParkingsContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
        sandbox = sinon.createSandbox();
        getSourceMethodStub = sandbox
            .stub()
            .callsFake(() =>
                JSON.parse(fs.readFileSync(path.join(__dirname, "../data/osm-parking-prohibitions-data-input.json"), "utf-8"))
            );
        getDataSourceConfigStub = sandbox
            .stub()
            .resolves([{ source: "isphk", datasource_prohibitions: "https://isphk.storage.url" }]);
        bulkSaveStub = sandbox.stub().resolves();
        testContainer = ParkingsContainer.createChildContainer();
        testContainer
            .registerSingleton(
                ModuleContainerToken.IptOictParkingsProhibitionsDataSource,
                class DummyFactory {
                    getParkingsProhibitionsData = getSourceMethodStub;
                }
            )
            .registerSingleton(
                ModuleContainerToken.CachedParkingSourcesRepository,
                class DummyRepository {
                    getParkingProhibitionsDataSourceConfig = getDataSourceConfigStub;
                }
            )
            .registerSingleton(
                ModuleContainerToken.ParkingsProhibitionsRepository,
                class DummyRepository {
                    bulkSave = bulkSaveStub;
                }
            );
    });

    beforeEach(() => {
        task = testContainer.resolve(ModuleContainerToken.SaveIptOictProhibitionsDataTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
    });

    after(() => {
        sandbox.restore();
    });

    it("should refresh data", async () => {
        await task["execute"]({ source: "isphk" });
        sandbox.assert.calledOnce(getSourceMethodStub);
        sandbox.assert.calledOnce(getDataSourceConfigStub);
        sandbox.assert.calledOnce(bulkSaveStub);
    });
});

import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { RefreshDataInDbTask } from "#ie/workers/tasks/RefreshDataInDbTask";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import sinon from "sinon";
import { SinonSandbox, SinonStub } from "sinon";
import fs from "fs";
import path from "path";

describe("RefreshDataInDbTask", () => {
    let testContainer: DependencyContainer;
    let connector: IDatabaseConnector;
    let testData: any[];
    let testDataTransformed: any[];

    let sandbox: SinonSandbox;
    let task: RefreshDataInDbTask;

    let bulkSaveStub: SinonStub;
    let getParkomatsStub: SinonStub;
    let transformStub: SinonStub;

    before(async () => {
        testContainer = ParkingsContainer.createChildContainer();
        connector = testContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
        testData = JSON.parse(fs.readFileSync(path.join(__dirname + "/../data/parkomats-datasource.json"), "utf8"));
        testDataTransformed = JSON.parse(fs.readFileSync(path.join(__dirname + "/../data/parkomats-transformed.json"), "utf8"));
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        task = ParkingsContainer.resolve(ModuleContainerToken.RefreshDataInDbTask);

        transformStub = sandbox.stub(task["transformation"], "transform").resolves(testDataTransformed);
        getParkomatsStub = sandbox.stub(task["dataSource"], "getParkomats").resolves(testData);
        bulkSaveStub = sandbox.stub(task["model"], "bulkSave");
    });

    afterEach(() => {
        testContainer.clearInstances();
        sandbox.restore();
    });

    it("Should call correct methods in correct order", async () => {
        await task["execute"]({});

        sandbox.assert.calledOnce(bulkSaveStub);
        sandbox.assert.calledWith(bulkSaveStub, testDataTransformed);
        sandbox.assert.calledOnce(transformStub);
        sandbox.assert.calledWith(transformStub, testData);
        sandbox.assert.calledOnce(getParkomatsStub);
        sandbox.assert.callOrder(getParkomatsStub, bulkSaveStub);
    });
});

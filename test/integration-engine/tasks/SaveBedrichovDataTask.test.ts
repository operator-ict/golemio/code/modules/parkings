import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { SaveBedrichovDataTask } from "#ie/workers/tasks/SaveBedrichovDataTask";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import SimpleConfig from "@golemio/core/dist/helpers/configuration/SimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { bedrichovInputDataTransformed } from "../data/bedrichov-data-input-transformed";
import { bedrichovInputData } from "../data/bedrichov-input-data";

describe("SaveBedrichovDataTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveBedrichovDataTask;
    let getAll: SinonStub, transformArray: SinonStub, bulkSaveStub: SinonStub;
    let testContainer: DependencyContainer;

    before(async () => {
        const postgresConnector = ParkingsContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
        testContainer = ParkingsContainer.createChildContainer();
        sandbox = sinon.createSandbox();
        getAll = sandbox.stub().callsFake(() => bedrichovInputData);

        transformArray = sandbox.stub().callsFake(() => bedrichovInputDataTransformed);
        bulkSaveStub = sandbox.stub().resolves();

        ParkingsContainer.registerInstance<ISimpleConfig>(
            CoreToken.SimpleConfig,
            new SimpleConfig({
                module: {
                    parking: {
                        bedrichov: {
                            datasource: {
                                url: "",
                                token: "",
                            },
                        },
                    },
                },
            })
        );

        testContainer.registerSingleton(
            ModuleContainerToken.ParkingProviderDataSourceFactory,
            class DummyFactory {
                getDataSource = () => ({ getAll: getAll });
            }
        );
        testContainer.registerSingleton(
            ModuleContainerToken.BedrichovMeasurementsTransformation,
            class DummyRepository {
                transformArray = transformArray;
            }
        );
        testContainer.registerSingleton(
            ModuleContainerToken.ParkingsMeasurementRepository,
            class DummyRepository {
                bulkSave = bulkSaveStub;
            }
        );
        testContainer.registerSingleton(
            ModuleContainerToken.ParkingsMeasurementsActualRepository,
            class DummyRepository {
                bulkSave = bulkSaveStub;
            }
        );
    });

    beforeEach(() => {
        task = testContainer.resolve(ModuleContainerToken.SaveBedrichovDataTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
    });

    after(() => {
        sandbox.restore();
    });

    it("should refresh data", async () => {
        await task["execute"]();
        sandbox.assert.calledTwice(bulkSaveStub);
    });
});

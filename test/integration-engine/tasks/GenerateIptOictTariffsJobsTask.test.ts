import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { GenerateIptOictTariffsJobsTask } from "#ie/workers/tasks/GenerateIptOictTariffsJobsTask";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { QueueManager } from "@golemio/core/dist/integration-engine";

describe("GenerateIptOictTariffsJobsTask", () => {
    let sandbox: SinonSandbox;
    let connector: IDatabaseConnector;
    let task: GenerateIptOictTariffsJobsTask;
    let sendMsgStub: SinonStub;
    let testContainer: DependencyContainer;

    before(async () => {
        testContainer = ParkingsContainer.createChildContainer();
        connector = testContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connector.connect();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sendMsgStub = sandbox.stub(QueueManager, "sendMessageToExchange");
        task = ParkingsContainer.resolve(ModuleContainerToken.GenerateIptOictTariffsJobsTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
        sandbox.restore();
    });

    it("should fetch tariff datasource list and generate tasks", async () => {
        await task["execute"]();

        sandbox.assert.callCount(sendMsgStub, 2);
        sandbox.assert.calledWith(sendMsgStub, sinon.match(/parkings$/), "saveIptOictTariffsData", {
            source: "isphk",
        });
        sandbox.assert.calledWith(sendMsgStub, sinon.match(/parkings$/), "saveIptOictTariffsData", {
            source: "test_cases",
        });
    });
});

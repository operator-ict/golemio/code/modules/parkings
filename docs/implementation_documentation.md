# Implementační dokumentace modulu **parkings**

## Záměr

Modul slouží k ukládání a poskytování informací o parkování.

**Dostupní poskytovatelé**:

-   Korid (Liberec)
-   TSK (Praha)
-   IPR (Praha)
-   IPT/OICT
-   TSK (Praha) API v2
-   Smart4City
-   Bedrichov

## Vstupní data

### Data nám jsou posílána

-   Přes Permission Proxy na Input Gateway, dodavatel Korid

    -   POST (/koridparkings/config) => RabbitMQ send Message `saveKoridConfToDB` ([Data example](../test/input-gateway/korid-parkings/data/korid-config.json))
    -   POST (/koridparkings/data) => RabbitMQ send Message `saveKoridDataToDB` ([Data example](../test/input-gateway/korid-parkings/data/korid-data.json))

-   na FTP, dodavatel TSK (parkovací zóny)

-   Data poskytovatelů přes /source
    -   slouží pro administraci a update statických hodnot k poskytovatelům parkovišť
    -   POST (/source) => RabbitMq send Message `saveParkingSourceToDB` ([Data example](../test/input-gateway/data/parkingSourceInput.json))

-   Přes Permission Proxy na Input Gateway, dodavatel ISPHK Hradec
    -   POST (/isphkparkings/measurements) => RabbitMQ send Message `saveIsphkMeasurements` ([Data example](../test/input-gateway/data/parkingIsphkInput.json))

### Data aktivně stahujeme

Data stahujeme pravidelně od IPR, TSK, z nového API TSK (značené jako v2) a Smart4City. Dále z blob storage IPT.

Pro ZTP parkoviště se z API se stahuje JSON z https://services5.arcgis.com/SBTXIEUGWbqzUecw/arcgis/rest/services/DOP_CUR_DOP_TSK_STANI_ZTP_B/FeatureServer/0/query?outFields=*&where=1%3D1&f=geojson
Pro parkovací zony se stahuje z https://services5.arcgis.com/SBTXIEUGWbqzUecw/arcgis/rest/services/DOP_CUR_DOP_ZPS_USEKY_P/FeatureServer/0/query?outFields=*&where=1%3D1&f=geojson

Pro statické (ručně zadané) tarify se stahuje JSON ze zdroje IPT.

Pro statické (ručně vytvořené) polygony placených parkovišť se stahuje GeoJSON ze zdroje IPT.

**Data o vjezdech** do parkovišť se stahují ze zdroje poskytovatele, pokud má uvedenou url vjezdů v tabulce poskytovatelů.

**Data o parkovacích automatech** OSM se stahují z exportovaného zdroje IPT.

**Data o platbách** se stahují z ručně vytvořeného zdroje IPT, nejsou-li součástí púvodního.

**Data o vjezdových omezeních** se stahují ze zdroje IPT, nejsou-li součástí púvodního (Mr. Parkit).

#### Zóny placeného stání

Zóny placeného stání používají 2 datové zdroje - TSK a Opendata IPR, které mají přesnější geometrii.
Další datový zdroj zón je API TSK (CIS VHD CD API) značené jako v2

-   zdroj dat (**TSK**):

    config FTPProtocolStrategy:

    ```
    filename: "Area_YYYY-MM-DD.json", // "Tarif_YYYY-MM-DD.tsv" pro tarify
     path: config.datasources.TSKFTPParkingZonesPath,
     url: {
         host: config.datasources.TSKFTP.host,
         port: config.datasources.TSKFTP.port,
         user: config.datasources.TSKFTP.user,
         password: config.datasources.TSKFTP.password,
     },
     encoding: "utf8",
    ```

-   formát dat (**TSK**)

    -   protokol: ftp
    -   datový typ:
        -   lokace: json
        -   tarify: tsv
    -   validační schéma:
        -   lokace: json
        -   tarify: tsv
    -   validační schéma:
        -   lokace [datasourceGeoJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/schema-definitions/index.ts)
        -   tarify [datasourceTariffJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/schema-definitions/index.ts)
    -   příklad vstupních dat:
        -   [lokace](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/test/integration-engine/data/ipr-parking-data-input.json)
        -   [tarify (tsv->json)](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/test/integration-engine/data/tsk-parking-tariff-data-input.json)

-   zdroj dat (**IPR**):

    config HTTPFetchProtocolStrategy:

    ```
    headers: {},
    method: "GET",
    url: config.datasources.ParkingZones,
    ```

-   formát dat (**IPR**)

    -   protokol: http
    -   datový typ: json
    -   validační schéma: [datasourceIPRJSONSchema](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/schema-definitions/index.ts)
    -   příklad vstupních dat

-   frekvence stahování

    -   cron definice:
        -   cron.dataplatform.parkings.saveParkingZonesPrague (`0 0 4 * * *`)

-   název rabbitmq fronty

    -   dataplatform.parkings.saveParkingZonesPrague

-   zdroj dat (**Bedrichov**):

    config HTTPFetchProtocolStrategy:

    ```
    headers: {
        Authorization: `Token token`,
    },
    method: "POST",
    url: datasources.parking.bedrichov.datasource.url,
    token: datasources.parking.bedrichov.datasource.token
    ```

-   formát dat (**Bedrichov**)

    -   protokol: http
    -   datový typ: json
    -   validační schéma: [BedrichovParkingSchema](../src/schema-definitions/datasources/BedrichovParkingSchema.ts)
    -   příklad vstupních dat: [BedrichovInputData](../test/integration-engine/data/bedrichov-input-data.ts)

-   frekvence stahování

    -   cron definice:
        -   cron.dataplatform.parkings.saveBedrichovData (`0 * * * * *`)

-   název rabbitmq fronty

    -   dataplatform.parkings.saveBedrichovData

-   zdroj dat (**TSK v2**):

    config HTTPFetchProtocolStrategy:

    ```
      headers: {
          Authorization: `Basic jmeno:heslo`,
      },
      method: "GET",
      url: this.config.datasources.parking.TskApiBaseUrl + this.config.datasources.parking.TskApiEndpoints[endpoint],
    ```

-   formát dat (**TSK v2**)

    -   zdroj používá 4 datové zdroje: sections, sectionList level 251 (zóny plac. stání) a level 253 (ZTP), tariffs
    -   protokol: http
    -   datový typ: json
    -   validační schéma sections: https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/schema-definitions/datasources/TskParkingSectionSchema.ts
    -   validační schéma sections level (251 i 253): https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/schema-definitions/datasources/TskParkingSectionLevelSchema.ts
    -   validační schéma tarify: https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/schema-definitions/datasources/TskParkingZoneTariffSchema.ts

    -   transformace a ukládání:
        -   sections => parkings
        -   section levels => parkings_location
        -   tariffs => parkings_tariffs

-   frekvence stahování

    -   cron definice:
        -   cron.dataplatform.parkings.saveParkingZonesPrague (`0 0 4 * * *`)

#### Parkovací automaty TSK

-   zdroj dat (**TSK v2**) je stejný, jako pro zóny, liší se jen konec URL: "ParkMachine/list"

-   formát dat (**TSK v2**)

    -   protokol: http
    -   datový typ: json
    -   validační schéma: https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/schema-definitions/datasources/TskParkingMachinesSchema.ts

-   frekvence stahování

    -   cron definice:
        -   cron.dataplatform.parkings.saveParkingZonesPrague (`0 0 4 * * *`)

-   cílová tabulka: `parking_machines`

#### Placené parkoviště

-   zdroj dat (**TSK**):

    config HTTPFetchProtocolStrategy:

    ```
    headers: {},
     method: "GET",
     url: config.datasources.TSKParkings,
    ```

    -   filter `lastUpdated` ne starší než 48h

-   formát dat

    -   protokol: http
    -   datový typ: json
    -   [validační schéma: InputParkingLotsSchema](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/schema-definitions/datasources/InputParkingLotsSchema.ts)
    -   [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/test/integration-engine/data/parking-lots-data-input.json)

-   frekvence stahování

    -   cron definice:
        -   cron.dataplatform.parkings.saveParkingLotsPrague (`0 0 */6 * * *`)
        -   cron.dataplatform.parkings.saveParkingLotsMeasurementsPrague (`0 */5 * * * *`)

-   název rabbitmq fronty
    -   dataplatform.parkings.saveParkingLotsPrague
    -   dataplatform.parkings.saveParkingLotsMeasurementsPrague

Parkoviště, která nemají tarif (je null), mohou dostat tarif ze statického zdroje IPT.

Vybraným parkovištím může být nahrazena location za Multipolygon ze zdroje IPT (výměna bodu za multipolygon). Původní bod je uložen do centroidu.

Tento zdroj je zastaralý a nespolehlivý. Může obsahovat stará a neplatná data, zejména o obsazenosti.

#### _MR.PARKIT Garages_

-   zdroj dat
    -   url: config.datasources.parkings.mrParkit.datasource.url
    -   parametry dotazu:
        -   apiVersion: config.datasources.parkings.mrParkit.datasource.apiVersion
        -   apiKey: config.datasources.parkings.mrParkit.datasource.apiKey
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [mrParkitJsonSchema](../src/schema-definitions/datasources/MrParkitJsonSchema.ts)
    -   příklad vstupnich dat: [InputDataFixture](../test/integration-engine/data/MrParkitInputDataFixture.ts)
    -   tarify z datového zdroje jsou namapované na tabulku parkings_tariffs, data ["mrParkitTransformation"](../test/integration-engine/data/MrParkitGarageTransformation.ts)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.parkings.saveMrParkitData
            -   rabin `0 */15 * * * *`
            -   prod `0 */15 * * * *`
-   názvy rabbitmq front
    -   dataplatform.parkings.saveMrParkitData

#### _Smart4City_

-   zdroj dat (seznam měst)
    -   url: config.datasources.parkings.Smart4City.ApiBaseUrl
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [smart4CityListJsonSchema](../src/schema-definitions/datasources/Smart4CityListJsonSchema.ts)
    -   příklad vstupnich dat: [smart4CityListInputDataFixture](../test/integration-engine/data/Smart4CityListInputDataFixture.ts)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.parkings.generateSmart4CityTaskList
            -   rabin: locations `0 0 5 * * *` / measurements `0 */1 * * * *`
            -   prod: locations `0 0 5 * * *` / measurements `0 */1 * * * *`
-   názvy rabbitmq front
    -   dataplatform.parkings.generateSmart4CityTaskList

-   zdroj dat (data měst)
    -   url: config.datasources.parkings.Smart4City.ApiBaseUrl + /`{city code}` + "/parkinglocations"
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [smart4CityLocationJsonSchema](../src/schema-definitions/datasources/Smart4CityLocationJsonSchema.ts)
    -   příklad vstupnich dat: [smart4CityInputDataFixture](../test/integration-engine/data/Smart4CityInputDataFixture.ts)
-   frekvence stahování
    -   dle `cron.dataplatform.parkings.generateSmart4CityTaskList`
-   názvy rabbitmq front
    -   dataplatform.parkings.updateSmart4CityLocations
    -   dataplatform.parkings.updateSmart4CityMeasurements

#### _parkomats_
-  zdroj dat
   -  url: `https://apicore.zpspraha.cz/CISICTService/CISICTService.svc/parkingsessionshistory`
   -  parametry
      -  `from` - nastavujeme na predchozi den 00
      -  `to` - nastavujeme na dnesni den v 04
   - poznamka stahujeme data za 28 hodin abychom o zadna data neprisli
   -  protokol: http
   -  datovy typ: json
   -  validacni schema: [parkomatInputSchema](../src/schema-definitions/parkomats/datasources/ParkomatInputSchema.ts)
-  frekvence stahovani:
   -  cron definice
      -  rabin: `0 04,16 * * *`
      -  prod: `0 07,19 * * *`
-  nazev rabbitmq fronty
   -  dataplatform.parkomats.refreshDataInDB


## Zpracování dat / transformace

Při transformaci dat lokací data obohacujeme o adresu parkoviště, která vychází z centroidu geometrie daného parkoviště a získává se z Photon API metodou `GeocodeApi.getExtendedAddressFromPhoton` v modulu Core.
Parkoviště, která nemají tarif (je null), mohou dostat tarif ze statického zdroje IPT.
Vybraným parkovištím může být nahrazena location za Multipolygon ze zdroje IPT (výměna bodu za multipolygon). Původní bod je uložen do centroidu.

### Worker _NewParkingsWorker_

#### Task _SaveParkingZonesPrague_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveParkingZonesPrague`
    -   bez parametrů
-   datové zdroje
    -   dataSourceTSK (ftp),
    -   dataSourceTariffTSK (ftp),
    -   dataSourceIPR
-   transformace

    Data pro `parkings` - TSK zdroj rozšiřujeme o geodata z Opendat IPR (geometrie stejné zóny mergujeme v jednu), které mají přesnější polohu parkovišť. Kde jsou k dispozici, nahrazujeme geometrii TSK.
    Data pro `parkings_location` - IPR zdroj (původní detailní geometrie) rozšiřujeme o geodata z TSK, pouze kdyžkteré mají přesnější polohu parkovišť. Kde jsou k dispozici, nahrazujeme geometrii TSK.

    -   [TSKParkingTransformation](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/integration-engine/TSKParkingTransformation.ts) - mapování pro `parkingsModel` a `parkingsLocationModel`
    -   [TSKParkingTariffTransformation](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/integration-engine/TSKParkingTariffTransformation.ts) - mapování pro `parkingsTariffsModel`
    -   [IPRParkingTransformation](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/integration-engine/IPRParkingTransformation.ts) - mapování pro `parkingsModel` a `parkingsLocationModel`

-   data modely
    -   parkingsModel -> `parkings`
    -   parkingsLocationModel -> `parkings_location` (detailnější geometrie, která vychází z položek IPRu)
    -   parkingPaymentRepository -> `parking_payments`
    -   parkingsTariffsModel -> `parkings_tariffs`
-   Důležité informace pro zpracování dat ze zdroje TariffTSK
    -   U parkovišť ZPS pod správou TSK nastaveno ve payment_method hodnotu `litacka`
-   Další zpracování TariffTSK
    -   před uložením jsou ceníky zkontrolovány podle pravidel: `ZeroMaxParkingTime`, `MaxChargeIterationsRule`, `MaxPriceRule`. Pokud jedno z pravidel není splněno tak se všechny položky se stejným cTariff přeskočí a tudíž neuloží do databáze. Záznam o tom, které položky neprošly kontrolou se uloží do tabulky `parkings_business_errors`

#### Task _SaveMrParkitData_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveMrParkitData`
    -   bez parametrů
-   datové zdroje
    -   dataSourceMrParkit (http)
-   transformace
    -   [MrParkitGarageTransformation](../src/integration-engine/workers/tasks/SaveMrParkitDataTask.ts) - transformace parkovišť, lokací i measurementů (pro každou garáž nálezí vždy jeden business objekt od každého)
-   data modely
    -   parkingsModel -> (schéma parkings) `parkings`
    -   parkingsLocationModel -> (schéma parkings) `parkings_location`
    -   parkingsMeasurementsModel -> (schéma parkings) `parkings_measurements`
        - přestože přesná data nedostáváme, tak ukládáme do tabulky hodnoty `available_spot_number` stav ve významu jestli je parkoviště volné. Absolutní počet volných míst není znám a je třeba s tím počítat na výstupním API.

#### Task _GenerateSmart4CityTaskListTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.generateSmart4CityTaskList`
    -   parametry:
        -   type: `locations`/`measurements`
-   závislé fronty
    -   název: `dataplatform.parkings.updateSmart4CityLocations` & `dataplatform.parkings.updateSmart4CityMeasurements`
    -   parametry:
        -   code: `kod města` napr. `nymburk`
-   datové zdroje
    -   Smart4City - seznam měst (http)

#### Task _UpdateSmart4CityLocationsTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.updateSmart4CityLocations`
    -   parametry:
        -   code: `kod města` napr. `nymburk`
-   datové zdroje
    -   Smart4City - data měst (http)
-   transformace
    -   [Smart4CityLocationTransformation](../src/integration-engine/transformations/Smart4CityLocationTransformation.ts) - transformace parkovišť, lokací i measurementů (pro každou garáž nálezí vždy jeden business objekt od každého)
-   data modely
    -   parkingsModel -> (schéma parkings) `parkings`
    -   parkingsLocationModel -> (schéma parkings) `parkings_location`

#### Task _UpdateSmart4CityMeasurementsTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.updateSmart4CityMeasurements`
    -   parametry:
        -   code: `kod města` napr. `nymburk`
-   datové zdroje
    -   Smart4City - data měst (http)
-   transformace
    -   [Smart4CityMeasurementTransformation](../src/integration-engine/transformations/Smart4CityMeasurementTransformation.ts) - transformace parkovišť, lokací i measurementů (pro každou garáž nálezí vždy jeden business objekt od každého)
-   data modely
    -   parkingsMeasurementsModel -> (schéma parkings) `parkings_measurements`

#### Task _SaveOsmDataTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveOsmDataTask`
    -   bez parametrů
-   datové zdroje
    -   OsmDataSource (http)
        - zvlášť endpoint pro parkoviště a parkovací místa
        - geojson formát
-   transformace
    -   [OsmParkingTransformation](../src/integration-engine/transformations/osm/OsmParkingTransformation.ts) - transformace parkovišť
    -   [OsmParkingSpace](../src/integration-engine/transformations/osm/OsmParkingSpaceTransformation.ts) - transformace lokací
    -   [OsmOpeningHoursTransformation](../src/integration-engine/transformations/osm/OsmOpeningHoursTransformation.ts) - transformace otvíracích dob
-   data modely
    -   parkingsModel -> (schéma parkings) `parkings_secondary`
        - `parkings` a `parkings_secondary` tabulky jsou ve vztahu rodič a potomek
    -   parkingsLocationModel -> (schéma parkings) `parkings_location`
    -   parkingsOpeningHoursModel -> (schéma parkings) `parkings_opening_hours`

#### Task _SaveOsmEntrancesDataTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveOsmEntrancesDataTask`
    -   bez parametrů
-   datové zdroje
    -   Ipt blob storage ručně tvořený zdroj (http)
        - geojson formát
-   transformace
    -   [OsmEntrancesTransformation](../src/integration-engine/transformations/osm/OsmEntrancesTransformation.ts)
-   data modely
    -   EntranceModel -> (schéma parkings) `parking_entrances`

#### Task _SaveOsmParkingMachinesTask_

-   vstupní rabbitmq fronta:
    -   název: `dataplatform.parkings.saveOsmParkingMachinesTask`
    -   bez parametrů
-   datové zdroje:
    - Ipt blob storage exportovaný zdroj (http)
      - geojson formát
-   transformace: [OsmParkingMachinesTransformation](../src/integration-engine/transformations/osm/OsmParkingMachinesTransformation.ts)
-   cílová tabulka: `parkings_machines`

#### Task _GenerateIptOictParkingJobsTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.generateIptOictParkingJobs`
    -   parametry:
        -   bez parametrů
-   závislé fronty
    -   název: `dataplatform.parkings.SaveIptOictParkingData`
    -   parametry:
        -   source: `jméno zdroje` dle. `parking_sources`
-   datové zdroje
    -   `parking_sources` - záznamy obsahují url datového zdroje v `datasource_parking` && `datasource_locations`

#### Task _SaveIptOictParkingDataTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveIptOictParkingData`
    -   parametry:
        -   source: `jméno zdroje` dle. `parking_sources`
-   datové zdroje
    -   IptOictDataSource (http)
        -   zvlášť endpoint pro parkoviště a parkovací místa
        -   geojson formát
        -   statická data
-   transformace
    -   [IptOictParkingSpaceTransformation](../src/integration-engine/transformations/IptOictParkingSpaceTransformation.ts) - transformace lokací parkovišť
    -   [IptOictParkingTransformation](../src/integration-engine/transformations/IptOictParkingTransformation.ts) - transformace parkovišť
    -   [IptOictOpeningHoursTransformation](../src/integration-engine/transformations/iptoict/IptOictOpeningHoursTransformation.ts) - transformace otvíracích dob
-   data modely
    -   parkingsModel -> (schéma parkings) `parkings`
    -   parkingsLocationModel -> (schéma parkings) `parkings_location`
    -   parkingsOpeningHoursModel -> (schéma parkings) `parkings_opening_hours`

#### Task _GenerateIptOictPaymentJobsTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.generateIptOictPaymentJobs`
    -   parametry:
        -   bez parametrů
-   závislé fronty
    -   název: `dataplatform.parkings.saveIptOictPaymentsData`
    -   parametry:
        -   source: `jméno zdroje` dle. `parking_sources`
-   datové zdroje
    -   `parking_sources` - záznamy obsahují url datového zdroje v `datasource_payments`

#### Task _GenerateIptOictProhibitionsJobsTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.generateIptOictProhibitionsJobs`
    -   parametry:
        -   bez parametrů
-   závislé fronty
    -   název: `dataplatform.parkings.saveIptOictProhibitionsDataTask`
    -   parametry:
        -   source: `jméno zdroje` dle. `parking_sources`
-   datové zdroje
    -   `parking_sources` - záznamy obsahují url datového zdroje v `datasource_prohibitions`

#### Task _SaveIptOictProhibitionsDataTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveIptOictProhibitionsDataTask`
    -   parametry:
        -   source: `jméno zdroje` dle. `parking_sources`
-   datové zdroje
    -   IptOictParkingsProhibitionsDataSource (http)
        -   json formát
        -   stahuje se z url uvedené v `parking_sources`, sloupec `datasource_prohibitions`
-   transformace
    -   [IptOictParkingProhibitionsTransformation](../src/integration-engine/transformations/IptOictParkingProhibitionsTransformation.ts) - transformace plateb
-   data modely
    -   ParkingProhibitionsModel -> (schéma parkings) `parking_prohibitions`

#### Task _SaveIptOictParkingDataTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveIptOictPaymentsData`
    -   parametry:
        -   source: `jméno zdroje` dle. `parking_sources`
-   datové zdroje
    -   IptOictPaymentsDataSource (http)
        -   json formát
        -   statická data
-   transformace
    -   [IptOictPaymentsTransformation](../src/integration-engine/transformations/IptOictPaymentsTransformation.ts) - transformace plateb
-   data modely
    -   PaymentModel -> (schéma parkings) `parking_payments`


#### Task _GenerateIptOictTariffsJobsTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.generateIptOictTariffsJobs`
    -   parametry:
        -   bez parametrů
-   závislé fronty
    -   název: `dataplatform.parkings.saveIptOictTariffsData`
    -   parametry:
        -   source: `jméno zdroje` dle. `parking_sources`
-   datové zdroje
    -   `parking_sources` - záznamy obsahují url datového zdroje v `datasource_tariffs`


#### Task _SaveIptOictTariffsDataTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveIptOictTariffsData`
    -   parametry:
        -   source: `jméno zdroje` = `source`
-   datové zdroje
    -   IptOictTariffsDataSource (http)
        -   statická data
-   transformace
    -   [IptOictTariffsTransformation](../src/integration-engine/transformations/IptOictTariffsTransformation.ts) - transformace tarifů parkovišť
-   data modely
    -   ParkingTariffsModel -> (schéma parkings) `parkings_tariffs`
    -   ParkingModel -> (schéma parkings) `parkings`

#### Task _SaveIsphkMeasurementsTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveIsphkDataTask`
    -   parametry:
        -   ([Data example](../test/input-gateway/data/parkingIsphkInput.json))
-   datové zdroje
    -   IG
-   závislé fronty
    -   název: `input.parkings.saveIsphkDataTask`
-   transformace

    -   [IsphkMeasurementTransformation](../src/integration-engine/transformations/iptoict/IsphkMeasurementTransformation.ts) - transformace measurements

-   data modely
    -   parkingsMeasurementsModel -> `parkings_measurements_part`
    -   parkingsMeasurementsActualModel -> `parkings_measurements_actual`

#### Task _SaveBedrichovDataTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveBedrichovDataTask`
    -   parametry:
        -   ([Data example](../test/integration-engine/data/bedrichov-input-data.ts))

-   datové zdroje
    -   dataSource (http)

-   transformace
    -   [BedrichovMeasurementsTransformation](../src/integration-engine/transformations/BedrichovMeasurementsTransformation.ts) - transformace measurements

-   data modely
    -   parkingsMeasurementsModel -> `parkings_measurements_part`
    -   parkingsMeasurementsActualModel -> `parkings_measurements_actual`

#### Task _SavePmdpMeasurementsTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.savePmdpMeasurements`
    -   parametry:
        -   ([Data example](../src/integration-engine/workers/schemas/PmdpTaskTypeSchema.ts))
-   datové zdroje
    -   PmdpMeasurementsDataSourceFactory (http)
        -   xml formát
        -   dynamická data
    - obohacené o info z `parkings` tabulky (manuální zdroj IPT OICT)
-   transformace
    -   [PmdpMeasurementTransformation](../src/integration-engine/transformations/pmdp/PmdpMeasurementTransformation.ts) - transformace measurements

-   data modely
    -   parkingsMeasurementsModel -> `parkings_measurements_part`
    -   parkingsMeasurementsActualModel -> `parkings_measurements_actual`

#### Task _CalculateAverageOccupancyTask_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.calculateAverageOccupancy`
    -   bez parametrů
-   datové zdroje
    -   `parkings_measurement_part` tabulka
-   data modely
    -   ParkingAverageOccupancyModel -> (schéma parkings) `parkings_average_occupancy`

#### Task _generateIptOictEntrancesJobsTask_

- generuje zprávy s parametrem `source` pro `SaveIptOictEntrancesDataTask`
- vstupní rabbitmq fronta
    - název: `dataplatform.parkings.generateIptOictEntrancesJobsTask`
    - bez parametrů
-   závislé fronty
    -   název: `dataplatform.parkings.saveIptOictEntrancesData`
    -   parametry:
        -   source: `jméno zdroje` dle. `parking_sources`
-   datové zdroje
    -   `parking_sources` - záznamy obsahují url datového zdroje v `datasource_entrances`

#### Task _SaveIptOictEntrancesDataTask_

- vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveIptOictEntrancesData`
    -   parametr je zdroj (source)
    -   zprávy s parametrem generuje `generateIptOictEntrancesJobsTask` přes stejnojmennou frontu bez parametrů
- datové zdroje
        - geojson formát
        - data se stahují z url uvedené v `parking_sources`, sloupec `datasource_entrances`
- transformace
    -   [IptOictEntrancesTransformation](../src/integration-engine/transformations/iptoict/IptOictEntrancesTransformation.ts)
- data modely
    -   EntranceModel -> (schéma parkings) `parking_entrances`

### Worker _ParkingsWorker_

#### _saveParkingLotsPrague_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveParkingLotsPrague`
    -   bez parametrů
-   datové zdroje
    -   dataSource (http),
    -   statický IPT zdroj (http),
-   transformace
    -   [ParkingLotsTransformation](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/integration-engine/ParkingLotsTransformation.ts) - mapování pro `parkingsModel` a `parkingsLocationModel`
-   data modely
    -   parkingsModel -> `parkings`
-   důležité informace pro zpracování dat ze zdroje TSKParkings
    -   V budoucnu přibydou další hodnoty parking_type: `park_sharing` pro službu Mr. Parkit atp.
    -   Parametr `zone_type` je povinný pokud je `parking_type == on_street`. Jinak má být `NULL`
    -   `zone_free` - placeholder pro Zóny neplaceného stání (zone_type), které se budou integrovat v budoucnu

#### _saveParkingLotsMeasurementsPrague_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveParkingLotsMeasurementsPrague`
    -   bez parametrů
-   datové zdroje
    -   dataSource (http),
-   transformace
    -   [ParkingLotsTransformation](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/integration-engine/ParkingLotsTransformation.ts) - mapování pro `parkingsModel` a `parkingsLocationModel`
-   data modely

    -   parkingsMeasurementsModel -> `parkings_measurements_part`
    -   parkingsMeasurementsActualModel -> `parkings_measurements_actual`

    Všechny historické údaje jsou uloženy v tabulce `parkings_measurements_part`. Aktualizujeme pouze aktuální údaje v tabulce `parkings_measurements_actual`.

#### _saveKoridConfToDB_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveKoridConfToDB`
    -   bez parametrů
-   datové zdroje
    -   IG
-   transformace
    -   [KoridParkingConfigTransformation](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/integration-engine/KoridParkingConfigTransformation.ts) - mapování pro `parkingsModel` a `parkingsLocationModel`
-   data modely
    -   parkingsModel -> `parkings`
    -   ParkingsLocationRepository -> `parkings_location`

#### _saveKoridDataToDB_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.saveKoridDataToDB`
    -   bez parametrů
-   datové zdroje
    -   IG
-   transformace
    -   [KoridParkingDataTransformation](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/blob/development/src/integration-engine/KoridParkingDataTransformation.ts) - mapování pro `parkingsModel` a `parkingsLocationModel`
-   data modely
    -   parkingsMeasurementsModel -> `parkings_measurements_part`
    -   parkingsMeasurementsActualModel -> `parkings_measurements_actual`

### ZtpParkingZonesWorker

#### _updateZtpParkings_

-   vstupní rabbitmq fronta
    -   název: `dataplatform.parkings.updateZtpParkings`
    -   bez parametrů
-   datové zdroje
    -   JSON stahovaný z IPR
    -   frekvence stahování: denně v 5 ráno (`0 5 */1 * *`)
-   transformace
    Mapování `IZtpParkingJsonFeature` na `IParking` + doplnění o adresu.
    Získává se z Photon API metodou `GeocodeApi.getExtendedAddressFromPhoton` v modulu Core.
-   data modely
    -   parkingsModel -> `parkings`

### Worker _StaticTariffsWorker_ a _UpdateStaticTariffsTask_

-   vstupní rabbitmq fronta:
    -   název: `updateStaticTariffs`
    -   bez parametrů
-   datový zdroj
    -   JSON stahovaný ze zdroje IPT
    -   [příklad vstupních dat](../test/integration-engine/data/StaticParkingTariffsData.json)
-   transformace
    -   mapování na `IParkingTariff`
    -   `relations` mapuje `StaticTariffsMatcher` tak, že do příslušného `IParking` doplní id tarifu, pokud je toto null
-   data modely
    -   parkingsModel -> `parkings`
    -   parkingTariffsmodel -> `parkings_tariffs`

### Worker ParkingSourcesWorker

-   ukládá data z queue `updateParkingSource` do db
-   zdroj je IG přes `ParkingSourcesRouter`
-   transformace není

### ParkomatsWorker

 #### _RefreshDataInDbTask_

-   vstupní rabbitmq fronta:
    -   název: `refreshDataInDB`
    -   optional paramtery
        -   from
        -   to
-   datový zdroj
    -   JSON stahovaný z API
-   transformace
    - Mapovani z `IParkomatInput` na `IParkomatDto`
-   data modely
    -   `parkomats`



### ParkomatsWorker

 #### _RefreshDataInDbTask_

-   vstupní rabbitmq fronta:
    -   název: `refreshDataInDB`
    -   optional paramtery
        -   from
        -   to
-   datový zdroj
    -   JSON stahovaný z API
-   transformace
    - Mapovani z `IParkomatInput` na `IParkomatDto`
-   data modely
    -   `parkomats`

### UpdateAddressWorker

 #### _UpdateAddressTask_

-   vstupní rabbitmq fronta:
    -   název: `updateAddress`
    -   parametry
        -   id
        -   POINT
    -   id je z tabulky parkings
-   obohacuje id o adresu, která vychází z centroidu geometrie daného parkoviště a získává se z Photon API metodou `GeocodeApi.getAddressByLatLngFromPhoton` v modulu Core

 #### _UpdateLocationAddressTask_

-   vstupní rabbitmq fronta:
    -   název: `UpdateLocationAddressTask`
    -   parametry
        -   id
        -   POINT
    -   id je z tabulky parkings_location
-   obohacuje id o adresu, která vychází z centroidu geometrie daného parkoviště a získává se z Photon API metodou `GeocodeApi.getAddressByLatLngFromPhoton` v modulu Core


 #### _UpdateAddressAndNameTask_
-   slouží pouze pro ztp parkoviště
-   vstupní rabbitmq fronta:
    -   název: `updateAddress`
    -   parametry
        -   id
        -   POINT
    -   id je z tabulky parkings s parking.source "ipr"
-   obohacuje id o adresu, která vychází z centroidu geometrie daného parkoviště a získává se z Photon API metodou `GeocodeApi.getAddressByLatLngFromPhoton` v modulu Core

 #### _UpdateAllParkingAdresses_

- vstupní rabbitmq fronta:
    -   název: `updateAllParkingAdresses`
    -   bez parametru
- aktualizuje při každém běhu dávku adres z Photonu v tabulce parkings. Dávkuje se tak, aby se nepřetěžoval Photon.

 #### _UpdateAllParkingsLocationAdresses_

-   vstupní rabbitmq fronta:
    -   název: `updateAllParkingsLocationAdresses`
    -   bez parametru
-   aktualizuje při každém běhu dávku adres z Photonu v tabulce parkings_location. Dávkuje se tak, aby se nepřetěžoval Photon.

#### _UpdateMissingParkingAdressesTask_

- vstupní rabbitmq fronta:
    -   název: `updateMissingParkingAdresses`
    -   bez parametru
-   doplní při každém běhu chybějící adresy v tabulce parkings.

#### _UpdateMissingParkingsLocationAdressesTask_

-   vstupní rabbitmq fronta:
    -   název: `updateMissingParkingsLocationAdresses`
    -   bez parametru
-   doplní při každém běhu chybějící adresy v tabulce parkings_location.

## Uložení dat

-   typ databáze
    -   PSQL
-   databázové schéma
    -   ![parkings er diagram](assets/parkings_erd.png)
-   retence dat
    -   `parkingsModel`, `parkingsLocationModel`, `parkingsMeasurementsActualModel`, `parkingsTariffsModel` update
    -   `parkingsMeasurementsModel` historizace bez promazávání
    -   `ParkingPaymentsRepository` - update s promazávaním dle zdroje

## Output API

### Obecné

-   Zdroj pro api
    -   PSQL databáze viz výše
-   OpenAPI dokumentace
    -   [OpenAPI](./openapi-output.yaml)
-   veřejné / neveřejné endpointy
    -   částečně veřejná na úrovni filtrování podle poskytovatele dat
    -   verze 3 neveřejná
-   Ořezání dle zájmového regionu u endpointů parking machines a parkings[statický soubor](../static-data/geojson/parkoviste_orez_okresy_max.geojson)
-   AsyncApi dokumentace
    -    [AsyncApi](./asyncapi.yaml)

#### _/v2/parking_

#### _/v2/parking/:id_

-   zdrojové tabulky
    -   parkings
    -   parkings_measurements_actual
-   implementováno dodatečně
    -   `reservation_url` - přidáno volitelný atribut, který použijeme v datech od MPLA
    -   Nový číselník `payment_methods`. Example: `{ payment_methods: ["card_online", "cash"] }`

#### _/v2/parking/detail_

#### _/v2/parking/detail/:id_

-   zdrojové tabulky
    -   parkings
    -   parkings_location
-   detailnejší geometrie, vychází z OpenData IPR a doplňuje parametry z TSK (ftp)

#### _/v2/parking/tariffs/_

#### _/v2/parking/tariffs/:tariffId_

-   zdrojové tabulky
    -   parkings_tariffs

#### _/v2/parking/measurements_

-   zdrojové tabulky
    -   parkings_measurements_part
-   implementováno dodatečně
    -   response header (`Link`) obsahuje metadata k předchozí a následující stránce
    -   implementována možnost dotazování pomocí parametrů `limit` a `offset`

#### _/v1/parkings_

#### _/v1/parkings/:id_

_:warning: Deprecated, vyžijte /parking nebo /parking/detail_

-   zdrojové tabulky
    -   parkings
    -   parkings_measurements_actual
    -   v_tsk_districts
        -   parkings
        -   common.citydistricts
    -   v_tsk_average_occupancy
        -   parkings_measurements_part
-   implementováno dodatečně
    -   zpětně kompatibilní s Mongo GeoJSON routerem
    -   navrácí pouze `tsk` data
    -   používá sloupec centroid pro geolokaci, je nutné, aby vracel pouze body, ale parkoviště může být i multipolygon

#### _/v1/parkings/history_

_:warning: Deprecated, využijte /parking/measurements_

-   zdrojové tabulky
    -   parkings_measurements_part
-   implementováno dodatečně
    -   zpětně kompatibilní s Mongo GeoJSON history routerem
    -   navrácí pouze `tsk` data

#### _/v3/parking_

-   zdrojové tabulky
    -   parkings (filtrováno skrz pohled `v_parkings_filtered`)
    -   v_parkings_filtered filtruje parkoviště která se překrývají s tím, že naše zdroje jsou nadřazeny osm
    -   parkings_secondary (sekundární zdroj aktuálně pouze data z osm, můžou obsahovat duplicitní prvky k primárním zdrojům)
    -   parkings_location
    -   parking_entrances
-   ~~zatím vytvořeno pro nové API od TSK další budou následovat~~ již integrováno několik dodavatelů
-   nový formát výstupu viz open api dokumentace
-   omezení geometrie na zájmový region

#### _/v3/parking/:id_

-   zdrojové tabulky
    -   parkings
    -   parkings_secondary
    -   parkings_location
    -   parking_entrances

#### _/v3/parking/:id/average-occupancy_

-   zdrojové tabulky
    -   `parkings_average_occupancy`
-   zobrazí průměrnou zaplněnost pro dané parkoviště

#### _/v3/parking-measurements_

-   zdrojové tabulky
    -   `v_parkings_latest_measurements`
-   zobrazí nejnovější měření parkoviště (za max 1h)

#### _/v3/parking-tariff_

-   zdrojové tabulky
    -   parkings_tariffs
-   na výstupu děleno do charge bands a charges
    -   pod jedním tariffem může být více charge bands ~ každý řádek se stejným `tariff_id` je jeden charge band
        -   `valid_from` a `valid_to` se vztahují k charge band se vypočítá jako min a maximum z `valid_from` a `valid_to` ze všech chargů
    -   pod jedním charge band může být více charges pozná se podle `charge_band_name`

#### _/v3/parking-tariff/:id_

-   zdrojové tabulky
    -   parkings_tariffs
-   zobrazí konkrétní tariff, data stejná jako v `_/v3/parking-tariff_`

#### _/v3/parking-machines_

-   zdrojové tabulky
    -   parkings_machines
-   seznam parkovacích automatů s geometrií
    -   `type` nabývá hodnot: `payment_machine, info_box
    -   omezení geometrie na zájmový region

#### _/v3/parking-machines/:id_

-   zdrojové tabulky
    -   parkings_machines
-   zobrazí konkrétní parkovací automat, data stejná jako v `_/v3/parking-machines_`

#### _/v3/parking-sources_

-   zdrojové tabulky
    -   parkings_sources
-   zobrazí dostupné datové zdroje

#### Open data k _/v3/_ endpointům

-   Omezení na open data / vše se řídí interním parametrem `isRestrictedToOpenData`. Ten je nutné nastavit ve všech rolích v pp, které mají přístup k `/v3/` endpointům. Povolené hodnoty: `true` / `false`. Pro opendataře je nutné nastavit limitaci `is-empty`.

create table parking_machines (
    id varchar(255) NOT NULL,
    "source" varchar(255) NOT NULL,
    code varchar(255) NULL,
    type varchar(255) NULL,
    location geometry NULL,
    valid_from timestamptz NULL,
    tariff_id varchar(255) NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT parking_machines_pk PRIMARY KEY (id)
);

CREATE INDEX parking_machines_location_idx ON parking_machines USING gist(location);

create view v_parking_machines_filtered as
with parking_machines_primary as (
    select * from parking_machines where source != 'osm'
), parking_machines_secondary as (
    select * from parking_machines where source = 'osm'
)
select * from parking_machines_primary p1
union all
select * from parking_machines_secondary p2
where p2.id not in (
    select distinct p2.id
    from parking_machines_primary p1
    inner join parking_machines_secondary p2 ON ST_Distance(p1.location::geography, p2.location::geography) < 15
);

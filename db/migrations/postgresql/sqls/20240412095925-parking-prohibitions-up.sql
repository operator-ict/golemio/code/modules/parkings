CREATE TABLE IF NOT EXISTS parking_prohibitions (
    parking_id varchar(50) NOT NULL,
    source varchar(255) NOT NULL,
    lpg boolean,
    bus boolean,
    truck boolean,
    motorcycle boolean,
    bicycle boolean,
    trailer boolean,
    created_at timestamptz NULL,
	updated_at timestamptz NULL,
	CONSTRAINT parking_prohibitions_pkey PRIMARY KEY (parking_id)
);
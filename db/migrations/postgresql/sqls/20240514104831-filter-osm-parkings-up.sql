ALTER TABLE
    parkings
ADD
    sanitized_location geometry GENERATED ALWAYS AS (st_makevalid("location")) STORED;

CREATE INDEX parkings_sanitized_location_idx ON parkings USING gist(sanitized_location);

CREATE VIEW v_parkings_filtered AS
SELECT
    *
FROM
    ONLY parkings p1
UNION
ALL
SELECT
    *
FROM
    parkings_secondary p2
WHERE
    p2.id NOT IN (
        SELECT
            DISTINCT p2.id
        FROM
            ONLY parkings p1
            INNER JOIN parkings_secondary p2 ON ST_Intersects(p1.sanitized_location, p2.sanitized_location)
    );

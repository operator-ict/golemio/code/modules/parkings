ALTER TABLE parkings_tariffs DROP COLUMN valid_from;
ALTER TABLE parkings_tariffs DROP COLUMN valid_to;
ALTER TABLE parkings_tariffs DROP COLUMN maximum_duration_seconds;
ALTER TABLE parkings_tariffs DROP COLUMN periods_of_time;

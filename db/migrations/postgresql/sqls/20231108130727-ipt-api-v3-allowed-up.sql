ALTER TABLE parking_sources RENAME COLUMN "ipt_allowed" TO "api_v3_allowed";
ALTER TABLE parking_sources ADD COLUMN "legacy_api_allowed" bool NOT NULL DEFAULT true;

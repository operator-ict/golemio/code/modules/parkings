ALTER TABLE parkings_location ALTER COLUMN special_access TYPE VARCHAR(255) USING array_to_string(special_access, ',');

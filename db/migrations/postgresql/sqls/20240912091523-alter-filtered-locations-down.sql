CREATE OR REPLACE VIEW v_parkings_filtered AS
SELECT
    *
FROM
    ONLY parkings p1
UNION
ALL
SELECT
    *
FROM
    parkings_secondary p2
WHERE
    p2.id NOT IN (
        SELECT
            DISTINCT p2.id
        FROM
            ONLY parkings p1
            INNER JOIN parkings_secondary p2 ON ST_Intersects(p1.sanitized_location, p2.sanitized_location)
    );

DROP INDEX parkings_secondary_sanitized_location_idx;

DROP INDEX parkings_location_sanitized_location_idx;

ALTER TABLE
    parkings_location DROP COLUMN sanitized_locations;

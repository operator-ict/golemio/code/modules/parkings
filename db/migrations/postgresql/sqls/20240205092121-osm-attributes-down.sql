ALTER TABLE parkings_secondary NO INHERIT parkings;

DROP TABLE parkings_secondary;

ALTER TABLE parkings DROP COLUMN covered;
ALTER TABLE parkings DROP COLUMN contact;
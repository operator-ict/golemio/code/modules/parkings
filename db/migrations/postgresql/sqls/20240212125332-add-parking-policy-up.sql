ALTER TABLE parkings_secondary NO INHERIT parkings;
ALTER TABLE parkings ADD parking_policy varchar(255) NULL;
ALTER TABLE parkings_secondary ADD parking_policy varchar(255) NULL;
ALTER TABLE parkings_secondary INHERIT parkings;

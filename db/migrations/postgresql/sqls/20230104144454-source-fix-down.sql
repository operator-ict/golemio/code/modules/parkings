UPDATE parkings SET source=UPPER(source) WHERE source != 'korid';
UPDATE parkings SET source='IPR ZTP' WHERE source = 'IPR';

UPDATE parkings_location SET source=UPPER(source);
UPDATE parkings_measurements_actual SET source=UPPER(source) WHERE source != 'korid';
UPDATE parkings_measurements_part SET source=UPPER(source) WHERE source != 'korid';
UPDATE parkings_tariffs SET source=UPPER(source) WHERE source != 'korid';

ALTER TABLE parkings_location ADD CONSTRAINT parkings_location_fkey FOREIGN KEY ("source",source_id)
    REFERENCES parkings("source",source_id);

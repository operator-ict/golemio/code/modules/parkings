ALTER TABLE parkings_tariffs ADD COLUMN valid_from timestamptz;
ALTER TABLE parkings_tariffs ADD COLUMN valid_to timestamptz;
ALTER TABLE parkings_tariffs ADD COLUMN maximum_duration_seconds int NULL;
ALTER TABLE parkings_tariffs ADD COLUMN periods_of_time jsonb;

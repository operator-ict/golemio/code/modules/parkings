ALTER TABLE parking_machines ADD COLUMN "source_id" varchar(255);
UPDATE parking_machines SET source_id = id;
ALTER TABLE parking_machines ADD CONSTRAINT parking_machines_source_key UNIQUE (source, source_id);

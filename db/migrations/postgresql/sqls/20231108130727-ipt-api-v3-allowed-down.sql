ALTER TABLE parking_sources RENAME COLUMN "api_v3_allowed" TO "ipt_allowed";
ALTER TABLE parking_sources DROP COLUMN "legacy_api_allowed";

ALTER TABLE parkings ADD covered boolean NULL;
ALTER TABLE parkings ADD contact json NULL;

create table "parkings_secondary" (like parkings INCLUDING CONSTRAINTS including indexes);

ALTER TABLE "parkings_secondary" INHERIT parkings;

COMMENT ON TABLE "parkings_secondary" IS 'List of parking places from secondary sources, that can contain same parking places as in main parking table.';

INSERT INTO "parking_sources" ("source", open_data, api_v3_allowed, payment_url, contact, legacy_api_allowed) 
    VALUES ('osm', false, true, NULL, NULL, false);


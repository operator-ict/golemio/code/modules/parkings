ALTER TABLE
    parkings
ADD
    area int GENERATED ALWAYS AS (round(st_area(ST_Transform(st_makevalid("location"),4326)::geography))) STORED;

CREATE or REPLACE view v_parkings_filtered as 
with parkings_loc as (
    select
        p.id as id,
        pl.sanitized_locations as location
    from
        only parkings p
    inner join parkings_location pl 
    on
        p.source = pl.source
        and p.source_id = pl.source_id
    where
        p.source in (
        select
            source
        from
            parking_sources ps
        where
            ps.api_v3_allowed )
)
select
    *
from
    only parkings p
union
all
select
    *
from
    only parkings_secondary ps
where
    ps.id not in (
    select
        distinct ps.id
    from
        parkings_loc plo
    inner join parkings_secondary ps on
        ST_Intersects(plo.location,
        ps.sanitized_location));
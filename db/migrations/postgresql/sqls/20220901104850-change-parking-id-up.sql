ALTER TABLE parkings.parkings ALTER COLUMN id TYPE varchar(50);
ALTER TABLE parkings.parkings ALTER COLUMN id SET NOT NULL;
ALTER TABLE parkings.parkings ALTER COLUMN id DROP DEFAULT;
UPDATE parkings.parkings SET id = LOWER(CONCAT(REPLACE(source, ' ', '-'), '-', source_id));

ALTER TABLE parkings.parkings_measurements_actual ALTER COLUMN parking_id SET NOT NULL;
ALTER TABLE parkings.parkings_measurements_part ALTER COLUMN parking_id SET NOT NULL;

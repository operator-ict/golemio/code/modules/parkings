ALTER SEQUENCE parkings_id_seq RESTART WITH 1;
UPDATE parkings.parkings SET id = nextval('parkings_id_seq');
ALTER TABLE parkings.parkings ALTER COLUMN id TYPE integer USING (id::integer);

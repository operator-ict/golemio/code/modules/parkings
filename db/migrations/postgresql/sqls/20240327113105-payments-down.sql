ALTER TABLE parking_sources ADD COLUMN "payment_url" varchar(255);

UPDATE parking_sources
SET payment_url = parking_sources.payment->>'web_url'::text;

ALTER TABLE parking_sources DROP COLUMN "payment";
ALTER TABLE parking_sources DROP COLUMN "reservation";

ALTER TABLE parking_sources DROP COLUMN "datasource_payments";

ALTER TABLE parkings_secondary ADD COLUMN web_app_payment_url text NULL;
ALTER TABLE parkings_secondary ADD COLUMN android_app_payment_url text NULL;
ALTER TABLE parkings_secondary ADD COLUMN ios_app_payment_url text NULL;

ALTER TABLE parkings ADD COLUMN web_app_payment_url text NULL;
ALTER TABLE parkings ADD COLUMN android_app_payment_url text NULL;
ALTER TABLE parkings ADD COLUMN ios_app_payment_url text NULL;

UPDATE parkings as p
SET
    web_app_payment_url = pp.payment_web_url,
    android_app_payment_url = pp.payment_android_url,
    ios_app_payment_url = pp.payment_ios_url
FROM
    parking_payments as pp
WHERE
    p.id = pp.parking_id;

DROP TABLE parking_payments;

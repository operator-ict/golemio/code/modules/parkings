ALTER TABLE
    parkings_location
ADD
    sanitized_locations geometry GENERATED ALWAYS AS (st_makevalid("location", 'method=structure')) STORED;

CREATE INDEX  parkings_secondary_sanitized_location_idx ON parkings_secondary USING gist(sanitized_location);

CREATE INDEX  parkings_location_sanitized_location_idx ON parkings_location USING gist(sanitized_locations);

CREATE or REPLACE view v_parkings_filtered as 
    with parkings_loc as (
        select
            p.id as id,
            pl.sanitized_locations as location
        from
            only parkings p
        inner join parkings_location pl 
        on
            p.source = pl.source
            and p.source_id = pl.source_id
        where
            p.source in (
            select
                source
            from
                parking_sources ps
            where
                ps.api_v3_allowed )
    )
    select
        *
    from
        only parkings p
    union
    all
    select
        *
    from
        only parkings_secondary ps
    where
        ps.id not in (
        select
            distinct ps.id
        from
            parkings_loc plo
        inner join parkings_secondary ps on
            ST_Intersects(plo.location,
            ps.sanitized_location));
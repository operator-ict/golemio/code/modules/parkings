ALTER TABLE parkings_location ALTER COLUMN special_access TYPE VARCHAR(50)[] USING string_to_array(special_access, ',');

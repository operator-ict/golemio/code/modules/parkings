CREATE TABLE parkings_business_errors (
	identifier uuid NOT NULL,
	provider varchar(10) NOT NULL,
    record_date date NOT NULL,
    "type" varchar(50) NOT NULL,
	detail json NOT NULL,	
	CONSTRAINT parkings_business_errors_pk PRIMARY KEY (identifier,provider,record_date)
);
ALTER TABLE parkings_secondary NO INHERIT parkings;
ALTER TABLE parkings DROP COLUMN parking_policy;
ALTER TABLE parkings_secondary DROP COLUMN parking_policy;
ALTER TABLE parkings_secondary INHERIT parkings;
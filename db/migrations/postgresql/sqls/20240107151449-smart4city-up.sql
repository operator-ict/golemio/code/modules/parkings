insert into parking_sources(source, open_data, api_v3_allowed, legacy_api_allowed, payment_url, contact) VALUES
('smart4city', false, true, false, null,
'{
    "email": "info@smart4city.cz",
    "phone": "+420 602 467 614",
    "web_url": "https://www.smart4city.cz/",
    "term_of_use_url": ""
}');

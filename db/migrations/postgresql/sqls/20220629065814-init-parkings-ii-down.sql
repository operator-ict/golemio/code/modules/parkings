DROP VIEW IF EXISTS v_park_and_ride;
DROP VIEW IF EXISTS v_park_and_ride_capacities;

DROP TABLE IF EXISTS parkings_tariffs;

DROP TABLE IF EXISTS parkings_measurements_actual;

DROP TABLE IF EXISTS parkings_measurements_part;

DROP TABLE IF EXISTS parkings_occupancies;
DROP SEQUENCE IF EXISTS parkings_occupancies_id_seq;

DROP TABLE IF EXISTS parkings_location;

DROP TABLE IF EXISTS parkings;
DROP SEQUENCE IF EXISTS parkings_id_seq;

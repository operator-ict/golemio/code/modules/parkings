ALTER TABLE parking_sources ADD COLUMN "datasource_payments" text;

ALTER TABLE parking_sources ADD COLUMN "payment" jsonb;
ALTER TABLE parking_sources ADD COLUMN "reservation" jsonb;

UPDATE parking_sources
SET
    payment = ('{ "web_url": "' || parking_sources.payment_url || '", "android_url": null, "ios_url": null, "discovery_url": null }')::jsonb;

ALTER TABLE parking_sources DROP COLUMN "payment_url";

CREATE TABLE IF NOT EXISTS parking_payments (
    parking_id varchar(50) NOT NULL,
    source varchar(255) NOT NULL,
    payment_web_url varchar(255),
    payment_android_url varchar(255),
    payment_ios_url varchar(255),
    payment_discovery_url varchar(255),
    reservation_type varchar(255),
    reservation_web_url varchar(255),
    reservation_android_url varchar(255),
    reservation_ios_url varchar(255),
    reservation_discovery_url varchar(255),
	created_at timestamptz NULL,
	updated_at timestamptz NULL,
	CONSTRAINT parking_payments_pkey PRIMARY KEY (parking_id)
);

INSERT INTO parking_payments (parking_id, source, payment_web_url, payment_android_url, payment_ios_url, created_at, updated_at)
SELECT
    id as parking_id,
    source,
    web_app_payment_url as payment_web_url,
    android_app_payment_url as payment_android_url,
    ios_app_payment_url as payment_ios_url,
    created_at,
    updated_at
FROM parkings
WHERE web_app_payment_url IS NOT NULL OR android_app_payment_url IS NOT NULL OR ios_app_payment_url IS NOT NULL;

ALTER TABLE parkings DROP COLUMN web_app_payment_url;
ALTER TABLE parkings DROP COLUMN android_app_payment_url;
ALTER TABLE parkings DROP COLUMN ios_app_payment_url;

ALTER TABLE parkings_secondary DROP COLUMN web_app_payment_url;
ALTER TABLE parkings_secondary DROP COLUMN android_app_payment_url;
ALTER TABLE parkings_secondary DROP COLUMN ios_app_payment_url;

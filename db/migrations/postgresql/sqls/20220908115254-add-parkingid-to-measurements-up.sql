ALTER TABLE parkings.parkings_measurements_actual ADD COLUMN parking_id character varying(50);
ALTER TABLE parkings_measurements_part ADD COLUMN parking_id character varying(50);
UPDATE parkings_measurements_part SET parking_id = LOWER(CONCAT(REPLACE(source, ' ', '-'), '-', source_id));
UPDATE parkings_measurements_actual SET parking_id = LOWER(CONCAT(REPLACE(source, ' ', '-'), '-', source_id));

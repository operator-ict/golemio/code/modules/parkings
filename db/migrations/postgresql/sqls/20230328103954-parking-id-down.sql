ALTER TABLE parkings.parkings_measurements_actual ALTER COLUMN parking_id DROP NOT NULL;
ALTER TABLE parkings.parkings_measurements_part ALTER COLUMN parking_id DROP NOT NULL;

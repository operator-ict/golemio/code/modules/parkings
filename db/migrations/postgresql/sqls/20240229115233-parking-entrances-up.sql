CREATE TABLE IF NOT EXISTS parking_entrances (
    entrance_id varchar(255)  NOT NULL,
    "source" varchar(255) NOT NULL,
    parking_id varchar(50) NOT NULL,    
    "location" geometry,
    entry bool,
    exit bool,
    entrance_type varchar(255) array,
    level float,
    max_height float,
    max_width float,
    max_length float,
    max_weight float,
	created_at timestamptz NULL,
	updated_at timestamptz NULL,
	CONSTRAINT parking_entrances_pkey PRIMARY KEY (entrance_id, parking_id)
);
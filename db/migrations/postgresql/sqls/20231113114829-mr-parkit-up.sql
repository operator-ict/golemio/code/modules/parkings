insert into parking_sources(source, open_data, api_v3_allowed, legacy_api_allowed, payment_url, contact) VALUES
 ('mr_parkit', false, true, false,
  null,
  '{
      "email": "info@mrparkit.com",
      "phone": "+420 277 277 977",
      "web_url": "https://www.mrparkit.com/",
      "term_of_use_url": "https://www.mrparkit.com/cs/pravni-dokumenty"
  }');

alter table parkings_location alter column total_spot_number drop not null;

ALTER TABLE parkings_location DROP CONSTRAINT IF EXISTS parkings_location_fkey;

UPDATE parkings SET source=LOWER(source);
UPDATE parkings SET source='ipr' WHERE source = 'ipr ztp';

UPDATE parkings_location SET source=LOWER(source);
UPDATE parkings_measurements_actual SET source=LOWER(source);
UPDATE parkings_measurements_part SET source=LOWER(source);
UPDATE parkings_tariffs SET source=LOWER(source);

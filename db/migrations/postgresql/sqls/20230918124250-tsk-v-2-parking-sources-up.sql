create table parking_sources (
    "source" varchar(255) NOT NULL,
     open_data bool NOT NULL,
     ipt_allowed bool NOT NULL,
     payment_url varchar(255) NULL,
     contact jsonb NULL,
     CONSTRAINT parking_sources_pkey PRIMARY KEY (source)
);

insert into parking_sources(source, open_data, ipt_allowed, payment_url, contact) VALUES
 ('tsk_v2', false, true,
  'https://ke-utc.appspot.com/static/onstreet.html?shortname={shortNameValue})',
  '{
      "email": "parking@tsk-praha.cz",
      "phone": "+420 257 015 257",
      "web_url": "https://parking.praha.eu/",
      "term_of_use_url": "https://parking.praha.eu/cs/moznosti-parkovani-v-praze/parkovani-v-zonach/"
  }');

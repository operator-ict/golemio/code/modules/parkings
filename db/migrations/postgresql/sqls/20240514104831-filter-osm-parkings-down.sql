DROP VIEW v_parkings_filtered;

DROP INDEX parkings_sanitized_location_idx;

ALTER TABLE
    parkings DROP COLUMN sanitized_location;

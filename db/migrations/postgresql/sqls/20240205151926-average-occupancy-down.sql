DROP PROCEDURE IF EXISTS calculate_average_occupancy();
DROP TABLE IF EXISTS parkings_average_occupancy;
DROP VIEW IF EXISTS v_parkings_latest_measurements;

CREATE INDEX IF NOT EXISTS parkings_measurements_part_idx ON parkings.parkings_measurements_part USING btree (date_modified desc);

DROP VIEW v_parkomats_sales;

CREATE OR REPLACE VIEW v_parkomats_sales
AS SELECT date_part('year'::text, parkomats.ticket_bought) AS year,
    "substring"(split_part(parkomats.parking_zone::text, '-'::text, 1), 2, 2) AS parking_zone,
    'Praha '::text || "substring"(split_part(parkomats.parking_zone::text, '-'::text, 1), 2, 2) AS city_district,
        CASE
            WHEN parkomats.channel::text = 'PARKMACHINE'::text THEN 'Parkovací automat'::text
            ELSE 'VPH'::text
        END AS channel,
    sum(parkomats.price) AS sale
   FROM parkomats
  GROUP BY (date_part('year'::text, parkomats.ticket_bought)), ("substring"(split_part(parkomats.parking_zone::text, '-'::text, 1), 2, 2)), ('Praha '::text || "substring"(split_part(parkomats.parking_zone::text, '-'::text, 1), 2, 2)), (
        CASE
            WHEN parkomats.channel::text = 'PARKMACHINE'::text THEN 'Parkovací automat'::text
            ELSE 'VPH'::text
        END);

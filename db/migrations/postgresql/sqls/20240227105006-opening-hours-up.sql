CREATE TABLE parkings_opening_hours (
	parking_id varchar(255) NOT NULL,
	"source" varchar(255) NOT NULL,
	valid_from timestamptz NOT NULL DEFAULT to_timestamp(0::double precision),
	valid_to timestamptz NULL,
	periods_of_time json NOT NULL,
	created_at timestamptz NULL,
	updated_at timestamptz NULL,
	CONSTRAINT parkings_opening_hours_pk PRIMARY KEY (parking_id, valid_from)
);
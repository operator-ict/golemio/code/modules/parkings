CREATE TABLE parkings_measurements_part_old (
	"source" varchar(255) NOT NULL,
	source_id varchar(255) NOT NULL,
	available_spot_number int4 NULL,
	closed_spot_number int4 NULL,
	occupied_spot_number int4 NULL,
	total_spot_number int4 NULL,
	date_modified int8 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	parking_id varchar(50) NOT NULL,
	CONSTRAINT parkings_measurements_part_pk_old PRIMARY KEY (source, source_id, date_modified)
)
PARTITION BY RANGE (date_modified);
CREATE INDEX parkings_measurements_part_idx_old ON ONLY parkings.parkings_measurements_part_old USING btree (date_modified DESC);

-- parkings.parkings_measurements_part_2021_old definition

CREATE TABLE parkings_measurements_part_2021_old PARTITION OF parkings.parkings_measurements_part_old  FOR VALUES FROM ('1609455600000') TO ('1640991600000');
CREATE INDEX parkings_measurements_part_2021_date_modified_idx ON parkings.parkings_measurements_part_2021_old USING btree (date_modified DESC);


-- parkings.parkings_measurements_part_2022_old definition

CREATE TABLE parkings_measurements_part_2022_old PARTITION OF parkings.parkings_measurements_part_old  FOR VALUES FROM ('1640991600000') TO ('1672527600000');
CREATE INDEX parkings_measurements_part_2022_date_modified_idx ON parkings.parkings_measurements_part_2022_old USING btree (date_modified DESC);


-- parkings.parkings_measurements_part_2023_old definition

CREATE TABLE parkings_measurements_part_2023_old PARTITION OF parkings.parkings_measurements_part_old  FOR VALUES FROM ('1672527600000') TO ('1704063600000');
CREATE INDEX parkings_measurements_part_2023_date_modified_idx ON parkings.parkings_measurements_part_2023_old USING btree (date_modified DESC);


-- parkings.parkings_measurements_part_2024_old definition

CREATE TABLE parkings_measurements_part_2024_old PARTITION OF parkings.parkings_measurements_part_old  FOR VALUES FROM ('1704063600000') TO ('1735686000000');
CREATE INDEX parkings_measurements_part_2024_date_modified_idx ON parkings.parkings_measurements_part_2024_old USING btree (date_modified DESC);


-- parkings.parkings_measurements_part_2025_old definition

CREATE TABLE parkings_measurements_part_2025_old PARTITION OF parkings.parkings_measurements_part_old  FOR VALUES FROM ('1735686000000') TO ('1767222000000');
CREATE INDEX parkings_measurements_part_2025_date_modified_idx ON parkings.parkings_measurements_part_2025_old USING btree (date_modified DESC);


-- parkings.parkings_measurements_part_max_old definition

CREATE TABLE parkings_measurements_part_max_old PARTITION OF parkings.parkings_measurements_part_old  FOR VALUES FROM ('1767222000000') TO (MAXVALUE);
CREATE INDEX parkings_measurements_part_max_date_modified_idx ON parkings.parkings_measurements_part_max_old USING btree (date_modified DESC);


-- parkings.parkings_measurements_part_min_old definition

CREATE TABLE parkings_measurements_part_min_old PARTITION OF parkings.parkings_measurements_part_old  FOR VALUES FROM (MINVALUE) TO ('1609455600000');
CREATE INDEX parkings_measurements_part_min_date_modified_idx ON parkings.parkings_measurements_part_min_old USING btree (date_modified DESC);

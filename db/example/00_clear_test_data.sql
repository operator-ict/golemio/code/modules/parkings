DELETE FROM parkings_tariffs;
DELETE FROM parkings_measurements_actual;
DELETE FROM parkings_measurements_part;
DELETE FROM parkings_average_occupancy;
DELETE FROM parkings_location;
DELETE FROM parkings;
DELETE FROM parkings_opening_hours;
DELETE FROM common.citydistricts where district_name_slug = 'praha-10';
truncate table parkings_business_errors;

truncate table parking_payments;
truncate table parking_sources;
truncate table parking_machines;

TRUNCATE  parking_entrances;



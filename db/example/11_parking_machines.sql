INSERT INTO parking_machines (id,"source",code,"type","location",valid_from,tariff_id,created_at,created_by,updated_at,updated_by,source_id) VALUES
('osm-n_7770679009','osm',NULL,'payment_machine','SRID=4326;POINT (14.4626206 50.0963113)','2024-06-12 11:17:52.422+02',NULL,'2024-06-12 11:17:55.331+02',NULL,'2024-06-12 11:17:55.331+02',NULL,'n_7770679009'),
('osm-n_7770679011', 'osm', NULL, 'payment_machine', 'SRID=4326;POINT (14.4672076 50.0946081)'::public.geometry, '2024-04-08 12:23:44.693', NULL, '2024-04-08 12:23:44.839', NULL, '2024-06-09 16:10:01.618', NULL, 'n_7770679011'),
('tsk_v2-a0e25b4f-83b4-4990-ad29-85f2636da799','tsk_v2','8000069','info_box','SRID=4326;POINT (14.463211568160052 50.09726448399468)','2023-03-27 14:22:55.37+02','3c2178d7-f3c6-4e5d-bbf8-89e9c9af9ee0','2024-06-12 11:17:55.046+02',NULL,'2024-06-12 11:17:55.046+02',NULL,'a0e25b4f-83b4-4990-ad29-85f2636da799'),
('tsk_v2-cbacd85a-9257-4cb1-bc24-4d251e293cd6','tsk_v2','8000068','payment_machine','SRID=4326;POINT (14.463479479257808 50.09713673615595)','2023-03-27 14:22:55.37+02','3c2178d7-f3c6-4e5d-bbf8-89e9c9af9ee0','2024-06-12 11:17:55.046+02',NULL,'2024-06-12 11:17:55.046+02',NULL,'cbacd85a-9257-4cb1-bc24-4d251e293cd6'),
('tsk_v2-ed44bcaa-3efb-411d-867e-51c22c094af0','tsk_v2','8000034','payment_machine','SRID=4326;POINT (14.462620643288227 50.096311253724274)','2023-03-27 14:22:55.37+02','b51b71ad-87ac-41e5-b2ce-a92a66ad45a3','2024-06-12 11:17:55.046+02',NULL,'2024-06-12 11:17:55.046+02',NULL,'ed44bcaa-3efb-411d-867e-51c22c094af0'),
('tsk_v2-fb7f8377-ad5d-4583-b420-f2039022a776','tsk_v2','8000035','payment_machine','SRID=4326;POINT (14.464719680196637 50.0969080143518)','2023-04-19 18:13:06.277+02','3c2178d7-f3c6-4e5d-bbf8-89e9c9af9ee0','2024-06-12 11:17:55.046+02',NULL,'2024-06-12 11:17:55.046+02',NULL,'fb7f8377-ad5d-4583-b420-f2039022a776');

INSERT INTO "parking_machines" (id,"source",code,"type","location",valid_from,tariff_id, source_id) VALUES
('tsk_v2-4521afac-66db-4680-abc5-0006837d977a','tsk_v2','10000088','info_box','SRID=4326;POINT (14.509560372775391 50.08336146182989)','2023-03-27 16:22:55.370','bdd68c37-2462-4825-befa-1eeab518dfbd', '4521afac-66db-4680-abc5-0006837d977a');

INSERT INTO "parking_machines" (id,"source",code,"type","location",valid_from,tariff_id, source_id) VALUES
('korid-praha','korid','10000088','info_box','SRID=4326;POINT (14.464719680196637 50.0969080143518)','2023-03-27 16:22:55.370','bdd68c37-2462-4825-befa-1eeab518dfbd', 'korid-praha'),
('korid-brno','korid','10000088','info_box','SRID=4326;POINT (16.599509 49.1759116)','2023-03-27 16:22:55.370','bdd68c37-2462-4825-befa-1eeab518dfbd', 'korid-brno');

# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.17.2] - 2025-02-11

### Fixed

-   error getAttributesToUpdate with empty array ([parkings#45](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/45))
-   parking and tariff relation for source mr_parkit ([p0255#97](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/97))

## [1.17.1] - 2025-01-23

### Fixed

-   marking of inactive parking spaces for the multiple data sources with the same source identifier ([p0255#96](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/96))

## [1.17.0] - 2025-01-14

### Fixed

-   updated_at for parkings is no longer updated while tariff processing if tariff_id stays null ([p0255#89](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/89))

### Changed

-   id creation for OSM parking spaces ([p0255#88](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/88))

## [1.16.4] - 2025-01-07

### Changed

-   ordering of arrays in v3 endpoints ([p0255#94](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/94))

## [1.16.3] - 2024-12-10

### Added

-   mark parkings that did not appear in last source data as inactive ([p0255#91](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/91))

## [1.16.2] - 2024-12-03

### Fixed

-   Exit when no data to process (ParkingsWorker, TSK source)

## [1.16.1] - 2024-11-26

### Fixed

-   OSM entrances entrance_type ([parkings#43](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/43))

## [1.16.0] - 2024-11-14

### Changed

-   photon extended search ([p0255#66](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/66))
-   change parking adress updates mechanism ([p0255#85](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/85))

### Removed

-   unused redis dependency

## [1.15.3] - 2024-11-05

### Added

-   AsyncAPI documentation ([integration-engine#266](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/266))

## [1.15.2] - 2024-10-22

### Fixed

-   Skip Bedrichov historic data validation ([parkings#44](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/44))

## [1.15.1] - 2024-10-15

### Changed

-   validate geometry of osm and iptoict data ([p0255#84](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/84))

### Fixed

-   ISPHK measurements ([p0255#83](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/83))
-   Use sanitized location in v2 output ([parkings#42](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/42))

## [1.15.0] - 2024-09-26

### Added

-   IPT OICT manual tariffs generalization using source table ([p0255#68](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/68))

## [1.14.0] - 2024-09-24

### Changed

-   Change parkings filtering in v_parkings_filtered ([p0255#78](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/78))

### Fixed

-   swagger component title ([p0255#81](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/81))

## [1.13.5] - 2024-09-12

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [1.13.4] - 2024-09-10

### Added

-   Bedrichov measurements integration ([p0255#80](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/80))

### Removed

-   temporary hotfix introduced in 1.10.1

## [1.13.3] - 2024-09-05

### Changed

-   logging photon

## [1.13.2] - 2024-08-27

### Fixed

-   filter invalid TSK parkigns & tariffs ([p0255#79](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/79))

### Added

-   add `activeOnly` filter to filter out spaces with zero spot number ([p0255#7](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/77))

## [1.13.1] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.13.0] - 2024-08-13

### Added

-   add capacity to ipt oict manual parking source ([p0255#75](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/75))
-   Check disallowed datasources in IE to prevent accidental conflicts ([p0255#73](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/73))

## [1.12.0] - 2024-08-06

### Added

-   add cache-control headers to v1 router ([core#107](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/107))

## [1.11.0] - 2024-08-01

### Added

-   /v3/parking-tariffs endpoint extension ([p0255#46](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/69))
-   IPT OICT manual tariffs for ISPHK ([p0255#67](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/67))

## [1.10.2] - 2024-07-25

### Added

-   add cache-control header to all responses ([core#107](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/107))

### Removed

-   remove redis useCacheMiddleware ([core#107](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/107))

## [1.10.1] - 2024-07-23

### Fixed

-   db timestamp "race condition" while save & delete ([p0255#46](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/46))
-   hotfix to prevent unknown parameter 3rd party failure when accessing /v2/measurements

## [1.10.0] - 2024-07-17

### Changed

-   limit the geographic area by the region of interest ([p0255#62](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/62))

### Added

-   PMDP measurements ([p0255#48](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/48))
-   filtration by parking_policy for v3/parking endpoint ([p0255#70](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/70))

### Fixed

-   open api description for GET /v3/parking-sources

## [1.9.6] - 2024-07-04

### Added

-   `GET /parking-sources` v3 API ([p0255#64](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/64))

## [1.9.5] - 2024-06-24

### Changed

-   entrance prohibitions are now managed universally via source ([p0255#65](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/65))

## [1.9.4] - 2024-06-19

### Changed

-   Filter secondary non-overlapping parking machines in V3 API ([p0255#46](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/46))

### Fixed

-   API docs ([p0255#55](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/55))

## [1.9.3] - 2024-06-17

### Changed

-   open data documentation for v3 endpoints
-   fixing open-data badge ([p0181#14](https://gitlab.com/operator-ict/golemio/projekty/oict/parkings/-/issues/14))

## [1.9.2] - 2024-06-05

### Changed

-   introduction of new param isRestrictedToOpenData to switch in v3 api between open data and all. ([p0255#63](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/63))

## [1.9.1] - 2024-05-29

### Changed

-   Tables parking_machines, tariffs and locations not updating on Rabin ([p0255#61](hhttps://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/61))

## [1.9.0] - 2024-05-27

### Changed

-   Include non-overlapping (in terms of locations of primary source parkings) secondary source parkings in V3 API ([p0255#46](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/46))

## [1.8.15] - 2024-05-20

### Added

-   Add `parking_type` attribute to `/v3/parking` ([p0255#56](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/56))
-   Add `parking_policy` for `tsk_v2` source ([p0255#59](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/59))

### Fixed

-   Move filter attribute to parking level (`/v3/parking`) ([p0255#60](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/60))

## [1.8.14] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

### Fixed

-   Filter sources properly on v2 endpoints ([p0255#57](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/57))

## [1.8.13] - 2024-05-06

### Removed

-   Remove `parkings_measurements_part_old` table (https://gitlab.com/operator-ict/golemio/projekty/oict/parkings/-/issues/13)

## [1.8.12] - 2024-04-29

### Added

-   Entrances downloaded via url stored in sources ([p0255#58](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/58))

## [1.8.11] - 2024-04-24

### Changed

-   Refactor parkomats worker + fixing parkingsessionshistory api calls ([parkings#41](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/41))

### Added

-   Integration of parking prohibitions ([p0255#35](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/35))

### Changed

-   Refactor parkomats worker + fixing parkingsessionshistory api calls ([parkings#41](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/41))

## [1.8.10] - 2024-04-15

### Added

-   Entrances downloaded via url stored in sources ([p0255#58](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/58))
-   Parking machines locations from OSM ([p0255#43](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/43))

## [1.8.9] - 2024-04-10

### Added

-   Integration of parking payments ([p0255#33](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/33))

## [1.8.8] - 2024-04-08

### Changed

-   replace axios for native fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

### Fixed

-   API docs inconsistencies ([parkings#328](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/merge_requests/328))

## [1.8.7] - 2024-04-03

### Added

-   Adding opening hours to the output gateway ([p0255#32](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/32))

## [1.8.6] - 2024-03-27

### Changed

-   Generic IPT OICT data sources ([parkings#309](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/merge_requests/309))

## [1.8.5] - 2024-03-25

### Fixed

-   Fix zoneType & parkingType parameters at endpoint /v2/parking ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.8.4] - 2024-03-20

### Fixed

-   Hotfix for endpoint /v2/parking and category parameter ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.8.3] - 2024-03-20

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

### Added

-   integration of parking entrances ([p0255#42](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/42))

### Removed

-   Remove `primarySourceId` filter from EPs `/v3/parking`, `/v3/parking-tariffs` and `/v3/parking-machines` ([p0255#50](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/50))

## [1.8.2] - 2024-03-18

### Added

-   integration of opening hours for osm and hradec datasources ([p0255#49](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/49))

## [1.8.1] - 2024-03-06

### Added

-   Hradec and iptoict parking data integration ([p0255#30](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/30))

## [1.8.0] - 2024-03-04

### Fixed

-   `/v3/parking-tariffs` limit fix

## [1.7.11] - 2024-02-28

### Added

-   Measurements API for v3 router ([p0255#47](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/47))

### Fixed

-   `/v3/parking/:id` id to correspond to `id` instead of `source_id`

## [1.7.10] - 2024-02-21

### Fixed

-   Distinguish known and unknown special access state ([p0255#29](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/29))

## [1.7.9] - 2024-02-19

### Changed

-   Update express validator to 7.0.1 ([core#94](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/94))

## [1.7.8] - 2024-02-14

### Added

-   OSM parking data integration ([p0255#16](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/16))

## [1.7.7] - 2024-01-29

### Changed

-   ArcGIS datasource replaced with paginated strategy ([general#530](https://gitlab.com/operator-ict/golemio/code/general/-/issues/530))

### Added

-   new attributes for Parking entities: `security` and `dimensions` ([p0255#24](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/24))

## [1.7.6] - 2024-01-22

### Added

-   Smart4City parking integration [p0255#14](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/14)

## [1.7.5] - 2024-01-15

### Changed

-   hotfix for outdated parking locations ([p0255#31](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/31))

## [1.7.4] - 2024-01-08

### Added

-   Mr. Parkit tariffs ([p0255#20](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/28))

## [1.7.3] - 2023-12-13

### Fixed

-   IPR ArcGIS datasource ([parkings#39](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/39))

## [1.7.2] - 2023-12-06

### Added

-   Glob min/max for tsk_v2 tariff integration. ([p0255#20](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/20))

## [1.7.1] - 2023-11-27

-   No changelog

## [1.7.0] - 2023-11-22

### Added

-   Add KORID locations to enable it on v3 output gateway ([o0255#11](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/11))
-   Add parking sources input api ([p0255#21](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/21))
-   Add TSK v2 sources integration ([p0255#18](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/18))
-   Add v3 output gateway ([p0255#17](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/17))
-   MR.PARKIT garages integration ([p0255#15](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/15))

## [1.6.1] - 2023-11-15

### Changed

-   Removing schema definitions ([schema-definitions#55](https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions/-/issues/55))

## [1.6.0] - 2023-09-27

### Added

-   API versioning - abstract router with version configuration ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.5.10] - 2023-08-30

-   No changelog

## [1.5.9] - 2023-08-23

### Changed

-   Run integration apidocs tests via Golemio CLI ([core#46](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/46))

## [1.5.8] - 2023-07-31

### Fixed

-   debug console log removed

## [1.5.7] - 2023-07-31

### Fixed

-   Hotfix DateTime.format pattern string

## [1.5.5] - 2023-07-31

### Changed

-   Replace moment with DateTime wrapper ([core#68](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/68))

## [1.5.4] - 2023-07-10

### Changed

-   `date_modified` bigint to timestamptz ([parkings#31](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/31))

## [1.5.3] - 2023-06-21

### Added

-   Input gateway API docs ([parkings#38](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/38))

## [1.5.2] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.5.1] - 2023-06-07

-   Improve docs and add Portman tests ([IPT#98](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/98))

## [1.5.0] - 2023-06-05

### Fixed

-   Error imports ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.4.14] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.4.13] - 2023-05-29

### Changed

-   Update openapi docs

## [1.4.12] - 2023-05-24

### Added

-   Add index on measurements table to speed selects up

### Removed

-   Chore: Removed unused method from ParkingsMeasurementsModels

## [1.4.11] - 2023-05-18

### Fixed

-   hotfix transformation date creation

## [1.4.10] - 2023-05-15

### Added

-   Business rules to check whether tsk tariffs are valid ([parkings#37](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/37))

## [1.4.9] - 2023-05-03

### Changed

-   Adjust IG logger emitter imports

## [1.4.8] - 2023-04-19

### Changed

-   Parking zones have centroid as point on feature (polygon) instead of real centroid ([p0131#126](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/126))

## [1.4.7] - 2023-04-12

### Changed

-   IPR parking zones validation change ([p0131#127](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/127))

## [1.4.6] - 2023-03-29

### Fixed

-   Set parking_id attribute as required

## [1.4.5] - 2023-03-13

### Added

-   Add static geo datasource for parking lots (Points -> MultiPolygon) ([p0131#106](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/106))

### Changed

-   Use MinPrice column for calculating prices (fractions of hour) at TSK parking zones
-   update of mongooose based validations to JSON Schema

### Fixed

-   Delete TSK tariffs before saving to prevent mixups
-   Allow TSK tariff pricePerHour to be 0

## [1.4.4] - 2023-03-08

### Fixed

-   Fix TSK legacy test data

## [1.4.3] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.4.2] - 2023-02-22

### Fixed

-   Date params of legacy TSK routes ([parkings#35](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/35))

## [1.4.1] - 2023-02-20

### Changed

-   Enrich parking addresses asynchronously ([p0131#111](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/111))

## [1.4.0] - 2023-02-06

### Changed

-   Migrate legacy TSK routes to PSQL ([parkings#33](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/33))

## [1.3.1] - 2023-02-01

### Added

-   Add IPT static tariffs data source processing ([parkings#2é4](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/104))

### Changed

-   Make date_modified required field and use it for filtering ([parkings#29](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/29))

## [1.3.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.2.6] - 2023-01-16

### Added

-   added column for view v_parkomats_sales ([parkings#25](https://gitlab.com/operator-ict/golemio/projekty/tsk/zps-platebni-transakce/-/issues/25))

### Changed

-   unite source names ([parkings#27](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/27))

### Removed

-   removed `X-Total-Count` header from `/parking/measurements` endpoint

## [1.2.5] - 2023-01-04

### Changed

-   OG /parkings/measurements:
    -   replaced `page`, `pageSize` params with `limit`, `offset` for consistency

### Fixed

-   zoneType and parkingType filter api documentation fix

### Added

-   Add filters zone_type and parking_type to /parking/detail/ ([ipt#77](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/77))

## [1.2.4] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.2.3] - 2022-10-11

### Changed

-   Changed parkings id to uuid string

## [1.2.2] - 2022-08-24

### Added

-   new filters updatedSince a minutesBefore for /parking and /parking/detail routes [intermodal#63](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/63)
-   new filters updatedSince a minutesBefore for measurements [intermodal#60](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/60)

## [1.2.1] - 2022-07-13

### Changed

-   Move database migrations from schema-definitions (schema public) to the module (schema waze_ccp) ([parkomats#3](https://gitlab.com/operator-ict/golemio/code/modules/parkomats/-/issues/3))

### Fixed

-   Ordering on paginated requests

## [1.2.0] - 2022-06-30

### Fixed

-   Open API `/tariffs` `charge_type` type and API doc ([parkings#15](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/15))

### Added

-   Add `latest` query param to Parking Spaces measurement endpoint ([intermodal#56](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/56))
-   Detailed Parking Spaces endpoint ([intermodal#52](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/52))
-   Add ZTP parkings ([intermodal#34](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/34))

### Changed

-   Merge parkomats with parkings ([parkomats#3](https://gitlab.com/operator-ict/golemio/code/modules/parkomats/-/issues/3))

## [1.1.0] - 2022-05-18

### Added

([parkings#9](https://gitlab.com/operator-ict/golemio/projekty/oict/parkings/-/issues/9))

-   IE: `parking_type`, `zone_type`, `centroid`, `android_app_payment_url`, `ios_app_payment_url` to public.parkings table.
    -   For Korid set:
        -   parking_type: `"other"`,
        -   zone_type: `null`
    -   For ZPS TSK set:
        -   parking_type: `"on_street"`
        -   accepts_litacka: `true`
    -   Added centroid calculation for all parkings (using turf npm pkg.)
    -   Payment methods where extended by `accepts_litacka: bool`, added to public.parkings_tariffs table.
    -   `payment_url` attribute renamed to `web_app_payment_url`
    -   Throttle process (using p-throttle npm pkg) was deleted
-   IE: created table parkings_measurements_actual with model `parkingsMeasurementsActualModel` (savingType: "insertOrUpdate").
-   IE: New method of GeocodeApi (CORE module) to get address: `getAddressByLatLngFromPhoton`.
-   OG /parking:
    -   `parking_type`,
    -   `zone_type`,
    -   `centroid`,
    -   `android_app_payment_url`,
    -   `ios_app_payment_url`
    -   `available_spots_number`
    -   `available_spots_last_updated`
    -   Model get data `available_spots_number` and `available_spots_last_updated` from new table `parkings_measurements_actual` now.
-   OG /parkings/tariffs:
    -   new enum `payment_methods`
        -   in output data it looks like array of accepted methods: `payment_methods: ["cash", "litacka"]`
    -   new version of enum `charge_type`
    -   `reservation_url`
-   OG /parkings/measurements:
    -   deleted params `limit`, `offset`
    -   added params `page`, `pageSize`
    -   new resp headers `X-Total-Count` and `Link` counted based on `page`, `pageSize` values.
    -   added 400 err resp with extended description from validator.
-   Implementation documentation

### Fixed

-   Open API documentation
-   json schema validation allows null for ulice property

## [1.0.12] - 2022-03-06

### Changed

-   New PK and table name for parking measurements.

## [1.0.11] - 2022-02-16

### Fixed

-   Empty polygons ([parkings#8](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/8))

## [1.0.9] - 2021-11-30

### Added

-   Category query filter ([parkings#2](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/2))
-   Parking zones prague payment_url ([parkings#4](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/4))


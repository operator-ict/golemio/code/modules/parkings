import { Point } from "@golemio/core/dist/shared/geojson";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IParkingMachine } from "./interfaces/IParkingMachine";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";

export class ParkingMachinesModel extends Model<IParkingMachine> implements IParkingMachine {
    public static tableName = "parking_machines";

    declare id: string;
    declare source: string;
    declare sourceId: string;
    declare code: string;
    declare type: string;
    declare location: Point;
    declare validFrom: Date;
    declare tariffId: string;

    public static attributeModel: ModelAttributes<ParkingMachinesModel> = {
        id: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        source: DataTypes.STRING,
        sourceId: DataTypes.STRING,
        code: DataTypes.STRING,
        type: DataTypes.STRING,
        location: DataTypes.GEOMETRY,
        validFrom: DataTypes.DATE,
        tariffId: DataTypes.STRING,
    };

    public static attributeList = Object.keys(ParkingMachinesModel.attributeModel);

    public static jsonSchema: JSONSchemaType<IParkingMachine[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "string" },
                source: { type: "string" },
                sourceId: { type: "string" },
                code: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                type: { type: "string" },
                location: { $ref: "#/definitions/geometry" },
                validFrom: { type: "object" },
            },
            required: ["id", "source", "sourceId", "type", "location"],
        },
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}

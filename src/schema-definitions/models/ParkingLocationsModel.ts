import { IAddress } from "#sch/models/interfaces/IAddress";
import { IParkingLocation } from "#sch/models/interfaces/IParkingLocation";
import { Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IGeoCoordinatesPoint, TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { DataTypes } from "@golemio/core/dist/shared/sequelize";

export class ParkingLocationsModel extends Model<ParkingLocationsModel> implements IParkingLocation {
    public static tableName = "parkings_location";

    declare id: string;
    declare source: string;
    declare source_id: string;
    declare data_provider: string;
    declare location: TGeoCoordinates;
    declare centroid: IGeoCoordinatesPoint | null;
    declare address: IAddress | null;
    declare total_spot_number: number | null;
    declare special_access: string[];

    public static attributeModel: ModelAttributes<ParkingLocationsModel> = {
        id: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        source: DataTypes.STRING,
        source_id: DataTypes.STRING,
        data_provider: DataTypes.STRING,
        location: DataTypes.GEOMETRY,
        centroid: DataTypes.GEOMETRY,
        address: DataTypes.JSON,
        total_spot_number: DataTypes.INTEGER,
        special_access: DataTypes.ARRAY(DataTypes.STRING(50)),
    };

    public static attributeList = Object.keys(ParkingLocationsModel.attributeModel);
}

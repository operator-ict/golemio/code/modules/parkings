import { TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions/SharedSchemaProvider";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IEntrance } from "./interfaces/IEntrance";

export class EntrancesModel extends Model<IEntrance> implements IEntrance {
    public static tableName = "parking_entrances";

    declare entrance_id: string;
    declare source: string;
    declare parking_id: string;
    declare location: TGeoCoordinates;
    declare entry: boolean | null;
    declare exit: boolean | null;
    declare entrance_type: string[] | null;
    declare level: number | null;
    declare max_height: number | null;
    declare max_width: number | null;
    declare max_length: number | null;
    declare max_weight: number | null;

    public static attributeModel: ModelAttributes<EntrancesModel> = {
        entrance_id: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        parking_id: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        source: {
            type: DataTypes.STRING,
        },
        location: DataTypes.GEOMETRY,
        entry: DataTypes.BOOLEAN,
        exit: DataTypes.BOOLEAN,
        entrance_type: DataTypes.ARRAY(DataTypes.STRING),
        level: DataTypes.FLOAT,
        max_height: DataTypes.FLOAT,
        max_width: DataTypes.FLOAT,
        max_length: DataTypes.FLOAT,
        max_weight: DataTypes.FLOAT,
    };

    public static jsonSchema: JSONSchemaType<IEntrance[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                entrance_id: { type: "string" },
                parking_id: { type: "string" },
                source: { type: "string" },
                entry: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
                exit: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
                level: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                max_height: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                max_width: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                max_length: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                max_weight: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                location: { $ref: "#/definitions/geometry" },
                entrance_type: {
                    oneOf: [
                        {
                            type: "array",
                            items: {
                                type: "string",
                            },
                        },
                        { type: "null", nullable: true },
                    ],
                },
            },
            required: ["entrance_id", "source", "parking_id"],
            additionalProperties: false,
        },
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}

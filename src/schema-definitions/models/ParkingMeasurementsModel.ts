import { IParkingMeasurements } from "#sch/models/interfaces/IParkingMeasurements";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

export class ParkingMeasurementsModel extends Model<IParkingMeasurements> implements IParkingMeasurements {
    public static tableName = "parkings_measurements_part";

    declare source: string;
    declare source_id: string;
    declare available_spot_number: number;
    declare closed_spot_number: number;
    declare occupied_spot_number: number;
    declare total_spot_number: number;
    declare date_modified: Date;
    declare parking_id: string;

    public static attributeModel: ModelAttributes<ParkingMeasurementsModel> = {
        source: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        source_id: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        available_spot_number: DataTypes.INTEGER,
        closed_spot_number: DataTypes.INTEGER,
        occupied_spot_number: DataTypes.INTEGER,
        total_spot_number: DataTypes.INTEGER,
        date_modified: DataTypes.DATE,
        parking_id: DataTypes.STRING,
    };

    public static rawAttributeModel = Object.keys(ParkingMeasurementsModel.attributeModel);
}

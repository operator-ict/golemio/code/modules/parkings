import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ITskDistrictsModel } from "./interfaces/ITskDistrictsModel";

export class TskDistrictsModel extends Model<TskDistrictsModel> implements ITskDistrictsModel {
    public static tableName = "v_tsk_districts";

    declare source_id: string;
    declare district?: string;

    public static attributeModel: ModelAttributes<TskDistrictsModel> = {
        source_id: {
            type: DataTypes.TEXT,
            unique: true,
        },
        district: DataTypes.TEXT,
    };
}

export interface IParkingProhibitions {
    parking_id: string;
    source: string;
    lpg?: boolean | null;
    bus?: boolean | null;
    truck?: boolean | null;
    motorcycle?: boolean | null;
    bicycle?: boolean | null;
    trailer?: boolean | null;
}

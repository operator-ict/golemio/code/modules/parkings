import { TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";

export interface IEntrance {
    entrance_id: string;
    source: string;
    parking_id: string;
    location: TGeoCoordinates;
    entry: boolean | null;
    exit: boolean | null;
    entrance_type: string[] | null;
    level: number | null;
    max_height: number | null;
    max_width: number | null;
    max_length: number | null;
    max_weight: number | null;
}

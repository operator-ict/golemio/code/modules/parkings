export interface IPayment {
    parking_id: string;
    source: string;
    payment_web_url?: string | null;
    payment_android_url?: string | null;
    payment_ios_url?: string | null;
    payment_discovery_url?: string | null;
    reservation_type?: string | null;
    reservation_web_url?: string | null;
    reservation_android_url?: string | null;
    reservation_ios_url?: string | null;
    reservation_discovery_url?: string | null;
}

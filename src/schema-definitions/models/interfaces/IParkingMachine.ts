import { Geometry } from "@golemio/core/dist/shared/geojson";

export interface IParkingMachine {
    id: string;
    source: string;
    sourceId: string;
    code: string | null;
    type: string;
    location: Geometry;
    validFrom: Date;
    tariffId: string | null;
}

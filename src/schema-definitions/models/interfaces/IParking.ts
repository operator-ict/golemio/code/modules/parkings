import { IPayment } from "#sch/models/interfaces/IPayment";
import { IGeoCoordinatesPoint, TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { IAddress } from "./IAddress";
import { IParkingLocation } from "./IParkingLocation";
import { IOpeningHours } from "./openingHours/IOpeningHours";
import { IParkingProhibitions } from "./IParkingProhibitions";

export interface IParkingWithLocationAndOccupancyInfo extends IParking {
    parking_locations: IParkingLocation[];
    parking_average_occupancy: boolean;
    parking_hours: IOpeningHours[];
    parking_entrances: IParkingEntrance[];
    parking_payments: IPayment;
    parking_prohibitions: IParkingProhibitions;
}

export interface IParking {
    id: string;
    source: string;
    source_id: string;
    data_provider: string;
    date_modified: Date;
    location: TGeoCoordinates;
    address: IAddress | null;
    name: string | null;
    area_served: string | null;
    total_spot_number: number | null;
    category: string | null;
    tariff_id: string | null;
    valid_from: Date | null;
    valid_to: Date | null;
    parking_type: string;
    zone_type: string | null;
    centroid: IGeoCoordinatesPoint | null;
    security: boolean | null;
    max_vehicle_dimensions: number[] | null; // height(m), width(m), length(m), weight(kg)
    updated_at: Date | null;
    covered: boolean | null;
    contact: IContact | null;
    parking_policy: string | null;
    sanitized_location: TGeoCoordinates;
    area: number;
    address_updated_at: Date | null;
    active: boolean;
}

export interface IContact {
    email?: string | null;
    phone?: string | null;
    website?: string | null;
}

export interface IParkingEntrance {
    entrance_id: string;
    parking_id: string;
    location: TGeoCoordinates;
    entry: boolean | null;
    exit: boolean | null;
    entrance_type: string[] | null;
    level: string | null;
    max_height: string | null;
    max_width: string | null;
    max_length: string | null;
    max_weight: string | null;
}

export interface IParkingSourcePayment {
    web_url: string | null;
    android_url: string | null;
    ios_url: string | null;
    discovery_url: string | null;
}

export interface IParkingSourceReservation extends IParkingSourcePayment {
    type: string | null;
}

export interface IParkingSource {
    source: string;
    name: string | null;
    open_data: boolean;
    api_v3_allowed: boolean;
    legacy_api_allowed: boolean;
    payment: IParkingSourcePayment | null;
    reservation: IParkingSourceReservation | null;
    contact: object | null;
    datasource_parking: string | null;
    datasource_locations: string | null;
    datasource_payments: string | null;
    datasource_entrances: string | null;
    datasource_prohibitions: string | null;
    datasource_tariffs: string | null;
}

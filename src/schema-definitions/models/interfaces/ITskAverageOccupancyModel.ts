export type AverageOccupancy = {
    [K in string]: Record<string, number>;
};

export interface ITskAverageOccupancyModel {
    source_id: string;
    average_occupancy: AverageOccupancy;
}

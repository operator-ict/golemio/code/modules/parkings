export interface IParkingTariffDescription {
    tariff_id: string;
    source: string;
    last_updated: Date | string;
    payment_mode: string;
    payment_additional_description?: string | null;
    free_of_charge: boolean;
    url_link_address?: string | null;
}

export interface IParkingTariffMeansOfPayment {
    accepts_payment_card?: boolean | null;
    accepts_cash?: boolean | null;
    accepts_mobile_payment?: boolean | null;
    accepts_litacka?: boolean | null;
}

interface IParkingTariffChargeBand extends IParkingTariffMeansOfPayment {
    charge_band_name: string;
    charge_currency: string;
}

export interface IParkingTariffCharge {
    charge: number;
    charge_type?: string | null;
    charge_order_index: number;
    charge_interval?: number | null;
    max_iterations_of_charge?: number | null;
    min_iterations_of_charge?: number | null;
    start_time_of_period?: string | null;
    end_time_of_period?: string | null;
}

export interface IParkingTariffPeriod {
    day_in_week: string;
    start: string;
    end: string;
    ph: string;
}

export interface IParkingTariff extends IParkingTariffDescription, IParkingTariffChargeBand, IParkingTariffCharge {
    allowed_vehicle_type?: string | null;
    allowed_fuel_type?: string | null;
    valid_from?: Date | string | null;
    valid_to?: Date | string | null;
    maximum_duration_seconds?: number | null;
    periods_of_time?: IParkingTariffPeriod[] | null;
    payment_methods?: string[] | null;
}

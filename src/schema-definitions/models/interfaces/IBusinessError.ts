export interface IBusinessError {
    identifier: string;
    provider: string;
    record_date: string;
    type: string;
    detail: object;
}

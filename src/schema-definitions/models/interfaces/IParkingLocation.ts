import { IGeoCoordinatesPoint, TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { IAddress } from "./IAddress";

export interface IParkingLocation {
    id: string;
    data_provider: string;
    location: TGeoCoordinates;
    address?: IAddress | null;
    centroid?: IGeoCoordinatesPoint | null;
    total_spot_number: number | null;
    source: string;
    source_id: string;
    special_access?: string[] | null;
}

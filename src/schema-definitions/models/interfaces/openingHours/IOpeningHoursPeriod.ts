export interface IOpeningHoursPeriod {
    day_in_week: string;
    start: string;
    end: string;
    ph: string;
    maximum_duration: number | null; // in minutes
    disc_parking: boolean | null;
}

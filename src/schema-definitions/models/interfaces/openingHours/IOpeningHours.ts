import { IOpeningHoursPeriod } from "./IOpeningHoursPeriod";

export interface IOpeningHours {
    parking_id: string;
    source: string;
    valid_from: string;
    valid_to: string | null;
    periods_of_time: IOpeningHoursPeriod[];
}

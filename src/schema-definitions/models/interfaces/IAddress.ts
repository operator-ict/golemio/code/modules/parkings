export interface IAddress {
    address_formatted: string;
    street_address?: string;
    postal_code?: string;
    address_locality?: string;
    address_region?: string;
    address_country: string;
    house_number?: string;
}

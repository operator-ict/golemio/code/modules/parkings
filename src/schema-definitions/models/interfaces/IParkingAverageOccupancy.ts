export interface IParkingAverageOccupancy {
    parking_id: string;
    source: string;
    source_id: string;
    day_of_week: number;
    hour: number;
    average_occupancy: number;
    record_count: number;
    last_updated: Date;
}

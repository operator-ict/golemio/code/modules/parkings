export interface IParkingMeasurements {
    source: string;
    source_id: string;
    available_spot_number: number;
    closed_spot_number: number;
    occupied_spot_number: number;
    total_spot_number: number;
    date_modified: Date;
    parking_id: string;
}

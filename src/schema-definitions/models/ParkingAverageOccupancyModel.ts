import { IParkingAverageOccupancy } from "#sch/models/interfaces/IParkingAverageOccupancy";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

export class ParkingAverageOccupancyModel extends Model<IParkingAverageOccupancy> implements IParkingAverageOccupancy {
    public static tableName = "parkings_average_occupancy";

    declare parking_id: string;
    declare source: string;
    declare source_id: string;
    declare day_of_week: number;
    declare hour: number;
    declare average_occupancy: number;
    declare record_count: number;
    declare last_updated: Date;

    public static attributeModel: ModelAttributes<ParkingAverageOccupancyModel> = {
        parking_id: DataTypes.STRING,
        source: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        source_id: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        day_of_week: {
            primaryKey: true,
            type: DataTypes.SMALLINT,
        },
        hour: {
            primaryKey: true,
            type: DataTypes.SMALLINT,
        },
        average_occupancy: DataTypes.DOUBLE,
        record_count: DataTypes.INTEGER,
        last_updated: DataTypes.DATE,
    };

    public static rawAttributesModel = Object.keys(ParkingAverageOccupancyModel.attributeModel).filter(
        (el) => !["hour", "average_occupancy"].includes(el)
    );
}

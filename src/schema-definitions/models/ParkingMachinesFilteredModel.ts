import { ParkingMachinesModel } from "#sch/models/ParkingMachinesModel";

export class ParkingMachinesFilteredModel extends ParkingMachinesModel {
    public static tableName = "v_parking_machines_filtered";
}

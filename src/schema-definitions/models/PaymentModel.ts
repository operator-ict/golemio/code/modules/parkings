import { IPayment } from "#sch/models/interfaces/IPayment";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

export class PaymentModel extends Model<PaymentModel> implements IPayment {
    public static tableName = "parking_payments";

    declare parking_id: string;
    declare source: string;
    declare payment_web_url: string | null;
    declare payment_android_url: string | null;
    declare payment_ios_url: string | null;
    declare payment_discovery_url: string | null;
    declare reservation_type: string | null;
    declare reservation_web_url: string | null;
    declare reservation_android_url: string | null;
    declare reservation_ios_url: string | null;
    declare reservation_discovery_url: string | null;

    public static attributeModel: ModelAttributes<PaymentModel> = {
        parking_id: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        source: DataTypes.STRING,
        payment_web_url: DataTypes.STRING,
        payment_android_url: DataTypes.STRING,
        payment_ios_url: DataTypes.STRING,
        payment_discovery_url: DataTypes.STRING,
        reservation_type: DataTypes.STRING,
        reservation_web_url: DataTypes.STRING,
        reservation_android_url: DataTypes.STRING,
        reservation_ios_url: DataTypes.STRING,
        reservation_discovery_url: DataTypes.STRING,
    };

    public static jsonSchema: JSONSchemaType<IPayment[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                parking_id: { type: "string" },
                source: { type: "string" },
                payment_web_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                payment_android_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                payment_ios_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                payment_discovery_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                reservation_type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                reservation_web_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                reservation_android_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                reservation_ios_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                reservation_discovery_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
            },
            required: ["parking_id", "source"],
            additionalProperties: false,
        },
    };

    public static attributeUpdateList = Object.keys(PaymentModel.attributeModel).concat("updated_at");
}

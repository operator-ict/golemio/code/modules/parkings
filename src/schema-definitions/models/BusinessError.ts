import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IBusinessError } from "./interfaces/IBusinessError";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export class BusinessError extends Model<BusinessError, IBusinessError> implements IBusinessError {
    declare identifier: string;
    declare provider: string;
    declare record_date: string;
    declare type: string;
    declare detail: object;

    public static attributeModel: ModelAttributes<BusinessError> = {
        identifier: { type: DataTypes.STRING, primaryKey: true },
        provider: { type: DataTypes.STRING, primaryKey: true },
        record_date: { type: DataTypes.DATEONLY, primaryKey: true },
        type: DataTypes.STRING,
        detail: DataTypes.JSON,
    };

    public static jsonSchema: JSONSchemaType<IBusinessError[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                identifier: { type: "string" },
                provider: { type: "string" },
                record_date: { type: "string", format: "date" },
                type: { type: "string" },
                detail: { type: "object" },
            },
            required: ["identifier", "provider", "record_date", "type", "detail"],
            additionalProperties: false,
        },
    };
}

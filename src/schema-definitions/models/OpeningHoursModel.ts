import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IOpeningHours } from "./interfaces/openingHours/IOpeningHours";
import { IOpeningHoursPeriod } from "./interfaces/openingHours/IOpeningHoursPeriod";

export class OpeningHoursModel extends Model<IOpeningHours> implements IOpeningHours {
    public static tableName = "parkings_opening_hours";

    declare parking_id: string;
    declare source: string;
    declare valid_from: string;
    declare valid_to: string | null;
    declare periods_of_time: IOpeningHoursPeriod[];

    public static attributeModel: ModelAttributes<OpeningHoursModel> = {
        parking_id: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        source: {
            type: DataTypes.STRING,
        },
        valid_from: {
            primaryKey: true,
            type: DataTypes.DATE,
        },
        valid_to: {
            type: DataTypes.DATE,
        },
        periods_of_time: DataTypes.JSON,
    };

    public static jsonSchema = {
        type: "array",
        items: {
            type: "object",
            properties: {
                parking_id: { type: "string" },
                source: { type: "string" },
                valid_from: { type: "string" },
                valid_to: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                periods_of_time: {
                    type: "array",
                    items: {
                        type: "object",
                        properties: {
                            day_in_week: { type: "string" },
                            start: { type: "string" },
                            end: { type: "string" },
                            ph: { type: "string" },
                            maximum_duration: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                            disc_parking: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
                        },
                        required: ["day_in_week", "start", "end", "ph"],
                    },
                },
            },
            required: ["parking_id", "source", "valid_from", "periods_of_time"],
        },
    };
}

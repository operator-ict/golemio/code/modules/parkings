import { IPayment } from "#sch/models/interfaces/IPayment";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IParkingProhibitions } from "./interfaces/IParkingProhibitions";

export class ParkingProhibitionsModel extends Model<IParkingProhibitions> implements IPayment {
    public static tableName = "parking_prohibitions";

    declare parking_id: string;
    declare source: string;
    declare lpg: boolean | null;
    declare bus: boolean | null;
    declare truck: boolean | null;
    declare motorcycle: boolean | null;
    declare bicycle: boolean | null;
    declare trailer: boolean | null;

    public static attributeModel: ModelAttributes<ParkingProhibitionsModel> = {
        parking_id: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        source: DataTypes.STRING,
        lpg: DataTypes.BOOLEAN,
        bus: DataTypes.BOOLEAN,
        truck: DataTypes.BOOLEAN,
        motorcycle: DataTypes.BOOLEAN,
        bicycle: DataTypes.BOOLEAN,
        trailer: DataTypes.BOOLEAN,
    };

    public static jsonSchema: JSONSchemaType<IParkingProhibitions[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                parking_id: { type: "string" },
                source: { type: "string" },
                lpg: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }], nullable: true },
                bus: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }], nullable: true },
                truck: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }], nullable: true },
                motorcycle: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }], nullable: true },
                bicycle: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }], nullable: true },
                trailer: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }], nullable: true },
            },
            required: ["parking_id", "source"],
            additionalProperties: false,
        },
    };

    public static attributeUpdateList = Object.keys(ParkingProhibitionsModel.attributeModel).concat("updated_at");
}

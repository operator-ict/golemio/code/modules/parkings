import { IParkingTariffPeriod } from "#sch/models/interfaces/IParkingTariff";
import { IParkingTariff } from "#sch/models/interfaces/IParkingTariff";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

export class ParkingTariffsModel extends Model<ParkingTariffsModel> implements IParkingTariff {
    public static tableName = "parkings_tariffs";

    declare tariff_id: string;
    declare source: string;
    declare last_updated: Date;
    declare payment_mode: string;
    declare payment_additional_description: string | null;
    declare free_of_charge: boolean;
    declare url_link_address: string | null;
    declare charge_band_name: string;
    declare accepts_payment_card: boolean | null;
    declare accepts_cash: boolean | null;
    declare accepts_mobile_payment: boolean | null;
    declare accepts_litacka: boolean | null;
    declare charge_currency: string;
    declare charge: number;
    declare charge_type: string | null;
    declare charge_order_index: number;
    declare charge_interval: number | null;
    declare max_iterations_of_charge: number | null;
    declare min_iterations_of_charge: number | null;
    declare start_time_of_period: string | null;
    declare end_time_of_period: string | null;
    declare allowed_vehicle_type: string | null;
    declare allowed_fuel_type: string | null;
    declare valid_from: Date | null;
    declare valid_to: Date | null;
    declare maximum_duration_seconds: number | null;
    declare periods_of_time: IParkingTariffPeriod[] | null;
    declare payment_methods: string[];

    public static attributeModel: ModelAttributes<ParkingTariffsModel> = {
        tariff_id: {
            primaryKey: true,
            type: DataTypes.STRING(50),
        },
        source: {
            allowNull: false,
            type: DataTypes.STRING(255),
        },
        last_updated: { type: DataTypes.DATE },
        payment_mode: { type: DataTypes.STRING(255) },
        payment_additional_description: { type: DataTypes.TEXT },
        free_of_charge: { type: DataTypes.BOOLEAN },
        url_link_address: { type: DataTypes.TEXT },
        charge_band_name: { type: DataTypes.STRING(50) },
        accepts_payment_card: { type: DataTypes.BOOLEAN },
        accepts_cash: { type: DataTypes.BOOLEAN },
        accepts_mobile_payment: { type: DataTypes.BOOLEAN },
        accepts_litacka: { type: DataTypes.BOOLEAN },
        charge_currency: { type: DataTypes.STRING(50) },
        charge: { type: DataTypes.DECIMAL },
        charge_type: { type: DataTypes.STRING(50) },
        charge_order_index: {
            primaryKey: true,
            type: DataTypes.SMALLINT,
        },
        charge_interval: { type: DataTypes.INTEGER },
        max_iterations_of_charge: { type: DataTypes.INTEGER },
        min_iterations_of_charge: { type: DataTypes.INTEGER },
        start_time_of_period: { type: DataTypes.STRING(50) },
        end_time_of_period: { type: DataTypes.STRING(50) },
        allowed_vehicle_type: { type: DataTypes.STRING(255) },
        allowed_fuel_type: { type: DataTypes.STRING(255) },
        valid_from: { type: DataTypes.DATE },
        valid_to: { type: DataTypes.DATE },
        maximum_duration_seconds: { type: DataTypes.INTEGER },
        periods_of_time: { type: DataTypes.JSONB },
        payment_methods: { type: DataTypes.JSONB },
    };

    public static jsonSchema: JSONSchemaType<IParkingTariff[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                tariff_id: { type: "string" },
                source: { type: "string" },
                last_updated: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "string", format: "date-time" },
                    ],
                },
                payment_mode: { type: "string" },
                payment_additional_description: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                free_of_charge: { type: "boolean" },
                url_link_address: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                charge_band_name: { type: "string" },
                accepts_payment_card: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }], nullable: true },
                accepts_cash: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }], nullable: true },
                accepts_mobile_payment: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }], nullable: true },
                accepts_litacka: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }], nullable: true },
                charge_currency: { type: "string" },
                charge: { type: "number" },
                charge_type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                charge_order_index: { type: "number" },
                charge_interval: { oneOf: [{ type: "number" }, { type: "null", nullable: true }], nullable: true },
                max_iterations_of_charge: { oneOf: [{ type: "number" }, { type: "null", nullable: true }], nullable: true },
                min_iterations_of_charge: { oneOf: [{ type: "number" }, { type: "null", nullable: true }], nullable: true },
                start_time_of_period: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                end_time_of_period: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                allowed_vehicle_type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                allowed_fuel_type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }], nullable: true },
                valid_from: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "string", format: "date-time" },
                        { type: "null", nullable: true },
                    ],
                    nullable: true,
                },
                valid_to: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "string", format: "date-time" },
                        { type: "null", nullable: true },
                    ],
                    nullable: true,
                },
                maximum_duration_seconds: { oneOf: [{ type: "number" }, { type: "null", nullable: true }], nullable: true },
                periods_of_time: {
                    type: "array",
                    items: {
                        type: "object",
                        properties: {
                            day_in_week: { type: "string" },
                            start: { type: "string" },
                            end: { type: "string" },
                            ph: { type: "string" },
                        },
                        required: ["day_in_week", "start", "end", "ph"],
                    },
                    nullable: true,
                },
                payment_methods: {
                    type: "array",
                    items: {
                        type: "string",
                        enum: [
                            "card_online",
                            "card_offline",
                            "litacka",
                            "cash",
                            "coins_only",
                            "mobile_app",
                            "sms_payment",
                            "apple_pay",
                            "google_pay",
                        ],
                    },
                    nullable: true,
                },
            },
            required: [
                "tariff_id",
                "source",
                "last_updated",
                "payment_mode",
                "free_of_charge",
                "charge_band_name",
                "charge_currency",
                "charge",
                "charge_order_index",
            ],
            additionalProperties: false,
        },
    };

    public static attributeUpdateList = Object.keys(ParkingTariffsModel.attributeModel)
        .filter((att) => !["tariff_id", "source"].includes(att))
        .concat("updated_at");
}

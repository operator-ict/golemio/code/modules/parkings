import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { AverageOccupancy, ITskAverageOccupancyModel } from "./interfaces/ITskAverageOccupancyModel";

export class TskAverageOccupancyModel extends Model<TskAverageOccupancyModel> implements ITskAverageOccupancyModel {
    public static tableName = "v_tsk_average_occupancy";

    declare source_id: string;
    declare average_occupancy: AverageOccupancy;

    public static attributeModel: ModelAttributes<TskAverageOccupancyModel> = {
        source_id: {
            type: DataTypes.TEXT,
            unique: true,
        },
        average_occupancy: DataTypes.JSON,
    };
}

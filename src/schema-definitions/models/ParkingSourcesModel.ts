import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IParkingSource, IParkingSourcePayment, IParkingSourceReservation } from "#sch/models/interfaces/IParkingSource";

export class ParkingSourcesModel extends Model<IParkingSource> implements IParkingSource {
    public static tableName = "parking_sources";

    declare source: string;
    declare name: string;
    declare open_data: boolean;
    declare api_v3_allowed: boolean;
    declare legacy_api_allowed: boolean;
    declare payment: IParkingSourcePayment | null;
    declare reservation: IParkingSourceReservation | null;
    declare contact: object | null;
    declare datasource_parking: string | null;
    declare datasource_locations: string | null;
    declare datasource_payments: string | null;
    declare datasource_entrances: string | null;
    declare datasource_prohibitions: string | null;
    declare datasource_tariffs: string | null;

    public static attributeModel: ModelAttributes<ParkingSourcesModel> = {
        source: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        name: DataTypes.STRING,
        open_data: DataTypes.BOOLEAN,
        api_v3_allowed: DataTypes.BOOLEAN,
        legacy_api_allowed: DataTypes.BOOLEAN,
        payment: DataTypes.JSONB,
        reservation: DataTypes.JSONB,
        contact: DataTypes.JSON,
        datasource_parking: DataTypes.STRING,
        datasource_locations: DataTypes.STRING,
        datasource_payments: DataTypes.STRING,
        datasource_entrances: DataTypes.STRING,
        datasource_prohibitions: DataTypes.STRING,
        datasource_tariffs: DataTypes.STRING,
    };

    public static attributeList = Object.keys(ParkingSourcesModel.attributeModel);
}

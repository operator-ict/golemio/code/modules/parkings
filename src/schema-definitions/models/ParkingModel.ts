import { IAddress } from "#sch/models/interfaces/IAddress";
import { IContact, IParking } from "#sch/models/interfaces/IParking";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IGeoCoordinatesPoint, TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";

export class ParkingModel extends Model<IParking> implements IParking {
    public static tableName = "parkings";

    declare id: string;
    declare source: string;
    declare source_id: string;
    declare data_provider: string;
    declare date_modified: Date;
    declare location: TGeoCoordinates;
    declare address: IAddress | null;
    declare name: string | null;
    declare area_served: string | null;
    declare total_spot_number: number | null;
    declare category: string | null;
    declare tariff_id: string | null;
    declare valid_from: Date | null;
    declare valid_to: Date | null;
    declare parking_type: string;
    declare zone_type: string | null;
    declare centroid: IGeoCoordinatesPoint | null;
    declare security: boolean | null;
    declare max_vehicle_dimensions: number[] | null; // height(m), width(m), length(m), weight(kg)
    declare updated_at: Date | null;
    declare covered: boolean | null;
    declare contact: IContact | null;
    declare parking_policy: string | null;
    declare sanitized_location: TGeoCoordinates;
    declare area: number;
    declare address_updated_at: Date | null;
    declare active: boolean;

    public static attributeModel: ModelAttributes<ParkingModel> = {
        id: {
            type: DataTypes.STRING,
            unique: true,
        },
        source: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        source_id: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        data_provider: DataTypes.STRING,
        name: DataTypes.STRING,
        category: DataTypes.STRING,
        date_modified: DataTypes.BIGINT,
        address: DataTypes.JSON,
        location: DataTypes.GEOMETRY,
        area_served: DataTypes.STRING, // city district
        total_spot_number: DataTypes.INTEGER,
        tariff_id: DataTypes.STRING,
        valid_from: DataTypes.DATE,
        valid_to: DataTypes.DATE,
        parking_type: DataTypes.STRING,
        zone_type: DataTypes.STRING,
        centroid: DataTypes.GEOMETRY,
        security: DataTypes.BOOLEAN,
        max_vehicle_dimensions: {
            allowNull: true,
            type: DataTypes.ARRAY(DataTypes.REAL), // height(m), width(m), length(m), weight(kg)
        },
        covered: DataTypes.BOOLEAN,
        contact: DataTypes.JSON,
        parking_policy: DataTypes.STRING,
        sanitized_location: DataTypes.GEOMETRY,
        area: DataTypes.INTEGER,
        address_updated_at: {
            allowNull: true,
            type: DataTypes.DATE,
        },
        active: DataTypes.BOOLEAN,
    };

    public static attributeList = Object.keys(ParkingModel.attributeModel);

    public static updateParkingTariffAttributeList = ["tariff_id", "updated_at"];
}

const forExport = {
    name: "Parkomats",
    pgSchema: "parkings",
    pgTableName: "parkomats",
};

export { forExport as Parkomats };

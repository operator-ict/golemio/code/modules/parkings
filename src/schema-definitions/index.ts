import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import Sequelize from "@golemio/core/dist/shared/sequelize";

// SDMA = Sequelize DefineModelAttributes

export interface IModelDefinition {
    name: string;
    outputJsonSchema: any;
    outputSequelizeAttributes: Sequelize.ModelAttributes;
    pgTableName: string;
}

const KORIDConfDatasourceJSONSchema = {
    type: "object",
    required: ["time", "geojson", "tarif"],
    additionalProperties: true,
    properties: {
        type: { type: "string" },
        version: { type: "string" },
        time: { type: "integer" },
        geojson: {
            type: "object",
            required: ["type", "features"],
            additionalProperties: true,
            properties: {
                type: { type: "string" },
                features: {
                    type: "array",
                    additionalProperties: true,
                    minItems: 1,
                    items: {
                        type: "object",
                        required: ["type", "properties", "geometry"],
                        additionalProperties: false,
                        properties: {
                            type: { type: "string" },
                            properties: {
                                type: "object",
                                required: ["groupid", "title", "total", "tarif"],
                                additionalProperties: true,
                                properties: {
                                    groupid: { type: "number" },
                                    title: { type: "string" },
                                    total: { type: "number" },
                                    tarif: { type: "string" },
                                },
                            },
                            geometry: {
                                type: "object",
                                required: ["type", "coordinates"],
                                additionalProperties: false,
                                properties: {
                                    type: { type: "string" },
                                    coordinates: { type: "array" },
                                },
                            },
                        },
                    },
                },
            },
        },
        tarif: {
            type: "object",
            required: ["lastUpdated", "paymentMode", "freeOfCharge", "chargeBand"],
            additionalProperties: true,
            properties: {
                lastUpdated: { type: "string" },
                paymentMode: {
                    type: "array",
                    items: { type: "string" },
                },
                paymentAdditionalDescription: { type: "string" },
                freeOfCharge: { type: "boolean" },
                urlLinkAddress: { type: "string" },
                chargeBand: {
                    type: "array",
                    additionalProperties: false,
                    items: {
                        type: "object",
                        required: ["chargeBandName", "chargeCurrency", "charge"],
                        additionalProperties: true,
                        properties: {
                            chargeBandName: { type: "string" },
                            acceptedMeansOfPayment: {
                                type: "array",
                                items: { type: "string" },
                            },
                            chargeCurrency: { type: "string" },
                            charge: {
                                type: "array",
                                required: ["charge", "chargeOrderIndex", "chargeInterval"],
                                additionalProperties: true,
                                items: {
                                    charge: { type: "number" },
                                    chargeType: { type: "string" },
                                    chargeOrderIndex: { type: "number" },
                                    chargeInterval: { type: "number" },
                                    maxIterationsOfCharge: { type: "number" },
                                    minIterationsOfCharge: { type: "number" },
                                    startTimeOfPeriod: { type: "string" },
                                    endTimeOfPeriod: { type: "string" },
                                },
                            },
                        },
                    },
                },
            },
        },
    },
};

const KORIDDatasourceJSONSchema = {
    type: "object",
    required: ["time", "data"],
    additionalProperties: true,
    properties: {
        type: { type: "string" },
        version: { type: "string" },
        time: { type: "integer" },
        data: {
            type: "object",
            additionalProperties: {
                type: "object",
                required: ["tot", "fr", "occ", "cls"],
                additionalProperties: false,
                properties: {
                    tot: { type: "integer" },
                    fr: { type: "integer" },
                    occ: { type: "integer" },
                    cls: { type: "integer" },
                },
            },
        },
    },
};

const parkingSDMA: Sequelize.ModelAttributes = {
    id: {
        type: Sequelize.STRING,
        unique: true,
    },
    source: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    source_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    data_provider: Sequelize.STRING,
    name: Sequelize.STRING,
    category: Sequelize.STRING,
    date_modified: Sequelize.BIGINT,
    address: Sequelize.JSON,
    location: Sequelize.GEOMETRY,
    area_served: Sequelize.STRING, // city district
    total_spot_number: Sequelize.INTEGER,
    tariff_id: Sequelize.STRING,
    valid_from: Sequelize.DATE,
    valid_to: Sequelize.DATE,
    parking_type: Sequelize.STRING,
    zone_type: Sequelize.STRING,
    centroid: Sequelize.GEOMETRY,
    security: Sequelize.BOOLEAN,
    max_vehicle_dimensions: {
        allowNull: true,
        type: Sequelize.ARRAY(Sequelize.REAL),
    }, // height(m), width(m), length(m), weight(kg)
    covered: Sequelize.BOOLEAN,
    contact: Sequelize.JSON,
    parking_policy: Sequelize.STRING,
    sanitized_location: Sequelize.GEOMETRY,
    area: Sequelize.INTEGER,
    active: Sequelize.BOOLEAN,
    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
    address_updated_at: { type: Sequelize.DATE }, // kdy se aktualizovala adresa
};

const parkingsFilteredSDMA: Sequelize.ModelAttributes = { ...parkingSDMA };

const parkingsMeasurementSDMA: Sequelize.ModelAttributes = {
    source: {
        primaryKey: true,
        allowNull: false,
        type: Sequelize.STRING,
    },
    source_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    parking_id: Sequelize.STRING,
    available_spot_number: Sequelize.INTEGER,
    closed_spot_number: Sequelize.INTEGER, // custom
    occupied_spot_number: Sequelize.INTEGER, // custom
    total_spot_number: Sequelize.INTEGER,
    date_modified: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const parkingsMeasurementActualSDMA: Sequelize.ModelAttributes = {
    ...parkingsMeasurementSDMA,
    date_modified: Sequelize.BIGINT,
};

const datasourceIPRJSONSchema = {
    type: "array",
    items: {
        type: "object",
        required: ["geometry", "properties", "type"],
        additionalProperties: true,
        properties: {
            type: { type: "string" },
            id: { type: "number" },
            geometry: {
                type: "object",
                required: ["type", "coordinates"],
                additionalProperties: false,
                properties: {
                    type: { type: "string" },
                    coordinates: { type: "array" },
                },
            },
            properties: {
                type: "object",
                required: ["TARIFTAB"],
                additionalProperties: true,
                properties: {
                    OBJECTID: { type: "number" },
                    PS_ZPS: { type: "string" },
                    SHAPE__Area: { type: "number" },
                    SHAPE__Length: { type: "number" },
                    TARIFTAB: { type: "string" },
                    TYPZONY: { type: "string" },
                    ZPS_ID: { type: "string" },
                },
            },
        },
    },
};

const datasourceTSKJSONSchema = {
    type: "array",
    items: {
        type: "object",
        required: ["geometry", "properties", "type"],
        additionalProperties: true,
        properties: {
            type: { type: "string" },
            geometry: {
                type: "object",
                required: ["type", "coordinates"],
                additionalProperties: false,
                properties: {
                    type: { type: "string" },
                    coordinates: { type: "array" },
                },
            },
            properties: {
                type: "object",
                required: ["tarifTab", "ps_zps_celkem", "typZony", "tarifName", "platnostOd", "platnostDo"],
                additionalProperties: true,
                properties: {
                    tarifTab: { type: "string" },
                    ps_zps_celkem: { type: "number" },
                    typZony: { type: "string" },
                    tarifName: { type: "string" },
                    platnostOd: { type: "string" },
                    platnostDo: { type: "string" },
                    ulice: { type: ["string", "null"] },
                    TARIF: { type: "number" },
                    CTARIF: { type: "number" },
                },
            },
        },
    },
};

const datasourceTSKTariffJSONSchema = {
    type: "array",
    items: {
        type: "object",
        required: ["TARIF", "CTARIF", "Day", "PricePerHour", "MaxParkingTime", "MaxPrice", "TimeFrom", "TimeTo", "PayAtHoliday"],
        additionalProperties: true,
        properties: {
            TARIF: { type: "string" },
            CTARIF: { type: "string" },
            Day: { type: "string" },
            PricePerHour: { type: "string" },
            MaxParkingTime: { type: "string" },
            MaxPrice: { type: "string" },
            TimeFrom: { type: "string" },
            TimeTo: { type: "string" },
            PayAtHoliday: { type: "string" },
        },
    },
};

const outputLocationJsonSchema = {
    type: "array",
    items: {
        $ref: "#/$defs/Item",
    },
    $defs: {
        Item: {
            type: "object",
            properties: {
                id: {
                    type: "string",
                },
                source: {
                    type: "string",
                },
                source_id: {
                    type: "string",
                },
                data_provider: {
                    type: "string",
                },
                location: {
                    type: "object",
                },
                centroid: {
                    type: "object",
                },
                address: {
                    type: "object",
                },
                total_spot_number: {
                    oneOf: [
                        {
                            type: "integer",
                        },
                        {
                            type: "null",
                            nullable: true,
                        },
                    ],
                },
                special_access: {
                    oneOf: [
                        {
                            type: "array",
                            items: {
                                type: "string",
                            },
                        },
                        {
                            type: "null",
                            nullable: true,
                        },
                    ],
                },
            },
            required: ["id", "data_provider", "location", "total_spot_number", "source", "source_id"],
        },
    },
};

const outputLocationSDMA: Sequelize.ModelAttributes = {
    id: {
        type: Sequelize.STRING,
        primaryKey: true,
    },
    source: Sequelize.STRING,
    source_id: Sequelize.STRING,
    data_provider: Sequelize.STRING,
    location: Sequelize.GEOMETRY,
    centroid: Sequelize.GEOMETRY,
    address: Sequelize.JSON,
    total_spot_number: Sequelize.INTEGER,
    special_access: Sequelize.ARRAY(Sequelize.STRING(50)),

    // ⬐ Audit fields
    create_batch_id: { type: Sequelize.BIGINT },
    created_at: { type: Sequelize.DATE },
    created_by: { type: Sequelize.STRING(150) },
    update_batch_id: { type: Sequelize.BIGINT },
    updated_at: { type: Sequelize.DATE },
    updated_by: { type: Sequelize.STRING(150) },
    address_updated_at: { type: Sequelize.DATE },
};

const forExport = {
    name: "Parkings",
    pgSchema: "parkings",
    ipr: {
        datasourceJsonSchema: datasourceIPRJSONSchema,
        name: "IPRParking",
    },
    tsk: {
        datasourceGeoJsonSchema: datasourceTSKJSONSchema,
        datasourceTariffJsonSchema: datasourceTSKTariffJSONSchema,
        name: "TSKParking",
    },
    korid: {
        datasourceJsonConfSchema: KORIDConfDatasourceJSONSchema,
        datasourceJsonSchema: KORIDDatasourceJSONSchema,
        name: "KoridParkings",
    },
    parkings: {
        outputSequelizeAttributes: parkingSDMA,
        pgTableName: "parkings",
    },
    parkingsFiltered: {
        name: "ParkingsFiltered",
        outputSequelizeAttributes: parkingsFilteredSDMA,
        pgTableName: "v_parkings_filtered",
    },
    location: {
        name: "ParkingsLocation",
        outputJsonSchema: outputLocationJsonSchema,
        outputSequelizeAttributes: outputLocationSDMA,
        pgTableName: "parkings_location",
    },
    measurements: {
        name: "ParkingsMeasurements",
        outputSequelizeAttributes: parkingsMeasurementSDMA,
        pgTableName: "parkings_measurements_part",
    },
    measurementsActual: {
        name: "ParkingsMeasurementsActual",
        outputSequelizeAttributes: parkingsMeasurementActualSDMA,
        pgTableName: "parkings_measurements_actual",
    },
    latestMeasurements: {
        name: "ParkingsLatestMeasurements",
        outputSequelizeAttributes: parkingsMeasurementSDMA,
        pgTableName: "v_parkings_latest_measurements",
    },
};

export interface ISchemaDefinition<T> {
    name: string;
    jsonSchema: JSONSchemaType<T>;
}

export { Parkomats } from "./Parkomats";
export { forExport as Parkings };

import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import Sequelize, { Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IParkomatDto } from "./interfaces/IParkomatDto";

export class ParkomatDto extends Model<IParkomatDto> implements IParkomatDto {
    declare channel: string;
    declare parking_zone: string;
    declare price: number;
    declare ticket_bought: string | null;
    declare transaction_id: string;
    declare validity_from: string | null;
    declare validity_to: string | null;

    public static attributeModel: ModelAttributes<ParkomatDto> = {
        channel: Sequelize.STRING,
        parking_zone: Sequelize.STRING,
        price: Sequelize.INTEGER,
        ticket_bought: Sequelize.DATE,
        transaction_id: {
            primaryKey: true,
            type: Sequelize.STRING,
        },
        validity_from: Sequelize.DATE,
        validity_to: Sequelize.DATE,
    };

    public static jsonSchema: JSONSchemaType<IParkomatDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                channel: { type: "string" },
                parking_zone: { type: "string" },
                price: { type: "number" },
                ticket_bought: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                transaction_id: { type: "string" },
                validity_from: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                validity_to: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
            required: ["transaction_id", "price"],
            additionalProperties: false,
        },
    };
}

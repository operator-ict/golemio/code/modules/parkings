export interface IParkomatDto {
    channel: string;
    parking_zone: string;
    price: number;
    ticket_bought: string | null;
    transaction_id: string;
    validity_from: string | null;
    validity_to: string | null;
}

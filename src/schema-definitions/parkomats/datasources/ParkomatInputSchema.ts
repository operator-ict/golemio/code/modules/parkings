import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IParkomatInput } from "./interfaces/IParkomatInput";

const parkomatInputSchema: JSONSchemaType<IParkomatInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            Channel: { type: "string" },
            DateFrom: { type: "string" },
            DateTime: { type: "string" },
            DateTo: { type: "string" },
            Id: { type: "string" },
            Price: { type: "number" },
            Section: { type: "string" },
        },
        required: ["Id", "Price"],
        additionalProperties: false,
    },
};

export { parkomatInputSchema as ParkomatInputSchema };

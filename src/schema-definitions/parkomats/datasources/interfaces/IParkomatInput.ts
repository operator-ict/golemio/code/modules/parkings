export interface IParkomatInput {
    Channel: string;
    DateFrom: string;
    DateTime: string;
    DateTo: string;
    Id: string;
    Price: number;
    Section: string;
}

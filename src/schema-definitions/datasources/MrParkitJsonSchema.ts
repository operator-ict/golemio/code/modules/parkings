import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IMrParkitGarageWithTariff } from "./interfaces/IMrParkitGarage";

export const mrParkitJsonSchema: JSONSchemaType<IMrParkitGarageWithTariff[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: {
                type: "string",
            },
            name: {
                type: "string",
            },
            address: {
                type: "string",
            },
            free: {
                type: "boolean",
            },
            latitude: {
                type: "number",
            },
            longitude: {
                type: "number",
            },
            minReservationDuration: {
                type: "number",
            },
            maxReservationDuration: {
                type: "number",
            },
            pricePerHour: {
                type: "number",
            },
            pricePerDay: {
                type: "number",
            },
            pricePerWeekend: {
                type: "number",
            },
            maxVehicleDimensions: {
                type: "object",
                properties: {
                    height: {
                        type: "number",
                    },
                    width: {
                        type: "number",
                    },
                    length: {
                        type: "number",
                    },
                },
                required: ["height", "width", "length"],
            },
            properties: {
                type: "object",
                properties: {
                    lpgCngAllowed: {
                        type: "boolean",
                    },
                    motorbikesAllowed: {
                        type: "boolean",
                    },
                    benefits: {
                        type: "array",
                        items: {
                            type: "string",
                        },
                    },
                },
                required: [],
            },
        },
        required: [
            "id",
            "name",
            "free",
            "address",
            "latitude",
            "longitude",
            "minReservationDuration",
            "maxReservationDuration",
            "pricePerHour",
            "pricePerDay",
            "pricePerWeekend",
        ],
    },
};

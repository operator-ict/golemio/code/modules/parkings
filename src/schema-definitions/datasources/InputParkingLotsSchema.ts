import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IInputParkingLots } from "./interfaces/IInputParkingLots";

const inputParkingLotsSchema: JSONSchemaType<IInputParkingLots[]> = {
    type: "array",
    items: {
        type: "object",
        required: ["id", "lastUpdated", "lat", "lng", "name", "numOfFreePlaces", "numOfTakenPlaces", "totalNumOfPlaces"],
        properties: {
            gid: { type: "integer" },
            id: { type: "integer" },
            lastUpdated: { type: "integer" },
            lat: { type: "number" },
            lng: { type: "number" },
            name: { type: "string" },
            numOfFreePlaces: { type: "integer" },
            numOfTakenPlaces: { type: "integer" },
            pr: { type: "boolean" },
            totalNumOfPlaces: { type: "integer" },
        },
    },
};

export { inputParkingLotsSchema as InputParkingLotsSchema };

import { ISchemaDefinition } from "#sch";
import { IStaticParkingLotsGeo } from "#sch/datasources/interfaces/IStaticParkingLotsGeo";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions/SharedSchemaProvider";

export const StaticParkingLotsGeoJsonSchema: ISchemaDefinition<IStaticParkingLotsGeo> = {
    name: "StaticParkingLotsGeo",
    jsonSchema: {
        type: "object",
        properties: {
            type: {
                type: "string",
                enum: ["FeatureCollection"],
            },
            features: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        type: {
                            type: "string",
                            enum: ["Feature"],
                        },
                        properties: {
                            type: "object",
                            properties: {
                                id: {
                                    type: "string",
                                },
                            },
                            required: ["id"],
                        },
                        geometry: {
                            $ref: "#/definitions/geometry",
                        },
                    },
                    required: ["geometry", "properties", "type"],
                },
            },
        },
        required: ["features", "type"],
        definitions: {
            // @ts-expect-error since it is referenced definition from other file ts doesnt like it.
            geometry: SharedSchemaProvider.Geometry,
        },
    },
};

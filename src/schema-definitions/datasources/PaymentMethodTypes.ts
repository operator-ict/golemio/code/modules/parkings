export type PaymentMethodTypes =
    | "card_online"
    | "card_offline"
    | "cash"
    | "coins_only"
    | "mobile_app"
    | "litacka"
    | "sms_payment"
    | "apple_pay"
    | "google_pay"
    | "unknown";

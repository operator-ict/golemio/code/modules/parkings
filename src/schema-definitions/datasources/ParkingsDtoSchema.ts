import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IParkingsDto } from "./interfaces/IParkingsDto";

const parkingsDtoSchema: JSONSchemaType<IParkingsDto[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "string" },
            source: { type: "string" },
            source_id: { type: "string" },
            data_provider: { type: "string" },
            name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            category: { type: "string" },
            date_modified: { type: "string" },
            address: { $ref: "#/definitions/address" },
            location: { $ref: "#/definitions/geometry" },
            area_served: { type: "string" },
            total_spot_number: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
            tariff_id: { type: "string" },
            valid_from: { type: "string" },
            valid_to: { type: "string" },
            parking_type: { type: "string" },
            zone_type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            centroid: { $ref: "#/definitions/geometry" },
            security: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
            max_vehicle_dimensions: {
                oneOf: [
                    {
                        type: "array",
                        items: { type: "number" },
                        minItems: 3,
                        maxItems: 4,
                    },
                    { type: "null", nullable: true },
                ],
            },
            covered: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
            contact: {
                oneOf: [
                    {
                        type: "object",
                        properties: {
                            email: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                            phone: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                            website: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                        },
                        required: [],
                        additionalProperties: false,
                    },
                    { type: "null", nullable: true },
                ],
            },
            parking_policy: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            area: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
            active: { type: "boolean" },
        },
        required: ["id", "source", "source_id"],
        additionalProperties: false,
    },
    definitions: {
        address: {
            type: "object",
            properties: {
                address_formatted: { type: "string" },
                street_address: { type: "string" },
                postal_code: { type: "string" },
                address_locality: { type: "string" },
                address_region: { type: "string" },
                address_country: { type: "string" },
                house_number: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
            required: [],
        },
        // @ts-expect-error
        geometry: SharedSchemaProvider.Geometry,
    },
};

export { parkingsDtoSchema as ParkingsDtoSchema };

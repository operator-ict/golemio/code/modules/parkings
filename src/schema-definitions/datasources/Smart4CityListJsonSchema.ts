import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { ISmart4CityList } from "#sch/datasources/interfaces/ISmart4CityList";

export const smart4CityListJsonSchema: JSONSchemaType<ISmart4CityList[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "number" },
            code: { type: "string" },
            name: { type: "string" },
            country: { type: "string" },
            latitude: { type: "number" },
            longitude: { type: "number" },
        },
        required: ["id", "code"],
        additionalProperties: true,
    },
};

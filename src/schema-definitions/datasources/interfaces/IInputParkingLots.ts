export interface IInputParkingLots {
    gid: number;
    id: number;
    lastUpdated: number;
    lat: number;
    lng: number;
    name: string;
    numOfFreePlaces: number;
    numOfTakenPlaces: number;
    pr: boolean;
    totalNumOfPlaces: number;
}

import { PaymentMethodTypes } from "../PaymentMethodTypes";
import { IStaticParkingTariffCharge } from "#sch/datasources/interfaces/IStaticParkingsTariffCharge";

export interface IStaticParkingTariff {
    tariff_id: string;
    source: string;
    last_updated: string;
    payment_mode: string;
    payment_additional_description: string | null;
    free_of_charge: boolean;
    url_link_address: string | null;
    charge_band_name: string;
    charge_currency: string;
    allowed_vehicle_type: string | null;
    allowed_fuel_type: string | null;
    payment_methods: PaymentMethodTypes[];
    charges: IStaticParkingTariffCharge[];
    reservation_url: string | null;
}

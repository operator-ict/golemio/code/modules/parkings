export interface ITskParkingZoneTariff {
    code: string | null;
    name: string | null;
    note: string | null;
    maxHour: number | null;
    maxPrice: number;
    maxPriceHoliday: number | null;
    minPrice: number;
    validFrom: string; // iso-date
    validTo: string; // iso-date
    divisibility: number | null;
    idOwner: string | null;
    idParkingArea: string | null;
    idTariffInt: number | null;
    idTariffDoprava: number | null;
    idTariffPM: number | null;
    idParkingAreaPrice: string | null;
    tariffGroups: ITskParkingZoneTariffGroup[] | null;
    id: string;
    activeFrom: string; // iso-date
    activeTo: string; // iso-date
    modifiedBy: string | null;
}

export interface ITskParkingZoneTariffGroup {
    idTariff: string;
    groupNumber: number | null;
    monday: boolean;
    tuesday: boolean;
    wednesday: boolean;
    thursday: boolean;
    friday: boolean;
    saturday: boolean;
    sunday: boolean;
    holiday: boolean | null;
    maxPrice: number | null;
    tariffDefinitions: ITskParkingZoneTariffDefinition[] | null;
    id: string;
    activeFrom: string; // iso-date
    activeTo: string; // iso-date
    modifiedBy: string | null;
}

export interface ITskParkingZoneTariffDefinition {
    idTariffGroup: string;
    pricePerHour: number;
    priceFlat: number | null;
    maxPriceItem: number | null;
    minPriceItem: number | null;
    timeFrom: string; // HH:MM:SS
    timeTo: string; // HH:MM:SS
    minuteFrom: number;
    minuteTo: number;
    onePerDay: boolean;
    id: string;
    activeFrom: string; // iso-date
    activeTo: string; // iso-date
    modifiedBy: string | null;
}

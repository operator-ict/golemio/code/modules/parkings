export interface ITskParkingMachine {
    idParkMachine: string;
    code: string;
    idStatus: number;
    type: string;
    idTariff: string;
    positionWKT: string;
    activeFrom: Date | string;
}

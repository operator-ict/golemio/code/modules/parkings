export interface ITskParkingSection {
    idSection: string;
    sectionCode: string;
    sectionCat: number;
    borderWKT: string;
    idTariff: string | null;
    projectedCapacity: number;
    activeFrom: string;
}

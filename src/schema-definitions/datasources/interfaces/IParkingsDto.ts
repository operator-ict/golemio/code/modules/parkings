import { IContact } from "#sch/models/interfaces/IParking";
import { Geometry } from "@golemio/core/dist/shared/geojson";

export interface IParkingsDto {
    id: string;
    source: string;
    source_id: string;
    data_provider: string;
    name: string | null;
    category: string;
    date_modified: string;
    address: object; // json
    location: Geometry; // geojson
    area_served: string; // city district
    total_spot_number: number;
    tariff_id: string;
    valid_from: string;
    valid_to: string;
    parking_type: string;
    zone_type: string;
    centroid: Geometry; // geojson
    covered?: boolean | null;
    contact?: IContact | null;
    area: number;
}

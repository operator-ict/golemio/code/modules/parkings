interface IBedrichovParking {
    id: string;
    name: string;
    provider: string;
    lat: number;
    lng: number;
    area: {
        type: string;
        coordinates: number[][][];
    };
    parkingLots: object[];
    type: string;
    paid: boolean;
    occupancy: {
        overall: {
            total: number;
            free: number;
        };
        grouped: object[];
        sourceEvents: string[];
    };
    capacity: {
        total: number;
    };
    publicPortal: {
        tileName: string;
        name?: string;
    };
}

export { IBedrichovParking };

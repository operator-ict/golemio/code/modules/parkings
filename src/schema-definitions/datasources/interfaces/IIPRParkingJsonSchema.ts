import { IGeoCoordinatesMultiPolygon, IGeoCoordinatesPolygon } from "@golemio/core/dist/output-gateway/Geo";

export interface IIPRParkingJsonFeature {
    type: string;
    id: number;
    geometry: IGeoCoordinatesPolygon | IGeoCoordinatesMultiPolygon;
    properties: {
        OBJECTID: number;
        PS_ZPS: string;
        TARIFTAB: string;
        TYPZONY: string;
        ZPS_ID: string;
        SHAPE__Area: number;
        SHAPE__Length: number;
    };
}

export interface ISmart4CityLocation {
    id: number;
    name: string;
    latitude: number;
    longitude: number;
    capacity: number;
    correctionunoccupied: number | null;
    closed: boolean;
    type: number;
}

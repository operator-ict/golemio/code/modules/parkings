export interface ITskParkingSectionLevel {
    idSectionItem: string;
    idSection: string;
    sectionItemCode: string;
    streetNameValid: string | null;
    sectionCat: number | null;
    borderWKT: string;
    parkingType: number | null;
    projectedCapacity: number;
    activeFrom: string;
}

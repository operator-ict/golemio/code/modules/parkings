export interface IParkingMeasurementDto {
    source: string;
    source_id: string;
    parking_id: string;
    available_spot_number: number | null;
    closed_spot_number: number;
    occupied_spot_number: number | null;
    total_spot_number: number | null;
    date_modified: Date | string;
}

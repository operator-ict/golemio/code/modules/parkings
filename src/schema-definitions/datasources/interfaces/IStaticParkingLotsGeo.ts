import { TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";

export interface IStaticParkingLotsGeo {
    type: "FeatureCollection";
    features: IStaticParkingLotsGeoFeature[];
}

export interface IStaticParkingLotsGeoFeature {
    type: "Feature";
    geometry: TGeoCoordinates;
    properties: {
        id: string;
    };
}

import { TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";

export interface IIptOictEntrancesProperties {
    parking_id: string;
    source_id: string;
    location: TGeoCoordinates;
    entry: boolean;
    exit: boolean;
    entrance_type: string[] | null;
    level: number;
    dimension: IDimension;
}

interface IDimension {
    max_height: number | null;
    max_width: number | null;
    max_length: number | null;
    max_weight: number | null;
}

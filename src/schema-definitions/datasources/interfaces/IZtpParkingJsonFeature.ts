import { IGeoCoordinatesPoint } from "@golemio/core/dist/output-gateway/Geo";

export interface IZtpParkingJsonFeature {
    type: string;
    id: number;
    geometry: IGeoCoordinatesPoint;
    properties: {
        OBJECTID: number;
        POCET_PS: number;
        ROZM_DELKA: number;
        ROZM_SIRKA: number;
        TYP_PS: number;
        KOLME_PS: number;
        POD_SKLON: number;
        PRIC_SKLON: number;
        TYP_POVRCH: number;
        POSK_POVRC: number;
        PES_ZONA: number;
        MAT_POVRCH: number;
        GLOBALID: string;
    };
}

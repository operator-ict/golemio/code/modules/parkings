export interface ISmart4CityList {
    id: number;
    code: string;
    name: string;
    country: string;
    latitude: number;
    longitude: number;
}

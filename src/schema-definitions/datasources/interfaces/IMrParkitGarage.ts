export interface IMrParkitGarage {
    id: string;
    name: string;
    address: string;
    free: boolean;
    latitude: number;
    longitude: number;
    maxVehicleDimensions: {
        length: number;
        width: number;
        height: number;
    };
    properties: {
        lpgCngAllowed: boolean;
        motorbikesAllowed: boolean;
        benefits: string[];
    };
}

export interface IMrParkitGarageWithTariff extends IMrParkitGarage {
    minReservationDuration: number;
    maxReservationDuration: number;
    pricePerHour: number;
    pricePerDay: number;
    pricePerWeekend: number;
}

export interface IPmdpMeasurement {
    kapacita: string;
    kp: string;
    dp_bez_rezervace: string;
    dp_rezervace: string;
    volno: string;
    volno_bez_prepoctu: string;
    datum_aktualizace: string;
}

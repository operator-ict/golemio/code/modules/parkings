import { IPmdpMeasurement } from "./interfaces/IPmdpMeasurement";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

const pmdpMeasurementSchema: JSONSchemaType<IPmdpMeasurement> = {
    type: "object",
    properties: {
        kapacita: { type: "string" },
        kp: { type: "string" },
        dp_bez_rezervace: { type: "string" },
        dp_rezervace: { type: "string" },
        volno: { type: "string" },
        volno_bez_prepoctu: { type: "string" },
        datum_aktualizace: { type: "string" },
    },
    required: ["volno"],
    additionalProperties: true,
};

export { pmdpMeasurementSchema as PmdpMeasurementSchema };

import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IBedrichovParking } from "./interfaces/IBedrichovParking";

const bedrichovParkingSchema: JSONSchemaType<IBedrichovParking[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "string" },
            name: { type: "string" },
            provider: { type: "string" },
            lat: { type: "number" },
            lng: { type: "number" },
            area: {
                type: "object",
                properties: {
                    type: { type: "string" },
                    coordinates: {
                        type: "array",
                        items: {
                            type: "array",
                            items: {
                                type: "array",
                                items: { type: "number" },
                            },
                        },
                    },
                },
                required: ["type", "coordinates"],
            },
            parkingLots: {
                type: "array",
                items: { type: "object" },
            },
            type: { type: "string" },
            paid: { type: "boolean" },
            occupancy: {
                type: "object",
                properties: {
                    overall: {
                        type: "object",
                        properties: {
                            total: { type: "number" },
                            free: { type: "number" },
                        },
                        required: ["total", "free"],
                    },
                    grouped: {
                        type: "array",
                        items: { type: "object" },
                    },
                    sourceEvents: {
                        type: "array",
                        items: { type: "string" },
                    },
                },
                required: ["overall", "grouped", "sourceEvents"],
            },
            capacity: {
                type: "object",
                properties: {
                    total: { type: "number" },
                },
                required: ["total"],
            },
            publicPortal: {
                type: "object",
                properties: {
                    tileName: { type: "string" },
                    name: { type: "string", nullable: true },
                },
                required: ["tileName"],
            },
        },
        required: [
            "id",
            "name",
            "provider",
            "lat",
            "lng",
            "area",
            "parkingLots",
            "type",
            "paid",
            "occupancy",
            "capacity",
            "publicPortal",
        ],
        additionalProperties: true,
    },
};

export { bedrichovParkingSchema as BedrichovParkingSchema };

import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IParkingSource } from "#sch/models/interfaces/IParkingSource";

export const InputParkingSourceSchema: JSONSchemaType<IParkingSource> = {
    type: "object",
    properties: {
        source: { type: "string" },
        name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        open_data: { type: "boolean" },
        api_v3_allowed: { type: "boolean" },
        legacy_api_allowed: { type: "boolean" },
        payment: {
            oneOf: [
                {
                    type: "object",
                    properties: {
                        web_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                        android_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                        ios_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                        discovery_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    },
                    required: ["web_url", "android_url", "ios_url", "discovery_url"],
                    additionalProperties: false,
                },
                { type: "null", nullable: true },
            ],
        },
        reservation: {
            oneOf: [
                {
                    type: "object",
                    properties: {
                        type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                        web_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                        android_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                        ios_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                        discovery_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    },
                    required: ["web_url", "android_url", "ios_url", "discovery_url"],
                    additionalProperties: false,
                },
                { type: "null", nullable: true },
            ],
        },
        contact: { oneOf: [{ type: "object" }, { type: "null", nullable: true }] },
        datasource_parking: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        datasource_locations: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        datasource_payments: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        datasource_entrances: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        datasource_prohibitions: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        datasource_tariffs: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
    },
    required: ["source", "open_data", "api_v3_allowed", "legacy_api_allowed"],
    additionalProperties: false,
};

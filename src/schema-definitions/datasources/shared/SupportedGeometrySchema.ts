export const SupportedGeometrySchema = {
    $id: "supportedGeometrySchema",
    title: "Geometry",
    description: "Supported parking output geometry",
    type: "object",
    required: ["type", "coordinates"],
    oneOf: [
        {
            title: "Point",
            properties: {
                type: { enum: ["Point"] },
                coordinates: {
                    type: "array",
                    minItems: 2,
                    maxItems: 2,
                    items: { type: "number" },
                    additionalItems: false,
                },
            },
        },
        {
            title: "Polygon",
            properties: {
                type: { enum: ["Polygon"] },
                coordinates: {
                    type: "array",
                    items: {
                        type: "array",
                        minItems: 4,
                        items: {
                            type: "array",
                            minItems: 2,
                            items: { type: "number" },
                        },
                    },
                },
            },
        },
        {
            title: "MultiPolygon",
            properties: {
                type: { enum: ["MultiPolygon"] },
                coordinates: {
                    type: "array",
                    items: {
                        type: "array",
                        items: {
                            type: "array",
                            minItems: 4,
                            items: {
                                type: "array",
                                minItems: 2,
                                items: { type: "number" },
                            },
                        },
                    },
                },
            },
        },
    ],
};

export interface IIsphkMeasurements {
    type: string;
    data: IIsphkMeasurementsItem[];
}

export interface IIsphkMeasurementsItem {
    id: string;
    capacity: number;
    free: number;
    status: string;
}

import { IIsphkMeasurements } from "./interfaces/IIsphkMeasurements";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

const isphkMeasurementsSchema: JSONSchemaType<IIsphkMeasurements> = {
    type: "object",
    properties: {
        type: { type: "string" },
        data: {
            type: "array",
            items: {
                type: "object",
                properties: {
                    id: { type: "string" },
                    capacity: { type: "integer" },
                    free: { type: "integer" },
                    status: { type: "string" },
                },
                required: ["id", "capacity", "free", "status"],
                additionalProperties: false,
            },
        },
    },
    required: ["data", "type"],
    additionalProperties: false,
};

export { isphkMeasurementsSchema as IsphkMeasurementsSchema };

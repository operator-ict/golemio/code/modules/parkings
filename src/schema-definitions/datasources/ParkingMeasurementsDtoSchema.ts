import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IParkingMeasurementDto } from "./interfaces/IParkingsMeasurementDto";

const parkingMeasurementsDtoSchema: JSONSchemaType<IParkingMeasurementDto[]> = {
    type: "array",
    items: {
        type: "object",
        required: ["source", "source_id"],
        additionalProperties: true,
        properties: {
            source: { type: "string" },
            source_id: { type: "string" },
            parking_id: { type: "string" },
            available_spot_number: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
            closed_spot_number: { type: "integer" },
            occupied_spot_number: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
            total_spot_number: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
            date_modified: { type: "string" },
        },
    },
};

export { parkingMeasurementsDtoSchema as ParkingMeasurementsDtoSchema };

import { ISchemaDefinition } from "#sch";
import { IStaticParkingTariff } from "#sch/datasources/interfaces/IStaticParkingTariff";

export interface StaticTariffRelation {
    id: string;
    tariff_id: string;
}

export interface StaticTariffsJson {
    tariffs: IStaticParkingTariff[];
    relations: StaticTariffRelation[];
}

export const StaticTariffsJsonSchema: ISchemaDefinition<StaticTariffsJson> = {
    name: "StaticTariffs",
    jsonSchema: {
        type: "object",
        properties: {
            tariffs: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        tariff_id: {
                            type: "string",
                        },
                        source: {
                            type: "string",
                        },
                        last_updated: {
                            type: "string",
                        },
                        payment_mode: {
                            type: "string",
                        },
                        payment_additional_description: {
                            oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                        },
                        free_of_charge: {
                            type: "boolean",
                        },
                        url_link_address: {
                            oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                        },
                        charge_band_name: {
                            type: "string",
                        },
                        charge_currency: {
                            type: "string",
                        },
                        allowed_vehicle_type: {
                            oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                        },
                        allowed_fuel_type: {
                            oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                        },
                        payment_methods: {
                            type: "array",
                            items: {
                                type: "string",
                            },
                        },
                        charges: {
                            type: "array",
                            items: {
                                type: "object",
                                properties: {
                                    charge: {
                                        type: "number",
                                    },
                                    charge_type: {
                                        type: "string",
                                    },
                                    charge_order_index: {
                                        type: "number",
                                    },
                                    charge_interval: {
                                        oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                                    },
                                    max_iterations_of_charge: {
                                        oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                                    },
                                    min_iterations_of_charge: {
                                        oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                                    },
                                    start_time_of_period: {
                                        oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                                    },
                                    end_time_of_period: {
                                        oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                                    },
                                },
                                required: ["charge", "charge_type", "charge_order_index"],
                            },
                        },
                        reservation_url: {
                            oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                        },
                    },
                    required: [
                        "tariff_id",
                        "source",
                        "last_updated",
                        "payment_mode",
                        "free_of_charge",
                        "charge_band_name",
                        "charge_currency",
                        "payment_methods",
                        "charges",
                    ],
                    additionalProperties: true,
                },
            },
            relations: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        id: {
                            type: "string",
                        },
                        tariff_id: {
                            type: "string",
                        },
                    },
                    required: ["id", "tariff_id"],
                },
            },
        },
        required: ["tariffs", "relations"],
    },
};

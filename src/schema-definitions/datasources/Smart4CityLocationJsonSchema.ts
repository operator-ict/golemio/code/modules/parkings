import { ISmart4CityLocation } from "#sch/datasources/interfaces/ISmart4CityLocation";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export const smart4CityLocationJsonSchema: JSONSchemaType<ISmart4CityLocation[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "number" },
            name: { type: "string" },
            latitude: { type: "number" },
            longitude: { type: "number" },
            capacity: { type: "number" },
            correctionunoccupied: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
            closed: { type: "boolean" },
            type: { type: "number" },
        },
        required: ["id"],
        additionalProperties: true,
    },
};

import { ISchemaDefinition } from "#sch";
import { ITskParkingSection } from "#sch/datasources/interfaces/ITskParkingSection";

export const TskParkingSectionSchema: ISchemaDefinition<ITskParkingSection[]> = {
    name: "TskParkingSectionsSchema",
    jsonSchema: {
        type: "array",
        items: {
            type: "object",
            properties: {
                idSection: { type: "string" },
                sectionCode: { type: "string" },
                sectionCat: { type: "integer" },
                borderWKT: { type: "string" },
                idTariff: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                projectedCapacity: { type: "integer" },
                activeFrom: { type: "string" },
            },
            required: ["idSection", "sectionCode", "sectionCat", "borderWKT", "idTariff", "projectedCapacity", "activeFrom"],
        },
    },
};

import { IIptOictPaymentsProperties } from "#sch/datasources/iptoict/interfaces/IIptOictPaymentsProperties";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export const iptOictPaymentsSchema: JSONSchemaType<IIptOictPaymentsProperties[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            parking_id: { type: "string" },
            payment: {
                type: "object",
                properties: {
                    web_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    android_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    ios_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    discovery_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                },
                required: ["web_url", "android_url", "ios_url", "discovery_url"],
                additionalProperties: false,
            },
            reservation: {
                type: "object",
                properties: {
                    reservation_type: { type: "string" },
                    web_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    android_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    ios_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    discovery_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                },
                required: ["reservation_type", "web_url", "android_url", "ios_url", "discovery_url"],
                additionalProperties: false,
            },
        },
        required: ["parking_id", "payment", "reservation"],
    },
    additionalProperties: false,
};

import { IIptOictTariffsProperties } from "#sch/datasources/iptoict/interfaces/IIptOictTariffsProperties";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export const iptOictTariffsSchema: JSONSchemaType<IIptOictTariffsProperties[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            parking_ids: {
                type: "array",
                items: { type: "string" },
            },
            charge_bands: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        maximum_duration: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                        valid_from: {
                            oneOf: [
                                { type: "string", format: "date-time" },
                                { type: "null", nullable: true },
                            ],
                        },
                        valid_to: {
                            oneOf: [
                                { type: "string", format: "date-time" },
                                { type: "null", nullable: true },
                            ],
                        },
                        last_modified_at_source: { type: "string", format: "date-time" },
                        free_of_charge: { type: "boolean" },
                        url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                        payment_mode: { type: "string" },
                        payment_methods: {
                            type: "array",
                            items: { type: "string" },
                        },
                        charge_band_name: { type: "string" },
                        charges: {
                            type: "array",
                            items: {
                                type: "object",
                                properties: {
                                    charge: { type: "string" },
                                    charge_type: { type: "string" },
                                    charge_order_index: { type: "number" },
                                    charge_interval: {
                                        oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                                    },
                                    max_iterations_of_charge: {
                                        oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                                    },
                                    min_iterations_of_charge: {
                                        oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                                    },
                                    valid_from: {
                                        oneOf: [
                                            { type: "string", format: "date-time" },
                                            { type: "null", nullable: true },
                                        ],
                                    },
                                    periods_of_time: {
                                        type: "array",
                                        items: {
                                            type: "object",
                                            properties: {
                                                day_in_week: { type: "string" },
                                                start: { type: "string" },
                                                end: { type: "string" },
                                                ph: { type: "string" },
                                            },
                                            required: ["day_in_week", "start", "end", "ph"],
                                            additionalProperties: false,
                                        },
                                    },
                                },
                                required: ["charge", "charge_order_index"],
                                additionalProperties: false,
                            },
                        },
                    },
                    required: ["charges", "charge_band_name", "free_of_charge", "payment_mode", "last_modified_at_source"],
                    additionalProperties: false,
                },
            },
        },
        required: ["parking_ids", "charge_bands"],
        additionalProperties: false,
    },
};

import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IIptOictParkingSpacesProperties } from "./interfaces/IIptOictParkingSpacesProperties";

export const iptOictParkingSpacesSchema: JSONSchemaType<IIptOictParkingSpacesProperties> = {
    type: "object",
    properties: {
        source: { type: "string" },
        parking_id: { type: "string" },
        id: { type: "string" },
        name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        capacity: { type: "integer" },
        access: {
            oneOf: [
                {
                    type: "array",
                    items: {
                        type: "string",
                        enum: [
                            "disabled",
                            "designated",
                            "delivery",
                            "RV",
                            "charging",
                            "resident",
                            "customer",
                            "bus",
                            "motorcycle",
                            "parent",
                            "truck",
                        ],
                    },
                },
                { type: "null", nullable: true },
            ],
        },
    },
    required: ["id", "capacity", "parking_id"],
    additionalProperties: false,
};

export interface IIptOictParkingSpacesProperties {
    parking_id: string;
    id: string;
    name: string | null;
    capacity: number;
    access: string[] | null;
    source: string;
}

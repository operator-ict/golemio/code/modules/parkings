export interface IptoictEntrancesProperties {
    parking_id: string;
    source_id: string;
    entry: boolean | null;
    exit: boolean | null;
    entrance_type: string[] | null;
    level: number | null;
    dimension: IDimension | null;
}

interface IDimension {
    max_height: number | null;
    max_width: number | null;
    max_length: number | null;
    max_weight: number | null;
}

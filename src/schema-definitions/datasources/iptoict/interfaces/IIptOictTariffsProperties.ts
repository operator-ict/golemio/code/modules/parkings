import { IParkingTariffPeriod } from "#sch/models/interfaces/IParkingTariff";

interface IIptOictCharge {
    charge: string;
    charge_type: string;
    charge_order_index: number;
    charge_interval: number | null;
    max_iterations_of_charge: number | null;
    min_iterations_of_charge: number | null;
    valid_from: string | null;
    periods_of_time: IParkingTariffPeriod[];
}

interface IIptOictChargeBand {
    maximum_duration: number | null;
    valid_from: string | null;
    valid_to: string | null;
    last_modified_at_source: string;
    free_of_charge: boolean;
    url: string | null;
    payment_mode: string;
    payment_methods: string[];
    charge_band_name: string;
    charges: IIptOictCharge[];
}

export interface IIptOictTariffsProperties {
    parking_ids: string[];
    charge_bands: IIptOictChargeBand[];
}

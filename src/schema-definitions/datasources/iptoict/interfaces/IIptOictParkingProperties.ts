import { IParkingTariffPeriod } from "#sch/models/interfaces/IParkingTariff";

export interface IIptOictParkingProperties {
    source: string;
    source_id: string;
    name: string | null;
    covered: boolean | null;
    supervised: SupervisedEnum | null;
    start_date: string | null;
    end_date: string | null;
    charge: string | null;
    opening_hours: IIptOictOpeningHours[] | null;
    maxstay: string | null;
    email: string | null;
    phone: string | null;
    website: string | null;
    parking_policy: string | null;
    parking_structure: string | null;
    capacity: number | null;
}

interface IIptOictOpeningHours {
    valid_from: string | null;
    valid_to: string | null;
    periods_of_time: IParkingTariffPeriod[];
}

export enum SupervisedEnum {
    No = "no",
    Yes = "yes",
}

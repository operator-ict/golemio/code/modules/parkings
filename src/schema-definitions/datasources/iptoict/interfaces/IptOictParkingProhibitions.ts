export interface IptoictParkingProhibitions {
    source: string;
    parking_id: string;
    prohibited_access: IProhibitedAccess;
}

interface IProhibitedAccess {
    "lpg/cng": boolean | null;
    bus: boolean | null;
    truck: boolean | null;
    motorcycle: boolean | null;
    bicycle: boolean | null;
    trailer: boolean | null;
}

export interface IIptOictPaymentsProperties {
    parking_id: string;
    payment: {
        web_url: string | null;
        android_url: string | null;
        ios_url: string | null;
        discovery_url: string | null;
    };
    reservation: {
        reservation_type: string;
        web_url: string | null;
        android_url: string | null;
        ios_url: string | null;
        discovery_url: string | null;
    };
}

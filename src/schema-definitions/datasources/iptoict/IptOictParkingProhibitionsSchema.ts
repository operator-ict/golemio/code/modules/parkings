import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IptoictParkingProhibitions } from "./interfaces/IptOictParkingProhibitions";

export const iptOictParkingProhibitionsSchema: JSONSchemaType<IptoictParkingProhibitions[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            parking_id: { type: "string" },
            source: { type: "string" },
            prohibited_access: {
                type: "object",
                properties: {
                    "lpg/cng": {
                        oneOf: [{ type: "boolean" }, { type: "null", nullable: true }],
                    },
                    bus: {
                        oneOf: [{ type: "boolean" }, { type: "null", nullable: true }],
                    },
                    truck: {
                        oneOf: [{ type: "boolean" }, { type: "null", nullable: true }],
                    },
                    motorcycle: {
                        oneOf: [{ type: "boolean" }, { type: "null", nullable: true }],
                    },
                    bicycle: {
                        oneOf: [{ type: "boolean" }, { type: "null", nullable: true }],
                    },
                    trailer: {
                        oneOf: [{ type: "boolean" }, { type: "null", nullable: true }],
                    },
                },
                required: [],
            },
        },
        required: ["parking_id", "prohibited_access", "source"],
    },
    additionalProperties: false,
};

import { SupportedGeometrySchema } from "#sch/datasources/shared/SupportedGeometrySchema";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IIptOictParkingProperties } from "./interfaces/IIptOictParkingProperties";
import { IIptOictParkingSpacesProperties } from "./interfaces/IIptOictParkingSpacesProperties";
import { iptoictEntrancesSchema } from "./IptoictEntrancesSchema";
import { iptOictParkingSchema } from "./IptOictParkingSchema";
import { iptOictParkingSpacesSchema } from "./IptOictParkingSpacesSchema";

export class IptOictFeatureCollectionSchemaGenerator {
    public static getParkingSchema() {
        return this.getSchema<IIptOictParkingProperties>(iptOictParkingSchema);
    }

    public static getParkingSpacesSchema() {
        return this.getSchema<IIptOictParkingSpacesProperties>(iptOictParkingSpacesSchema);
    }

    public static getParkingEntrancesSchema() {
        return this.getSchema<IIptOictParkingSpacesProperties>(iptoictEntrancesSchema as any);
    }

    protected static getSchema<T>(propertiesSchema: JSONSchemaType<T>) {
        return {
            type: "object",
            properties: {
                type: {
                    type: "string",
                    enum: ["FeatureCollection"],
                },
                features: {
                    type: "array",
                    items: {
                        type: "object",
                        properties: {
                            type: {
                                type: "string",
                                enum: ["Feature"],
                            },
                            geometry: SupportedGeometrySchema,
                            properties: propertiesSchema,
                        },
                        required: ["type", "geometry", "properties"],
                    },
                },
            },
            required: ["type", "features"],
        };
    }
}

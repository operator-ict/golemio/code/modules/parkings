import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IptoictEntrancesProperties } from "./interfaces/IptoictEntrancesProperties";

export const iptoictEntrancesSchema: JSONSchemaType<IptoictEntrancesProperties> = {
    type: "object",
    properties: {
        parking_id: { type: "string" },
        source_id: { type: "string" },
        entry: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
        exit: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
        level: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
        dimension: {
            oneOf: [
                {
                    type: "object",
                    properties: {
                        max_height: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                        max_width: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                        max_length: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                        max_weight: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                    },
                    required: [],
                },
                { type: "null", nullable: true },
            ],
        },
        entrance_type: {
            oneOf: [
                {
                    type: "array",
                    items: {
                        type: "string",
                    },
                },
                { type: "null", nullable: true },
            ],
        },
    },
    required: ["parking_id", "source_id"],
    additionalProperties: false,
};

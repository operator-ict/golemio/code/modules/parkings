import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IIptOictParkingProperties } from "./interfaces/IIptOictParkingProperties";

export const iptOictParkingSchema: JSONSchemaType<IIptOictParkingProperties> = {
    type: "object",
    properties: {
        source: { type: "string" },
        source_id: { type: "string" },
        name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        covered: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
        supervised: {
            oneOf: [
                { type: "string", enum: ["yes", "no"] },
                { type: "null", nullable: true },
            ],
        },
        start_date: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        end_date: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        charge: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        opening_hours: {
            oneOf: [
                {
                    type: "array",
                    items: {
                        type: "object",
                        properties: {
                            valid_from: {
                                oneOf: [
                                    { type: "string", format: "date-time" },
                                    { type: "null", nullable: true },
                                ],
                            },
                            valid_to: {
                                oneOf: [
                                    { type: "string", format: "date-time" },
                                    { type: "null", nullable: true },
                                ],
                            },
                            periods_of_time: {
                                type: "array",
                                items: {
                                    type: "object",
                                    properties: {
                                        day_in_week: { type: "string" },
                                        start: { type: "string" },
                                        end: { type: "string" },
                                        ph: { type: "string" },
                                    },
                                    required: ["day_in_week", "start", "end", "ph"],
                                },
                            },
                        },
                        required: ["periods_of_time"],
                    },
                },
                { type: "null", nullable: true },
            ],
        },
        maxstay: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        email: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        phone: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        website: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        capacity: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
        parking_policy: {
            oneOf: [
                {
                    type: "string",
                    enum: ["park_and_ride", "kiss_and_ride", "commercial", "customer_only", "zone", "park_sharing"],
                },
                { type: "null", nullable: true },
            ],
        },
        parking_structure: {
            oneOf: [
                { type: "string", enum: ["on_street", "underground", "multi_storey", "surface", "rooftop"] },
                { type: "null", nullable: true },
            ],
        },
    },
    required: ["source_id", "opening_hours", "source"],
};

import { ISchemaDefinition } from "#sch";
import { ITskParkingMachine } from "#sch/datasources/interfaces/ITskParkingMachine";

export const TskParkingMachinesSchema: ISchemaDefinition<ITskParkingMachine[]> = {
    name: "TskParkingMachines",
    jsonSchema: {
        type: "array",
        items: {
            type: "object",
            properties: {
                idParkMachine: { type: "string" },
                code: { type: "string" },
                idStatus: { type: "number" },
                type: { type: "string" },
                idTariff: { type: "string" },
                positionWKT: { type: "string" },
                activeFrom: { type: "string" },
            },
            required: ["idParkMachine", "code", "idStatus", "type", "idTariff", "positionWKT", "activeFrom"],
        },
    },
};

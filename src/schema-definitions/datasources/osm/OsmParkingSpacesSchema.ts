import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IOsmParkingSpacesProperties } from "./interfaces/IOsmParkingSpacesProperties";

export const osmParkingSpacesSchema: JSONSchemaType<IOsmParkingSpacesProperties> = {
    type: "object",
    properties: {
        parking_id: { type: "string" },
        parking_space_id: { type: "string" },
        osm_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        capacity: { type: "integer" },
        access: {
            oneOf: [
                {
                    type: "string",
                    enum: [
                        "disabled",
                        "designated",
                        "delivery",
                        "RV",
                        "charging",
                        "resident",
                        "customer",
                        "bus",
                        "motorcycle",
                        "parent",
                        "truck",
                    ],
                },
                { type: "null", nullable: true },
            ],
        },
    },
    required: ["parking_id"],
    additionalProperties: false,
};

export interface IOsmEntranceProperties {
    osm_id: string;
    parking_id: string;
    source_id: string | null;
    name: string | null;
    entry: boolean | null;
    exit: boolean | null;
    entrance_type: string[] | null;
    level: number | null;
    maxheight: number | null;
    maxwidth: number | null;
    maxlength: number | null;
    maxweight: number | null;
}

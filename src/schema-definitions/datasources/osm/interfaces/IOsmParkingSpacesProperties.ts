export interface IOsmParkingSpacesProperties {
    parking_space_id: string;
    parking_id: string;
    osm_id: string | null;
    name: string | null;
    capacity: number;
    access: string | null;
}

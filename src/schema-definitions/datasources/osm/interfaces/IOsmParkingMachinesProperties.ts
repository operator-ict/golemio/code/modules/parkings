export interface IOsmParkingMachinesProperties {
    parking_id: string;
    osm_id: string;
    name: string | null;
}

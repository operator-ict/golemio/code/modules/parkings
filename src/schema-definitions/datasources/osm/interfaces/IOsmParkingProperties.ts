export interface IOsmParkingProperties {
    osm_id: string;
    name: string | null;
    covered: "yes" | "no" | null;
    supervised: "yes" | "no" | null;
    start_date: string | null;
    end_date: string | null;
    charge: string | null;
    opening_hours: string | null;
    maxstay: string | null;
    email: string | null;
    phone: string | null;
    website: string | null;
    parking_policy: string | null;
    parking_structure: string | null;
}

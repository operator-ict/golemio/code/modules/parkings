import { SupportedGeometrySchema } from "#sch/datasources/shared/SupportedGeometrySchema";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IOsmParkingProperties } from "./interfaces/IOsmParkingProperties";
import { IOsmParkingSpacesProperties } from "./interfaces/IOsmParkingSpacesProperties";
import { IOsmEntranceProperties } from "./interfaces/IOsmEntranceProperties";
import { IOsmParkingMachinesProperties } from "#sch/datasources/osm/interfaces/IOsmParkingMachinesProperties";
import { osmEntrancesParkingSchema } from "./OsmEntrancesParkingSchema";
import { osmParkingSchema } from "./OsmParkingSchema";
import { osmParkingSpacesSchema } from "./OsmParkingSpacesSchema";
import { osmParkingMachinesSchema } from "#sch/datasources/osm/OsmParkingMachinesSchema";

export class OsmSchemaGenerator {
    public static getParkingSchema() {
        return this.getSchema<IOsmParkingProperties>(osmParkingSchema);
    }

    public static getParkingSpacesSchema() {
        return this.getSchema<IOsmParkingSpacesProperties>(osmParkingSpacesSchema);
    }

    public static getEntranceSchema() {
        return this.getSchema<IOsmEntranceProperties>(osmEntrancesParkingSchema);
    }

    public static getParkingMachinesSchema() {
        return this.getSchema<IOsmParkingMachinesProperties>(osmParkingMachinesSchema);
    }

    private static getSchema<T>(propertiesSchema: JSONSchemaType<T>) {
        return {
            type: "object",
            properties: {
                type: {
                    type: "string",
                    enum: ["FeatureCollection"],
                },
                features: {
                    type: "array",
                    items: {
                        type: "object",
                        properties: {
                            type: {
                                type: "string",
                                enum: ["Feature"],
                            },
                            geometry: SupportedGeometrySchema,
                            properties: propertiesSchema,
                        },
                        required: ["type", "geometry", "properties"],
                    },
                },
            },
            required: ["type", "features"],
        };
    }
}

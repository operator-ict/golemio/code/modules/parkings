import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IOsmParkingMachinesProperties } from "#sch/datasources/osm/interfaces/IOsmParkingMachinesProperties";

export const osmParkingMachinesSchema: JSONSchemaType<IOsmParkingMachinesProperties> = {
    type: "object",
    properties: {
        parking_id: { type: "string" },
        osm_id: { type: "string" },
        name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
    },
    required: ["parking_id", "osm_id"],
    additionalProperties: true,
};

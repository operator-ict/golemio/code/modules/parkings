import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IOsmParkingProperties } from "./interfaces/IOsmParkingProperties";

export const osmParkingSchema: JSONSchemaType<IOsmParkingProperties> = {
    type: "object",
    properties: {
        osm_id: { type: "string" },
        name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        covered: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        supervised: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        start_date: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        end_date: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        charge: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        opening_hours: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        maxstay: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        email: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        phone: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        website: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        parking_policy: {
            oneOf: [
                {
                    type: "string",
                    enum: ["park_and_ride", "kiss_and_ride", "commercial", "customer_only", "zone", "park_sharing"],
                },
                { type: "null", nullable: true },
            ],
        },
        parking_structure: {
            oneOf: [
                { type: "string", enum: ["on_street", "underground", "multi_storey", "surface", "rooftop"] },
                { type: "null", nullable: true },
            ],
        },
    },
    required: ["osm_id"],
};

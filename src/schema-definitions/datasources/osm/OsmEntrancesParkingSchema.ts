import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IOsmEntranceProperties } from "./interfaces/IOsmEntranceProperties";

export const osmEntrancesParkingSchema: JSONSchemaType<IOsmEntranceProperties> = {
    type: "object",
    properties: {
        osm_id: { type: "string" },
        parking_id: { type: "string" },
        source_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        entry: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
        exit: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
        maxheight: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
        level: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
        maxwidth: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
        maxlength: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
        maxweight: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
        entrance_type: {
            oneOf: [
                {
                    type: "array",
                    items: {
                        type: "string",
                    },
                },
                { type: "null", nullable: true },
            ],
        },
    },
    required: ["osm_id", "parking_id"],
    additionalProperties: false,
};

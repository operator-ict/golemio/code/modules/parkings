import { ISchemaDefinition } from "#sch";
import { ITskParkingSectionLevel } from "#sch/datasources/interfaces/ITskParkingSectionLevel";

export const TskParkingSectionLevelSchema: ISchemaDefinition<ITskParkingSectionLevel[]> = {
    name: "TskParkingLevelSchema",
    jsonSchema: {
        type: "array",
        items: {
            type: "object",
            properties: {
                idSectionItem: { type: "string" },
                idSection: { type: "string" },
                sectionItemCode: { type: "string" },
                streetNameValid: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                sectionCat: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                borderWKT: { type: "string" },
                parkingType: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                projectedCapacity: { type: "integer" },
                activeFrom: { type: "string" },
            },
            required: ["idSectionItem", "idSection", "sectionItemCode", "borderWKT", "projectedCapacity", "activeFrom"],
        },
    },
};

import { IZtpParkingJsonFeature } from "#sch/datasources/interfaces/IZtpParkingJsonFeature";
import { ISchemaDefinition } from "#sch";

export const ZtpParkingsJsonSchema: ISchemaDefinition<IZtpParkingJsonFeature[]> = {
    name: "ZtpParkingsJsonSchema",
    jsonSchema: {
        type: "array",
        items: {
            type: "object",
            properties: {
                type: {
                    type: "string",
                },
                id: {
                    type: "number",
                },
                geometry: {
                    type: "object",
                    properties: {
                        type: {
                            type: "string",
                        },
                        coordinates: {
                            type: "array",
                            items: {
                                type: "number",
                            },
                        },
                    },
                    required: ["type", "coordinates"],
                    additionalProperties: false,
                },
                properties: {
                    type: "object",
                    properties: {
                        OBJECTID: {
                            type: "number",
                        },
                        POCET_PS: {
                            type: "number",
                        },
                        ROZM_DELKA: {
                            type: "number",
                        },
                        ROZM_SIRKA: {
                            type: "number",
                        },
                        TYP_PS: {
                            type: "number",
                        },
                        KOLME_PS: {
                            type: "number",
                        },
                        POD_SKLON: {
                            type: "number",
                        },
                        PRIC_SKLON: {
                            type: "number",
                        },
                        TYP_POVRCH: {
                            type: "number",
                        },
                        POSK_POVRC: {
                            type: "number",
                        },
                        PES_ZONA: {
                            type: "number",
                        },
                        MAT_POVRCH: {
                            type: "number",
                        },
                        GLOBALID: {
                            type: "string",
                        },
                    },
                    required: [
                        "OBJECTID",
                        "POCET_PS",
                        "ROZM_DELKA",
                        "ROZM_SIRKA",
                        "TYP_PS",
                        "KOLME_PS",
                        "POD_SKLON",
                        "PRIC_SKLON",
                        "TYP_POVRCH",
                        "POSK_POVRC",
                        "PES_ZONA",
                        "MAT_POVRCH",
                        "GLOBALID",
                    ],

                    additionalProperties: false,
                },
            },
            required: ["type", "geometry", "properties"],
            additionalProperties: false,
        },
    },
};

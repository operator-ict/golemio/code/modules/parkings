import {
    v1ParkingRouter,
    v2ParkingRouter,
    v3ParkingMachinesRouter,
    v3ParkingRouter,
    v3ParkingTariffRouter,
    v3ParkingMeasurementsRouter,
    v3ParkingSourcesRouter,
} from "#og/routers";

export * from "./models";
export * from "./routers";

export const routers = [
    v1ParkingRouter,
    v2ParkingRouter,
    v3ParkingMachinesRouter,
    v3ParkingRouter,
    v3ParkingTariffRouter,
    v3ParkingMeasurementsRouter,
    v3ParkingSourcesRouter,
];

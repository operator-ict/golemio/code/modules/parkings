import { Parkings } from "#sch";
import { PaymentModel } from "#sch/models/PaymentModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models/SequelizeModel";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ParkingPaymentsRepository extends SequelizeModel {
    constructor() {
        super("ParkingPaymentsRepository", PaymentModel.tableName, PaymentModel.attributeModel, {
            schema: Parkings.pgSchema,
        });
    }

    public GetAll(): never {
        throw new Error("Not implemented");
    }

    public GetOne(): never {
        throw new Error("Not implemented");
    }
}

import { IParkingTariffsParams } from "#og/helpers/params/IParkingTariffsParams";
import { Parkings } from "#sch";
import { IParkingTariff } from "#sch/models/interfaces/IParkingTariff";
import { ParkingTariffsModel } from "#sch/models/ParkingTariffsModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models/SequelizeModel";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ParkingTariffsRepository extends SequelizeModel {
    constructor() {
        super("OGParkingTariffRepository", ParkingTariffsModel.tableName, ParkingTariffsModel.attributeModel, {
            schema: Parkings.pgSchema,
        });
        this.associate();
    }

    private associate() {
        this.sequelizeModel.hasMany(this.sequelizeModel, {
            as: "parking_tariffs",
            foreignKey: "tariff_id",
            sourceKey: "tariff_id",
        });
        this.sequelizeModel.belongsTo(this.sequelizeModel, {
            foreignKey: "tariff_id",
            targetKey: "tariff_id",
        });
    }

    public async GetAll(
        params: IParkingTariffsParams
    ): Promise<Array<{ tariff_id: string; parking_tariffs: Array<IParkingTariff & { updated_at: Date }> }>> {
        try {
            let where: Sequelize.WhereOptions<IParkingTariff> = {};

            if (params.primarySource) {
                where.source = { [Op.in]: params.primarySource };
            } else {
                throw new GeneralError("List of sources must be provided! ", this.name, undefined, 500);
            }

            if (params.validFrom) {
                where.valid_from = { [Op.gte]: params.validFrom };
            }

            if (params.validTo) {
                where.valid_to = { [Op.gte]: params.validTo };
            }

            const result = await this.sequelizeModel.findAll({
                attributes: [Sequelize.literal("DISTINCT ON(tariff_id) 1") as unknown as string, "tariff_id"],
                include: {
                    model: this.sequelizeModel,
                    attributes: { include: ["updated_at"] },
                    as: "parking_tariffs",
                    required: true,
                    subQuery: false,
                    order: [
                        ["tariff_id", "ASC"],
                        ["charge_order_index", "ASC"],
                    ],
                },
                order: [
                    ["parking_tariffs", "tariff_id", "ASC"],
                    ["parking_tariffs", "charge_band_name", "ASC"],
                    ["parking_tariffs", "charge_order_index", "ASC"],
                ],
                where: where,
                limit: params.limit,
                offset: params.offset,
            });

            return result;
        } catch (err) {
            throw new GeneralError("Database error ~ GetAll", this.name, err, 500);
        }
    }

    public async GetOne(id: string, sources?: string[]): Promise<Array<IParkingTariff & { updated_at: Date }> | null> {
        try {
            if (!sources) {
                throw new GeneralError("List of sources must be provided!", this.name, undefined, 500);
            }

            const results = await this.sequelizeModel.findAll({
                attributes: { include: ["updated_at"] },
                where: {
                    tariff_id: id,
                    source: {
                        [Op.in]: sources,
                    },
                },
                order: [
                    ["charge_band_name", "ASC"],
                    ["charge_order_index", "ASC"],
                ],
                raw: true,
            });

            if (results.length === 0) {
                return null;
            }

            return results;
        } catch (err) {
            throw new GeneralError("Database error ~ GetOne", this.name, err, 500);
        }
    }
}

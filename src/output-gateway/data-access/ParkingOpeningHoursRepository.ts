import { Parkings } from "#sch";
import { OpeningHoursModel } from "#sch/models/OpeningHoursModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ParkingOpeningHoursRepository extends SequelizeModel {
    constructor() {
        super("ParkingsOpeningHoursRepository", OpeningHoursModel.tableName, OpeningHoursModel.attributeModel, {
            schema: Parkings.pgSchema,
        });
    }

    GetAll(): never {
        throw new Error("Not implemented");
    }

    GetOne(): never {
        throw new Error("Not implemented");
    }
}

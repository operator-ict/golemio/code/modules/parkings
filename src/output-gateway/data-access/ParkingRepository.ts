import { SourceEnum } from "#helpers/constants/SourceEnum";
import { ParkingAverageOccupancyRepository } from "#og/data-access/ParkingAverageOccupancyRepository";
import { ParkingEntrancesRepository } from "#og/data-access/ParkingEntrancesRepository";
import { ParkingPaymentsRepository } from "#og/data-access/ParkingPaymentsRepository";
import { ParkingProhibitionsRepository } from "#og/data-access/ParkingProhibitionsRepository";
import { GeoConfigHelper } from "#og/helpers/GeoConfigHelper";
import { IParkingParams } from "#og/helpers/params/IParkingParams";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { ParkingsLocationModel } from "#og/models/ParkingsLocationModel";
import { Parkings } from "#sch";
import { IParking, IParkingWithLocationAndOccupancyInfo } from "#sch/models/interfaces/IParking";
import { IParkingLocation } from "#sch/models/interfaces/IParkingLocation";
import { IOpeningHours } from "#sch/models/interfaces/openingHours/IOpeningHours";
import { SequelizeModel, TGeoCoordinates } from "@golemio/core/dist/output-gateway";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Op, Sequelize, WhereOptions, literal } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ParkingOpeningHoursRepository } from "./ParkingOpeningHoursRepository";
import { IRepositorySetup } from "./interfaces/IRepositorySetup";
@injectable()
export class ParkingRepository extends SequelizeModel {
    public static readonly defaultSetup: IRepositorySetup = {
        name: "OGParkingRepository",
        tableName: Parkings.parkings.pgTableName,
        attributes: Parkings.parkings.outputSequelizeAttributes,
        options: { schema: Parkings.pgSchema },
    };
    private geoConfigHelper: GeoConfigHelper;
    protected readonly shouldHideSecondarySourcesFromPrimaryData: boolean = true;

    constructor(
        @inject(ModuleContainerToken.ParkingRepositorySetup)
        setup: IRepositorySetup,
        @inject(ModuleContainerToken.ParkingsLocationModel) protected locationModel: ParkingsLocationModel,
        @inject(ModuleContainerToken.ParkingAverageOccupancyRepository)
        private averageOccupancyRepository: ParkingAverageOccupancyRepository,
        @inject(ModuleContainerToken.ParkingOpeningHoursRepository)
        protected parkingOpeningHoursRepository: ParkingOpeningHoursRepository,
        @inject(ModuleContainerToken.ParkingEntrancesRepository)
        protected entrancesRepository: ParkingEntrancesRepository,
        @inject(ModuleContainerToken.ParkingPaymentsRepository)
        protected paymentsRepository: ParkingPaymentsRepository,
        @inject(ModuleContainerToken.ParkingProhibitionsRepository)
        protected parkingProhibitionsRepository: ParkingProhibitionsRepository
    ) {
        super(setup.name, setup.tableName, setup.attributes, setup.options);
        this.geoConfigHelper = ParkingsContainer.resolve<GeoConfigHelper>(ModuleContainerToken.GeoConfigHelper);
        this.associate();
    }

    public associate() {
        this.sequelizeModel.hasMany(this.locationModel.sequelizeModel, {
            as: "parking_locations",
            foreignKey: "source_id",
            sourceKey: "source_id",
        });
        this.sequelizeModel.hasMany(this.entrancesRepository.sequelizeModel, {
            as: "parking_entrances",
            foreignKey: "parking_id",
            sourceKey: "id",
        });
        this.sequelizeModel.hasOne(this.averageOccupancyRepository.sequelizeModel, {
            as: "parking_average_occupancy",
            foreignKey: "source_id",
            sourceKey: "source_id",
        });
        this.sequelizeModel.hasMany(this.parkingOpeningHoursRepository.sequelizeModel, {
            as: "parking_hours",
            foreignKey: "parking_id",
            sourceKey: "id",
        });
        this.sequelizeModel.hasOne(this.paymentsRepository.sequelizeModel, {
            as: "parking_payments",
            foreignKey: "parking_id",
            sourceKey: "id",
        });
        this.sequelizeModel.hasOne(this.parkingProhibitionsRepository.sequelizeModel, {
            as: "parking_prohibitions",
            foreignKey: "parking_id",
            sourceKey: "id",
        });
    }

    public async GetAll(params: IParkingParams): Promise<IParkingWithLocationAndOccupancyInfo[]> {
        try {
            let { whereLocation, where, whereOpeningHours } = await this.prepareWhereConditions(params);

            const result = await this.sequelizeModel.findAll({
                attributes: { include: ["updated_at"] },
                include: [
                    {
                        model: this.locationModel.sequelizeModel,
                        as: "parking_locations",
                        on: {
                            source: {
                                [Op.eq]: Sequelize.col(`"${this.sequelizeModel.tableName}".source`),
                            },
                            source_id: {
                                [Op.eq]: Sequelize.col(`"${this.sequelizeModel.tableName}".source_id`),
                            },
                        },
                        required: true,
                        subQuery: false,
                        where: whereLocation,
                    },
                    {
                        model: this.parkingOpeningHoursRepository.sequelizeModel,
                        as: "parking_hours",
                        on: {
                            parking_id: {
                                [Op.eq]: Sequelize.col(`"${this.sequelizeModel.tableName}".id`),
                            },
                        },
                        required: false,
                        subQuery: false,
                        where: whereOpeningHours,
                    },
                    {
                        model: this.averageOccupancyRepository.sequelizeModel,
                        as: "parking_average_occupancy",
                        attributes: ["source_id"],
                        on: {
                            source: {
                                [Op.eq]: Sequelize.col(`"${this.sequelizeModel.tableName}".source`),
                            },
                            source_id: {
                                [Op.eq]: Sequelize.col(`"${this.sequelizeModel.tableName}".source_id`),
                            },
                        },
                        required: false,
                        subQuery: false,
                    },
                    {
                        model: this.entrancesRepository.sequelizeModel,
                        as: "parking_entrances",
                        on: {
                            parking_id: {
                                [Op.eq]: Sequelize.col(`"${this.sequelizeModel.tableName}".id`),
                            },
                        },
                        required: false,
                        subQuery: false,
                    },
                    {
                        model: this.paymentsRepository.sequelizeModel,
                        as: "parking_payments",
                        required: false,
                        subQuery: false,
                    },
                    {
                        model: this.parkingProhibitionsRepository.sequelizeModel,
                        as: "parking_prohibitions",
                        attributes: ["lpg", "bus", "truck", "motorcycle", "bicycle", "trailer"],
                        required: false,
                        subQuery: false,
                    },
                ],
                where,
                order: [
                    ["id", "ASC"],
                    [{ model: this.locationModel.sequelizeModel, as: "parking_locations" }, "id", "ASC"],
                    [{ model: this.parkingOpeningHoursRepository.sequelizeModel, as: "parking_hours" }, "valid_from", "ASC"],
                    [
                        { model: this.averageOccupancyRepository.sequelizeModel, as: "parking_average_occupancy" },
                        "day_of_week",
                        "ASC",
                    ],
                    [{ model: this.averageOccupancyRepository.sequelizeModel, as: "parking_average_occupancy" }, "hour", "ASC"],
                    [{ model: this.entrancesRepository.sequelizeModel, as: "parking_entrances" }, "entrance_id", "ASC"],
                ],
                limit: params.limit,
                offset: params.offset,
                bind: params.boundingBox && {
                    lonMin: params.boundingBox[1],
                    latMin: params.boundingBox[2],
                    lonMax: params.boundingBox[3],
                    latMax: params.boundingBox[0],
                },
            });

            return result.map((element) => {
                const result = element.dataValues;

                result.parking_locations = result.parking_locations.map((location: any) => location.dataValues);
                result.parking_average_occupancy = !!result.parking_average_occupancy;
                result.parking_hours = result.parking_hours.map((item: any) => item.dataValues);

                return result;
            });
        } catch (err) {
            throw new GeneralError("Database error ~ GetAll " + err.message, this.name, err, 500);
        }
    }

    public async GetOne(id: string, sources?: string[]): Promise<IParkingWithLocationAndOccupancyInfo | null> {
        try {
            if (!sources) {
                throw new GeneralError("List of sources must be provided!", this.name, undefined, 500);
            }

            const result = await this.sequelizeModel.findOne({
                attributes: { include: ["updated_at"] },
                include: [
                    {
                        model: this.locationModel.sequelizeModel,
                        as: "parking_locations",
                        on: {
                            source: {
                                [Op.eq]: Sequelize.col(`"${this.sequelizeModel.tableName}".source`),
                            },
                            source_id: {
                                [Op.eq]: Sequelize.col(`"${this.sequelizeModel.tableName}".source_id`),
                            },
                        },
                        required: true,
                        subQuery: false,
                    },
                    {
                        model: this.parkingOpeningHoursRepository.sequelizeModel,
                        as: "parking_hours",
                        on: {
                            parking_id: {
                                [Op.eq]: Sequelize.col(`"${this.sequelizeModel.tableName}".id`),
                            },
                        },
                        required: false,
                        subQuery: false,
                    },
                    {
                        model: this.averageOccupancyRepository.sequelizeModel,
                        as: "parking_average_occupancy",
                        attributes: ["source_id"],
                        on: {
                            source: {
                                [Op.eq]: Sequelize.col(`"${this.sequelizeModel.tableName}".source`),
                            },
                            source_id: {
                                [Op.eq]: Sequelize.col(`"${this.sequelizeModel.tableName}".source_id`),
                            },
                        },
                        required: false,
                        subQuery: false,
                    },
                    {
                        model: this.entrancesRepository.sequelizeModel,
                        as: "parking_entrances",
                        on: {
                            parking_id: {
                                [Op.eq]: Sequelize.col(`"${this.sequelizeModel.tableName}".id`),
                            },
                        },
                        required: false,
                        subQuery: false,
                    },
                    {
                        model: this.paymentsRepository.sequelizeModel,
                        as: "parking_payments",
                        required: false,
                        subQuery: false,
                    },
                    {
                        model: this.parkingProhibitionsRepository.sequelizeModel,
                        as: "parking_prohibitions",
                        attributes: ["lpg", "bus", "truck", "motorcycle", "bicycle", "trailer"],
                        required: false,
                        subQuery: false,
                    },
                ],
                where: {
                    id,
                    source: {
                        [Op.in]: sources,
                    },
                },
                order: [
                    [{ model: this.locationModel.sequelizeModel, as: "parking_locations" }, "id", "ASC"],
                    [{ model: this.parkingOpeningHoursRepository.sequelizeModel, as: "parking_hours" }, "valid_from", "ASC"],
                    [
                        { model: this.averageOccupancyRepository.sequelizeModel, as: "parking_average_occupancy" },
                        "day_of_week",
                        "ASC",
                    ],
                    [{ model: this.averageOccupancyRepository.sequelizeModel, as: "parking_average_occupancy" }, "hour", "ASC"],
                    [{ model: this.entrancesRepository.sequelizeModel, as: "parking_entrances" }, "entrance_id", "ASC"],
                ],
            });

            if (result === null) {
                return null;
            }

            const resultData = result.dataValues;
            resultData.parking_locations = resultData.parking_locations.map((location: any) => location.dataValues);
            resultData.parking_hours = resultData.parking_hours.map((item: any) => item.dataValues);
            resultData.parking_average_occupancy = !!resultData.parking_average_occupancy;
            return resultData;
        } catch (err) {
            throw new GeneralError("Database error ~ GetOne " + err.message, this.name, err, 500);
        }
    }

    private async prepareWhereConditions(params: IParkingParams) {
        let where: WhereOptions<IParking> = {};
        let whereLocation: WhereOptions<IParkingLocation> = {};
        let whereOpeningHours: WhereOptions<IOpeningHours> = {};

        let primarySources: string[] = [];
        const areaLimit = Sequelize.fn(
            "ST_BuildArea",
            Sequelize.fn("ST_GeomFromGeoJSON", JSON.stringify(this.geoConfigHelper.returnGeoCoordinates()))
        );

        if (params.primarySource) {
            primarySources = params.primarySource;
        } else {
            throw new GeneralError("List of sources must be provided! ", this.name, undefined, 500);
        }

        // DO NOT SHOW OSM PARKINGS IF THERE ARE OTHER SOURCES
        if (this.shouldHideSecondarySourcesFromPrimaryData && primarySources.length > 1) {
            primarySources = primarySources.filter((source) => source !== SourceEnum.OSM);
        }

        where.source = { [Op.in]: primarySources };

        where.location = Sequelize.where(
            Sequelize.fn("ST_Within", Sequelize.col("sanitized_location"), areaLimit),
            "true"
        ) as unknown as TGeoCoordinates;

        if (params.validFrom) {
            where.valid_from = { [Op.gte]: params.validFrom };
        }

        if (params.minutesBefore) {
            where.updated_at = { [Op.gte]: Sequelize.literal(`NOW() - INTERVAL '${params.minutesBefore} minutes'`) };
        }

        if (params.updatedSince) {
            where.updated_at = { [Op.gte]: params.updatedSince };
        }

        if (params.parkingPolicy) {
            where.parking_policy = { [Op.in]: params.parkingPolicy };
        }

        if (params.activeOnly === true) {
            where.active = true;
        }

        if (params.boundingBox) {
            where = {
                ...where,
                [Op.and]: literal(`ST_Contains(ST_MakeEnvelope($lonMin, $latMin, $lonMax, $latMax, 4326), location)`),
            };
        }

        if (params.accessDedicatedTo) {
            whereLocation.special_access = { [Op.overlap]: params.accessDedicatedTo };
        }

        if (params.openTo) {
            whereOpeningHours.valid_from = { [Op.lte]: params.openTo };
        }

        if (params.openFrom) {
            whereOpeningHours.valid_to = {
                [Op.or]: [{ [Op.gte]: params.openFrom }, { [Op.eq]: null }],
            };
        }

        return { whereLocation, where, whereOpeningHours };
    }
}

import { Parkings } from "#sch";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models/SequelizeModel";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ParkingMeasurementRepository extends SequelizeModel {
    constructor() {
        super(
            "ParkingMeasurementRepository",
            Parkings.measurements.pgTableName,
            Parkings.measurements.outputSequelizeAttributes,
            {
                schema: Parkings.pgSchema,
            }
        );
    }

    public GetAll(): never {
        throw new Error("Not implemented");
    }

    public GetOne(): never {
        throw new Error("Not implemented");
    }
}

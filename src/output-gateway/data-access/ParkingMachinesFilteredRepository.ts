import { ParkingMachinesRepository } from "#og/data-access/ParkingMachinesRepository";
import { Parkings } from "#sch";
import { ParkingMachinesFilteredModel } from "#sch/models/ParkingMachinesFilteredModel";
import { singleton } from "@golemio/core/dist/shared/tsyringe";

@singleton()
export class ParkingMachinesFilteredRepository extends ParkingMachinesRepository {
    protected override readonly shouldHideSecondarySourcesFromPrimaryData: boolean = false;

    constructor() {
        super({
            name: "OGParkingMachinesFilteredRepository",
            tableName: ParkingMachinesFilteredModel.tableName,
            attributes: ParkingMachinesFilteredModel.attributeModel,
            options: {
                schema: Parkings.pgSchema,
                underscored: true,
            },
        });
    }

    public override GetOne(): never {
        throw new Error("Not implemented");
    }
}

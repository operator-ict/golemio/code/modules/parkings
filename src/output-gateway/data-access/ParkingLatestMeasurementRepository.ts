import { IParkingLatestMeasurements } from "#og/data-access/interfaces/IParkingMeasurementsDto";
import { IParkingMeasurementsParams } from "#og/helpers/params/IParkingMeasurementsParams";
import { Parkings } from "#sch";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models/SequelizeModel";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ParkingLatestMeasurementRepository extends SequelizeModel {
    constructor() {
        super(
            "ParkingLatestMeasurementRepository",
            Parkings.latestMeasurements.pgTableName,
            Parkings.latestMeasurements.outputSequelizeAttributes,
            {
                schema: Parkings.pgSchema,
            }
        );
    }

    public async GetAll(params: IParkingMeasurementsParams): Promise<IParkingLatestMeasurements[]> {
        try {
            if (!params.primarySource) {
                throw new GeneralError("List of sources must be provided! ", this.name, undefined, 500);
            }

            let where: Sequelize.WhereOptions<IParkingLatestMeasurements> = {
                source: { [Sequelize.Op.in]: params.primarySource },
            };

            if (params.parkingId) {
                where.parking_id = { [Sequelize.Op.in]: params.parkingId };
            }

            return await this.sequelizeModel.findAll({
                attributes: {
                    include: ["updated_at"],
                },
                where,
                order: [
                    ["source", "ASC"],
                    ["source_id", "ASC"],
                    ["date_modified", "DESC"],
                ],
                limit: params.limit,
                offset: params.offset,
            });
        } catch (err) {
            throw new GeneralError("Database error: GetAll", this.name, err, 500);
        }
    }

    public GetOne(): never {
        throw new Error("Not implemented");
    }
}

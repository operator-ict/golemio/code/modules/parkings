import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { ParkingAverageOccupancyRepository } from "#og/data-access/ParkingAverageOccupancyRepository";
import { ParkingEntrancesRepository } from "#og/data-access/ParkingEntrancesRepository";
import { ParkingPaymentsRepository } from "#og/data-access/ParkingPaymentsRepository";
import { ParkingProhibitionsRepository } from "#og/data-access/ParkingProhibitionsRepository";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { ParkingsLocationModel } from "#og/models/ParkingsLocationModel";
import { Parkings } from "#sch";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ParkingOpeningHoursRepository } from "./ParkingOpeningHoursRepository";
import { ParkingRepository } from "./ParkingRepository";

@injectable()
export class ParkingsFilteredRepository extends ParkingRepository {
    protected override readonly shouldHideSecondarySourcesFromPrimaryData: boolean = false;

    constructor(
        @inject(ModuleContainerToken.ParkingsLocationModel) locationModel: ParkingsLocationModel,
        @inject(ModuleContainerToken.ParkingAverageOccupancyRepository)
        averageOccupancyRepository: ParkingAverageOccupancyRepository,
        @inject(ModuleContainerToken.CachedParkingSourcesRepository)
        cachedParkingSourcesRepository: CachedParkingSourcesRepository,
        @inject(ModuleContainerToken.ParkingOpeningHoursRepository)
        parkingOpeningHoursRepository: ParkingOpeningHoursRepository,
        @inject(ModuleContainerToken.ParkingEntrancesRepository)
        entrancesRepository: ParkingEntrancesRepository,
        @inject(ModuleContainerToken.ParkingPaymentsRepository)
        paymentsRepository: ParkingPaymentsRepository,
        @inject(ModuleContainerToken.ParkingProhibitionsRepository)
        parkingProhibitionsRepository: ParkingProhibitionsRepository
    ) {
        super(
            {
                name: "ParkingsFilteredRepository",
                tableName: Parkings.parkingsFiltered.pgTableName,
                attributes: Parkings.parkingsFiltered.outputSequelizeAttributes,
                options: {
                    schema: Parkings.pgSchema,
                },
            },
            locationModel,
            averageOccupancyRepository,
            parkingOpeningHoursRepository,
            entrancesRepository,
            paymentsRepository,
            parkingProhibitionsRepository
        );
    }

    public override GetOne(): never {
        throw new Error("Not implemented");
    }
}

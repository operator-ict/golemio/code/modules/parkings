import { ParkingTariffsModel } from "#sch/models/ParkingTariffsModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { Parkings } from "#sch/index";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";
import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";

export interface IGetTariffsOptions {
    source?: string;
}

export interface IParkingsTariffCharge {
    charge: string; // decimal
    charge_type: string | null;
    charge_order_index: number;
    charge_interval: number | null;
    max_iterations_of_charge: number | null;
    min_iterations_of_charge: number | null;
    start_time_of_period: string | null;
    end_time_of_period: string | null;
}

export interface IParkingsTariffDescription {
    tariff_id: string;
    source: string;
    last_updated: string;
    payment_mode: string;
    payment_additional_description: string | null;
    free_of_charge: boolean;
    url_link_address: string | null;
    charge_band_name: string;
    accepts_payment_card: boolean | null;
    accepts_cash: boolean | null;
    accepts_mobile_payment: boolean | null;
    accepts_litacka: boolean | null;
    charge_currency: string;
    allowed_vehicle_type: string | null;
    allowed_fuel_type: string | null;
    valid_from: string | null;
    valid_to: string | null;
}

export interface IParkingsTariff extends IParkingsTariffCharge, IParkingsTariffDescription {}

export class V2ParkingTariffsRepository extends SequelizeModel {
    public constructor(private parkingSourcesRepository: CachedParkingSourcesRepository) {
        super("V2TariffRepository", ParkingTariffsModel.tableName, ParkingTariffsModel.attributeModel, {
            schema: Parkings.pgSchema,
        });
    }

    public GetAll = async (options: IGetTariffsOptions = {}): Promise<IParkingsTariff[]> => {
        const { source } = options;

        let where: Sequelize.WhereOptions = {};
        let attributes: Sequelize.FindAttributeOptions = { include: [], exclude: [] };

        if (source) {
            where.source = source;
        } else {
            where.source = {
                [Op.in]: await this.parkingSourcesRepository.getLegacySources({ isRestrictedToOpenData: false }),
            };
        }

        const order: Sequelize.Order = [];
        order.push(["tariff_id", "asc"], ["charge_order_index", "asc"]);

        try {
            return await this.sequelizeModel.findAll({
                attributes,
                order,
                raw: true,
                where,
            });
        } catch (err) {
            throw new GeneralError("Database error", "ParkingTariffsModel", err, 500);
        }
    };

    public GetOne = async (tariffId: string): Promise<IParkingsTariff[]> => {
        return await this.sequelizeModel.findAll({
            where: {
                tariff_id: tariffId,
                source: {
                    [Op.in]: await this.parkingSourcesRepository.getLegacySources({ isRestrictedToOpenData: false }),
                },
            },
            raw: true,
        });
    };
}

import { buildGeojsonFeatureType } from "@golemio/core/dist/output-gateway";
import { Model } from "@golemio/core/dist/shared/sequelize";
import { ITskParkingLotFeature } from "../interfaces/ITskParkingLotFeature";

export enum ParkingTypeEnum {
    ParkAndRide = "park_and_ride",
    ParkPaidPrivate = "park_paid_private",
}

export class TskFeatureMapper {
    public static formatOutput(record: Model): ITskParkingLotFeature | undefined {
        const { parking_type, id, last_updated, payment_link, ...rest } = record.get({ plain: true });
        return buildGeojsonFeatureType("centroid", {
            parking_type: this.createParkingTypeObject(parking_type),
            id: Number.isNaN(parseInt(id)) ? id : parseInt(id),
            last_updated: last_updated ? new Date(last_updated).getTime() : null,
            ...rest,
            ...this.getPaymentAttributes(payment_link),
        }) as ITskParkingLotFeature;
    }

    private static createParkingTypeObject(parkingType: ParkingTypeEnum): {
        description: string;
        id: 1 | 2;
    } {
        return parkingType === ParkingTypeEnum.ParkAndRide
            ? { description: "P+R parkoviště", id: 1 }
            : { description: "placené parkoviště", id: 2 };
    }

    private static getPaymentAttributes(paymentLink: string | null): {
        payment_link?: string | null;
        payment_shortname?: string | null;
    } {
        if (!paymentLink) {
            return {};
        }

        return {
            payment_link: paymentLink,
            payment_shortname: paymentLink.split("?shortname=").pop(),
        };
    }
}

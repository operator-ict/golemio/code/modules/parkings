import { Parkings } from "#sch";
import { TskAverageOccupancyModel } from "#sch/models";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";

export class TskAverageOccupancyRepository extends SequelizeModel {
    constructor() {
        super("TskAverageOccupancyRepository", TskAverageOccupancyModel.tableName, TskAverageOccupancyModel.attributeModel, {
            schema: Parkings.pgSchema,
        });
    }

    GetAll(): Promise<void> {
        return Promise.resolve();
    }

    GetOne(_id: unknown): Promise<void> {
        return Promise.resolve();
    }
}

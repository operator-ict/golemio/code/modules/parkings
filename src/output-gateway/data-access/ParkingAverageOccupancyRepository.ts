import { Parkings } from "#sch";
import { IParkingAverageOccupancy } from "#sch/models/interfaces/IParkingAverageOccupancy";
import { IParkingMeasurements } from "#sch/models/interfaces/IParkingMeasurements";
import { ParkingAverageOccupancyModel } from "#sch/models/ParkingAverageOccupancyModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models/SequelizeModel";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize from "@golemio/core/dist/shared/sequelize";

@injectable()
export class ParkingAverageOccupancyRepository extends SequelizeModel {
    constructor() {
        super(
            "ParkingAverageOccupancyRepository",
            ParkingAverageOccupancyModel.tableName,
            ParkingAverageOccupancyModel.attributeModel,
            {
                schema: Parkings.pgSchema,
            }
        );
    }

    public async GetOccupancy(
        id: string,
        sources: string[]
    ): Promise<Array<Omit<IParkingAverageOccupancy, "hour"> & { hour: string }> | null> {
        try {
            const where: Sequelize.WhereOptions<IParkingMeasurements> = {
                parking_id: id,
                source: { [Sequelize.Op.in]: sources },
            };

            const results = await this.sequelizeModel.findAll({
                attributes: [
                    ...ParkingAverageOccupancyModel.rawAttributesModel,
                    [Sequelize.fn("ROUND", Sequelize.col("average_occupancy")), "average_occupancy"],
                    [Sequelize.fn("LPAD", Sequelize.literal("hour::text"), 2, "0"), "hour"],
                ],
                where,
                order: [
                    ["source", "ASC"],
                    ["source_id", "ASC"],
                    ["day_of_week", "ASC"],
                    ["hour", "ASC"],
                ],
                raw: true,
            });

            if (results.length === 0) {
                return null;
            }

            return results;
        } catch (err) {
            throw new GeneralError("Database error: GetOne", this.name, err, 500);
        }
    }

    public GetAll(): never {
        throw new Error("Not implemented");
    }

    public GetOne(): never {
        throw new Error("Not implemented");
    }
}

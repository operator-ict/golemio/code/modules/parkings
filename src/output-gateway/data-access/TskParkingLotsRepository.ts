import { ParkingPaymentsRepository } from "#og/data-access/ParkingPaymentsRepository";
import { Parkings } from "#sch";
import { ParkingMeasurementsDtoSchema } from "#sch/datasources/ParkingMeasurementsDtoSchema";

import {
    buildGeojsonFeatureCollection,
    IGeoJsonAllFilterParameters,
    IGeoJSONFeatureCollection,
    IPropertyResponseModel,
    SequelizeModel,
} from "@golemio/core/dist/output-gateway";
import { IGeoJsonModel } from "@golemio/core/dist/output-gateway/models/interfaces/IGeoJsonModel";
import Sequelize, { FindAttributeOptions, Includeable } from "@golemio/core/dist/shared/sequelize";
import { ParkingsMeasurementsModels } from "../models/ParkingsMeasurementsModels";
import { TskFeatureMapper } from "./helpers/TskFeatureMapper";
import { ITskParkingLotFeature } from "./interfaces/ITskParkingLotFeature";
import { TskAverageOccupancyRepository } from "./TskAverageOccupancyRepository";
import { TskDistrictsRepository } from "./TskDistrictsRepository";

export class TskParkingLotsRepository extends SequelizeModel implements IGeoJsonModel {
    private readonly measurementsRepository: SequelizeModel;
    private readonly tskDistrictsRepository: TskDistrictsRepository;
    private readonly tskAverageRepository: TskAverageOccupancyRepository;
    private readonly paymentsRepository: ParkingPaymentsRepository;

    constructor() {
        super(Parkings.name, Parkings.parkings.pgTableName, Parkings.parkings.outputSequelizeAttributes, {
            schema: Parkings.pgSchema,
        });

        this.measurementsRepository = new ParkingsMeasurementsModels({
            ...Parkings.measurementsActual,
            outputJsonSchema: ParkingMeasurementsDtoSchema,
        });
        this.tskDistrictsRepository = new TskDistrictsRepository();
        this.tskAverageRepository = new TskAverageOccupancyRepository();
        this.paymentsRepository = new ParkingPaymentsRepository();

        this.sequelizeModel.hasOne(this.measurementsRepository.sequelizeModel, {
            as: "measurements",
            foreignKey: "source_id",
            sourceKey: "source_id",
        });

        this.sequelizeModel.hasOne(this.tskDistrictsRepository.sequelizeModel, {
            as: "tsk_districts",
            foreignKey: "source_id",
            sourceKey: "source_id",
        });

        this.sequelizeModel.hasOne(this.tskAverageRepository.sequelizeModel, {
            as: "tsk_average_occupancy",
            foreignKey: "source_id",
            sourceKey: "source_id",
        });

        this.sequelizeModel.hasOne(this.paymentsRepository.sequelizeModel, {
            as: "parking_payments",
            foreignKey: "parking_id",
            sourceKey: "id",
        });
    }

    public GetAll = async (options: IGeoJsonAllFilterParameters = {}): Promise<IGeoJSONFeatureCollection> => {
        let whereAttributes: Sequelize.WhereOptions = {
            source: { [Sequelize.Op.in]: ["tsk", "TSK"] },
            [Sequelize.Op.and]: [Sequelize.where(Sequelize.fn("ST_GeometryType", Sequelize.col("centroid")), "ST_Point")],
        };
        let order: Sequelize.Order = [];

        if (options.updatedSince) {
            whereAttributes.updated_at = {
                [Sequelize.Op.gt]: options?.updatedSince,
            };
        }

        if (options.lat && options.lng) {
            if (options.range) {
                whereAttributes.range = Sequelize.where(
                    Sequelize.fn(
                        "ST_DWithin",
                        Sequelize.col("centroid"),
                        Sequelize.cast(
                            Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                            "geography"
                        ),
                        options.range,
                        true
                    ),
                    "true"
                );
            }

            order.push(
                Sequelize.fn(
                    "ST_Distance",
                    Sequelize.col("centroid"),
                    Sequelize.cast(
                        Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                        "geography"
                    )
                )
            );
        }

        const result = await this.sequelizeModel.findAll({
            attributes: this.attributeOptions,
            include: [
                ...this.includeOptions,
                {
                    as: "tsk_districts",
                    model: this.tskDistrictsRepository.sequelizeModel,
                    attributes: [],
                    where:
                        options.districts && options.districts.length > 0
                            ? {
                                  district: options.districts,
                              }
                            : undefined,
                },
                {
                    model: this.paymentsRepository.sequelizeModel,
                    as: "parking_payments",
                    required: false,
                    subQuery: false,
                },
            ],
            where: whereAttributes,
            limit: options?.limit,
            offset: Number.isInteger(options?.offset) ? options?.offset : undefined,
            order,
        });

        return buildGeojsonFeatureCollection(
            result.map((record) => {
                return TskFeatureMapper.formatOutput(record);
            })
        );
    };

    public GetOne = async (id: string): Promise<ITskParkingLotFeature | undefined> => {
        const result = await this.sequelizeModel.findOne({
            attributes: this.attributeOptions,
            where: {
                source_id: id,
                source: {
                    [Sequelize.Op.in]: ["tsk", "TSK"],
                },
                [Sequelize.Op.and]: [Sequelize.where(Sequelize.fn("ST_GeometryType", Sequelize.col("centroid")), "ST_Point")],
            },
            include: [
                ...this.includeOptions,
                {
                    as: "tsk_districts",
                    model: this.tskDistrictsRepository.sequelizeModel,
                    attributes: [],
                },
            ],
        });

        return result && TskFeatureMapper.formatOutput(result);
    };

    public IsPrimaryIdNumber(_idKey: string): Promise<boolean> {
        return Promise.resolve(false);
    }

    public PrimaryIdentifierSelection(id: string): object {
        return { id };
    }

    public GetProperties = async (): Promise<IPropertyResponseModel[]> => {
        return [];
    };

    private get attributeOptions(): FindAttributeOptions {
        return [
            "centroid",
            "parking_type",
            [Sequelize.col("parkings.source_id"), "id"],
            "name",
            [Sequelize.col("measurements.available_spot_number"), "num_of_free_places"],
            [Sequelize.col("measurements.occupied_spot_number"), "num_of_taken_places"],
            [Sequelize.col("measurements.updated_at"), "updated_at"],
            [Sequelize.col("parkings.total_spot_number"), "total_num_of_places"],
            "address",
            [Sequelize.col("tsk_average_occupancy.average_occupancy"), "average_occupancy"],
            [Sequelize.col("tsk_districts.district"), "district"],
            [Sequelize.col("measurements.date_modified"), "last_updated"],
        ];
    }

    private get includeOptions(): Includeable[] {
        return [
            {
                as: "measurements",
                model: this.measurementsRepository.sequelizeModel,
                attributes: [],
            },
            {
                as: "tsk_average_occupancy",
                model: this.tskAverageRepository.sequelizeModel,
                attributes: [],
                required: true,
            },
        ];
    }
}

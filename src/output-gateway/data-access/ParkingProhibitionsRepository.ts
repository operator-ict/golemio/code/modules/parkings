import { Parkings } from "#sch";
import { ParkingProhibitionsModel } from "#sch/models/ParkingProhibitionsModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models/SequelizeModel";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ParkingProhibitionsRepository extends SequelizeModel {
    constructor() {
        super("ParkingProhibitionsRepository", ParkingProhibitionsModel.tableName, ParkingProhibitionsModel.attributeModel, {
            schema: Parkings.pgSchema,
        });
    }

    public GetAll(): never {
        throw new Error("Not implemented");
    }

    public GetOne(): never {
        throw new Error("Not implemented");
    }
}

import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IRepositorySetup } from "#og/data-access/interfaces/IRepositorySetup";
import { GeoConfigHelper } from "#og/helpers/GeoConfigHelper";
import { IParkingMachinesParams } from "#og/helpers/params/IParkingMachinesParams";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { ParkingMachinesModel } from "#sch/models/ParkingMachinesModel";
import { IParkingMachine } from "#sch/models/interfaces/IParkingMachine";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models/SequelizeModel";
import { Geometry } from "@golemio/core/dist/shared/geojson";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Op, Sequelize, WhereOptions, literal } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ParkingMachinesRepository extends SequelizeModel {
    protected readonly shouldHideSecondarySourcesFromPrimaryData: boolean = true;
    private geoConfigHelper: GeoConfigHelper;

    constructor(setup: IRepositorySetup) {
        super(setup.name, setup.tableName, setup.attributes, setup.options);
        this.geoConfigHelper = ParkingsContainer.resolve<GeoConfigHelper>(ModuleContainerToken.GeoConfigHelper);
    }

    public async GetAll(params: IParkingMachinesParams): Promise<IParkingMachine[]> {
        try {
            let where: WhereOptions<IParkingMachine> = {};
            let primarySources: string[] = [];
            const areaLimit = Sequelize.fn(
                "ST_BuildArea",
                Sequelize.fn("ST_GeomFromGeoJSON", JSON.stringify(this.geoConfigHelper.returnGeoCoordinates()))
            );

            if (params.primarySource) {
                primarySources = params.primarySource;
            } else {
                throw new GeneralError("List of sources must be provided! ", this.name, undefined, 500);
            }

            // DO NOT SHOW OSM PARKING MACHINES IF THERE ARE OTHER SOURCES
            if (this.shouldHideSecondarySourcesFromPrimaryData && primarySources.length > 1) {
                primarySources = primarySources.filter((source) => source !== SourceEnum.OSM);
            }

            where.source = { [Op.in]: primarySources };
            where.location = Sequelize.where(
                Sequelize.fn("ST_Within", Sequelize.col("location"), areaLimit),
                "true"
            ) as unknown as Geometry;

            if (params.validFrom) {
                where.validFrom = { [Op.gte]: params.validFrom };
            }

            if (params.type) {
                where.type = { [Op.in]: params.type };
            }

            if (params.codeMask) {
                where.code = { [Op.like]: params.codeMask };
            }

            if (params.boundingBox) {
                where = {
                    ...where,
                    [Op.and]: literal(`ST_Contains(ST_MakeEnvelope($lonMin, $latMin, $lonMax, $latMax, 4326), location)`),
                };
            }

            return await this.sequelizeModel.findAll<ParkingMachinesModel>({
                raw: true,
                where,
                limit: params.limit,
                offset: params.offset,
                bind: params.boundingBox && {
                    lonMin: params.boundingBox[1],
                    latMin: params.boundingBox[2],
                    lonMax: params.boundingBox[3],
                    latMax: params.boundingBox[0],
                },
                order: [["id", "ASC"]],
            });
        } catch (err) {
            throw new GeneralError("Database error ~ GetAll", this.name, err, 500);
        }
    }

    public async GetOne(id: any): Promise<IParkingMachine | null> {
        try {
            return await this.sequelizeModel.findByPk<ParkingMachinesModel>(id, { raw: true });
        } catch (err) {
            throw new GeneralError("Database error ~ GetOne", this.name, err, 500);
        }
    }
}

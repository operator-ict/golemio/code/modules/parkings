import { ModelAttributes, ModelOptions } from "@golemio/core/dist/shared/sequelize";

export interface IRepositorySetup {
    name: string;
    tableName: string;
    attributes: ModelAttributes<any>;
    options?: ModelOptions<any>;
}

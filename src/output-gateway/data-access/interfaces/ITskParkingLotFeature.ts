import { AverageOccupancy } from "#sch/models/interfaces/ITskAverageOccupancyModel";
import { IGeoJSONFeature } from "@golemio/core/dist/output-gateway";

interface IParkingType {
    description: string | null;
    id: number;
}

interface IParkingLotAddress {
    address_country: string | null;
    address_formatted: string | null;
    street_address: string | null;
    postal_code: string | null;
    address_locality: string | null;
    address_region: string | null;
}

interface ITskParkingLotFeatureProperties {
    parking_type: IParkingType | null;
    id: string | number;
    name: string;
    num_of_free_places: number;
    num_of_taken_places: number;
    updated_at: string | null;
    total_num_of_places: number;
    average_occupancy: AverageOccupancy;
    district: string | null;
    address: IParkingLotAddress | null;
    last_updated: number | null;
    payment_link?: string | null;
    payment_shortname?: string | null;
}

export interface ITskParkingLotFeature extends IGeoJSONFeature {
    properties: ITskParkingLotFeatureProperties;
}

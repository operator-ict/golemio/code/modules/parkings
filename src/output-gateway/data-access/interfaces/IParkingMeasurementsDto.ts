import { IParkingMeasurements } from "#sch/models/interfaces/IParkingMeasurements";

export interface IParkingLatestMeasurements extends IParkingMeasurements {
    updated_at: Date;
}

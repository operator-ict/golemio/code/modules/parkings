import { Parkings } from "#sch";
import { EntrancesModel } from "#sch/models/EntranceModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models/SequelizeModel";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ParkingEntrancesRepository extends SequelizeModel {
    constructor() {
        super("ParkingEntrancesRepository", EntrancesModel.tableName, EntrancesModel.attributeModel, {
            schema: Parkings.pgSchema,
        });
    }

    public GetAll(): never {
        throw new Error("Not implemented");
    }

    public GetOne(): never {
        throw new Error("Not implemented");
    }
}

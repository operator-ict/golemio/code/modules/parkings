import { IParkingSourcesParams } from "#og/helpers/params/IParkingSourcesParams";
import { Parkings } from "#sch";
import { IParking } from "#sch/models/interfaces/IParking";
import { IParkingSource } from "#sch/models/interfaces/IParkingSource";
import { ParkingSourcesModel } from "#sch/models/ParkingSourcesModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models/SequelizeModel";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Op, WhereOptions } from "@golemio/core/dist/shared/sequelize";

@injectable()
export class ParkingSourcesRepository extends SequelizeModel {
    constructor() {
        super("OGParkingSourcesRepository", ParkingSourcesModel.tableName, ParkingSourcesModel.attributeModel, {
            schema: Parkings.pgSchema,
        });
    }

    public async GetAll(params: IParkingSourcesParams): Promise<IParkingSource[]> {
        try {
            let where: WhereOptions<IParking> = {};
            let primarySources: string[] = [];

            if (params.primarySource) {
                primarySources = params.primarySource;
            } else {
                throw new GeneralError("List of sources must be provided! ", this.name, undefined, 500);
            }

            where.source = { [Op.in]: primarySources };

            return await this.sequelizeModel.findAll({
                raw: true,
                where,
                order: [["source", "ASC"]],
                attributes: ["name", "source", "contact"],
            });
        } catch (err) {
            throw new GeneralError("Database error ~ GetAll", this.name, err, 500);
        }
    }

    public GetOne(): never {
        throw new Error("Not implemented");
    }
}

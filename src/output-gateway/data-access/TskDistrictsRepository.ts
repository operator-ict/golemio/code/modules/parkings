import { Parkings } from "#sch";
import { TskDistrictsModel } from "#sch/models";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";

export class TskDistrictsRepository extends SequelizeModel {
    constructor() {
        super("TskDistrictsRepository", TskDistrictsModel.tableName, TskDistrictsModel.attributeModel, {
            schema: Parkings.pgSchema,
        });
    }

    GetAll(): Promise<void> {
        return Promise.resolve();
    }

    GetOne(_id: unknown): Promise<void> {
        return Promise.resolve();
    }
}

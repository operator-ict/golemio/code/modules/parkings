import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { V3ParkingMachinesController } from "#og/controllers/v3/V3ParkingMachinesController";
import { V3ParkingSourcesController } from "#og/controllers/v3/V3ParkingSourcesController";
import { ParkingAverageOccupancyRepository } from "#og/data-access/ParkingAverageOccupancyRepository";
import { ParkingEntrancesRepository } from "#og/data-access/ParkingEntrancesRepository";
import { ParkingLatestMeasurementRepository } from "#og/data-access/ParkingLatestMeasurementRepository";
import { ParkingMachinesFilteredRepository } from "#og/data-access/ParkingMachinesFilteredRepository";
import { ParkingMachinesRepository } from "#og/data-access/ParkingMachinesRepository";
import { ParkingMeasurementRepository } from "#og/data-access/ParkingMeasurementRepository";
import { ParkingOpeningHoursRepository } from "#og/data-access/ParkingOpeningHoursRepository";
import { ParkingPaymentsRepository } from "#og/data-access/ParkingPaymentsRepository";
import { ParkingProhibitionsRepository } from "#og/data-access/ParkingProhibitionsRepository";
import { ParkingRepository } from "#og/data-access/ParkingRepository";
import { ParkingSourcesRepository } from "#og/data-access/ParkingSourcesRepository";
import { ParkingsFilteredRepository } from "#og/data-access/ParkingsFilteredRepository";
import { ParkingTariffsRepository } from "#og/data-access/ParkingTariffsRepository";
import { GeoConfigHelper } from "#og/helpers/GeoConfigHelper";
import { ParkingsLocationModel } from "#og/models/ParkingsLocationModel";
import { PrimarySourceValidator } from "#og/routers/v3/helpers/PrimarySourceValidator";
import { ParkingMachinesDtoTransformation } from "#og/transformations/v3/ParkingMachinesDtoTransformation";
import { Parkings } from "#sch";
import { ParkingMachinesModel } from "#sch/models/ParkingMachinesModel";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import { DependencyContainer, instanceCachingFactory } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainerToken";

//#region Initialization
const parkingsContainer: DependencyContainer = OutputGatewayContainer.createChildContainer();
//#endregion

//#region Models
parkingsContainer.register(ModuleContainerToken.ParkingsLocationModel, ParkingsLocationModel);
//#endregion

//#region Repositories
parkingsContainer
    .register(ModuleContainerToken.CachedParkingSourcesRepository, {
        useFactory: (c) =>
            new CachedParkingSourcesRepository(c.resolve(CoreToken.PostgresConnector), c.resolve(CoreToken.Logger)),
    })
    .registerSingleton(ModuleContainerToken.ParkingRepository, ParkingRepository)
    .registerInstance(ModuleContainerToken.ParkingRepositorySetup, ParkingRepository.defaultSetup)
    .registerSingleton(ModuleContainerToken.ParkingsFilteredRepository, ParkingsFilteredRepository)
    .register(ModuleContainerToken.ParkingMachinesRepository, {
        useFactory: instanceCachingFactory((c) => {
            return new ParkingMachinesRepository({
                name: "OGParkingMachinesRepository",
                tableName: ParkingMachinesModel.tableName,
                attributes: ParkingMachinesModel.attributeModel,
                options: {
                    schema: Parkings.pgSchema,
                    underscored: true,
                },
            });
        }),
    })
    .registerSingleton(ModuleContainerToken.ParkingMachinesFilteredRepository, ParkingMachinesFilteredRepository)
    .registerSingleton(ModuleContainerToken.ParkingMeasurementRepository, ParkingMeasurementRepository)
    .registerSingleton(ModuleContainerToken.ParkingLatestMeasurementRepository, ParkingLatestMeasurementRepository)
    .registerSingleton(ModuleContainerToken.ParkingAverageOccupancyRepository, ParkingAverageOccupancyRepository)
    .registerSingleton(ModuleContainerToken.ParkingTariffRepository, ParkingTariffsRepository)
    .registerSingleton(ModuleContainerToken.ParkingOpeningHoursRepository, ParkingOpeningHoursRepository)
    .registerSingleton(ModuleContainerToken.ParkingEntrancesRepository, ParkingEntrancesRepository)
    .registerSingleton(ModuleContainerToken.ParkingPaymentsRepository, ParkingPaymentsRepository)
    .registerSingleton(ModuleContainerToken.ParkingProhibitionsRepository, ParkingProhibitionsRepository)
    .registerSingleton(ModuleContainerToken.ParkingSourcesRepository, ParkingSourcesRepository);
//#endregion

//#region Transformations
parkingsContainer.registerSingleton(ModuleContainerToken.ParkingMachinesDtoTransformation, ParkingMachinesDtoTransformation);
//#endregion

//#region Controllers
parkingsContainer
    .registerSingleton<V3ParkingMachinesController>(ModuleContainerToken.V3ParkingMachinesController, V3ParkingMachinesController)
    .registerSingleton<V3ParkingSourcesController>(ModuleContainerToken.V3ParkingSourcesController, V3ParkingSourcesController);
//#endregion

//#region Validators
parkingsContainer.register(ModuleContainerToken.PrimarySourceValidator, PrimarySourceValidator);
//#endregion

//#region Helpers
parkingsContainer.register(ModuleContainerToken.GeoConfigHelper, GeoConfigHelper);
//#endregion

//#region Controllers
parkingsContainer.registerSingleton(ModuleContainerToken.V3ParkingMachinesController, V3ParkingMachinesController);
//#endregion

export { parkingsContainer as ParkingsContainer };

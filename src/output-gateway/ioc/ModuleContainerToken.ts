const ModuleContainerToken = {
    // Models
    ParkingsLocationModel: Symbol(),
    // Repositories
    CachedParkingSourcesRepository: Symbol(),
    ParkingRepository: Symbol(),
    ParkingRepositorySetup: Symbol(),
    ParkingsFilteredRepository: Symbol(),
    ParkingMeasurementRepository: Symbol(),
    ParkingLatestMeasurementRepository: Symbol(),
    ParkingAverageOccupancyRepository: Symbol(),
    ParkingMachinesRepository: Symbol(),
    ParkingMachinesFilteredRepository: Symbol(),
    ParkingTariffRepository: Symbol(),
    ParkingOpeningHoursRepository: Symbol(),
    ParkingEntrancesRepository: Symbol(),
    ParkingPaymentsRepository: Symbol(),
    ParkingProhibitionsRepository: Symbol(),
    ParkingSourcesRepository: Symbol(),
    // Validators
    PrimarySourceValidator: Symbol(),
    // Transformations
    ParkingMachinesDtoTransformation: Symbol(),
    // Controllers
    V3ParkingMachinesController: Symbol(),
    V3ParkingSourcesController: Symbol(),
    // Helpers
    GeoConfigHelper: Symbol(),
};

export { ModuleContainerToken };

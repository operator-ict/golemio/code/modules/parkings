import { ParkingLatestMeasurementRepository } from "#og/data-access/ParkingLatestMeasurementRepository";
import { IParkingMeasurementsParams } from "#og/helpers/params/IParkingMeasurementsParams";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { ParkingMeasurementDtoTransformation } from "#og/transformations/v3/ParkingMeasurementDtoTransformation";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { AbstractV3ParkingController } from "./AbstractV3ParkingController";

export class V3ParkingMeasurementsController extends AbstractV3ParkingController {
    private latestMeasurementRepository: ParkingLatestMeasurementRepository;
    private transformation: ParkingMeasurementDtoTransformation;

    constructor() {
        super();

        this.latestMeasurementRepository = ParkingsContainer.resolve<ParkingLatestMeasurementRepository>(
            ModuleContainerToken.ParkingLatestMeasurementRepository
        );
        this.transformation = new ParkingMeasurementDtoTransformation();
    }

    public getAllLatest = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const params = await this.getAllLatestParams(req);
            const result = await this.latestMeasurementRepository.GetAll(params);

            return res.json(this.transformation.transformArray(result));
        } catch (err) {
            next(err);
        }
    };

    private async getAllLatestParams(req: Request): Promise<IParkingMeasurementsParams> {
        const { allowedSources } = await this.getAllowedScope(req);

        try {
            return {
                primarySource: allowedSources,
                parkingId: req.query.parkingId
                    ? req.query.parkingId instanceof Array
                        ? (req.query.parkingId as string[])
                        : [req.query.parkingId as string]
                    : undefined,
                limit: req.query.limit ? Number(req.query.limit) : undefined,
                offset: req.query.offset ? Number(req.query.offset) : 0,
            };
        } catch (err) {
            throw new GeneralError("Param parsing error", this.constructor.name, err, 500);
        }
    }
}

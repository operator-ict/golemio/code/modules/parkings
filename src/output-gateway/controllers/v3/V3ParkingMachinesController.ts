import { ParkingMachinesRepository } from "#og/data-access/ParkingMachinesRepository";
import { ParkingMachinesFilteredRepository } from "#og/data-access/ParkingMachinesFilteredRepository";
import { IParkingMachinesParams } from "#og/helpers/params/IParkingMachinesParams";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { ParkingMachinesDtoTransformation } from "#og/transformations/v3/ParkingMachinesDtoTransformation";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractV3ParkingController } from "./AbstractV3ParkingController";

@injectable()
export class V3ParkingMachinesController extends AbstractV3ParkingController {
    constructor(
        @inject(ModuleContainerToken.ParkingMachinesRepository) private repository: ParkingMachinesRepository,
        @inject(ModuleContainerToken.ParkingMachinesFilteredRepository)
        private filteredRepository: ParkingMachinesFilteredRepository,
        @inject(ModuleContainerToken.ParkingMachinesDtoTransformation) private transformation: ParkingMachinesDtoTransformation
    ) {
        super();
    }

    public getAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const params: IParkingMachinesParams = await this.parseParams(req);
            const result =
                params.primarySource?.length === 1
                    ? await this.repository.GetAll(params)
                    : await this.filteredRepository.GetAll(params);
            const transformedResult = this.transformation.transformToFeatureCollection(result);
            res.json(transformedResult);
        } catch (err) {
            next(err);
        }
    };

    public getOne = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { allowedSources } = await this.getAllowedScope(req);
            const id = req.params.id;
            const result = await this.repository.GetOne(id);

            if (result === null || !allowedSources.includes(result.source)) {
                throw new GeneralError("not_found", "V3ParkingMachinesController", undefined, 404);
            } else {
                const transformedResult = this.transformation.transformElement(result);
                res.json(transformedResult);
            }
        } catch (err) {
            next(err);
        }
    };

    private async parseParams(req: Request): Promise<IParkingMachinesParams> {
        try {
            const { allowedSources } = await this.getAllowedScope(req);

            return {
                primarySource: allowedSources,
                validFrom: req.query.validFrom ? (req.query.validFrom as string) : undefined,
                type: req.query.type
                    ? req.query.type instanceof Array
                        ? (req.query.type as string[])
                        : [req.query.type as string]
                    : undefined,
                boundingBox: req.query.boundingBox
                    ? (req.query.boundingBox as string).split(",").map((x) => parseFloat(x))
                    : undefined,
                codeMask: req.query.codeMask ? (req.query.codeMask as string) : undefined,
                limit: req.query.limit ? parseInt(req.query.limit as string) : 1000,
                offset: req.query.offset ? parseInt(req.query.offset as string) : undefined,
            };
        } catch (err) {
            throw new GeneralError("Param parsing error", "V3ParkingMachinesController", err, 500);
        }
    }
}

import { ParkingTariffsRepository } from "#og/data-access/ParkingTariffsRepository";
import { IParkingTariffsParams } from "#og/helpers/params/IParkingTariffsParams";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { ParkingTariffDtoTransformation } from "#og/transformations/v3/ParkingTariffDtoTransformation";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { AbstractV3ParkingController } from "./AbstractV3ParkingController";

export class V3ParkingTariffsController extends AbstractV3ParkingController {
    private repository: ParkingTariffsRepository;
    private transformation: ParkingTariffDtoTransformation;

    constructor() {
        super();
        this.repository = ParkingsContainer.resolve<ParkingTariffsRepository>(ModuleContainerToken.ParkingTariffRepository);
        this.transformation = new ParkingTariffDtoTransformation();
    }

    public getAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const params: IParkingTariffsParams = await this.parseParams(req);
            const result = await this.repository.GetAll(params);

            const transformedResult = this.transformation.transformElement(result);
            res.json(transformedResult);
        } catch (err) {
            next(err);
        }
    };

    public getOne = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { allowedSources } = await this.getAllowedScope(req);
            const id = req.params.id;
            const result = await this.repository.GetOne(id, allowedSources);

            if (result === null) {
                throw new GeneralError("not_found", "V3ParkingTariffsController", undefined, 404);
            } else {
                const transformedResult = this.transformation.transformElement([
                    {
                        tariff_id: result[0].tariff_id,
                        parking_tariffs: result,
                    },
                ]);

                res.json(transformedResult[0]);
            }
        } catch (err) {
            next(err);
        }
    };

    private async parseParams(req: Request): Promise<IParkingTariffsParams> {
        try {
            const { allowedSources } = await this.getAllowedScope(req);

            return {
                primarySource: allowedSources,
                validFrom: req.query.validFrom ? (req.query.validFrom as string) : undefined,
                validTo: req.query.validTo ? (req.query.validTo as string) : undefined,
                limit: req.query.limit ? parseInt(req.query.limit as string) : 1000,
                offset: req.query.offset ? parseInt(req.query.offset as string) : undefined,
            };
        } catch (err) {
            throw new GeneralError("Param parsing error", "V3ParkingTariffsController", err, 500);
        }
    }
}

import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import OpenDataHelper from "#og/helpers/OpenDataHelper";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { Request } from "@golemio/core/dist/shared/express";

export class AbstractV3ParkingController {
    protected cachedParkingSourcesRepository: CachedParkingSourcesRepository;

    constructor() {
        this.cachedParkingSourcesRepository = ParkingsContainer.resolve<CachedParkingSourcesRepository>(
            ModuleContainerToken.CachedParkingSourcesRepository
        );
    }

    protected async getAllowedScope(req: Request) {
        const isRestrictedToOpenData = OpenDataHelper.parseOpenDataParam(req);
        const requestedSources = req.query.primarySource
            ? req.query.primarySource instanceof Array
                ? (req.query.primarySource as string[])
                : [req.query.primarySource as string]
            : undefined;

        const roleLimitedTo = await this.cachedParkingSourcesRepository.getV3Sources({ isRestrictedToOpenData });
        const allowedSources = OpenDataHelper.getIntersectionOfAllowedSources(requestedSources, roleLimitedTo);

        return { isRestrictedToOpenData, allowedSources };
    }
}

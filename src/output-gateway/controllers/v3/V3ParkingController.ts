import { ParkingAverageOccupancyRepository } from "#og/data-access/ParkingAverageOccupancyRepository";
import { ParkingRepository } from "#og/data-access/ParkingRepository";
import { ParkingsFilteredRepository } from "#og/data-access/ParkingsFilteredRepository";
import { IParkingParams } from "#og/helpers/params/IParkingParams";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { ParkingDtoTransformation } from "#og/transformations/v3/ParkingDtoTransformation";
import { ILogger } from "@golemio/core/dist/helpers";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { AbstractV3ParkingController } from "./AbstractV3ParkingController";

export class V3ParkingController extends AbstractV3ParkingController {
    private parkingRepository: ParkingRepository;
    private parkingsFilteredRepository: ParkingsFilteredRepository;
    private averageOccupancyRepository: ParkingAverageOccupancyRepository;
    private logger: ILogger;

    constructor() {
        super();
        this.parkingRepository = ParkingsContainer.resolve<ParkingRepository>(ModuleContainerToken.ParkingRepository);
        this.parkingsFilteredRepository = ParkingsContainer.resolve<ParkingsFilteredRepository>(
            ModuleContainerToken.ParkingsFilteredRepository
        );
        this.averageOccupancyRepository = ParkingsContainer.resolve<ParkingAverageOccupancyRepository>(
            ModuleContainerToken.ParkingAverageOccupancyRepository
        );
        this.logger = ParkingsContainer.resolve<ILogger>(CoreToken.Logger);
    }

    public getAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const params: IParkingParams = await this.parseParams(req);
            const paymentsConfig = await this.cachedParkingSourcesRepository.getParkingPaymentsConfig({
                isRestrictedToOpenData: params.isRestrictedToOpenData,
            });
            const transformation = new ParkingDtoTransformation(paymentsConfig, this.logger);
            const result =
                params.primarySource?.length === 1
                    ? await this.parkingRepository.GetAll(params)
                    : await this.parkingsFilteredRepository.GetAll(params);
            const transformedResult = transformation.transformToFeatureCollection(result);
            res.json(transformedResult);
        } catch (err) {
            next(err);
        }
    };

    public getOne = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { isRestrictedToOpenData, allowedSources } = await this.getAllowedScope(req);
            const id = req.params.id;
            const result = await this.parkingRepository.GetOne(id, allowedSources);

            if (result === null) {
                throw new GeneralError("not_found", "V3ParkingController", undefined, 404);
            }

            const paymentsConfig = await this.cachedParkingSourcesRepository.getParkingPaymentsConfig({ isRestrictedToOpenData });
            const transformation = new ParkingDtoTransformation(paymentsConfig, this.logger);
            const transformedResult = transformation.transformElement(result);
            res.json(transformedResult);
        } catch (err) {
            next(err);
        }
    };

    public getAverageOccupancy = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { allowedSources } = await this.getAllowedScope(req);
            const id = req.params.id;
            const result = await this.averageOccupancyRepository.GetOccupancy(id, allowedSources);

            if (!result) {
                throw new GeneralError("not_found", this.constructor.name, undefined, 404);
            }

            const transformation = new ParkingDtoTransformation({}, this.logger);
            return res.json(transformation.transformAverageOccupancyArray(result));
        } catch (err) {
            next(err);
        }
    };

    private async parseParams(req: Request): Promise<IParkingParams> {
        try {
            const { isRestrictedToOpenData, allowedSources } = await this.getAllowedScope(req);

            return {
                primarySource: allowedSources,
                validFrom: req.query.validFrom ? (req.query.validFrom as string) : undefined,
                openFrom: req.query.openingHoursValidFrom ? (req.query.openingHoursValidFrom as string) : undefined,
                openTo: req.query.openingHoursValidTo ? (req.query.openingHoursValidTo as string) : undefined,
                boundingBox: req.query.boundingBox
                    ? (req.query.boundingBox as string).split(",").map((x) => parseFloat(x))
                    : undefined,
                limit: req.query.limit ? parseInt(req.query.limit as string) : 1000,
                offset: req.query.offset ? parseInt(req.query.offset as string) : undefined,
                accessDedicatedTo: req.query.accessDedicatedTo
                    ? req.query.accessDedicatedTo instanceof Array
                        ? (req.query.accessDedicatedTo as string[])
                        : [req.query.accessDedicatedTo as string]
                    : undefined,
                minutesBefore: req.query.minutesBefore ? parseInt(req.query.minutesBefore as string) : undefined,
                updatedSince: req.query.updatedSince ? (req.query.updatedSince as string) : undefined,
                isRestrictedToOpenData: isRestrictedToOpenData,
                parkingPolicy: req.query.parkingPolicy
                    ? req.query.parkingPolicy instanceof Array
                        ? (req.query.parkingPolicy as string[])
                        : [req.query.parkingPolicy as string]
                    : undefined,
                activeOnly: (req.query.activeOnly as string) !== "false",
            };
        } catch (err) {
            throw new GeneralError("Param parsing error", "V3ParkingController", err, 500);
        }
    }
}

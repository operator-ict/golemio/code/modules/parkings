import { ParkingSourcesRepository } from "#og/data-access/ParkingSourcesRepository";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractV3ParkingController } from "./AbstractV3ParkingController";

@injectable()
export class V3ParkingSourcesController extends AbstractV3ParkingController {
    constructor(@inject(ModuleContainerToken.ParkingSourcesRepository) private repository: ParkingSourcesRepository) {
        super();
    }

    public getAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { isRestrictedToOpenData, allowedSources } = await this.getAllowedScope(req);
            const result = await this.repository.GetAll({
                primarySource: allowedSources,
                isRestrictedToOpenData,
            });
            res.json(result);
        } catch (err) {
            next(err);
        }
    };
}

import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { IGetTariffsOptions, IParkingsTariff, V2ParkingTariffsRepository } from "#og/data-access/v2/V2ParkingTariffsRepository";
import { ParamSanitizerHelper } from "#og/helpers/ParamSanitizerHelper";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { models } from "#og/models";
import { IGetParkingQueryOptions } from "#og/models/interfaces/IGetQueryOptions";
import { IGetMeasurementsFilterParams, ParkingsMeasurementsModels } from "#og/models/ParkingsMeasurementsModels";
import { ParkingsModel } from "#og/models/ParkingsModel";
import { RouteVersion } from "#og/routers/constants";
import { IOutputParkingTariffsDto } from "#og/transformations/interfaces/IOutputParkingTariffsDto";
import { ParkingsTransformationFabric } from "#og/transformations/ParkingsTransformationFabric";
import { PaymentMethodTypes } from "#sch/datasources/PaymentMethodTypes";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import {
    CacheHeaderMiddleware,
    checkErrors,
    pagination,
    paginationLimitMiddleware,
    parseCoordinates,
} from "@golemio/core/dist/output-gateway";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import PaginationHelper from "../../helpers/PaginationHelper";
import { PrimarySourceValidator } from "../v3/helpers/PrimarySourceValidator";

export interface IGetLocationsQuery {
    latlng?: string;
    range?: string;
    limit?: string;
    offset?: string;
    source?: string;
    sourceId?: string;
    category?: string[];
    updatedSince?: string;
    minutesBefore?: string;
    parkingType?: string[];
    zoneType?: string[];
}

export interface IGetMeasurementsQuery {
    source?: string;
    sourceId?: string;
    from?: string;
    to?: string;
    latest?: string;
    limit?: string;
    offset?: string;
    updatedSince?: string;
    minutesBefore?: string;
}

export interface IGetTariffsQuery {
    source?: string;
    limit?: string;
    offset?: string;
}

class V2ParkingRouter extends AbstractRouter {
    protected ParkingsModel: ParkingsModel;
    protected ParkingsMeasurementsModel: ParkingsMeasurementsModels;
    protected ParkingsMeasurementsActualModel: ParkingsMeasurementsModels;
    protected ParkingsTariffsModel: V2ParkingTariffsRepository;
    protected ParkingsTransformationFabric: ParkingsTransformationFabric;
    private primarySourceValidator: PrimarySourceValidator;
    private parkingSourcesRepository: CachedParkingSourcesRepository;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;

    constructor() {
        super(RouteVersion.v2, "parking");
        this.ParkingsModel = models.ParkingsModel;
        this.ParkingsMeasurementsModel = models.ParkingsMeasurementsModel;
        this.ParkingsMeasurementsActualModel = models.ParkingsMeasurementsActualModel;
        this.ParkingsTariffsModel = models.ParkingsTariffsModel;
        this.ParkingsTransformationFabric = new ParkingsTransformationFabric();
        this.primarySourceValidator = ParkingsContainer.resolve(ModuleContainerToken.PrimarySourceValidator);
        this.parkingSourcesRepository = ParkingsContainer.resolve(ModuleContainerToken.CachedParkingSourcesRepository);
        this.cacheHeaderMiddleware = ParkingsContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.initRoutes();
    }

    private getLocations = async (
        req: Request<{}, {}, {}, IGetLocationsQuery>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        const { latlng, range, limit, offset, source, sourceId, category, updatedSince, minutesBefore } = req.query;

        if (updatedSince && minutesBefore) {
            throw new GeneralError(
                "Query should not contain both updatedSince and minutesBefore",
                this.constructor.name,
                undefined,
                400
            );
        }

        try {
            const coords = await parseCoordinates(latlng as string, range as string);

            const options: IGetParkingQueryOptions = {
                ...coords,
                source,
                sourceId,
                category,
                limit: limit ? Number(limit) : undefined,
                offset: offset ? Number(offset) : 0,
                minutesBefore: minutesBefore ? Number(minutesBefore) : undefined,
                updatedSince: updatedSince ? new Date(updatedSince) : undefined,
            };

            const data = await this.ParkingsModel.GetAll(options);
            const collection = this.ParkingsTransformationFabric.buildFeatureCollection(data);

            res.status(200).send(collection);
        } catch (err) {
            next(err);
        }
    };

    private getLocationById = async (
        req: Request<{ id: string }, {}, {}, {}>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            const data = await this.ParkingsModel.GetOne(req.params.id);
            if (!data) {
                throw new GeneralError(`Id ${req.params.id} not found`, this.constructor.name, undefined, 404);
            }
            const { measurements, parking_payments, ...rest } = data;
            const resultFeature = this.ParkingsTransformationFabric.buildFeatureItem({
                ...rest,
                ...measurements,
                ...parking_payments,
            });
            res.status(200).send(resultFeature);
        } catch (err) {
            next(err);
        }
    };

    private getDetailedLocations = async (
        req: Request<{}, {}, {}, IGetLocationsQuery>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        const { latlng, range, limit, offset, source, sourceId, category, updatedSince, minutesBefore } = req.query;

        if (updatedSince && minutesBefore) {
            throw new GeneralError(
                "Query should not contain both updatedSince and minutesBefore",
                this.constructor.name,
                undefined,
                400
            );
        }

        try {
            const coords = await parseCoordinates(latlng as string, range as string);

            const options: IGetParkingQueryOptions = {
                ...coords,
                source,
                sourceId,
                category,
                limit: limit ? Number(limit) : undefined,
                offset: offset ? Number(offset) : 0,
                minutesBefore: minutesBefore ? Number(minutesBefore) : undefined,
                updatedSince: updatedSince ? new Date(updatedSince) : undefined,
                zoneType: req.query.zoneType,
                parkingType: req.query.parkingType,
            };

            const data = await this.ParkingsModel.GetAllDetail(options);
            const collection = this.ParkingsTransformationFabric.buildFeatureCollection(data);

            res.status(200).send(collection);
        } catch (err) {
            next(err);
        }
    };

    private getDetailedLocationById = async (
        req: Request<{ id: string }, {}, {}, {}>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            const data = await this.ParkingsModel.GetOneDetail(req.params.id);
            if (!data) {
                throw new GeneralError(`Id ${req.params.id} not found`, this.constructor.name, undefined, 404);
            }
            const { measurements, parking_payments, ...rest } = data;
            const resultFeature = this.ParkingsTransformationFabric.buildFeatureItem({
                ...rest,
                ...measurements,
                ...parking_payments,
            });
            res.status(200).send(resultFeature);
        } catch (err) {
            next(err);
        }
    };

    private getTariffs = async (req: Request<{}, {}, {}, IGetTariffsQuery>, res: Response, next: NextFunction): Promise<void> => {
        const { source } = req.query;
        try {
            const options: IGetTariffsOptions = { source };

            const data = await this.ParkingsTariffsModel.GetAll(options);
            const dataNormalized = this.normalizeTariffs(data);

            res.status(200).send(dataNormalized);
        } catch (err) {
            next(err);
        }
    };

    private normalizeTariff = (data: IParkingsTariff[]): IOutputParkingTariffsDto => {
        const charges = [];
        for (const element of data) {
            charges.push(this.ParkingsTransformationFabric.getChargeData(element));
        }

        const paymentMethods: PaymentMethodTypes[] = [];
        if (data[0].accepts_payment_card) paymentMethods.push("card_offline");
        if (data[0].accepts_cash) paymentMethods.push("cash");
        if (data[0].accepts_mobile_payment) paymentMethods.push("mobile_app");
        if (data[0].accepts_litacka) paymentMethods.push("litacka");

        let tariffNormalized: IOutputParkingTariffsDto = {
            tariff_id: data[0].tariff_id,
            source: data[0].source,
            last_updated: data[0].last_updated,
            payment_mode: data[0].payment_mode,
            payment_additional_description: data[0].payment_additional_description,
            free_of_charge: data[0].free_of_charge,
            url_link_address: data[0].url_link_address,
            charge_band_name: data[0].charge_band_name,
            payment_methods: paymentMethods,
            charge_currency: data[0].charge_currency,
            allowed_vehicle_type: data[0].allowed_vehicle_type,
            allowed_fuel_type: data[0].allowed_fuel_type,
            charges,
            // For future integration MPLA:
            reservation_url: null,
        };

        return tariffNormalized;
    };

    private normalizeTariffs = (data: IParkingsTariff[]): IOutputParkingTariffsDto[] => {
        const tariffGroups = data.reduce((acc, item) => {
            acc[item.tariff_id] = acc[item.tariff_id] || [];
            acc[item.tariff_id].push(item);
            return acc;
        }, <Record<string, IParkingsTariff[]>>{});

        const result = [];
        for (const groupKey in tariffGroups) {
            if (tariffGroups.hasOwnProperty(groupKey)) result.push(this.normalizeTariff(tariffGroups[groupKey]));
        }

        return result;
    };

    private getTariffById = async (
        req: Request<{ tariffId: string }, {}, {}, {}>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            const data = await this.ParkingsTariffsModel.GetOne(req.params.tariffId);

            if (!data || !data.length) {
                throw new GeneralError(`TariffId ${req.params.tariffId} not found`, this.constructor.name, undefined, 404);
            }

            res.status(200).send(this.normalizeTariff(data));
        } catch (err) {
            next(err);
        }
    };

    private getMeasurements = async (
        req: Request<{}, {}, {}, IGetMeasurementsQuery>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        const filterParams = req.query;
        if (filterParams.updatedSince && filterParams.minutesBefore) {
            throw new GeneralError(
                "Query should not contain both updatedSince and minutesBefore",
                this.constructor.name,
                undefined,
                400
            );
        }

        try {
            const latest = filterParams.latest === "true";

            const measureFrom = filterParams.from ? new Date(filterParams.from).toISOString() : undefined;
            const measureTo = filterParams.to ? new Date(filterParams.to).toISOString() : undefined;

            let limit = filterParams.limit ? Number(filterParams.limit) : 10000;
            let offset = filterParams.offset ? Number(filterParams.offset) : 0;
            let minutesBefore = filterParams.minutesBefore ? Number(filterParams.minutesBefore) : undefined;
            let updatedSince = filterParams.updatedSince ? new Date(filterParams.updatedSince) : undefined;

            const source = filterParams.source
                ? [filterParams.source]
                : await this.parkingSourcesRepository.getLegacySources({ isRestrictedToOpenData: false });

            // Get data from DB
            const options: IGetMeasurementsFilterParams = {
                source,
                sourceId: filterParams.sourceId,
                measureFrom,
                measureTo,
                updatedSince,
                minutesBefore,
                limit,
                offset,
            };

            const data = latest
                ? await this.ParkingsMeasurementsActualModel.GetAll(options)
                : await this.ParkingsMeasurementsModel.GetAll(options);

            // Prepare paging Links for header
            let pagingLinksDefinition = PaginationHelper.generatePagingUrl(filterParams, data.length, offset, limit);

            if (pagingLinksDefinition) {
                res.setHeader("Link", `${pagingLinksDefinition}`);
            }
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    protected initRoutes = (): void => {
        this.router.get(
            "/tariffs",
            [
                query("source")
                    .optional()
                    .customSanitizer(ParamSanitizerHelper.sourceSanitizer())
                    .custom(this.primarySourceValidator.validateLegacy)
                    .not()
                    .isArray(),
            ],
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(12 * 60 * 60, 60),
            this.getTariffs
        );

        this.router.get(
            "/tariffs/:tariffId",
            param("tariffId").exists(),
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(12 * 60 * 60, 60),
            this.getTariffById
        );

        this.router.get(
            "/measurements",
            [
                query("source")
                    .optional()
                    .customSanitizer(ParamSanitizerHelper.sourceSanitizer())
                    .custom(this.primarySourceValidator.validateLegacy)
                    .not()
                    .isArray(),
                query("sourceId").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                query("from").optional().isISO8601().not().isArray(),
                query("to").optional().isISO8601().not().isArray(),
                query("latest").optional().isBoolean().not().isArray(),
                query("minutesBefore").optional().isNumeric().not().isArray(),
                query("updatedSince").optional().isISO8601().not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.constructor.name),
            this.cacheHeaderMiddleware.getMiddleware(5 * 60, 5),
            this.getMeasurements
        );

        this.router.get(
            "/detail/:id",
            [param("id").optional().isString()],
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(5 * 60, 5),
            this.getDetailedLocationById
        );

        this.router.get(
            "/detail",
            [
                query("latlng").optional().isLatLong().not().isArray(),
                query("range").optional().isNumeric().not().isArray(),
                query("source")
                    .optional()
                    .customSanitizer(ParamSanitizerHelper.sourceSanitizer())
                    .custom(this.primarySourceValidator.validateLegacy)
                    .not()
                    .isArray(),
                query("sourceId").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                query("category")
                    .optional()
                    .not()
                    .isEmpty({ ignore_whitespace: true })
                    .customSanitizer(ParamSanitizerHelper.toArraySanitizer()),
                query("minutesBefore").optional().isNumeric().not().isArray(),
                query("updatedSince").optional().not().isEmpty({ ignore_whitespace: true }).isISO8601().not().isArray(),
                query("parkingType")
                    .optional()
                    .not()
                    .isEmpty({ ignore_whitespace: true })
                    .customSanitizer(ParamSanitizerHelper.toArraySanitizer()),
                query("zoneType")
                    .optional()
                    .not()
                    .isEmpty({ ignore_whitespace: true })
                    .customSanitizer(ParamSanitizerHelper.toArraySanitizer()),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.constructor.name),
            this.cacheHeaderMiddleware.getMiddleware(5 * 60, 5),
            this.getDetailedLocations
        );

        this.router.get(
            "/:id",
            [param("id").optional().isString()],
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(5 * 60, 5),
            this.getLocationById
        );

        this.router.get(
            "/",
            [
                query("latlng").optional().isLatLong().not().isArray(),
                query("range").optional().isNumeric().not().isArray(),
                query("source")
                    .optional()
                    .customSanitizer(ParamSanitizerHelper.sourceSanitizer())
                    .custom(this.primarySourceValidator.validateLegacy)
                    .not()
                    .isArray(),
                query("sourceId").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                query("category")
                    .optional()
                    .not()
                    .isEmpty({ ignore_whitespace: true })
                    .customSanitizer(ParamSanitizerHelper.toArraySanitizer()),
                query("minutesBefore").optional().isNumeric().not().isArray(),
                query("updatedSince").optional().isISO8601().not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.constructor.name),
            this.cacheHeaderMiddleware.getMiddleware(5 * 60, 5),
            this.getLocations
        );
    };
}

const v2ParkingRouter: AbstractRouter = new V2ParkingRouter();

export { v2ParkingRouter };

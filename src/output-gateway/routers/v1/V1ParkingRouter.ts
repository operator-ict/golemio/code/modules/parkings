import { TskParkingLotsRepository } from "#og/data-access/TskParkingLotsRepository";
import { models } from "#og/models";
import { ParkingsMeasurementsModels } from "#og/models/ParkingsMeasurementsModels";
import { RouteVersion } from "#og/routers/constants";
import { AbstractGeoJsonRouter } from "@golemio/core/dist/helpers/routing/AbstractGeoJsonRouter";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { RequestHandler } from "@golemio/core/dist/shared/express";
import { query } from "@golemio/core/dist/shared/express-validator";

const CACHE_MAX_AGE = 5 * 60;
const CACHE_STALE_WHILE_REVALIDATE = 5;

/**
 * @deprecated Legacy router, use Parking Spaces /parking endpoints instead
 */
class V1ParkingRouter extends AbstractGeoJsonRouter {
    private readonly measurementsHistoryRepository: ParkingsMeasurementsModels;

    constructor() {
        super(RouteVersion.v1, "parkings", new TskParkingLotsRepository());
        this.measurementsHistoryRepository = models.ParkingsMeasurementsModel;
        this.initRoutes({ maxAge: CACHE_MAX_AGE, staleWhileRevalidate: CACHE_STALE_WHILE_REVALIDATE });
        this.router.get(
            "/history",
            [
                query("from").optional().isISO8601().not().isArray(),
                query("to").optional().isISO8601().not().isArray(),
                query("sensorId").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
            ],
            pagination,
            paginationLimitMiddleware("TskParkingLotsLegacyRouter"),
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(CACHE_MAX_AGE, CACHE_STALE_WHILE_REVALIDATE),
            this.GetAllTskMeasurements
        );
    }

    public GetAllTskMeasurements: RequestHandler = async (req, res, next) => {
        const timestampFrom = req.query.from ? new Date(req.query.from as string).toISOString() : undefined;
        const timestampTo = req.query.to ? new Date(req.query.to as string).toISOString() : undefined;

        try {
            const data = await this.measurementsHistoryRepository.GetAllTskMeasurements({
                limit: req.query.limit ? parseInt(req.query.limit as string) : undefined,
                offset: req.query.offset ? parseInt(req.query.offset as string) : undefined,
                from: timestampFrom,
                to: timestampTo,
                sensorId: req.query.sensorId as string,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };
}

const v1ParkingRouter: AbstractRouter = new V1ParkingRouter();

export { v1ParkingRouter };

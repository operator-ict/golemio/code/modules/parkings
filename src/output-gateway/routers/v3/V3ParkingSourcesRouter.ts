import { V3ParkingSourcesController } from "#og/controllers/v3/V3ParkingSourcesController";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { checkErrors } from "@golemio/core/dist/output-gateway/Validation";
import { query } from "@golemio/core/dist/shared/express-validator";
import { RouteVersion } from "../constants";
class V3ParkingSourcesRouter extends AbstractRouter {
    private controller: V3ParkingSourcesController;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;

    constructor() {
        super(RouteVersion.v3, "parking-sources");
        this.cacheHeaderMiddleware = ParkingsContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.controller = ParkingsContainer.resolve(ModuleContainerToken.V3ParkingSourcesController);
        this.initRoutes();
    }

    protected initRoutes(expire?: string | number | undefined): void {
        this.router.get(
            "/",
            [query("isRestrictedToOpenData").optional().isBoolean().not().isArray()],
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(12 * 60 * 60, 60),
            this.controller.getAll
        );
    }
}

const v3ParkingSourcesRouter: AbstractRouter = new V3ParkingSourcesRouter();

export { v3ParkingSourcesRouter };

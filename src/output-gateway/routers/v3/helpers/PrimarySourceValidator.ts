import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { CustomValidator } from "@golemio/core/dist/shared/express-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ValidationError } from "@golemio/core/dist/shared/golemio-errors";

@injectable()
export class PrimarySourceValidator {
    constructor(
        @inject(ModuleContainerToken.CachedParkingSourcesRepository) private repository: CachedParkingSourcesRepository
    ) {}

    public validate: CustomValidator = async (value, options) => {
        const allowedSources = await this.repository.getV3Sources({ isRestrictedToOpenData: false });
        if (!this.isAllowedSource(value, allowedSources)) {
            throw new ValidationError("Source not allowed!");
        }
    };

    public validateLegacy: CustomValidator = async (value, options) => {
        const allowedSources = await this.repository.getLegacySources({ isRestrictedToOpenData: false });
        if (!this.isAllowedSource(value, allowedSources)) {
            throw new ValidationError("Source not allowed!");
        }
    };

    private isAllowedSource = (value: any, allowedSources: string[]): boolean => {
        if (value instanceof Array) {
            return value.every((source: string) => allowedSources.includes(source));
        } else {
            return allowedSources.includes(value);
        }
    };
}

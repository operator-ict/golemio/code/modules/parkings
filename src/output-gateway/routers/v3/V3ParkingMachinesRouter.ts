import { V3ParkingMachinesController } from "#og/controllers/v3/V3ParkingMachinesController";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { RouteVersion } from "../constants";
import { PrimarySourceValidator } from "./helpers/PrimarySourceValidator";

class V3ParkingMachinesRouter extends AbstractRouter {
    private controller: V3ParkingMachinesController;
    private primarySourceValidator: PrimarySourceValidator;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;
    private supportedTypes = ["payment_machine", "info_box"]; // TODO move to constants

    constructor() {
        super(RouteVersion.v3, "parking-machines");

        this.controller = ParkingsContainer.resolve(ModuleContainerToken.V3ParkingMachinesController);
        this.primarySourceValidator = ParkingsContainer.resolve(ModuleContainerToken.PrimarySourceValidator);
        this.cacheHeaderMiddleware = ParkingsContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.initRoutes();
    }

    protected initRoutes(): void {
        this.router.get(
            "/",
            [
                query("isRestrictedToOpenData").optional().isBoolean().not().isArray(),
                query("primarySource").optional().custom(this.primarySourceValidator.validate),
                query("validFrom").optional().isISO8601().not().isArray(),
                query("type").optional().isIn(this.supportedTypes),
                query("boundingBox")
                    .optional()
                    .matches(/^\d+\.?\d*(,\d+\.?\d*){3}$/) // 50.123,14.243,50.017,14.573
                    .not()
                    .isArray(),
                query("codeMask")
                    .optional()
                    .matches(/^[\w%]*$/) // e.g. %middle% it is used for sql like
                    .not()
                    .isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.path),
            this.cacheHeaderMiddleware.getMiddleware(12 * 60 * 60, 60),
            this.controller.getAll
        );

        this.router.get(
            "/:id",
            param("id").exists().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
            query("isRestrictedToOpenData").optional().isBoolean().not().isArray(),
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(12 * 60 * 60, 60),
            this.controller.getOne
        );
    }
}

const v3ParkingMachinesRouter: AbstractRouter = new V3ParkingMachinesRouter();

export { v3ParkingMachinesRouter };

import { V3ParkingMeasurementsController } from "#og/controllers/v3/V3ParkingMeasurementsController";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { query } from "@golemio/core/dist/shared/express-validator";
import { RouteVersion } from "../constants";
import { PrimarySourceValidator } from "./helpers/PrimarySourceValidator";

class V3ParkingMeasurementsRouter extends AbstractRouter {
    private controller: V3ParkingMeasurementsController;
    private primarySourceValidator: PrimarySourceValidator;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;

    constructor() {
        super(RouteVersion.v3, "parking-measurements");

        this.controller = new V3ParkingMeasurementsController();
        this.primarySourceValidator = ParkingsContainer.resolve(ModuleContainerToken.PrimarySourceValidator);
        this.cacheHeaderMiddleware = ParkingsContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);

        this.initRoutes();
    }

    protected initRoutes(): void {
        this.router.get(
            "/",
            [
                query("isRestrictedToOpenData").optional().isBoolean().not().isArray(),
                query("primarySource").optional().custom(this.primarySourceValidator.validate),
                query("parkingId").optional().not().isEmpty({ ignore_whitespace: true }),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.path),
            this.cacheHeaderMiddleware.getMiddleware(60, 5),
            this.controller.getAllLatest
        );
    }
}

const v3ParkingMeasurementsRouter: AbstractRouter = new V3ParkingMeasurementsRouter();

export { v3ParkingMeasurementsRouter };

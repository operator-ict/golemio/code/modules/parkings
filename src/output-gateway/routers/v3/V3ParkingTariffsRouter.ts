import { V3ParkingTariffsController } from "#og/controllers/v3/V3ParkingTariffsController";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { RouteVersion } from "../constants";
import { PrimarySourceValidator } from "./helpers/PrimarySourceValidator";
class V3ParkingTariffsRouter extends AbstractRouter {
    private controller: V3ParkingTariffsController;
    private primarySourceValidator: PrimarySourceValidator;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;

    constructor() {
        super(RouteVersion.v3, "parking-tariffs");
        this.cacheHeaderMiddleware = ParkingsContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.controller = new V3ParkingTariffsController();
        this.primarySourceValidator = ParkingsContainer.resolve(ModuleContainerToken.PrimarySourceValidator);
        this.initRoutes();
    }

    protected initRoutes(expire?: string | number | undefined): void {
        this.router.get(
            "/",
            [
                query("isRestrictedToOpenData").optional().isBoolean().not().isArray(),
                query("primarySource").optional().custom(this.primarySourceValidator.validate),
                query("validFrom").optional().isISO8601().not().isArray(),
                query("validTo").optional().isISO8601().not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.path),
            this.cacheHeaderMiddleware.getMiddleware(12 * 60 * 60, 60),
            this.controller.getAll
        );

        this.router.get(
            "/:id",
            param("id").exists().isUUID(),
            query("isRestrictedToOpenData").optional().isBoolean().not().isArray(),
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(12 * 60 * 60, 60),
            this.controller.getOne
        );
    }
}

const v3ParkingTariffRouter: AbstractRouter = new V3ParkingTariffsRouter();

export { v3ParkingTariffRouter };

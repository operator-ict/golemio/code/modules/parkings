import { ParkingPolicyEnum } from "#helpers/constants/ParkingPolicyEnum";
import { V3ParkingController } from "#og/controllers/v3/V3ParkingController";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { RouteVersion } from "../constants";
import { PrimarySourceValidator } from "./helpers/PrimarySourceValidator";

class V3ParkingRouter extends AbstractRouter {
    private controller: V3ParkingController;
    private primarySourceValidator: PrimarySourceValidator;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;
    private allowedAccessDedicatedTo = [
        "disabled",
        "charging",
        "parent",
        "delivery",
        "customer",
        "resident",
        "bus",
        "designated",
        "truck",
        "all",
        "motorcycle",
        "RV",
    ];

    private allowedParkingPolicyType = Object.values(ParkingPolicyEnum);

    constructor() {
        super(RouteVersion.v3, "parking");
        this.cacheHeaderMiddleware = ParkingsContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.controller = new V3ParkingController();
        this.primarySourceValidator = ParkingsContainer.resolve(ModuleContainerToken.PrimarySourceValidator);

        this.initRoutes();
    }

    protected initRoutes(): void {
        this.router.get(
            "/",
            [
                query("isRestrictedToOpenData").optional().isBoolean().not().isArray(),
                query("primarySource").optional().custom(this.primarySourceValidator.validate),
                query("primarySourceId").optional().isString(),
                query("validFrom").optional().isISO8601().not().isArray(),
                query("openingHoursValidFrom").optional().isISO8601(),
                query("openingHoursValidTo").optional().isISO8601(),
                query("boundingBox")
                    .optional()
                    .matches(/^\d+\.?\d*(,\d+\.?\d*){3}$/) // 50.123,14.243,50.017,14.573
                    .not()
                    .isArray(),
                query("accessDedicatedTo").optional().isIn(this.allowedAccessDedicatedTo),
                query("minutesBefore").optional().isInt({ min: 1 }).not().isArray(),
                query("updatedSince").optional().isISO8601().not().isArray(),
                query("parkingPolicy").optional().isIn(this.allowedParkingPolicyType),
                query("activeOnly").optional().isBoolean().not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.path),
            this.cacheHeaderMiddleware.getMiddleware(5 * 60, 5),
            this.controller.getAll
        );

        this.router.get(
            "/:id/average-occupancy",
            [param("id").exists().isString(), query("isRestrictedToOpenData").optional().isBoolean().not().isArray()],
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(8 * 60 * 60, 60),
            this.controller.getAverageOccupancy
        );

        this.router.get(
            "/:id",
            [param("id").exists().isString(), query("isRestrictedToOpenData").optional().isBoolean().not().isArray()],
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(5 * 60, 5),
            this.controller.getOne
        );
    }
}

const v3ParkingRouter: AbstractRouter = new V3ParkingRouter();

export { v3ParkingRouter };

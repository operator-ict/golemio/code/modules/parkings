import { IParkingsModels } from "#og";
import { Parkings } from "#sch";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ParkingsLocationModel extends SequelizeModel {
    public static MODEL_RELATION_PARKING = "parking";

    constructor() {
        super(Parkings.location.name, Parkings.location.pgTableName, Parkings.location.outputSequelizeAttributes, {
            schema: Parkings.pgSchema,
        });
    }

    public Associate = (m: IParkingsModels) => {
        this.sequelizeModel.belongsTo(m.ParkingsModel.sequelizeModel, {
            as: ParkingsLocationModel.MODEL_RELATION_PARKING,
            foreignKey: "source_id",
        });
    };

    GetAll(): never {
        throw new Error("Not implemented");
    }

    GetOne(): never {
        throw new Error("Not implemented");
    }
}

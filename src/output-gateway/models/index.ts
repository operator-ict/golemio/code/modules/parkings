import { ParkingPaymentsRepository } from "#og/data-access/ParkingPaymentsRepository";
import { Parkings } from "#sch";
import { ParkingMeasurementsDtoSchema } from "#sch/datasources/ParkingMeasurementsDtoSchema";
import { ParkingsLocationModel } from "./ParkingsLocationModel";
import { ParkingsMeasurementsModels } from "./ParkingsMeasurementsModels";
import { ParkingsModel } from "./ParkingsModel";
import { V2ParkingTariffsRepository } from "../data-access/v2/V2ParkingTariffsRepository";
import { ParkingsContainer } from "#og/ioc/Di";
import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";

export interface IParkingsModels {
    ParkingsModel: ParkingsModel;
    ParkingsLocationModel: ParkingsLocationModel;
    ParkingsMeasurementsModel: ParkingsMeasurementsModels;
    ParkingsMeasurementsActualModel: ParkingsMeasurementsModels;
    ParkingsTariffsModel: V2ParkingTariffsRepository;
    ParkingPaymentsRepository: ParkingPaymentsRepository;
}

const models: IParkingsModels = {
    ParkingsLocationModel: new ParkingsLocationModel(),
    ParkingsModel: new ParkingsModel(
        ParkingsContainer.resolve<CachedParkingSourcesRepository>(ModuleContainerToken.CachedParkingSourcesRepository)
    ),
    ParkingsMeasurementsModel: new ParkingsMeasurementsModels({
        ...Parkings.measurements,
        outputJsonSchema: ParkingMeasurementsDtoSchema,
    }),
    ParkingsMeasurementsActualModel: new ParkingsMeasurementsModels({
        ...Parkings.measurementsActual,
        outputJsonSchema: ParkingMeasurementsDtoSchema,
    }),
    ParkingsTariffsModel: new V2ParkingTariffsRepository(
        ParkingsContainer.resolve<CachedParkingSourcesRepository>(ModuleContainerToken.CachedParkingSourcesRepository)
    ),
    ParkingPaymentsRepository: new ParkingPaymentsRepository(),
};

for (const type of Object.keys(models)) {
    const model = (models as any)[type];
    if (model.hasOwnProperty("Associate")) {
        model.Associate(models);
    }
}

export { models };

import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { ParkingPaymentsRepository } from "#og/data-access/ParkingPaymentsRepository";
import { GeoConfigHelper } from "#og/helpers/GeoConfigHelper";
import { ParkingsContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { getParkingDetailAttributes } from "#og/models/helpers/DetailAttributesHelper";
import { IGetParkingQueryOptions } from "#og/models/interfaces/IGetQueryOptions";
import { Parkings } from "#sch/index";
import { IPostalAddress } from "@golemio/core/dist/integration-engine/helpers/GeocodeApi";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { sequelizeConnection } from "@golemio/core/dist/output-gateway/database";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";
import { IParkingsModels } from ".";
import ICentroidPg from "./interfaces/ICentroidPg";

export interface IParkings {
    id: string; // big serial
    source: string;
    source_id: string;
    data_provider: string | null;
    name: string;
    category: string | null;
    date_modified: string | null;
    address: IPostalAddress | null;
    location: TGeoCoordinates;
    area_served: string | null;
    total_spot_number: number;
    tariff_id: string | null;
    valid_from: string | null;
    valid_to: string | null;
    distance?: number;
    parking_type: string | null;
    zone_type: string | null;
    centroid: ICentroidPg;
    measurements: {
        available_spots_last_updated: string;
        available_spots_number: number;
    };
    parking_payments: {
        payment_web_url: string | null;
        payment_android_url: string | null;
        payment_ios_url: string | null;
    };
}

export class ParkingsModel extends SequelizeModel {
    public static MODEL_RELATION_ACTUAL_MEASUREMENTS = "measurements";
    public static MODEL_RELATION_PARKING_LOCATIONS = "parking_locations";
    public static MODEL_RELATION_PARKING_PAYMENTS = "parking_payments";

    private paymentsRepository: ParkingPaymentsRepository;
    private geoConfigHelper: GeoConfigHelper;

    public constructor(private parkingSourcesRepository: CachedParkingSourcesRepository) {
        super(Parkings.name, Parkings.parkings.pgTableName, Parkings.parkings.outputSequelizeAttributes, {
            schema: Parkings.pgSchema,
        });
        this.paymentsRepository = ParkingsContainer.resolve<ParkingPaymentsRepository>(
            ModuleContainerToken.ParkingPaymentsRepository
        );
        this.geoConfigHelper = ParkingsContainer.resolve<GeoConfigHelper>(ModuleContainerToken.GeoConfigHelper);
    }

    public Associate = (m: IParkingsModels) => {
        this.sequelizeModel.hasOne(m.ParkingsMeasurementsActualModel.sequelizeModel, {
            as: ParkingsModel.MODEL_RELATION_ACTUAL_MEASUREMENTS,
            foreignKey: "source_id",
            sourceKey: "source_id",
        });
        this.sequelizeModel.hasMany(m.ParkingsLocationModel.sequelizeModel, {
            as: ParkingsModel.MODEL_RELATION_PARKING_LOCATIONS,
            foreignKey: "source_id",
            sourceKey: "source_id",
        });
        this.sequelizeModel.hasOne(m.ParkingPaymentsRepository.sequelizeModel, {
            as: ParkingsModel.MODEL_RELATION_PARKING_PAYMENTS,
            foreignKey: "parking_id",
            sourceKey: "id",
        });
    };

    public GetAll = async (options: IGetParkingQueryOptions = {}): Promise<IParkings[]> => {
        const { lat, lng, range, source, sourceId, category, limit, offset, updatedSince, minutesBefore } = options;
        const order: Sequelize.Order = [];
        const areaLimit = Sequelize.fn(
            "ST_BuildArea",
            Sequelize.fn("ST_GeomFromGeoJSON", JSON.stringify(this.geoConfigHelper.returnGeoCoordinates()))
        );

        let where: Sequelize.WhereOptions = {};
        let attributes: Sequelize.FindAttributeOptions = { include: [], exclude: [] };

        try {
            if (lat && lng) {
                const location = Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", lng, lat), 4326);
                const distance = Sequelize.fn("ST_Distance", Sequelize.col("sanitized_location"), location, true);
                attributes.include!.push([distance, "distance"]);
                order.push([Sequelize.col("distance"), "asc"]);

                if (range) {
                    where.range = Sequelize.where(
                        Sequelize.fn("ST_DWithin", location, Sequelize.col("sanitized_location"), range, true),
                        "true"
                    );
                }
            }

            where.sanitized_location = Sequelize.where(
                Sequelize.fn("ST_Within", Sequelize.col("sanitized_location"), areaLimit),
                "true"
            );

            if (source) {
                where.source = source;
            } else {
                where.source = {
                    [Sequelize.Op.in]: await this.parkingSourcesRepository.getLegacySources({ isRestrictedToOpenData: false }),
                };
            }

            if (sourceId) {
                where.source_id = sourceId;
            }

            if (category) {
                where.category = {
                    [Sequelize.Op.in]: category,
                };
            }

            if (minutesBefore) {
                where.updated_at = {
                    [Sequelize.Op.gte]: Sequelize.literal(`NOW() - INTERVAL '${minutesBefore} minutes'`),
                };
            }

            if (updatedSince) {
                where.updated_at = {
                    [Sequelize.Op.gte]: updatedSince,
                };
            }

            order.push([Sequelize.col("source"), "asc"]);
            order.push([Sequelize.col("source_id"), "asc"]);

            return await this.sequelizeModel.findAll({
                attributes,
                include: [
                    {
                        as: ParkingsModel.MODEL_RELATION_ACTUAL_MEASUREMENTS,
                        model: sequelizeConnection.models[Parkings.measurementsActual.pgTableName],
                        attributes: [
                            [Sequelize.col("date_modified"), "available_spots_last_updated"],
                            [Sequelize.col("available_spot_number"), "available_spots_number"],
                        ],
                    },
                    {
                        as: ParkingsModel.MODEL_RELATION_PARKING_PAYMENTS,
                        model: this.paymentsRepository.sequelizeModel,
                        attributes: ["payment_web_url", "payment_android_url", "payment_ios_url"],
                    },
                ],
                limit,
                offset,
                order,
                raw: true,
                nest: true,
                where,
            });
        } catch (err) {
            throw new GeneralError("Database error", "ParkingsModel", err, 500);
        }
    };

    public GetOne = async (id: string): Promise<IParkings> => {
        return await this.sequelizeModel.findOne({
            include: [
                {
                    as: ParkingsModel.MODEL_RELATION_ACTUAL_MEASUREMENTS,
                    model: sequelizeConnection.models[Parkings.measurementsActual.pgTableName],
                    attributes: [
                        [Sequelize.col("date_modified"), "available_spots_last_updated"],
                        [Sequelize.col("available_spot_number"), "available_spots_number"],
                    ],
                },
                {
                    as: ParkingsModel.MODEL_RELATION_PARKING_PAYMENTS,
                    model: this.paymentsRepository.sequelizeModel,
                    attributes: ["payment_web_url", "payment_android_url", "payment_ios_url"],
                },
            ],
            where: {
                id,
                source: { [Op.in]: await this.parkingSourcesRepository.getLegacySources({ isRestrictedToOpenData: false }) },
            },
            raw: true,
            nest: true,
        });
    };

    public GetAllDetail = async (options: IGetParkingQueryOptions = {}): Promise<IParkings[]> => {
        const { limit, offset } = options;
        const order: Sequelize.Order = [];
        const where: Sequelize.WhereOptions = {};
        const attributes = getParkingDetailAttributes();

        try {
            if (options.lat && options.lng) {
                const location = Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326);
                const distance = Sequelize.fn(
                    "ST_Distance",
                    Sequelize.literal(
                        `CASE WHEN ${ParkingsModel.MODEL_RELATION_PARKING_LOCATIONS}.sanitized_locations IS NULL
                        THEN ${Parkings.parkings.pgTableName}.sanitized_location
                        ELSE ${ParkingsModel.MODEL_RELATION_PARKING_LOCATIONS}.sanitized_locations END`
                    ),
                    location,
                    true
                );

                (attributes as Array<string | Sequelize.ProjectionAlias>).push([distance, "distance"]);
                order.push([Sequelize.col("distance"), "asc"]);

                if (options.range) {
                    where.range = Sequelize.where(
                        Sequelize.fn("ST_DWithin", location, Sequelize.col("parkings.sanitized_location"), options.range, true),
                        "true"
                    );
                }
            }

            if (options.source) {
                where.source = options.source;
            } else {
                where.source = {
                    [Op.in]: await this.parkingSourcesRepository.getLegacySources({ isRestrictedToOpenData: false }),
                };
            }

            if (options.sourceId) {
                where.source_id = options.sourceId;
            }

            if (options.category) {
                where.category = {
                    [Sequelize.Op.in]: options.category,
                };
            }

            if (options.minutesBefore) {
                const now = Date.now();
                const beforeTime = now - Number(options.minutesBefore) * 60 * 1000;
                where.date_modified = {
                    [Sequelize.Op.gte]: new Date(beforeTime).toISOString(),
                };
            }

            if (options.updatedSince) {
                where.date_modified = {
                    [Sequelize.Op.gte]: new Date(options.updatedSince).toISOString(),
                };
            }

            if (options.parkingType) {
                where.parking_type = {
                    [Sequelize.Op.in]: options.parkingType,
                };
            }

            if (options.zoneType) {
                where.zone_type = {
                    [Sequelize.Op.in]: options.zoneType,
                };
            }

            order.push([Sequelize.col("source"), "asc"]);
            order.push([Sequelize.col("source_id"), "asc"]);

            return await this.sequelizeModel.findAll({
                attributes,
                include: [
                    {
                        as: ParkingsModel.MODEL_RELATION_PARKING_LOCATIONS,
                        model: sequelizeConnection.models[Parkings.location.pgTableName],
                        attributes: [],
                        required: false,
                        where: Sequelize.literal(`"parking_locations".updated_at > ("parkings".updated_at - interval '2 day')`),
                    },
                    {
                        as: ParkingsModel.MODEL_RELATION_ACTUAL_MEASUREMENTS,
                        model: sequelizeConnection.models[Parkings.measurementsActual.pgTableName],
                        attributes: [
                            [Sequelize.col("date_modified"), "available_spots_last_updated"],
                            [Sequelize.col("available_spot_number"), "available_spots_number"],
                        ],
                    },
                    {
                        as: ParkingsModel.MODEL_RELATION_PARKING_PAYMENTS,
                        model: this.paymentsRepository.sequelizeModel,
                        attributes: ["payment_web_url", "payment_android_url", "payment_ios_url"],
                    },
                ],
                limit,
                offset,
                order,
                raw: true,
                nest: true,
                where,
                subQuery: false,
            });
        } catch (err) {
            throw new GeneralError("Database error", "ParkingsLocationModel", err, 500);
        }
    };

    public GetOneDetail = async (id: string): Promise<IParkings> => {
        const [parkingId, locationId] = id.split("_");
        return await this.sequelizeModel.findOne({
            attributes: getParkingDetailAttributes(),
            include: [
                {
                    as: ParkingsModel.MODEL_RELATION_PARKING_LOCATIONS,
                    model: sequelizeConnection.models[Parkings.location.pgTableName],
                    attributes: [],
                    ...(locationId && { where: { id: locationId } }),
                },
                {
                    as: ParkingsModel.MODEL_RELATION_ACTUAL_MEASUREMENTS,
                    model: sequelizeConnection.models[Parkings.measurementsActual.pgTableName],
                    attributes: [
                        [Sequelize.col("date_modified"), "available_spots_last_updated"],
                        [Sequelize.col("available_spot_number"), "available_spots_number"],
                    ],
                },
                {
                    as: ParkingsModel.MODEL_RELATION_PARKING_PAYMENTS,
                    model: this.paymentsRepository.sequelizeModel,
                    attributes: ["payment_web_url", "payment_android_url", "payment_ios_url"],
                },
            ],
            where: {
                id: parkingId,
                source: {
                    [Op.in]: await this.parkingSourcesRepository.getLegacySources({ isRestrictedToOpenData: false }),
                },
            },
            raw: true,
            nest: true,
            subQuery: false,
        });
    };
}

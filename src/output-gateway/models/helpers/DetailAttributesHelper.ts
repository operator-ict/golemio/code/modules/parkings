import Sequelize from "@golemio/core/dist/shared/sequelize";
import { Parkings } from "#sch";
import { ParkingsModel } from "#og/models/ParkingsModel";

export const getParkingDetailAttributes = (): Sequelize.FindAttributeOptions => [
    [
        Sequelize.literal(
            `CASE WHEN ${ParkingsModel.MODEL_RELATION_PARKING_LOCATIONS}.id IS NULL
                THEN ${Parkings.parkings.pgTableName}.id::text
                ELSE CONCAT(${Parkings.parkings.pgTableName}.id, '_', ${ParkingsModel.MODEL_RELATION_PARKING_LOCATIONS}.id) END`
        ),
        "id",
    ],
    "source",
    "source_id",
    [
        Sequelize.fn(
            "COALESCE",
            Sequelize.col(`${ParkingsModel.MODEL_RELATION_PARKING_LOCATIONS}.data_provider`),
            Sequelize.col(`${Parkings.parkings.pgTableName}.data_provider`)
        ),
        "data_provider",
    ],
    "name",
    "category",
    "date_modified",
    [
        Sequelize.fn(
            "COALESCE",
            Sequelize.col(`${ParkingsModel.MODEL_RELATION_PARKING_LOCATIONS}.address`),
            Sequelize.col(`${Parkings.parkings.pgTableName}.address`)
        ),
        "address",
    ],
    [
        Sequelize.fn(
            "COALESCE",
            Sequelize.col(`${ParkingsModel.MODEL_RELATION_PARKING_LOCATIONS}.location`),
            Sequelize.col(`${Parkings.parkings.pgTableName}.location`)
        ),
        "location",
    ],
    "area_served",
    [
        Sequelize.fn(
            "COALESCE",
            Sequelize.col(`${ParkingsModel.MODEL_RELATION_PARKING_LOCATIONS}.total_spot_number`),
            Sequelize.col(`${Parkings.parkings.pgTableName}.total_spot_number`)
        ),
        "total_spot_number",
    ],
    "tariff_id",
    "valid_from",
    "valid_to",
    "parking_type",
    "zone_type",
    [
        Sequelize.fn(
            "COALESCE",
            Sequelize.col(`${ParkingsModel.MODEL_RELATION_PARKING_LOCATIONS}.centroid`),
            Sequelize.col(`${Parkings.parkings.pgTableName}.centroid`)
        ),
        "centroid",
    ],
];

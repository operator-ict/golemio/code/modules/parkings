import { IModelDefinition, Parkings } from "#sch";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ITskMeasurementsFilterParams, ITskMeasurementsRecord } from "./interfaces/TskMeasurementsInterfaces";

export interface ICountMeasurementsFilterParams {
    source?: string[];
    sourceId?: string;
    measureTo?: string;
    measureFrom?: string;
    minutesBefore?: number;
    updatedSince?: Date;
}

export interface IGetMeasurementsFilterParams extends ICountMeasurementsFilterParams {
    limit?: number;
    offset?: number;
}

export interface IParkingsMeasurement {
    source: string;
    source_id: string;
    available_spot_number: number;
    closed_spot_number: number;
    occupied_spot_number: number;
    total_spot_number: number;
    date_modified: string; // bigInt date
    parking_id: string;
}

export class ParkingsMeasurementsModels extends SequelizeModel {
    public constructor(modelDefinition: IModelDefinition) {
        super(modelDefinition.name, modelDefinition.pgTableName, modelDefinition.outputSequelizeAttributes, {
            schema: Parkings.pgSchema,
        });
    }

    public GetAll = async (options: IGetMeasurementsFilterParams): Promise<IParkingsMeasurement[]> => {
        const { measureFrom, measureTo, limit, offset, source, sourceId, minutesBefore, updatedSince } = options;
        const order: Sequelize.Order = [];
        let where: Sequelize.WhereOptions = {};
        let attributes: Sequelize.FindAttributeOptions = { include: [], exclude: [] };

        if (source) {
            where.source = {
                [Sequelize.Op.in]: source,
            };
        }

        if (sourceId) {
            where.source_id = sourceId;
        }

        if (measureFrom) {
            where.date_modified = measureTo
                ? { [Sequelize.Op.between]: [measureFrom, measureTo] }
                : { [Sequelize.Op.gte]: measureFrom };
        } else {
            if (measureTo) {
                where.date_modified = { [Sequelize.Op.lte]: measureTo };
            }
        }

        if (minutesBefore) {
            where.updated_at = {
                [Sequelize.Op.gte]: Sequelize.literal(`NOW() - INTERVAL '${minutesBefore} minutes'`),
            };
        }

        if (updatedSince) {
            where.updated_at = {
                [Sequelize.Op.gte]: updatedSince,
            };
        }

        order.push(["date_modified", "desc"]);

        try {
            return await this.sequelizeModel.findAll({
                attributes,
                limit,
                offset,
                order,
                raw: true,
                where,
            });
        } catch (err) {
            throw new GeneralError(`Database error: ${err.message}`, "ParkingMeasurementsModel", err, 500);
        }
    };

    /**
     * Get TSK measurements for the legacy history endpoint (/parkings/history)
     */
    public GetAllTskMeasurements = async (options: ITskMeasurementsFilterParams): Promise<ITskMeasurementsRecord[]> => {
        const { limit, offset, from, to, sensorId } = options;
        let where: Sequelize.WhereOptions = {
            source: { [Sequelize.Op.in]: ["tsk", "TSK"] },
        };

        if (sensorId) {
            where.source_id = sensorId;
        }

        if (from) {
            where.date_modified = to ? { [Sequelize.Op.between]: [from, to] } : { [Sequelize.Op.gte]: from };
        } else {
            if (to) {
                where.date_modified = { [Sequelize.Op.lte]: to };
            }
        }

        try {
            const result = await this.sequelizeModel.findAll({
                attributes: [
                    ["source_id", "id"],
                    ["available_spot_number", "num_of_free_places"],
                    ["occupied_spot_number", "num_of_taken_places"],
                    "updated_at",
                    ["total_spot_number", "total_num_of_places"],
                    ["date_modified", "last_updated"],
                ],
                limit,
                offset,
                order: [["date_modified", "desc"]],
                raw: true,
                where,
            });

            return result.map((record) => {
                const { id, last_updated, ...rest } = record;
                return {
                    id: Number.isNaN(parseInt(id)) ? id : parseInt(id),
                    last_updated: last_updated ? new Date(last_updated).getTime() : null,
                    ...rest,
                };
            });
        } catch (err) {
            throw new GeneralError("GetAllTskMeasurements: Error while fetching measurements", this.constructor.name, err, 500);
        }
    };

    public GetOne = async (): Promise<void> => {};
}

import { IGeoCoordinatesPoint } from "@golemio/core/dist/output-gateway";

export default interface ICentroidPg extends IGeoCoordinatesPoint {
    crs: { type: string; properties: { name: string } };
}

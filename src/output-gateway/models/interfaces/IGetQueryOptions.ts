export interface IGetParkingQueryOptions {
    lat?: number;
    lng?: number;
    source?: string;
    sourceId?: string;
    category?: string[];
    range?: number;
    limit?: number;
    offset?: number;
    minutesBefore?: number;
    updatedSince?: Date;
    parkingType?: string[];
    zoneType?: string[];
}

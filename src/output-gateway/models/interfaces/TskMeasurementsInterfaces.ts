export interface ITskMeasurementsFilterParams {
    limit?: number;
    offset?: number;
    from?: string;
    to?: string;
    sensorId?: string;
}

export interface ITskMeasurementsRecord {
    id: string | number;
    num_of_free_places: number;
    num_of_taken_places: number;
    updated_at: string | null;
    total_num_of_places: number;
    last_updated: number | null;
}

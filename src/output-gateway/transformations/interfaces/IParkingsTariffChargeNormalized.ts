export interface IParkingsTariffChargeNormalized {
    charge: number;
    charge_type: string;
    charge_order_index: number;
    charge_interval: number | null;
    max_iterations_of_charge: number | null;
    min_iterations_of_charge: number | null;
    start_time_of_period: string | null;
    end_time_of_period: string | null;
}

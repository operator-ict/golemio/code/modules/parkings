export interface IV3ParkingMeasurementsDto {
    parking_id: string;
    primary_source: string;
    primary_source_id: string;
    has_free_spots: boolean;
    total_spot_number: number | null;
    free_spot_number: number | null;
    closed_spot_number: number | null;
    occupied_spot_number: number | null;
    last_updated: Date;
    average_occupancy?: V3ParkingAverageOccupancyDto;
}

export type V3ParkingAverageOccupancyDto = {
    [K in string]: Record<string, number>;
};

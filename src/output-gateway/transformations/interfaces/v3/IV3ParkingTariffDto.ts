import { IParkingTariffPeriod } from "#sch/models/interfaces/IParkingTariff";

export interface IV3Charge {
    id: string;
    charge: number;
    charge_type: string;
    charge_order_index: number;
    charge_interval: number;
    max_iterations_of_charge: number;
    min_iterations_of_charge: number;
    valid_from: string;
    valid_to: string;
    periods_of_time: IParkingTariffPeriod[];
}

export interface IV3ChargeBand {
    primary_source: string;
    primary_source_id: string;
    maximum_duration: number;
    valid_from: string;
    valid_to: string;
    last_updated: Date;
    last_modified_at_source: Date;
    charges: Array<Partial<IV3Charge>>;
}

export interface IV3ParkingTariffDto {
    id: string;
    charge_bands: Array<Partial<IV3ChargeBand>>;
}

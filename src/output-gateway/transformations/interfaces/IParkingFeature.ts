import { IGeoCoordinatesPoint, TGeoCoordinates } from "@golemio/core/dist/output-gateway";
import { IPostalAddress } from "@golemio/core/dist/integration-engine";

export interface IParkingsFeature {
    geometry: TGeoCoordinates;
    properties: {
        id: string; // big serial
        source: string;
        source_id: string;
        data_provider: string | null;
        name: string;
        category: string | null;
        date_modified: string | null;
        address_formatted: string | null;
        address: IPostalAddress | null;
        area_served: string | null;
        web_app_payment_url: string | null;
        android_app_payment_url: string | null;
        ios_app_payment_url: string | null;
        total_spot_number: number;
        tariff_id: string | null;
        valid_from: string | null;
        valid_to: string | null;
        parking_type: string;
        zone_type: string | null;
        centroid: IGeoCoordinatesPoint;
        available_spots_last_updated: string | null;
        available_spots_number: number | null;
    };
    type: string;
}

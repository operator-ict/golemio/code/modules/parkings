import { IParkingsTariff } from "#og/data-access/v2/V2ParkingTariffsRepository";
import {
    buildGeojsonFeatureCollection,
    buildGeojsonFeatureType,
    IGeoCoordinatesPoint,
    IGeoJSONFeatureCollection,
    TGeoCoordinates,
} from "@golemio/core/dist/output-gateway";
import * as turf from "@turf/turf";
import { IParkings } from "../models/ParkingsModel";
import { IParkingsFeature } from "./interfaces/IParkingFeature";
import { IParkingsTariffChargeNormalized } from "#og/transformations/interfaces/IParkingsTariffChargeNormalized";
import { IPostalAddress } from "@golemio/core/dist/integration-engine";
import ICentroidPg from "#og/models/interfaces/ICentroidPg";
import { dateTime } from "@golemio/core/dist/helpers";

export interface IParkingsFlattened {
    id: string; // big serial
    source: string;
    source_id: string;
    data_provider: string | null;
    name: string;
    category: string | null;
    date_modified: string | null;
    address: IPostalAddress | null;
    location: TGeoCoordinates;
    area_served: string | null;
    payment_web_url: string | null;
    payment_android_url: string | null;
    payment_ios_url: string | null;
    total_spot_number: number;
    tariff_id: string | null;
    valid_from: string | null;
    valid_to: string | null;
    distance?: number;
    parking_type: string | null;
    zone_type: string | null;
    centroid: ICentroidPg;
    available_spots_last_updated: string;
    available_spots_number: number;
}

export class ParkingsTransformationFabric {
    public buildFeatureItem = (locationFromDb: IParkingsFlattened): IParkingsFeature => {
        return <IParkingsFeature>buildGeojsonFeatureType("location", {
            id: locationFromDb.id,
            source: locationFromDb.source,
            source_id: locationFromDb.source_id,
            data_provider: locationFromDb.data_provider,
            name: locationFromDb.name,
            category: locationFromDb.category,
            date_modified: locationFromDb.date_modified ? new Date(Number(locationFromDb.date_modified)).toISOString() : null,
            address_formatted: locationFromDb.address ? locationFromDb.address.address_formatted : null,
            address: locationFromDb.address,
            location: locationFromDb.location,
            area_served: locationFromDb.area_served,
            web_app_payment_url: locationFromDb.payment_web_url,
            android_app_payment_url: locationFromDb.payment_android_url,
            ios_app_payment_url: locationFromDb.payment_ios_url,
            total_spot_number: locationFromDb.total_spot_number,
            tariff_id: locationFromDb.tariff_id,
            valid_from: locationFromDb.valid_from,
            valid_to: locationFromDb.valid_to,
            parking_type: locationFromDb.parking_type,
            zone_type: locationFromDb.zone_type,
            centroid: this.getCentroid(locationFromDb),
            available_spots_last_updated: this.transformAvailableSpotsLastUpdated(locationFromDb.available_spots_last_updated),
            available_spots_number: locationFromDb.available_spots_number || null,
        });
    };

    private transformAvailableSpotsLastUpdated = (lastUpdatedTime: string) => {
        if (!lastUpdatedTime) return null;
        return dateTime(new Date(+lastUpdatedTime)).setTimeZone("Europe/Prague").toISOString({ includeMillis: false });
    };

    public buildFeatureCollection = (locations: IParkings[]): IGeoJSONFeatureCollection => {
        const allLocations = locations.map((location) => {
            const { measurements, parking_payments, ...rest } = location;
            return this.buildFeatureItem({ ...rest, ...measurements, ...parking_payments });
        });

        return buildGeojsonFeatureCollection(allLocations);
    };

    public getChargeData = (data: IParkingsTariff): IParkingsTariffChargeNormalized => ({
        charge: +data.charge,
        charge_type: data.charge_type || "other",
        charge_order_index: data.charge_order_index,
        charge_interval: data.charge_interval,
        max_iterations_of_charge: data.max_iterations_of_charge,
        min_iterations_of_charge: data.min_iterations_of_charge,
        start_time_of_period: data.start_time_of_period,
        end_time_of_period: data.end_time_of_period,
    });

    private getCentroid(locationFromDb: IParkingsFlattened) {
        let centroidTransformed: IGeoCoordinatesPoint;

        if (locationFromDb.centroid) {
            centroidTransformed = { type: locationFromDb.centroid.type, coordinates: locationFromDb.centroid.coordinates };
        } else if (locationFromDb.location.type === "Point") {
            centroidTransformed = { type: locationFromDb.location.type, coordinates: locationFromDb.location.coordinates };
        } else {
            centroidTransformed = turf.centroid(locationFromDb.location).geometry as IGeoCoordinatesPoint;
        }
        return centroidTransformed;
    }
}

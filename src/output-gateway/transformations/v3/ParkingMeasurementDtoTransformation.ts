import { IParkingLatestMeasurements } from "#og/data-access/interfaces/IParkingMeasurementsDto";
import { IV3ParkingMeasurementsDto } from "#og/transformations/interfaces/v3/IV3ParkingMeasurementsDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";

export class ParkingMeasurementDtoTransformation extends AbstractTransformation<
    IParkingLatestMeasurements,
    IV3ParkingMeasurementsDto
> {
    public name: string = "ParkingMeasurementDtoTransformation";

    protected transformInternal = (data: IParkingLatestMeasurements): IV3ParkingMeasurementsDto => {
        const hasOccupancyInfo = !(data.total_spot_number === null);
        const hasFreeSpots = !!data.available_spot_number;

        return {
            parking_id: data.parking_id,
            primary_source: data.source,
            primary_source_id: data.source_id,
            has_free_spots: hasFreeSpots,
            total_spot_number: data.total_spot_number,
            free_spot_number: hasOccupancyInfo ? data.available_spot_number : null,
            closed_spot_number: hasOccupancyInfo ? data.closed_spot_number : null,
            occupied_spot_number: hasOccupancyInfo ? data.occupied_spot_number : null,
            last_updated: data.date_modified,
        };
    };
}

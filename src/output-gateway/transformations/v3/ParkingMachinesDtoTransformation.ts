import { IParkingMachine } from "#sch/models/interfaces/IParkingMachine";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IGeoJSONFeature, buildGeojsonFeature, buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway/Geo";
import { Point } from "@golemio/core/dist/shared/geojson";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ParkingMachinesDtoTransformation extends AbstractTransformation<IParkingMachine, IGeoJSONFeature> {
    public name: string = "ParkingMachinesDtoTransformation";

    public transformToFeatureCollection = (elements: IParkingMachine[]) => {
        try {
            return buildGeojsonFeatureCollection(this.transformArray(elements));
        } catch (err) {
            throw new GeneralError("Transformation error", this.name, err, 500);
        }
    };

    protected transformInternal = (element: IParkingMachine) => {
        return buildGeojsonFeature(
            {
                id: element.id,
                primary_source: element.source,
                primary_source_id: element.sourceId,
                code: element.code,
                machine_type: element.type,
                valid_from: element.validFrom,
                tariff_id: element.tariffId,
                lon: (element.location as Point).coordinates[0],
                lat: (element.location as Point).coordinates[1],
            },
            "lon",
            "lat",
            true
        );
    };
}

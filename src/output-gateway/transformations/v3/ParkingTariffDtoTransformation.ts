import { IParkingTariff } from "#sch/models/interfaces/IParkingTariff";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IV3Charge, IV3ChargeBand, IV3ParkingTariffDto } from "../interfaces/v3/IV3ParkingTariffDto";
import { DateTime } from "@golemio/core/dist/shared/luxon";

export class ParkingTariffDtoTransformation extends AbstractTransformation<
    Array<{ tariff_id: string; parking_tariffs: Array<IParkingTariff & { updated_at: Date }> }>,
    Array<Partial<IV3ParkingTariffDto>>
> {
    public name: string = "ParkingTariffDtoTransformation";

    protected transformInternal = (
        elements: Array<{ tariff_id: string; parking_tariffs: Array<IParkingTariff & { updated_at: Date }> }>
    ) => {
        return elements.map((el) => {
            const result: IV3ParkingTariffDto = {
                id: el.tariff_id,
                charge_bands: this.transformToChargeBands(el.parking_tariffs),
            };

            return result;
        });
    };

    private transformToChargeBands(sameTariff: Array<IParkingTariff & { updated_at: Date }>): Array<Partial<IV3ChargeBand>> {
        const groupedChargeBands = sameTariff.reduce((acc, curr) => {
            if (!acc[curr.charge_band_name]) {
                acc[curr.charge_band_name] = [];
            }

            acc[curr.charge_band_name].push(curr);
            return acc;
        }, {} as { [key: string]: Array<IParkingTariff & { updated_at: Date }> });

        return Object.keys(groupedChargeBands).map((key) => {
            const element = groupedChargeBands[key][0];
            const charbandValidFrom = this.minDate(groupedChargeBands[key].map((element) => element.valid_from as Date));
            const charbandValidTo = this.maxDate(groupedChargeBands[key].map((element) => element.valid_to as Date));

            return {
                primary_source: element.source,
                primary_source_id: element.tariff_id,
                maximum_duration: element.maximum_duration_seconds,
                valid_from: charbandValidFrom,
                valid_to: charbandValidTo,
                last_updated: element.updated_at,
                last_modified_at_source: element.last_updated,
                payment_mode: element.payment_mode,
                url: element.url_link_address,
                free_of_charge: element.free_of_charge,
                payment_methods: element.payment_methods ?? [],
                charges: this.transformToCharges(groupedChargeBands[key]),
            } as Partial<IV3ChargeBand>;
        });
    }

    private transformToCharges(elements: IParkingTariff[]): Array<Partial<IV3Charge>> {
        return elements.map((element) => {
            return {
                id: element.tariff_id,
                charge: element.charge,
                charge_type: element.charge_type,
                charge_order_index: element.charge_order_index,
                charge_interval: element.charge_interval,
                max_iterations_of_charge: element.max_iterations_of_charge,
                min_iterations_of_charge: element.min_iterations_of_charge,
                valid_from: (element.valid_from as Date)?.toISOString(),
                valid_to: (element.valid_to as Date)?.toISOString(),
                periods_of_time: element.periods_of_time,
            } as Partial<IV3Charge>;
        });
    }

    private minDate(isoStrings: Array<Date | null>): string | null {
        isoStrings = isoStrings.filter((el) => el !== null) as Date[];

        if (isoStrings.length === 0) {
            return null;
        }

        return DateTime.min(...isoStrings.map((el) => DateTime.fromJSDate(el as Date)))
            .toJSDate()
            .toISOString();
    }

    private maxDate(isoStrings: Array<Date | null>): string | null {
        isoStrings = isoStrings.filter((el) => el !== null) as Date[];

        if (isoStrings.length === 0) {
            return null;
        }

        return DateTime.max(...isoStrings.map((el) => DateTime.fromJSDate(el as Date)))
            .toJSDate()
            .toISOString();
    }
}

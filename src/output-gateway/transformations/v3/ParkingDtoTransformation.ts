import { TParkingPaymentsSourceConfig } from "#helpers/data-access/CachedParkingSourcesRepository";
import { V3ParkingAverageOccupancyDto } from "#og/transformations/interfaces/v3/IV3ParkingMeasurementsDto";
import { IParking, IParkingEntrance, IParkingWithLocationAndOccupancyInfo } from "#sch/models/interfaces/IParking";
import { IParkingAverageOccupancy } from "#sch/models/interfaces/IParkingAverageOccupancy";
import { IParkingLocation } from "#sch/models/interfaces/IParkingLocation";
import { IParkingProhibitions } from "#sch/models/interfaces/IParkingProhibitions";
import { IOpeningHours } from "#sch/models/interfaces/openingHours/IOpeningHours";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IGeoJSONFeature, buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway/Geo";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export class ParkingDtoTransformation extends AbstractTransformation<IParkingWithLocationAndOccupancyInfo, IGeoJSONFeature> {
    public name: string = "ParkingDtoTransformation";

    constructor(private paymentSources: TParkingPaymentsSourceConfig, private logger: ILogger) {
        super();
    }

    public transformToFeatureCollection = (elements: IParkingWithLocationAndOccupancyInfo[]) => {
        try {
            return buildGeojsonFeatureCollection(this.transformArray(elements));
        } catch (err) {
            throw new GeneralError("Transformation error", this.name, err, 500);
        }
    };

    protected transformInternal = (element: IParkingWithLocationAndOccupancyInfo) => {
        const properties = {
            id: element.id,
            primary_source: element.source,
            primary_source_id: element.source_id,
            name: element.name,
            valid_from: element.valid_from,
            centroid: element?.centroid
                ? {
                      type: element?.centroid?.type, // sequelize returns crs as party of geometry object
                      coordinates: element?.centroid?.coordinates,
                  }
                : undefined,
            last_updated_at: element.updated_at,
            last_modified_at_source: element.date_modified,
            security: element.security,
            dimensions: element.max_vehicle_dimensions
                ? {
                      max_height: element.max_vehicle_dimensions[0],
                      max_width: element.max_vehicle_dimensions[1],
                      max_length: element.max_vehicle_dimensions[2],
                  }
                : null,
            covered: element.covered,
            capacity: element.total_spot_number,
            parking_policy: element.parking_policy,
            contact: element.contact && {
                phone: element.contact.phone,
                email: element.contact.email,
                web_url: element.contact.website,
            },
            filter: element.category ?? null,
            parking_type: element.parking_type ?? null,
            space: buildGeojsonFeatureCollection(
                element.parking_locations.map((location) => {
                    return this.transformLocation(location);
                })
            ),
            opening_hours: element?.parking_hours
                ? element.parking_hours.map((element: IOpeningHours) => {
                      return this.transformParkingHours(element);
                  })
                : [],
            entrances: buildGeojsonFeatureCollection(
                element.parking_entrances.map((entrance) => {
                    return this.transformEntrance(entrance);
                })
            ),
            payment: this.transformPayment(element.source, element),
            reservation: this.transformReservation(element.source, element),
            parking_prohibitions: this.transFormParkingProhibitions(element.parking_prohibitions),
            tariff: element.tariff_id,
            has_occupancy_info: element.parking_average_occupancy,
            area: element.area,
            address: element.address,
        };

        return {
            geometry: {
                coordinates: element.location.coordinates,
                type: element.location.type,
            },
            properties: properties,
            type: "Feature",
        } as IGeoJSONFeature;
    };

    private transFormParkingProhibitions = (element: IParkingProhibitions) => {
        return {
            "lpg/cng": element?.lpg ?? null,
            bus: element?.bus ?? null,
            truck: element?.truck ?? null,
            motorcycle: element?.motorcycle ?? null,
            bicycle: element?.bicycle ?? null,
            trailer: element?.trailer ?? null,
        };
    };

    private transformLocation = (element: IParkingLocation) => {
        const properties = {
            id: element.id,
            capacity: element.total_spot_number,
            access_dedicated_to: element.special_access,
            address: element.address,
        };

        return {
            geometry: {
                coordinates: element.location.coordinates,
                type: element.location.type,
            },
            properties: properties,
            type: "Feature",
        } as IGeoJSONFeature;
    };

    private transformParkingHours = (element: IOpeningHours) => {
        return {
            valid_from: new Date(element.valid_from).valueOf() === new Date(0).valueOf() ? null : element.valid_from,
            valid_to: element.valid_to,
            periods_of_time: element.periods_of_time,
        };
    };

    private transformEntrance = (element: IParkingEntrance) => {
        const properties = {
            entry: element.entry,
            exit: element.exit,
            entrance_type: element.entrance_type,
            level: element?.level ? parseFloat(element.level) : null,
            dimension: {
                max_height: element?.max_height ? parseFloat(element.max_height) : null,
                max_width: element?.max_width ? parseFloat(element.max_width) : null,
                max_length: element?.max_length ? parseFloat(element.max_length) : null,
                max_weight: element?.max_weight ? parseFloat(element.max_weight) : null,
            },
        };

        return {
            geometry: {
                coordinates: element.location.coordinates,
                type: element.location.type,
            },
            properties: properties,
            type: "Feature",
        } as IGeoJSONFeature;
    };

    private transformPayment = (source: string, element: IParkingWithLocationAndOccupancyInfo) => {
        const sourcePayment = this.paymentSources[source]?.payment ?? {
            web_url: null,
            android_url: null,
            ios_url: null,
            discovery_url: null,
        };

        return {
            web_url: element.parking_payments?.payment_web_url
                ? element.parking_payments.payment_web_url
                : this.getDynamicPaymentLink(sourcePayment.web_url, element),
            android_url: element.parking_payments?.payment_android_url
                ? element.parking_payments.payment_android_url
                : this.getDynamicPaymentLink(sourcePayment.android_url, element),
            ios_url: element.parking_payments?.payment_ios_url
                ? element.parking_payments.payment_ios_url
                : this.getDynamicPaymentLink(sourcePayment.ios_url, element),
            discovery_url: element.parking_payments?.payment_discovery_url
                ? element.parking_payments.payment_discovery_url
                : this.getDynamicPaymentLink(sourcePayment.discovery_url, element),
        };
    };

    private transformReservation = (source: string, element: IParkingWithLocationAndOccupancyInfo) => {
        const sourceReservation = this.paymentSources[source]?.reservation ?? {
            type: null,
            web_url: null,
            android_url: null,
            ios_url: null,
            discovery_url: null,
        };

        return {
            reservation_type: element.parking_payments?.reservation_type
                ? element.parking_payments.reservation_type
                : sourceReservation.type,
            web_url: element.parking_payments?.reservation_web_url
                ? element.parking_payments.reservation_web_url
                : this.getDynamicPaymentLink(sourceReservation.web_url, element),
            android_url: element.parking_payments?.reservation_android_url
                ? element.parking_payments.reservation_android_url
                : this.getDynamicPaymentLink(sourceReservation.android_url, element),
            ios_url: element.parking_payments?.reservation_ios_url
                ? element.parking_payments.reservation_ios_url
                : this.getDynamicPaymentLink(sourceReservation.ios_url, element),
            discovery_url: element.parking_payments?.reservation_discovery_url
                ? element.parking_payments.reservation_discovery_url
                : this.getDynamicPaymentLink(sourceReservation.discovery_url, element),
        };
    };

    private getDynamicPaymentLink = (link: string | null, el: IParkingWithLocationAndOccupancyInfo) => {
        const placeholder = link?.match(/[^{\}]+(?=})/);
        if (placeholder && placeholder[0]) {
            const value = el[placeholder[0] as keyof IParking];
            if (!value) {
                this.logger.error(`Invalid payment link shortcode (${placeholder[0]}) for ${el.source}: ${link}`);
                return null;
            }
            return link?.replace(`{${placeholder}}`, el[placeholder[0] as keyof IParking] as string);
        }

        return link;
    };

    public transformAverageOccupancyArray = (
        data: Array<Omit<IParkingAverageOccupancy, "hour"> & { hour: string }>
    ): V3ParkingAverageOccupancyDto => {
        const result: V3ParkingAverageOccupancyDto = {};
        for (const item of data) {
            if (!result[item.day_of_week]) {
                result[item.day_of_week] = { [item.hour]: item.average_occupancy };
            } else {
                result[item.day_of_week][item.hour] = item.average_occupancy;
            }
        }
        return result;
    };
}

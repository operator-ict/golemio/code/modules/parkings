export interface IParkingSourcesParams {
    primarySource: string[];
    isRestrictedToOpenData: boolean;
}

import { IPaginationParams } from "./IPaginationParams";

export interface IParkingMeasurementsParams extends IPaginationParams {
    primarySource: string[];
    parkingId?: string[];
}

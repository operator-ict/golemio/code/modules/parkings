import { IPaginationParams } from "./IPaginationParams";

export interface IParkingMachinesParams extends IPaginationParams {
    primarySource?: string[];
    validFrom?: string;
    type?: string[];
    boundingBox?: number[]; // [topLeft.lat,topLeft.lon,bottomRight.lat,bottomRight.lon]
    codeMask?: string;
}

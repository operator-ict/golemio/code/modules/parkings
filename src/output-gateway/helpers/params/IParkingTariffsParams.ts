import { IPaginationParams } from "./IPaginationParams";

export interface IParkingTariffsParams extends IPaginationParams {
    primarySource?: string[];
    validFrom?: string;
    validTo?: string;
}

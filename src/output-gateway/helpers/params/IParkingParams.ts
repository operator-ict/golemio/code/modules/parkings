import { IPaginationParams } from "./IPaginationParams";

export interface IParkingParams extends IPaginationParams {
    primarySource?: string[];
    validFrom?: string;
    boundingBox?: number[];
    accessDedicatedTo?: string[];
    minutesBefore?: number;
    updatedSince?: string;
    openFrom?: string;
    openTo?: string;
    isRestrictedToOpenData: boolean;
    parkingPolicy?: string[];
    activeOnly?: boolean;
}

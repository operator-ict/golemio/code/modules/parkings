import { CustomSanitizer } from "@golemio/core/dist/shared/express-validator";

export class ParamSanitizerHelper {
    public static sourceSanitizer = (): CustomSanitizer => {
        return (value: string) => {
            const sanitized = value.trim().toLowerCase();
            if (sanitized === "ipr ztp") {
                return "ipr";
            }
            return sanitized;
        };
    };

    public static toArraySanitizer = (): CustomSanitizer => {
        return (value) => {
            return Array.isArray(value) ? value : [value];
        };
    };
}

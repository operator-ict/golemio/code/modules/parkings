import { Geometry } from "@golemio/core/dist/shared/geojson";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import fs from "fs";
import path from "path";

@injectable()
export class GeoConfigHelper {
    private readonly RELATIVE_PATH_GEOJSON = "../../../static-data/geojson/";
    private fileName = "parkoviste_orez_okresy_max.geojson";
    private geoCordinates: Geometry;

    constructor() {
        this.geoCordinates = this.loadGeoJson();
    }

    public returnGeoCoordinates() {
        return this.geoCordinates;
    }

    private loadGeoJson() {
        const pathToGeoJson = path.join(__dirname, this.RELATIVE_PATH_GEOJSON, this.fileName);
        const regionOfInterest = JSON.parse(fs.readFileSync(pathToGeoJson).toString()).features[0].geometry;

        return regionOfInterest as Geometry;
    }
}

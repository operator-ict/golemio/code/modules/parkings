import { IGetMeasurementsQuery } from "#og/routers/v2/V2ParkingRouter";

export default class PaginationHelper {
    public static generatePagingUrl(filterParams: IGetMeasurementsQuery, currentCount: number, offset: number, limit: number) {
        let pagingLinksDefinition = "";
        let requestUrl = "/parking/measurements?";
        requestUrl += `limit=${limit}`;
        if (filterParams.from) requestUrl += `&from=${filterParams.from}`;
        if (filterParams.to) requestUrl += `&to=${filterParams.to}`;
        if (filterParams.source) requestUrl += `&source=${filterParams.source}`;
        if (filterParams.sourceId) requestUrl += `&sourceId=${filterParams.sourceId}`;
        if (offset > 1) {
            const prevOffset = offset - limit < 0 ? 0 : offset - limit;

            pagingLinksDefinition += `<${requestUrl}&offset=${prevOffset}>; rel=previous`;
        }
        if (currentCount === limit) {
            if (pagingLinksDefinition) pagingLinksDefinition += ", ";
            pagingLinksDefinition += `<${requestUrl}&offset=${offset + limit}>; rel=next`;
        }

        return pagingLinksDefinition;
    }
}

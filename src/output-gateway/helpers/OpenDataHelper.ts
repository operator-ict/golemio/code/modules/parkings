import { log } from "@golemio/core/dist/output-gateway/Logger";
import { Request } from "@golemio/core/dist/shared/express";

export default class OpenDataHelper {
    public static parseOpenDataParam(req: Request): boolean {
        if (req.query["isRestrictedToOpenData"] === "false") {
            log.debug("Showing also private data");
            return false;
        }
        log.debug("Showing only public data");
        return true;
    }

    public static getIntersectionOfAllowedSources(requestedSources: string[] | undefined, accessibleSources: string[]) {
        let intersection: string[];
        if (requestedSources) {
            intersection = requestedSources.filter((item) => accessibleSources.includes(item));
        } else {
            intersection = accessibleSources;
        }
        return intersection;
    }
}

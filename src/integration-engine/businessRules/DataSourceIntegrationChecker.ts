import { SourceEnum } from "#helpers/constants/SourceEnum";
import { ValidationError } from "@golemio/core/dist/shared/golemio-errors";
import { IParkingSource } from "#sch/models/interfaces/IParkingSource";

export class DataSourceIntegrationChecker {
    private readonly disallowedDataSources = new Map<string, string[]>([
        [SourceEnum.IPR, ["parking", "locations"]],
        [SourceEnum.Korid, ["parking", "locations", "tariffs"]],
        [SourceEnum.Mr_Parkit, ["parking", "locations", "prohibitions", "tariffs"]],
        [SourceEnum.OSM, ["parking", "locations"]],
        [SourceEnum.Smart4City, ["parking", "locations"]],
        [SourceEnum.TSK, ["parking", "locations", "tariffs"]],
        [SourceEnum.TSK_V2, ["parking", "locations", "tariffs"]],
        [SourceEnum.Manual, ["tariffs"]],
    ]);

    public checkAllowedDataSources(inputData: IParkingSource): void {
        const sourceName = inputData.source;

        const isDataSourceNull = (disallowed: string) => inputData[`datasource_${disallowed}` as keyof IParkingSource] == null;
        const disallowedFields = this.disallowedDataSources.get(sourceName);
        if (disallowedFields && !disallowedFields.every(isDataSourceNull)) {
            throw new ValidationError(`Disallowed datasource for ${sourceName}!`, "DatasourceIntegrationChecker");
        }
    }
}

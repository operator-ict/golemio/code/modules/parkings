import { TskCalculationHelper } from "#ie/helpers/TskCalculationHelper";
import { ITariffInfo } from "../interfaces/ITariffInfo";
import { IRule } from "./IRule";

export class MaxChargeIterationsRule implements IRule<ITariffInfo> {
    public isValid(tariffInfo: ITariffInfo) {
        if (
            tariffInfo.maxParkingTime === undefined ||
            tariffInfo.pricePerHour === undefined ||
            tariffInfo.minPrice === undefined
        ) {
            return false;
        }

        const maxChargeIterations = TskCalculationHelper.getMaxChargeIterations(
            tariffInfo.maxParkingTime,
            tariffInfo.minPrice,
            tariffInfo.pricePerHour
        );

        return Math.round(maxChargeIterations) === maxChargeIterations;
    }
}

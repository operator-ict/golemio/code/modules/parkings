export enum RuleEnum {
    MaxPriceRule = "MaxPriceRule",
    MaxChargeIterationsRule = "MaxChargeIterationsRule",
    ZeroMaxParkingTime = "ZeroMaxParkingTime",
}

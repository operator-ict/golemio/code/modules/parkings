import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IRule } from "./IRule";
import { MaxChargeIterationsRule } from "./MaxChargeInteractionsRule";
import { MaxPriceRule } from "./MaxPriceRule";
import { RuleEnum } from "./RuleEnum";
import { ZeroMaxParkingTime } from "./ZeroMaxParkingTime";

export default class RuleFactory {
    private instances: Map<RuleEnum, IRule<any>>;

    constructor() {
        this.instances = new Map();
        this.instances.set(RuleEnum.MaxChargeIterationsRule, new MaxChargeIterationsRule());
        this.instances.set(RuleEnum.MaxPriceRule, new MaxPriceRule());
        this.instances.set(RuleEnum.ZeroMaxParkingTime, new ZeroMaxParkingTime());
    }

    public get(ruleType: RuleEnum) {
        if (!this.instances.has(ruleType)) {
            throw new GeneralError(`Unable to find rule: '${ruleType.toString()}'.`, RuleFactory.name);
        }

        return this.instances.get(ruleType)!;
    }
}

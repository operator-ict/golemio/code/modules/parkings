export interface IRule<T> {
    isValid(params: T): boolean;
}

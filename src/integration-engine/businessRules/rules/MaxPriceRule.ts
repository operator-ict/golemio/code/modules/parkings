import { ITariffInfo } from "../interfaces/ITariffInfo";
import { IRule } from "./IRule";

export class MaxPriceRule implements IRule<ITariffInfo> {
    public isValid(tariffInfo: ITariffInfo) {
        if (
            tariffInfo.pricePerHour === undefined ||
            tariffInfo.maxParkingTime === undefined ||
            tariffInfo.maxPrice === undefined
        ) {
            return false;
        }

        return !(tariffInfo.pricePerHour * tariffInfo.maxParkingTime < tariffInfo.maxPrice);
    }
}

import { ITariffInfo } from "../interfaces/ITariffInfo";
import { IRule } from "./IRule";

export class ZeroMaxParkingTime implements IRule<ITariffInfo> {
    public isValid(tariffInfo: ITariffInfo) {
        return tariffInfo.maxParkingTime !== 0;
    }
}

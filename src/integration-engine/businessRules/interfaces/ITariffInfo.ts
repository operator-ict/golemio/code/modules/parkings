export interface ITariffInfo {
    cTariff?: number;
    pricePerHour?: number;
    maxParkingTime?: number;
    maxPrice?: number;
    minPrice?: number;
}

import { RuleEnum } from "../rules/RuleEnum";

export interface ITariffCheckerResult<T> {
    isValid: boolean;
    failedRule?: RuleEnum;
    failedInput?: T;
}

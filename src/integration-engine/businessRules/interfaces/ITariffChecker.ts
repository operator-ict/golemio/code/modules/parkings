import { ITariffCheckerResult } from "./ITariffCheckerResult";

export interface ITariffChecker {
    isTariffValid(tariffInfo: any): ITariffCheckerResult<any>;
}

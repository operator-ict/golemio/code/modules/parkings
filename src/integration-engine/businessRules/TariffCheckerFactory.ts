import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { TskTariffChecker } from "./TskTariffChecker";

export enum TariffCheckerEnum {
    TSK = "tsk",
}

export default class TariffCheckerFactory {
    constructor() {}

    public static get(checkerType: TariffCheckerEnum) {
        switch (checkerType) {
            case TariffCheckerEnum.TSK:
                return new TskTariffChecker();
            default:
                throw new GeneralError(`Unexpected type: '${checkerType}' in a TariffCheckerFactory.`, TariffCheckerFactory.name);
        }
    }
}

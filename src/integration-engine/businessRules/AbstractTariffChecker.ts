import { ITariffChecker } from "./interfaces/ITariffChecker";
import { ITariffCheckerResult } from "./interfaces/ITariffCheckerResult";
import { RuleEnum } from "./rules/RuleEnum";
import RuleFactory from "./rules/RuleFactory";

export abstract class AbstractTariffChecker<T> implements ITariffChecker {
    protected abstract ruleList: RuleEnum[];
    protected ruleFactory: RuleFactory;

    constructor() {
        this.ruleFactory = new RuleFactory();
    }

    public isTariffValid(tariffInfo: T): ITariffCheckerResult<T> {
        for (const ruleType of this.ruleList) {
            if (!this.ruleFactory.get(ruleType).isValid(tariffInfo)) {
                return {
                    isValid: false,
                    failedRule: ruleType,
                    failedInput: tariffInfo,
                };
            }
        }

        return { isValid: true };
    }
}

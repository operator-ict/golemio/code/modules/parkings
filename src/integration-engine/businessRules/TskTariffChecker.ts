import { AbstractTariffChecker } from "./AbstractTariffChecker";
import { ITariffInfo } from "./interfaces/ITariffInfo";
import { RuleEnum } from "./rules/RuleEnum";

export class TskTariffChecker extends AbstractTariffChecker<ITariffInfo> {
    protected ruleList = [RuleEnum.ZeroMaxParkingTime, RuleEnum.MaxChargeIterationsRule, RuleEnum.MaxPriceRule];
}

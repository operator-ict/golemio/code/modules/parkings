import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { CachedPmdpParkingRepository } from "#helpers/data-access/CachedPmdpParkingRepository";
import { OpeningHoursParser } from "#helpers/osm/OpeningHoursParser";
import { TskTariffChecker } from "#ie/businessRules/TskTariffChecker";
import { ParkingProviderDataSourceFactory } from "#ie/datasources/ParkingProviderDataSourceFactory";
import { TskParkingDataSourceFactory } from "#ie/datasources/TskParkingDataSourceFactory";
import { BedrichovDataSource } from "#ie/datasources/bedrichov/BedrichovDataSource";
import { IptOictDataSource } from "#ie/datasources/iptoict/IptOictDataSource";
import { IptOictEntrancesDataSource } from "#ie/datasources/iptoict/IptOictEntrancesDataSource";
import { IptOictParkingsProhibitionsDataSource } from "#ie/datasources/iptoict/IptOictParkingsProhibitionsDataSource";
import { IptOictPaymentsDataSource } from "#ie/datasources/iptoict/IptOictPaymentsDataSource";
import { IptOictTariffsDataSource } from "#ie/datasources/iptoict/IptOictTariffsDataSource";
import { MrParkitDataSourceProvider } from "#ie/datasources/mr-parkit/MrParkitDataSourceProvider";
import { OsmDataSource } from "#ie/datasources/osm/OsmDataSource";
import { OsmEntrancesDataSource } from "#ie/datasources/osm/OsmEntrancesDataSource";
import { OsmParkingMachinesDataSource } from "#ie/datasources/osm/OsmParkingMachinesDataSource";
import { ParkomatsDataSource } from "#ie/datasources/parkomats/ParkomatsDataSource";
import { PmdpMeasurementsDataSourceFactory } from "#ie/datasources/pmdp/PmdpMeasurementsDataSource";
import { Smart4CityListDataSourceProvider } from "#ie/datasources/smart4city/Smart4CityListDataSourceProvider";
import { Smart4CityLocationDataSourceProvider } from "#ie/datasources/smart4city/Smart4CityLocationDataSourceProvider";
import { SmartCityListDatasourceCache } from "#ie/datasources/smart4city/SmartCityListDatasourceCache";
import { AverageOccupancyRepository } from "#ie/repositories/AverageOccupancyRepository";
import { OpenHoursRepository } from "#ie/repositories/OpenHoursRepository";
import { ParkingEntrancesRepository } from "#ie/repositories/ParkingEntrancesRepository";
import { ParkingMachinesRepository } from "#ie/repositories/ParkingMachinesRepository";
import { ParkingPaymentsRepository } from "#ie/repositories/ParkingPaymentsRepository";
import { ParkingTariffsRepository } from "#ie/repositories/ParkingTariffsRepository";
import { ParkingsBusinessErrorsRepository } from "#ie/repositories/ParkingsBusinessErrorsRepository";
import { ParkingsLocationRepository } from "#ie/repositories/ParkingsLocationRepository";
import { ParkingsMeasurementRepository } from "#ie/repositories/ParkingsMeasurementRepository";
import { ParkingsMeasurementsActualRepository } from "#ie/repositories/ParkingsMeasurementsActualRepository";
import { ParkingsProhibitionsRepository } from "#ie/repositories/ParkingsProhibitionsRepository";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { ParkingsSecondaryRepository } from "#ie/repositories/ParkingsSecondaryRepository";
import { ParkomatsRepository } from "#ie/repositories/ParkomatsRepository";
import { BedrichovMeasurementsTransformation } from "#ie/transformations/BedrichovMeasurementsTransformation";
import { ParkomatsTransformation } from "#ie/transformations/ParkomatsTransformation";
import { TskParkingMachinesTransformation } from "#ie/transformations/TskParkingMachinesTransformation";
import { OsmOpeningHoursTransformation } from "#ie/transformations/osm/OsmOpeningHoursTransformation";
import { CalculateAverageOccupancyTask } from "#ie/workers/tasks/CalculateAverageOccupancyTask";
import { GenerateIptOictEntrancesJobsTask } from "#ie/workers/tasks/GenerateIptOictEntrancesJobsTask";
import { GenerateIptOictParkingJobsTask } from "#ie/workers/tasks/GenerateIptOictParkingJobsTask";
import { GenerateIptOictPaymentJobsTask } from "#ie/workers/tasks/GenerateIptOictPaymentJobsTask";
import { GenerateIptOictProhibitionsJobsTask } from "#ie/workers/tasks/GenerateIptOictProhibitionsJobsTask";
import { GenerateIptOictTariffsJobsTask } from "#ie/workers/tasks/GenerateIptOictTariffsJobsTask";
import { GenerateSmart4CityTaskListTask } from "#ie/workers/tasks/GenerateSmart4CityTaskListTask";
import { RefreshDataInDbTask } from "#ie/workers/tasks/RefreshDataInDbTask";
import { SaveBedrichovDataTask } from "#ie/workers/tasks/SaveBedrichovDataTask";
import { SaveIptOictEntrancesDataTask } from "#ie/workers/tasks/SaveIptOictEntrancesDataTask";
import { SaveIptOictParkingDataTask } from "#ie/workers/tasks/SaveIptOictParkingDataTask";
import { SaveIptOictPaymentsDataTask } from "#ie/workers/tasks/SaveIptOictPaymentsDataTask";
import { SaveIptOictProhibitionsDataTask } from "#ie/workers/tasks/SaveIptOictProhibitionsDataTask";
import { SaveIptOictTariffsDataTask } from "#ie/workers/tasks/SaveIptOictTariffsDataTask";
import { SaveIsphkMeasurementsTask } from "#ie/workers/tasks/SaveIsphkMeasurementsTask";
import { SaveMrParkitDataTask } from "#ie/workers/tasks/SaveMrParkitDataTask";
import { SaveOsmDataTask } from "#ie/workers/tasks/SaveOsmDataTask";
import { SaveOsmEntrancesDataTask } from "#ie/workers/tasks/SaveOsmEntrancesDataTask";
import { SaveOsmParkingMachinesTask } from "#ie/workers/tasks/SaveOsmParkingMachinesTask";
import { SavePmdpMeasurementsTask } from "#ie/workers/tasks/SavePmdpMeasurementsTask";
import { UpdateAddressTask } from "#ie/workers/tasks/UpdateAddressTask";
import { UpdateAllParkingAdresses } from "#ie/workers/tasks/UpdateAllParkingAdresses";
import { UpdateSmart4CityLocationsTask } from "#ie/workers/tasks/UpdateSmart4CityLocationsTask";
import { UpdateSmart4CityMeasurementsTask } from "#ie/workers/tasks/UpdateSmart4CityMeasurementsTask";
import { ITskParkingMachine } from "#sch/datasources/interfaces/ITskParkingMachine";
import { ITskParkingSection } from "#sch/datasources/interfaces/ITskParkingSection";
import { ITskParkingSectionLevel } from "#sch/datasources/interfaces/ITskParkingSectionLevel";
import { ITskParkingZoneTariff } from "#sch/datasources/interfaces/ITskParkingZoneTariff";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IConfiguration } from "@golemio/core/dist/integration-engine/config";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer, instanceCachingFactory } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainerToken";
import { UpdateAllParkingsLocationAdresses } from "#ie/workers/tasks/UpdateAllParkingsLocationAdresses";
import { UpdateLocationAddressTask } from "#ie/workers/tasks/UpdateLocationAddressTask";
import { UpdateAddressAndNameTask } from "#ie/workers/tasks/UpdateAddressAndNameTask";
import { UpdateMissingParkingsAddressesTask } from "#ie/workers/tasks/UpdateMissingParkingsAddressesTask";
import { UpdateMissingParkingsLocationAddressesTask } from "#ie/workers/tasks/UpdateMissingParkingsLocationAddressesTask";

//#region Initialization
const parkingsContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
const config = IntegrationEngineContainer.resolve<IConfiguration>(ContainerToken.Config);
const parkingDataSourceFactory = new TskParkingDataSourceFactory(config);
//#endregion

//#region Datasource
parkingsContainer.register<DataSource<ITskParkingSectionLevel>>(ModuleContainerToken.TskParkingSectionLevel251DataSource, {
    useFactory: instanceCachingFactory<DataSource<ITskParkingSectionLevel>>(() =>
        parkingDataSourceFactory.getDataSource("parkingSectionLevel251")
    ),
});
parkingsContainer.register<DataSource<ITskParkingSectionLevel>>(ModuleContainerToken.TskParkingSectionLevel253DataSource, {
    useFactory: instanceCachingFactory<DataSource<ITskParkingSectionLevel>>(() =>
        parkingDataSourceFactory.getDataSource("parkingSectionLevel253")
    ),
});
parkingsContainer.register<DataSource<ITskParkingSection>>(ModuleContainerToken.TskParkingSectionsDataSource, {
    useFactory: instanceCachingFactory<DataSource<ITskParkingSection>>(() =>
        parkingDataSourceFactory.getDataSource("parkingSections")
    ),
});
parkingsContainer.register<DataSource<ITskParkingMachine>>(ModuleContainerToken.TskParkingMachinesDataSource, {
    useFactory: instanceCachingFactory<DataSource<ITskParkingMachine>>(() =>
        parkingDataSourceFactory.getDataSource("parkingMachines")
    ),
});
parkingsContainer.register<DataSource<ITskParkingZoneTariff[]>>(ModuleContainerToken.TskParkingZonesTariffsDataSource, {
    useFactory: instanceCachingFactory<DataSource<ITskParkingZoneTariff[]>>(() =>
        parkingDataSourceFactory.getDataSource("parkingTariffs")
    ),
});
parkingsContainer.register(ModuleContainerToken.MrParkitDataSourceProvider, MrParkitDataSourceProvider);
parkingsContainer.register(ModuleContainerToken.Smart4CityListDataSourceProvider, Smart4CityListDataSourceProvider);
parkingsContainer.register(ModuleContainerToken.Smart4CityLocationDataSourceProvider, Smart4CityLocationDataSourceProvider);
parkingsContainer.register(ModuleContainerToken.SmartCityListDatasourceCache, SmartCityListDatasourceCache);
parkingsContainer.registerSingleton(ModuleContainerToken.ParkingProviderDataSourceFactory, ParkingProviderDataSourceFactory);
parkingsContainer.registerSingleton(ModuleContainerToken.OsmDataSource, OsmDataSource);
parkingsContainer.registerSingleton(ModuleContainerToken.OsmEntrancesDataSource, OsmEntrancesDataSource);
parkingsContainer.registerSingleton(ModuleContainerToken.IptOictDataSource, IptOictDataSource);
parkingsContainer.registerSingleton(ModuleContainerToken.ParkomatsDataSource, ParkomatsDataSource);
parkingsContainer.registerSingleton(ModuleContainerToken.IptOictPaymentsDataSource, IptOictPaymentsDataSource);
parkingsContainer.registerSingleton(ModuleContainerToken.PmdpMeasurementsDataSourceFactory, PmdpMeasurementsDataSourceFactory);
parkingsContainer.registerSingleton(
    ModuleContainerToken.IptOictParkingsProhibitionsDataSource,
    IptOictParkingsProhibitionsDataSource
);
parkingsContainer.registerSingleton(ModuleContainerToken.OsmParkingMachinesDataSource, OsmParkingMachinesDataSource);
parkingsContainer.registerSingleton(ModuleContainerToken.IptOictEntrancesDataSource, IptOictEntrancesDataSource);
parkingsContainer.registerSingleton(ModuleContainerToken.IptOictTariffsDataSource, IptOictTariffsDataSource);
parkingsContainer.registerSingleton(ModuleContainerToken.ParkomatsDataSource, ParkomatsDataSource);
parkingsContainer.registerSingleton(ModuleContainerToken.BedrichovDataSource, BedrichovDataSource);
//#endregion

//#region Helpers
parkingsContainer
    .register(ModuleContainerToken.TskTariffChecker, TskTariffChecker)
    .register(ModuleContainerToken.OsmOpeningHoursParser, OpeningHoursParser);
//#endregion

//#region Transformation
parkingsContainer
    .register(ModuleContainerToken.TskParkingMachinesTransformation, TskParkingMachinesTransformation)
    .register(ModuleContainerToken.OsmOpeningHoursTransformation, OsmOpeningHoursTransformation)
    .register(ModuleContainerToken.BedrichovMeasurementsTransformation, BedrichovMeasurementsTransformation)
    .register(ModuleContainerToken.ParkomatsTransformation, ParkomatsTransformation);
//#endregion

//#region Repositories
parkingsContainer
    .register(ModuleContainerToken.CachedParkingSourcesRepository, {
        useFactory: (c) =>
            new CachedParkingSourcesRepository(c.resolve(CoreToken.PostgresConnector), c.resolve(CoreToken.Logger)),
    })
    .register(ModuleContainerToken.CachedPmdpParkingRepository, {
        useFactory: (c) => new CachedPmdpParkingRepository(c.resolve(CoreToken.PostgresConnector), c.resolve(CoreToken.Logger)),
    })
    .register(ModuleContainerToken.ParkingsRepository, ParkingsRepository)
    .register(ModuleContainerToken.ParkingsSecondaryRepository, ParkingsSecondaryRepository)
    .register(ModuleContainerToken.ParkingsLocationRepository, ParkingsLocationRepository)
    .register(ModuleContainerToken.ParkingTariffsRepository, ParkingTariffsRepository)
    .register(ModuleContainerToken.ParkingsBusinessErrorsRepository, ParkingsBusinessErrorsRepository)
    .register(ModuleContainerToken.ParkingMachinesRepository, ParkingMachinesRepository)
    .register(ModuleContainerToken.AverageOccupancyRepository, AverageOccupancyRepository)
    .register(ModuleContainerToken.ParkingEntrancesRepository, ParkingEntrancesRepository)
    .register(ModuleContainerToken.ParkingPaymentsRepository, ParkingPaymentsRepository)
    .register(ModuleContainerToken.ParkingsMeasurementRepository, ParkingsMeasurementRepository)
    .register(ModuleContainerToken.ParkingsMeasurementsActualRepository, ParkingsMeasurementsActualRepository)
    .register(ModuleContainerToken.ParkingsProhibitionsRepository, ParkingsProhibitionsRepository)
    .register(ModuleContainerToken.OpenHoursRepository, OpenHoursRepository);
parkingsContainer.register(ModuleContainerToken.ParkomatsRepository, ParkomatsRepository);

//#endregion

//#region Tasks
parkingsContainer.registerSingleton(ModuleContainerToken.SaveMrParkitDataTask, SaveMrParkitDataTask);
parkingsContainer.registerSingleton(ModuleContainerToken.GenerateSmart4CityTaskListTask, GenerateSmart4CityTaskListTask);
parkingsContainer.registerSingleton(ModuleContainerToken.UpdateSmart4CityLocationsTask, UpdateSmart4CityLocationsTask);
parkingsContainer.registerSingleton(ModuleContainerToken.UpdateSmart4CityMeasurementsTask, UpdateSmart4CityMeasurementsTask);
parkingsContainer.registerSingleton(ModuleContainerToken.SaveOsmDataTask, SaveOsmDataTask);
parkingsContainer.registerSingleton(ModuleContainerToken.GenerateIptOictParkingJobsTask, GenerateIptOictParkingJobsTask);
parkingsContainer.registerSingleton(ModuleContainerToken.SaveIptOictParkingDataTask, SaveIptOictParkingDataTask);
parkingsContainer.registerSingleton(ModuleContainerToken.GenerateIptOictPaymentJobsTask, GenerateIptOictPaymentJobsTask);
parkingsContainer.registerSingleton(ModuleContainerToken.SaveIptOictPaymentsDataTask, SaveIptOictPaymentsDataTask);
parkingsContainer.registerSingleton(ModuleContainerToken.GenerateIptOictTariffsJobsTask, GenerateIptOictTariffsJobsTask);
parkingsContainer.registerSingleton(ModuleContainerToken.SaveIptOictTariffsDataTask, SaveIptOictTariffsDataTask);
parkingsContainer.registerSingleton(ModuleContainerToken.SaveIsphkMeasurementsTask, SaveIsphkMeasurementsTask);
parkingsContainer.registerSingleton(ModuleContainerToken.CalculateAverageOccupancyTask, CalculateAverageOccupancyTask);
parkingsContainer.registerSingleton(ModuleContainerToken.SaveOsmEntrancesDataTask, SaveOsmEntrancesDataTask);
parkingsContainer.registerSingleton(ModuleContainerToken.SaveIptOictProhibitionsDataTask, SaveIptOictProhibitionsDataTask);
parkingsContainer.registerSingleton(ModuleContainerToken.RefreshDataInDbTask, RefreshDataInDbTask);
parkingsContainer.registerSingleton(ModuleContainerToken.SaveOsmParkingMachinesTask, SaveOsmParkingMachinesTask);
parkingsContainer.registerSingleton(
    ModuleContainerToken.GenerateIptOictProhibitionsJobsTask,
    GenerateIptOictProhibitionsJobsTask
);
parkingsContainer.registerSingleton(ModuleContainerToken.GenerateIptOictEntrancesJobsTask, GenerateIptOictEntrancesJobsTask);
parkingsContainer.registerSingleton(ModuleContainerToken.SaveIptOictEntrancesDataTask, SaveIptOictEntrancesDataTask);
parkingsContainer.registerSingleton(ModuleContainerToken.RefreshDataInDbTask, RefreshDataInDbTask);
parkingsContainer.registerSingleton(ModuleContainerToken.SavePmdpMeasurementsTask, SavePmdpMeasurementsTask);
parkingsContainer.registerSingleton(ModuleContainerToken.SaveBedrichovDataTask, SaveBedrichovDataTask);
parkingsContainer.registerSingleton(ModuleContainerToken.UpdateAllParkingAdresses, UpdateAllParkingAdresses);
parkingsContainer.registerSingleton(ModuleContainerToken.UpdateAllParkingsLocationAdresses, UpdateAllParkingsLocationAdresses);
parkingsContainer.registerSingleton(ModuleContainerToken.UpdateAddressTask, UpdateAddressTask);
parkingsContainer.registerSingleton(ModuleContainerToken.UpdateLocationAddressTask, UpdateLocationAddressTask);
parkingsContainer.registerSingleton(ModuleContainerToken.UpdateAddressAndNameTask, UpdateAddressAndNameTask);
parkingsContainer.registerSingleton(ModuleContainerToken.UpdateMissingParkingAdressesTask, UpdateMissingParkingsAddressesTask);
parkingsContainer.registerSingleton(
    ModuleContainerToken.UpdateMissingParkingsLocationAdressesTask,
    UpdateMissingParkingsLocationAddressesTask
);
//#endregion

export { parkingsContainer as ParkingsContainer };

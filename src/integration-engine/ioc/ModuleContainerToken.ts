const ModuleContainerToken = {
    TskTariffChecker: Symbol(),
    CachedParkingSourcesRepository: Symbol(),
    ParkingsRepository: Symbol(),
    ParkingsSecondaryRepository: Symbol(),
    ParkingsLocationRepository: Symbol(),
    ParkingTariffsRepository: Symbol(),
    ParkingsBusinessErrorsRepository: Symbol(),
    ParkingMachinesRepository: Symbol(),
    AverageOccupancyRepository: Symbol(),
    ParkingsMeasurementRepository: Symbol(),
    ParkingsMeasurementsActualRepository: Symbol(),
    ParkingPaymentsRepository: Symbol(),
    OpenHoursRepository: Symbol(),

    TskParkingMachinesDataSource: Symbol(),
    TskParkingMachinesTransformation: Symbol(),
    TskParkingSectionLevel251DataSource: Symbol(),
    TskParkingSectionLevel253DataSource: Symbol(),
    TskParkingSectionsDataSource: Symbol(),
    TskParkingSectionTransformation: Symbol(),

    TskParkingZonesTariffsDataSource: Symbol("TskParkingZonesTariffsDataSource"),
    ParkingProviderDataSourceFactory: Symbol(),

    //#region MR.PARKIT
    MrParkitDataSourceProvider: Symbol(),
    SaveMrParkitDataTask: Symbol(),
    //#endregion

    //#region Isp HK
    SaveIsphkMeasurementsTask: Symbol(),
    //#endregion

    //#region PMDP
    PmdpMeasurementsDataSourceFactory: Symbol(),
    CachedPmdpParkingRepository: Symbol(),
    SavePmdpMeasurementsTask: Symbol(),
    //#endregion

    //#region Smart4City
    Smart4CityListDataSourceProvider: Symbol(),
    Smart4CityLocationDataSourceProvider: Symbol(),
    SmartCityListDatasourceCache: Symbol(),
    GenerateSmart4CityTaskListTask: Symbol(),
    UpdateSmart4CityLocationsTask: Symbol(),
    UpdateSmart4CityMeasurementsTask: Symbol(),
    //#endregion

    //#region OSM
    OsmDataSource: Symbol(),
    SaveOsmDataTask: Symbol(),
    OsmEntrancesDataSource: Symbol(),
    SaveOsmEntrancesDataTask: Symbol(),
    OsmParkingMachinesDataSource: Symbol(),
    SaveOsmParkingMachinesTask: Symbol(),
    //#endregion

    //#region Entrances
    ParkingEntrancesRepository: Symbol(),
    OsmOpeningHoursParser: Symbol(),
    OsmOpeningHoursTransformation: Symbol(),
    //#endregion

    //#region Bedrichov
    BedrichovMeasurementsTransformation: Symbol(),
    BedrichovDataSource: Symbol(),
    SaveBedrichovDataTask: Symbol(),
    //#endregion

    //#region IPT OICT
    /* locations */
    IptOictDataSource: Symbol(),
    GenerateIptOictParkingJobsTask: Symbol(),
    SaveIptOictParkingDataTask: Symbol(),

    /* payments */
    IptOictPaymentsDataSource: Symbol(),
    GenerateIptOictPaymentJobsTask: Symbol(),
    SaveIptOictPaymentsDataTask: Symbol(),

    /* Entrances */
    IptOictEntrancesDataSource: Symbol(),
    GenerateIptOictEntrancesJobsTask: Symbol(),
    SaveIptOictEntrancesDataTask: Symbol(),

    /* Tariffs */
    GenerateIptOictTariffsJobsTask: Symbol(),
    SaveIptOictTariffsDataTask: Symbol(),
    IptOictTariffsDataSource: Symbol(),
    //#endregion

    /* prohibitions */
    IptOictParkingsProhibitionsDataSource: Symbol(),
    IptOictParkingProhibitionsTransformation: Symbol(),
    GenerateIptOictProhibitionsJobsTask: Symbol(),
    SaveIptOictProhibitionsDataTask: Symbol(),
    ParkingsProhibitionsRepository: Symbol(),
    //#endregion

    //#region Average occupancy
    CalculateAverageOccupancyTask: Symbol(),
    //#endregion

    //#region Photon
    UpdateAllParkingAdresses: Symbol(),
    UpdateAllParkingsLocationAdresses: Symbol(),
    UpdateAddressTask: Symbol(),
    UpdateLocationAddressTask: Symbol(),
    UpdateAddressAndNameTask: Symbol(),
    UpdateMissingParkingAdressesTask: Symbol(),
    UpdateMissingParkingsLocationAdressesTask: Symbol(),
    //#endregion

    //#region parkomats
    ParkomatsTransformation: Symbol(),
    ParkomatsRepository: Symbol(),
    ParkomatsDataSource: Symbol(),
    RefreshDataInDbTask: Symbol(),

    //#endregion
};
export { ModuleContainerToken };

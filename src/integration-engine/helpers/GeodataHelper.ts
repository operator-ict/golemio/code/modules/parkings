import * as turf from "@turf/turf";
import { IIPRParkingElementTransformed } from "#ie/transformations/IPRParkingTransformation";
import {
    GeoCoordinatesType,
    IGeoCoordinatesMultiPolygon,
    IGeoCoordinatesPoint,
    IGeoCoordinatesPolygon,
    TGeoCoordinates,
} from "@golemio/core/dist/output-gateway/Geo";

export const mergeGeometries = (data: IIPRParkingElementTransformed[]): IGeoCoordinatesMultiPolygon | IGeoCoordinatesPolygon => {
    let result;

    for (const element of data) {
        if (result) {
            if (result.type === GeoCoordinatesType.Polygon) {
                result = {
                    type: GeoCoordinatesType.MultiPolygon,
                    coordinates: [result.coordinates as number[][][]],
                };
            }

            if (element.location.type === GeoCoordinatesType.Polygon) {
                (result.coordinates as number[][][][]).push(element.location.coordinates as number[][][]);
            } else {
                (result.coordinates as number[][][][]).push(...(element.location.coordinates as number[][][][]));
            }
        } else {
            result = element.location;
        }
    }

    return result as IGeoCoordinatesMultiPolygon | IGeoCoordinatesPolygon;
};

export const getPointOnFeature = (location: TGeoCoordinates): IGeoCoordinatesPoint => {
    if (location.type === "Point") {
        return location;
    }

    return turf.pointOnFeature(location).geometry as IGeoCoordinatesPoint;
};

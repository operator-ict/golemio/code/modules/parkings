import { IParking, IParkingLocation } from "#ie/ParkingInterface";
import { IIPRParkingElementTransformed, IIPRParkingTransformed } from "#ie/transformations/IPRParkingTransformation";
import getUuidByString from "uuid-by-string";
import { getPointOnFeature, mergeGeometries } from "#ie/helpers/GeodataHelper";

const IPR_DATA_PROVIDER = "/opendata.iprpraha.cz";

const getLocationData = async (
    dataTsk: IParking,
    dataIprTransformed: IIPRParkingElementTransformed[] | undefined
): Promise<IParkingLocation[]> => {
    const locationBaseData: Omit<IParkingLocation, "id"> = {
        data_provider: dataTsk.data_provider,
        location: dataTsk.location,
        centroid: dataTsk.centroid,
        total_spot_number: dataTsk.total_spot_number,
        source: dataTsk.source,
        source_id: dataTsk.source_id,
    };

    if (!dataIprTransformed || dataIprTransformed.length === 1) {
        return [
            {
                ...locationBaseData,
                id: dataIprTransformed
                    ? dataIprTransformed[0].id
                    : getUuidByString(
                          JSON.stringify({ source_id: dataTsk.source_id, source: `${dataTsk.source}`.toUpperCase() })
                      ),
                total_spot_number: dataIprTransformed ? dataIprTransformed[0].total_spot_number : dataTsk.total_spot_number,
            },
        ];
    }

    return dataIprTransformed.map((el) => {
        const centroid = getPointOnFeature(el.location);

        return {
            id: el.id,
            ...locationBaseData,
            location: el.location,
            total_spot_number: el.total_spot_number,
            centroid,
        };
    });
};

export const mergeParkingZoneDataSets = async (
    dataTsk: IParking[],
    dataIpr: IIPRParkingTransformed
): Promise<{ parking: IParking[]; locations: IParkingLocation[] }> => {
    const result = {
        parking: [] as IParking[],
        locations: [] as IParkingLocation[],
    };

    for (const item of dataTsk) {
        const parkingData = { ...item };

        if (dataIpr[item.source_id]) {
            parkingData.location = mergeGeometries(dataIpr[item.source_id]);
            parkingData.data_provider += IPR_DATA_PROVIDER;
        }

        parkingData.centroid = getPointOnFeature(parkingData.location);

        const locationData = await getLocationData(parkingData, dataIpr[item.source_id]);

        result.parking.push(parkingData);
        result.locations.push(...locationData);
    }
    return result;
};

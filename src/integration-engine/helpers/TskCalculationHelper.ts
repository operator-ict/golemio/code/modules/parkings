export class TskCalculationHelper {
    public static getMinPrice(minPrice: number, pricePerHour: number) {
        return minPrice === 0 ? pricePerHour : minPrice;
    }

    public static getMaxChargeIterations(maxParkingTime: number, minPrice: number, pricePerHour: number) {
        if (pricePerHour === 0) {
            return maxParkingTime;
        } else {
            minPrice = this.getMinPrice(minPrice, pricePerHour);

            return (maxParkingTime * pricePerHour) / minPrice;
        }
    }

    public static getChargeInterval(minPrice: number, pricePerHour: number) {
        if (pricePerHour === 0) {
            return 3600;
        } else {
            minPrice = this.getMinPrice(minPrice, pricePerHour);

            return (3600 / pricePerHour) * minPrice;
        }
    }

    public static getCharge(minPrice: number, pricePerHour: number) {
        if (pricePerHour === 0) {
            return 0;
        } else {
            return this.getMinPrice(minPrice, pricePerHour);
        }
    }
}

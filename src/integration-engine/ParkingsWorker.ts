import { ParkingTariffsRepository } from "#ie/repositories/ParkingTariffsRepository";
import { ParkingsLocationRepository } from "#ie/repositories/ParkingsLocationRepository";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { ParkingLotsTransformation } from "#ie/transformations/ParkingLotsTransformation";
import { UpdateAddressWorker } from "#ie/workers/UpdateAddressWorker";
import { InputParkingLotsSchema } from "#sch/datasources/InputParkingLotsSchema";
import { ParkingMeasurementsDtoSchema } from "#sch/datasources/ParkingMeasurementsDtoSchema";
import { IStaticParkingLotsGeoFeature } from "#sch/datasources/interfaces/IStaticParkingLotsGeo";
import { Parkings } from "#sch/index";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { IConfiguration } from "@golemio/core/dist/integration-engine/config";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { StaticParkingLotsGeoDataSourceFactory } from "./datasources/StaticParkingLotsGeoDataSourceFactory";
import { ParkingsContainer } from "./ioc/Di";
import { ModuleContainerToken } from "./ioc/ModuleContainerToken";
import { TskAverageOccupancyRepository } from "./repositories/TskAverageOccupancyRepository";
import { KoridParkingConfigTransformation } from "./transformations/KoridParkingConfigTransformation";
import { KoridParkingDataTransformation } from "./transformations/KoridParkingDataTransformation";
import { SourceEnum } from "#helpers/constants/SourceEnum";

export class ParkingsWorker extends BaseWorker {
    private dataSource: DataSource;
    private koridParkingConfigTransformation: KoridParkingConfigTransformation;
    private koridParkingDataTransformation: KoridParkingDataTransformation;
    private parkingsModel: ParkingsRepository;
    private parkingsMeasurementsModel: PostgresModel;
    private parkingsMeasurementsActualModel: PostgresModel;
    private parkingsTariffsModel: ParkingTariffsRepository;
    private tskAverageOccupancyRepository: TskAverageOccupancyRepository;
    private parkingLotsTransformation: ParkingLotsTransformation;
    private config: IConfiguration;
    private logger: ILogger;
    private parkingsLocationRepository: ParkingsLocationRepository;

    constructor() {
        super();
        this.config = ParkingsContainer.resolve<IConfiguration>(ContainerToken.Config);
        this.logger = ParkingsContainer.resolve<ILogger>(ContainerToken.Logger);
        const dataTypeStrategy = new JSONDataTypeStrategy({ resultsPath: "results" });
        // filter items with lastUpdated lower than two days
        dataTypeStrategy.setFilter((item) => item.lastUpdated > new Date().getTime() - 2 * 24 * 60 * 60 * 1000);
        this.dataSource = new DataSource(
            Parkings.name + "DataSource",
            new HTTPFetchProtocolStrategy({
                headers: {},
                method: "GET",
                url: this.config.datasources.TSKParkings,
            }),
            dataTypeStrategy,
            new JSONSchemaValidator(Parkings.name + "DataSource", InputParkingLotsSchema)
        );

        this.parkingLotsTransformation = new ParkingLotsTransformation();
        this.koridParkingConfigTransformation = new KoridParkingConfigTransformation();
        this.koridParkingDataTransformation = new KoridParkingDataTransformation();

        this.parkingsMeasurementsModel = new PostgresModel(
            Parkings.measurements.name + "Model",
            {
                outputSequelizeAttributes: Parkings.measurements.outputSequelizeAttributes,
                pgSchema: Parkings.pgSchema,
                pgTableName: Parkings.measurements.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Parkings.measurements.name + "PgModelValidator", ParkingMeasurementsDtoSchema)
        );

        this.parkingsMeasurementsActualModel = new PostgresModel(
            Parkings.measurementsActual.name + "Model",
            {
                outputSequelizeAttributes: Parkings.measurementsActual.outputSequelizeAttributes,
                pgSchema: Parkings.pgSchema,
                pgTableName: Parkings.measurementsActual.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Parkings.measurementsActual.name + "PgModelValidator", ParkingMeasurementsDtoSchema)
        );

        this.tskAverageOccupancyRepository = new TskAverageOccupancyRepository();

        this.parkingsModel = ParkingsContainer.resolve<ParkingsRepository>(ModuleContainerToken.ParkingsRepository);
        this.parkingsTariffsModel = ParkingsContainer.resolve<ParkingTariffsRepository>(
            ModuleContainerToken.ParkingTariffsRepository
        );
        this.parkingsLocationRepository = new ParkingsLocationRepository();
    }

    /**
     *  Parking lots Prague queue worker method
     *  - store all data for parking lots in Prague.
     */
    public saveParkingLotsPrague = async (): Promise<void> => {
        const data = await this.dataSource.getAll();

        if (data.length === 0) {
            this.logger.debug("No recent data. Nothing to do.");
            return;
        }

        let staticGeoData: IStaticParkingLotsGeoFeature[] | undefined;
        try {
            // "optional" datasource enhancing geolocations
            // objectively the datasource is static data maintained by IPT
            staticGeoData = (
                await StaticParkingLotsGeoDataSourceFactory.getDataSource(
                    this.config.datasources.StaticParkingLotsGeoSourceUrl
                ).getAll()
            ).features;
        } catch (err) {
            this.logger.error(`Error while getting static parking lots geo: ${err.message}`);
        }

        const { geo: geoData } = await this.parkingLotsTransformation.transform(data, staticGeoData);

        await this.parkingsModel.saveActiveParkingsWithoutAddress(geoData, SourceEnum.TSK);

        await QueueManager.sendMessageToExchange(
            this.config.RABBIT_EXCHANGE_NAME + "." + UpdateAddressWorker.workerName.toLowerCase(),
            "updateMissingParkingsAddresses",
            {}
        );
    };

    /**
     *  Parking lots measurements Prague queue worker method
     *  - store all data for parking lots in Prague.
     */
    public saveParkingLotsMeasurementsPrague = async (): Promise<void> => {
        const data = await this.dataSource.getAll();

        if (data.length === 0) {
            this.logger.debug("No recent data. Nothing to do.");
            return;
        }

        const { measurements: measurementsData } = await this.parkingLotsTransformation.transform(data);
        await this.parkingsMeasurementsModel.bulkSave(measurementsData);
        await this.parkingsMeasurementsActualModel.bulkSave(measurementsData);
    };

    /**
     *  Stores locations and tariffs of parking lots in Liberec.
     */
    public saveKoridConfToDB = async (msg: any): Promise<void> => {
        const inputData = JSON.parse(msg.content.toString());
        const transformedData = await this.koridParkingConfigTransformation.transform(inputData);
        await this.parkingsModel.saveActiveParkingsWithoutAddress(transformedData.geo, SourceEnum.Korid);
        await this.parkingsTariffsModel.bulkSave(transformedData.tariff);
        await this.parkingsLocationRepository.saveWithoutAddress(transformedData.location);

        await QueueManager.sendMessageToExchange(
            this.config.RABBIT_EXCHANGE_NAME + "." + UpdateAddressWorker.workerName.toLowerCase(),
            "updateMissingParkingsAddresses",
            {}
        );
    };

    /**
     *  Stores measurements (available spots/occupancies) of parking lots in Liberec.
     */
    public saveKoridDataToDB = async (msg: any): Promise<void> => {
        const inputData = JSON.parse(msg.content.toString());
        const transformedData = await this.koridParkingDataTransformation.transform(inputData);
        await this.parkingsMeasurementsModel.bulkSave(transformedData);
        await this.parkingsMeasurementsActualModel.bulkSave(transformedData);
    };

    /**
     *  Refresh TSK average occupancy view (serving data to legacy /parkings endpoints)
     */
    public refreshTskOccupancyView = async (): Promise<void> => {
        try {
            await this.tskAverageOccupancyRepository.refreshData();
        } catch (err) {
            throw new GeneralError(
                "refreshTskOccupancyView: error while refreshing TSK average occupancy view",
                this.constructor.name,
                err
            );
        }
    };
}

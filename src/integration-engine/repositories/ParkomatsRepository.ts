import { Parkomats } from "#sch";
import { ParkomatDto } from "#sch/parkomats/models/ParkomatDto";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class ParkomatsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            Parkomats.name + "Model",
            {
                outputSequelizeAttributes: ParkomatDto.attributeModel,
                pgSchema: Parkomats.pgSchema,
                pgTableName: Parkomats.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Parkomats.name + "ModelValidator", ParkomatDto.jsonSchema)
        );
    }
}

import { Parkings } from "#sch";
import { IPayment } from "#sch/models/interfaces/IPayment";
import { PaymentModel } from "#sch/models/PaymentModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Op } from "@golemio/core/dist/shared/sequelize";

export class ParkingPaymentsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "ParkingPaymentsRepository",
            {
                outputSequelizeAttributes: PaymentModel.attributeModel,
                pgSchema: Parkings.pgSchema,
                pgTableName: PaymentModel.tableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("ParkingPaymentsRepositoryValidator", PaymentModel.jsonSchema)
        );
    }

    public async updatePaymentsBySource(payments: IPayment[], source: string, processingDate: Date) {
        await this.bulkSave(payments, PaymentModel.attributeUpdateList);
        await this.sequelizeModel.destroy({
            where: {
                source: { [Op.eq]: source },
                updated_at: { [Op.lt]: processingDate },
            },
        });
    }
}

import { Parkings } from "#sch";
import { TskAverageOccupancyModel } from "#sch/models";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { QueryTypes } from "@golemio/core/dist/shared/sequelize";

export class TskAverageOccupancyRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "TskAverageOccupancyRepository",
            {
                pgTableName: TskAverageOccupancyModel.tableName,
                pgSchema: Parkings.pgSchema,
                outputSequelizeAttributes: TskAverageOccupancyModel.attributeModel,
                savingType: "insertOnly", // TODO readOnly
            },
            // @ts-expect-error read only repository
            undefined
        );
    }

    /**
     *  Refresh the v_tsk_average_occupancy view
     */
    public async refreshData(): Promise<void> {
        await this.sequelizeModel.sequelize!.query(
            `refresh materialized view concurrently ${Parkings.pgSchema}.${TskAverageOccupancyModel.tableName}`,
            {
                plain: true,
                type: QueryTypes.SELECT,
            }
        );
    }
}

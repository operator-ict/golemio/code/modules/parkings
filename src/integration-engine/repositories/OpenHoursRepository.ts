import { Parkings } from "#sch";
import { OpeningHoursModel } from "#sch/models/OpeningHoursModel";
import { IOpeningHours } from "#sch/models/interfaces/openingHours/IOpeningHours";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Op } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class OpenHoursRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "ParkingsOpenHoursRepository",
            {
                pgTableName: OpeningHoursModel.tableName,
                pgSchema: Parkings.pgSchema,
                outputSequelizeAttributes: OpeningHoursModel.attributeModel,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("ParkingsOpenHoursRepositoryValidator", OpeningHoursModel.jsonSchema)
        );
    }

    public async mergeOpenHours(openHours: IOpeningHours[], source: string[], processingDate: Date): Promise<void> {
        await this.bulkSave<OpeningHoursModel>(openHours, ["periods_of_time", "updated_at"]);
        if (source) {
            await this.sequelizeModel.destroy({
                where: {
                    source: {
                        [Op.in]: source,
                    },
                    updated_at: {
                        [Op.lt]: processingDate,
                    },
                },
            });
        }
    }
}

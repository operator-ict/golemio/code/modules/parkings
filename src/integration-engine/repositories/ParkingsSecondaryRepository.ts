import { IParking } from "#ie/ParkingInterface";
import { Parkings } from "#sch";
import { ParkingsDtoSchema } from "#sch/datasources/ParkingsDtoSchema";
import { ParkingModel } from "#sch/models/ParkingModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Op, Transaction } from "@golemio/core/dist/shared/sequelize";

export class ParkingsSecondaryRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            Parkings.name + "Model",
            {
                outputSequelizeAttributes: ParkingModel.attributeModel,
                pgSchema: Parkings.pgSchema,
                pgTableName: "parkings_secondary",
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Parkings.name + "SecondaryPgModelValidator", ParkingsDtoSchema)
        );
    }

    public saveActiveParkingsWithoutAddress = async (parkings: IParking[], source: string): Promise<void> => {
        const transaction = await this.sequelizeModel.sequelize!.transaction();
        try {
            await this.markInactiveParkings(parkings, source, transaction);
            await this.saveWithoutAddress(parkings, transaction);
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw new GeneralError("Database error", "ParkingsModel", err, 500);
        }
    };

    public GetOne = async (id: string): Promise<IParking | null> => {
        try {
            return await this.sequelizeModel.findOne({
                where: { id },
                raw: true,
                nest: true,
            });
        } catch (err) {
            throw new GeneralError("Database error", "ParkingsModel", err, 500);
        }
    };

    private saveWithoutAddress = async (parkings: IParking[], transaction: Transaction): Promise<void> => {
        await this.bulkSave(parkings, this.getParkingAttributesToUpdate(), false, false, transaction);
    };

    private async markInactiveParkings(parkings: IParking[], source: string, transaction: Transaction): Promise<void> {
        const activeIds = parkings.map((parking) => parking.id);
        await this.update(
            {
                active: false,
            },
            {
                where: {
                    source,
                    active: true,
                    id: {
                        [Op.notIn]: activeIds,
                    },
                },
                transaction,
            }
        );
    }

    private getParkingAttributesToUpdate(): string[] {
        return ParkingModel.attributeList.filter((key) => !["address", "address_updated_at"].includes(key)).concat("updated_at");
    }
}

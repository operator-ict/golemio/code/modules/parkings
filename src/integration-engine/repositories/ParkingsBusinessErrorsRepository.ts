import { Parkings } from "#sch";
import { BusinessError } from "#sch/models/BusinessError";
import { IBusinessError } from "#sch/models/interfaces/IBusinessError";
import { IModel } from "@golemio/core/dist/integration-engine/models/IModel";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models/PostgresModel";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class ParkingsBusinessErrorsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "ParkingsBusinessErrorsRepository",
            {
                outputSequelizeAttributes: BusinessError.attributeModel,
                pgSchema: Parkings.pgSchema,
                pgTableName: "parkings_business_errors",
                savingType: "insertOnly",
                sequelizeAdditionalSettings: {
                    timestamps: false,
                },
            },
            new JSONSchemaValidator("ParkingsBusinessErrorsRepositoryValidator", BusinessError.jsonSchema)
        );
    }

    public save = async (errors: IBusinessError[]) => {
        await this.validator.Validate(errors);
        await this.sequelizeModel.bulkCreate<BusinessError>(errors, { ignoreDuplicates: true });
    };
}

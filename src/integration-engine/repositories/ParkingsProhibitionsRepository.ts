import { Parkings } from "#sch";
import { ParkingProhibitionsModel } from "#sch/models/ParkingProhibitionsModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class ParkingsProhibitionsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "ParkingsProhibitionsRepository",
            {
                outputSequelizeAttributes: ParkingProhibitionsModel.attributeModel,
                pgSchema: Parkings.pgSchema,
                pgTableName: ParkingProhibitionsModel.tableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("ParkingsProhibitionsRepository", ParkingProhibitionsModel.jsonSchema)
        );
    }
}

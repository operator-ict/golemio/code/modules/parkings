import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { Parkings } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { ParkingMachinesModel } from "#sch/models/ParkingMachinesModel";

export class ParkingMachinesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "ParkingMachinesRepository",
            {
                outputSequelizeAttributes: ParkingMachinesModel.attributeModel,
                pgSchema: Parkings.pgSchema,
                pgTableName: "parking_machines",
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("ParkingMachinesRepositoryValidator", ParkingMachinesModel.jsonSchema)
        );
    }
}

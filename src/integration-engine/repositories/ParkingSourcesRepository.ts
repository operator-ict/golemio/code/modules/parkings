import { Parkings } from "#sch";
import { ParkingSourcesModel } from "#sch/models/ParkingSourcesModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { InputParkingSourceSchema } from "#sch/datasources/InputParkingSourceSchema";

export class ParkingSourcesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "IEParkingSourcesRepository",
            {
                outputSequelizeAttributes: ParkingSourcesModel.attributeModel,
                pgSchema: Parkings.pgSchema,
                pgTableName: ParkingSourcesModel.tableName,
                savingType: "insertOrUpdate",
                sequelizeAdditionalSettings: {
                    timestamps: false,
                },
            },
            new JSONSchemaValidator("ParkingSourcePgModelValidator", InputParkingSourceSchema)
        );
    }
}

import { Parkings } from "#sch";
import { ParkingMeasurementsDtoSchema } from "#sch/datasources/ParkingMeasurementsDtoSchema";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ParkingsMeasurementRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            Parkings.measurements.name + "Model",
            {
                outputSequelizeAttributes: Parkings.measurements.outputSequelizeAttributes,
                pgSchema: Parkings.pgSchema,
                pgTableName: Parkings.measurements.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Parkings.measurements.name + "PgModelValidator", ParkingMeasurementsDtoSchema)
        );
    }
}

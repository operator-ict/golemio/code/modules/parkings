import { IParking } from "#ie/ParkingInterface";
import { Parkings } from "#sch";
import { ParkingsDtoSchema } from "#sch/datasources/ParkingsDtoSchema";
import { ParkingModel } from "#sch/models/ParkingModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { Op, Sequelize, Transaction } from "@golemio/core/dist/shared/sequelize";

@injectable()
export class ParkingsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            Parkings.name + "Model",
            {
                outputSequelizeAttributes: ParkingModel.attributeModel,
                pgSchema: Parkings.pgSchema,
                pgTableName: ParkingModel.tableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("ParkingsRepositoryValidator", ParkingsDtoSchema)
        );
    }

    public GetOne = async (id: string): Promise<IParking | null> => {
        try {
            return await this.sequelizeModel.findOne({
                where: { id },
                raw: true,
                nest: true,
            });
        } catch (err) {
            throw new GeneralError("Database error", "ParkingsModel", err, 500);
        }
    };

    public saveActiveParkingsWithoutAddress = async (
        parkings: IParking[],
        source: string,
        dataProvider?: string
    ): Promise<void> => {
        const transaction = await this.sequelizeModel.sequelize!.transaction();
        try {
            await this.markInactiveParkings(parkings, source, transaction, dataProvider);
            await this.saveWithoutAddress(parkings, transaction);
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw new GeneralError("Database error", "ParkingsModel", err, 500);
        }
    };

    public saveActiveParkingsWithoutAddressAndName = async (
        parkings: IParking[],
        source: string,
        dataProvider?: string
    ): Promise<void> => {
        const transaction = await this.sequelizeModel.sequelize!.transaction();
        try {
            await this.markInactiveParkings(parkings, source, transaction, dataProvider);
            await this.saveWithoutAddressAndName(parkings, transaction);
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw new GeneralError("Database error", "ParkingsModel", err, 500);
        }
    };

    private saveWithoutAddress = async (parkings: IParking[], transaction: Transaction): Promise<void> => {
        await this.bulkSave(parkings, this.getParkingAttributesToUpdate(), false, false, transaction);
    };

    private saveWithoutAddressAndName = async (parkings: IParking[], transaction: Transaction): Promise<void> => {
        await this.bulkSave(parkings, this.getParkingAttributesToUpdateWithoutName(), false, false, transaction);
    };

    public async findWithOutdatedAddress(batchSize: number, updateInterval: number): Promise<IParking[]> {
        return this.find({
            limit: batchSize,
            where: {
                address_updated_at: {
                    [Op.lte]: Sequelize.literal(`NOW() - INTERVAL '${updateInterval} days'`),
                },
            },
        });
    }

    public async findWithoutAddress(): Promise<IParking[]> {
        return this.find({
            where: {
                address: null,
                address_updated_at: null,
            },
        });
    }

    private async markInactiveParkings(
        parkings: IParking[],
        source: string,
        transaction: Transaction,
        dataProvider?: string
    ): Promise<void> {
        const activeIds = parkings.map((parking) => parking.id);
        await this.update(
            {
                active: false,
            },
            {
                where: {
                    source,
                    ...(dataProvider && { data_provider: dataProvider }),
                    active: true,
                    id: {
                        [Op.notIn]: activeIds,
                    },
                },
                transaction,
            }
        );
    }

    private getParkingAttributesToUpdate(): string[] {
        return ParkingModel.attributeList
            .filter((key) => !["address", "address_updated_at", "sanitized_location", "area"].includes(key))
            .concat("updated_at");
    }

    private getParkingAttributesToUpdateWithoutName(): string[] {
        return ParkingModel.attributeList
            .filter((key) => !["address", "address_updated_at", "name", "sanitized_location", "area"].includes(key))
            .concat("updated_at");
    }
}

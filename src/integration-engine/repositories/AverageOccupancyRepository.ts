import { Parkings } from "#sch";
import { ParkingAverageOccupancyModel } from "#sch/models/ParkingAverageOccupancyModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { QueryTypes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class AverageOccupancyRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "AverageOccupancyRepository",
            {
                pgTableName: ParkingAverageOccupancyModel.tableName,
                pgSchema: Parkings.pgSchema,
                outputSequelizeAttributes: ParkingAverageOccupancyModel.attributeModel,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("AverageOccupancyRepository", {})
        );
    }

    /**
     *  Trigger calculate_average_occupancy function to calculate occupancy delta
     *  since last update and add it to average in `parkings_average_occupancy`.
     */
    public async calculateAverageOccupancy(): Promise<void> {
        await this.sequelizeModel.sequelize!.query(`CALL ${Parkings.pgSchema}.calculate_average_occupancy()`, {
            plain: true,
            type: QueryTypes.SELECT,
        });
    }
}

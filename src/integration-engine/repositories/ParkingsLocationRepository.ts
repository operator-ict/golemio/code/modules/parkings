import { ParkingLocationsModel } from "#sch/models/ParkingLocationsModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { Parkings } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IParking } from "#ie/ParkingInterface";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IParkingLocation } from "#sch/models/interfaces/IParkingLocation";
import { Op, Sequelize } from "@golemio/core/dist/shared/sequelize";

@injectable()
export class ParkingsLocationRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            Parkings.location.name + "Model",
            {
                outputSequelizeAttributes: ParkingLocationsModel.attributeModel,
                pgSchema: Parkings.pgSchema,
                pgTableName: ParkingLocationsModel.tableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Parkings.location.name + "PgModelValidator", Parkings.location.outputJsonSchema)
        );
    }

    public GetOne = async (id: string): Promise<IParking | null> => {
        try {
            return await this.sequelizeModel.findOne({
                where: { id },
                raw: true,
                nest: true,
            });
        } catch (err) {
            throw new GeneralError("Database error", "ParkingsModel", err, 500);
        }
    };

    public saveWithoutAddress = async (parkings: IParkingLocation[]): Promise<void> => {
        try {
            await this.bulkSave(parkings, this.getParkingAttributesToUpdate());
        } catch (err) {
            throw new GeneralError("Database error", "ParkingsModel", err, 500);
        }
    };

    public async findWithOutdatedAddress(batchSize: number, updateInterval: number): Promise<IParkingLocation[]> {
        return this.find({
            limit: batchSize,
            where: {
                address_updated_at: {
                    [Op.lte]: Sequelize.literal(`NOW() - INTERVAL '${updateInterval} days'`),
                },
            },
        });
    }

    public async findWithoutAddress(): Promise<IParkingLocation[]> {
        return this.find({
            where: {
                address: null,
                address_updated_at: null,
            },
        });
    }

    private getParkingAttributesToUpdate(): string[] {
        return ParkingLocationsModel.attributeList
            .filter((key) => !["address", "address_updated_at"].includes(key))
            .concat("updated_at");
    }
}

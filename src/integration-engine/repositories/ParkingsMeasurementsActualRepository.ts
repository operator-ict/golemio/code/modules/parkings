import { Parkings } from "#sch";
import { ParkingMeasurementsDtoSchema } from "#sch/datasources/ParkingMeasurementsDtoSchema";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ParkingsMeasurementsActualRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            Parkings.measurementsActual.name + "Model",
            {
                outputSequelizeAttributes: Parkings.measurementsActual.outputSequelizeAttributes,
                pgSchema: Parkings.pgSchema,
                pgTableName: Parkings.measurementsActual.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Parkings.measurementsActual.name + "PgModelValidator", ParkingMeasurementsDtoSchema)
        );
    }
}

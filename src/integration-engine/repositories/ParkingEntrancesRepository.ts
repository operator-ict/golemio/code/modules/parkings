import { Parkings } from "#sch";
import { EntrancesModel } from "#sch/models/EntranceModel";
import { IEntrance } from "#sch/models/interfaces/IEntrance";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Op } from "@golemio/core/dist/shared/sequelize";

export class ParkingEntrancesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "ParkingEntrancesRepository",
            {
                outputSequelizeAttributes: EntrancesModel.attributeModel,
                pgSchema: Parkings.pgSchema,
                pgTableName: EntrancesModel.tableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("ParkingEntrancesRepositoryValidator", EntrancesModel.jsonSchema)
        );
    }

    public async mergeEntrances(entrances: IEntrance[], source: string[], processingDate: Date): Promise<void> {
        await this.bulkSave<EntrancesModel>(entrances);

        if (source) {
            await this.sequelizeModel.destroy({
                where: {
                    source: {
                        [Op.in]: source,
                    },
                    updated_at: {
                        [Op.lt]: processingDate,
                    },
                },
            });
        }
    }
}

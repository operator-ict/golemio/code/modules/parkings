import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { Parkings } from "#sch";
import { IParkingTariff } from "#sch/models/interfaces/IParkingTariff";
import { ParkingModel } from "#sch/models/ParkingModel";
import { ParkingTariffsModel } from "#sch/models/ParkingTariffsModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ParkingsSecondaryRepository } from "./ParkingsSecondaryRepository";
import { isSecondarySource } from "#helpers/constants/SourceEnum";

@injectable()
export class ParkingTariffsRepository extends PostgresModel implements IModel {
    constructor(
        @inject(ModuleContainerToken.ParkingsRepository) private parkingRepository: ParkingsRepository,
        @inject(ModuleContainerToken.ParkingsSecondaryRepository) private parkingSecondaryRepository: ParkingsSecondaryRepository
    ) {
        super(
            "ParkingTariffsRepository",
            {
                outputSequelizeAttributes: ParkingTariffsModel.attributeModel,
                pgSchema: Parkings.pgSchema,
                pgTableName: ParkingTariffsModel.tableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("ParkingTariffsRepositoryValidator", ParkingTariffsModel.jsonSchema)
        );
    }

    public deleteByTariffIds = async (transformedTSKData: IParkingTariff[]): Promise<number> => {
        const tariffIds = transformedTSKData.map((tariff) => tariff.tariff_id);
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    tariff_id: {
                        [Sequelize.Op.in]: tariffIds,
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Database error: Could not delete tariffs", this.constructor.name, err, 500);
        }
    };

    public merge = async (transformedData: IParkingTariff[], source: string, processingDate: Date): Promise<void> => {
        if (await this.validate(transformedData)) {
            const transaction = await this.sequelizeModel.sequelize!.transaction();
            try {
                await this.sequelizeModel.bulkCreate(transformedData as any, {
                    updateOnDuplicate: ParkingTariffsModel.attributeUpdateList,
                    transaction: transaction,
                });
                await this.sequelizeModel.destroy({
                    where: {
                        source: source,
                        updated_at: { [Op.lt]: processingDate },
                    },
                    transaction: transaction,
                });
                await transaction.commit();
            } catch (err) {
                await transaction.rollback();
                throw new GeneralError(
                    `Database error: Could not merge tariffs for source: ${source}`,
                    this.constructor.name,
                    err
                );
            }
        }
    };

    public replaceTariffs = async (
        tariffData: IParkingTariff[],
        relationData: Array<{ id: string; source: string; source_id: string; tariff_id: string }>,
        source: string,
        processingDate: Date
    ): Promise<void> => {
        if (await this.validate(tariffData)) {
            const transaction = await this.sequelizeModel.sequelize!.transaction();
            try {
                await this.sequelizeModel.bulkCreate(tariffData as any, {
                    updateOnDuplicate: ParkingTariffsModel.attributeUpdateList,
                    transaction,
                });
                await this.sequelizeModel.destroy({
                    where: {
                        source: source,
                        updated_at: { [Op.lt]: processingDate },
                    },
                    transaction,
                });
                const parkingRepository = this.getParkingRepository(source);
                await parkingRepository["sequelizeModel"].bulkCreate(relationData, {
                    updateOnDuplicate: ParkingModel.updateParkingTariffAttributeList,
                    transaction,
                });
                await parkingRepository["sequelizeModel"].update(
                    { tariff_id: null },
                    {
                        where: {
                            source: { [Op.eq]: source },
                            updated_at: { [Op.lt]: processingDate },
                            tariff_id: { [Op.not]: null },
                        },
                        transaction,
                    }
                );
                await transaction.commit();
            } catch (err) {
                await transaction.rollback();
                throw new GeneralError(
                    `Database error: Could not merge tariffs for source: ${source}`,
                    this.constructor.name,
                    err
                );
            }
        }
    };

    private getParkingRepository = (source: string) => {
        if (isSecondarySource(source)) {
            return this.parkingSecondaryRepository;
        } else {
            return this.parkingRepository;
        }
    };
}

import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { Parkings } from "#sch/index";
import { ParkingsWorker } from "#ie/ParkingsWorker";
import { ZtpParkingZonesWorker } from "#ie/ZtpParkingZones/ZtpParkingZonesWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: Parkings.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + Parkings.name.toLowerCase(),
        queues: [
            {
                name: "saveKoridConfToDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: ParkingsWorker,
                workerMethod: "saveKoridConfToDB",
            },
            {
                name: "saveKoridDataToDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: ParkingsWorker,
                workerMethod: "saveKoridDataToDB",
            },
            {
                name: "saveParkingLotsPrague",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: ParkingsWorker,
                workerMethod: "saveParkingLotsPrague",
            },
            {
                name: "saveParkingLotsMeasurementsPrague",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: ParkingsWorker,
                workerMethod: "saveParkingLotsMeasurementsPrague",
            },
            {
                name: "refreshTskOccupancyView",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 59 * 60 * 1000, // 59 minutes
                },
                worker: ParkingsWorker,
                workerMethod: "refreshTskOccupancyView",
            },
            {
                name: "updateZtpParkings",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: ZtpParkingZonesWorker,
                workerMethod: "updateZtpParkings",
            },
        ],
    },
];

export { queueDefinitions };

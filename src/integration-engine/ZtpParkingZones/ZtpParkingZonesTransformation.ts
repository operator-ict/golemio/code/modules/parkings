import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { IZtpParkingJsonFeature } from "#sch/datasources/interfaces/IZtpParkingJsonFeature";
import { IParking } from "#ie/ParkingInterface";
import { SourceEnum } from "#helpers/constants/SourceEnum";

export class ZtpParkingZonesTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    public transformationDate: string;

    constructor(transformationDate: Date) {
        super();
        this.name = "ZtpParkings";
        this.transformationDate = transformationDate.toISOString();
    }

    transform = async (data: IZtpParkingJsonFeature[]): Promise<IParking[]> => {
        let parkings = [];
        for (const element of data) {
            const parking = await this.transformElement(element);
            parkings.push(parking);
        }
        return parkings;
    };

    transformElement = async (element: IZtpParkingJsonFeature): Promise<IParking> => {
        let parkingName = "Parkoviště ZTP";

        return {
            id: `ipr-ztp-ztp-${element.properties.OBJECTID.toString()}`,
            source: SourceEnum.IPR,
            source_id: `ztp-${element.properties.OBJECTID.toString()}`,
            data_provider: "opendata.iprpraha.cz",
            date_modified: this.transformationDate,
            location: element.geometry,
            name: parkingName,
            total_spot_number: element.properties.POCET_PS,
            parking_type: "disabled_parking",
            centroid: element.geometry,
            active: true,
        };
    };
}

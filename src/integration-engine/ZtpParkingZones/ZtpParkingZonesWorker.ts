import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { ZtpParkingZonesDataSourceFactory } from "#ie/ZtpParkingZones/ZtpParkingZonesDataSourceFactory";
import { ZtpParkingZonesTransformation } from "#ie/ZtpParkingZones/ZtpParkingZonesTransformation";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { config } from "@golemio/core/dist/integration-engine/config";
import { UpdateAddressWorker } from "#ie/workers/UpdateAddressWorker";
import { SourceEnum } from "#helpers/constants/SourceEnum";

export class ZtpParkingZonesWorker extends BaseWorker {
    private dataSource: DataSource;
    private transformer: ZtpParkingZonesTransformation;
    private parkingRepository: ParkingsRepository;

    constructor() {
        super();
        this.dataSource = ZtpParkingZonesDataSourceFactory.getDataSource();
        this.transformer = new ZtpParkingZonesTransformation(new Date());
        this.parkingRepository = new ParkingsRepository();
    }

    public updateZtpParkings = async (): Promise<void> => {
        try {
            const data = await this.dataSource.getAll();
            const transformedData = await this.transformer.transform(data);
            await this.parkingRepository.saveActiveParkingsWithoutAddressAndName(transformedData, SourceEnum.IPR);

            await QueueManager.sendMessageToExchange(
                config.RABBIT_EXCHANGE_NAME + "." + UpdateAddressWorker.workerName.toLowerCase(),
                "updateMissingParkingsAddresses",
                {}
            );
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError(`Error while refreshing ZTP Parking data: ${err.message}`, this.constructor.name, err);
            }
        }
    };
}

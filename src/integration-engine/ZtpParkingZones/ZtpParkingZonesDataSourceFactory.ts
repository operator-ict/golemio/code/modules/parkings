import {
    DataSource,
    PaginatedHTTPProtocolStrategy,
    JSONDataTypeStrategy,
} from "@golemio/core/dist/integration-engine/datasources";
import { config } from "@golemio/core/dist/integration-engine/config";
import { ZtpParkingsJsonSchema } from "#sch/datasources/ZtpParkingsJsonSchema";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class ZtpParkingZonesDataSourceFactory {
    public static readonly API_URL = config.datasources.ZtpParkingsApiUrl;

    public static getDataSource(): DataSource {
        return new DataSource(
            ZtpParkingsJsonSchema.name,
            new PaginatedHTTPProtocolStrategy({
                headers: {},
                method: "GET",
                url: ZtpParkingZonesDataSourceFactory.API_URL,
            }),
            new JSONDataTypeStrategy({ resultsPath: "features" }),
            new JSONSchemaValidator(ZtpParkingsJsonSchema.name, ZtpParkingsJsonSchema.jsonSchema)
        );
    }
}

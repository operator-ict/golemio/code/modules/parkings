import { SourceEnum } from "#helpers/constants/SourceEnum";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IParking } from "#ie/ParkingInterface";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { wktToGeoJSON } from "@terraformer/wkt";
import { TGeoCoordinates } from "@golemio/core/dist/output-gateway";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ITskParkingSection } from "#sch/datasources/interfaces/ITskParkingSection";
import { getPointOnFeature } from "#ie/helpers/GeodataHelper";
import { DateTime } from "@golemio/core/dist/helpers";

@injectable()
export class TskParkingSectionTransformation extends AbstractTransformation<ITskParkingSection, IParking> {
    name: string = "TskParkingSectionTransformation";

    constructor(private transformationDate: Date) {
        super();
    }

    protected transformInternal = (section: ITskParkingSection): IParking => {
        const location = wktToGeoJSON(section.borderWKT) as TGeoCoordinates;
        const valid_from = DateTime.fromISO(section.activeFrom, { timeZone: "Europe/Prague" }).toISOString();

        return {
            id: "tsk2-" + section.sectionCode,
            source: SourceEnum.TSK_V2,
            source_id: section.idSection,
            data_provider: "www.tsk-praha.cz",
            location,
            name: section.sectionCode,
            total_spot_number: section.projectedCapacity,
            category: this.getSectionCat(section.sectionCat),
            tariff_id: section.idTariff ? section.idTariff : undefined,
            valid_from,
            parking_type: "on_street",
            zone_type: "zone",
            date_modified: this.transformationDate.toISOString(),
            centroid: getPointOnFeature(location),
            parking_policy: "zone",
            active: true,
        };
    };

    private getSectionCat(catNo: number): string {
        switch (catNo) {
            case 1:
                return "blue";
            case 2:
                return "violet";
            case 3:
                return "orange";
            case 6:
                return "other";
            case 7:
                return "other";
            default:
                throw new GeneralError(`Unknown section category ${catNo} in TSK parking zones.`);
        }
    }
}

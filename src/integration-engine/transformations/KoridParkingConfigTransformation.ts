import { IParkingTariff, IParkingTariffMeansOfPayment } from "#sch/models/interfaces/IParkingTariff";
import * as turf from "@turf/turf";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { IGeoCoordinatesPoint } from "@golemio/core/dist/output-gateway/Geo";
import { Parkings } from "#sch";
import getUuidByString from "uuid-by-string";
import { IParking, IParkingLocation } from "#ie/ParkingInterface";

export class KoridParkingConfigTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly dataSource: string;

    constructor() {
        super();
        this.name = Parkings.korid.name + "Config";
        this.dataSource = "korid";
    }

    public transform = async (
        data: any
    ): Promise<{ geo: IParking[]; tariff: IParkingTariff[]; location: IParkingLocation[] }> => {
        const geoResults = [];
        const tariffResults = [];
        const locationResults: IParkingLocation[] = [];
        const dateModified = new Date(data.time).toISOString();

        for (const element of data.geojson.features) {
            geoResults.push(
                await this.transformElement({
                    ...element,
                    time: dateModified,
                })
            );
            locationResults.push(
                this.transformLocation({
                    ...element,
                    time: dateModified,
                })
            );
        }

        tariffResults.push(...this.transformTariffElement(data.tarif));

        return { geo: geoResults, tariff: tariffResults, location: locationResults };
    };

    protected transformElement = async (element: any): Promise<IParking> => {
        return {
            id: `${this.dataSource}-${element.properties.groupid}`.toLowerCase(),
            data_provider: "www.korid.cz",
            date_modified: element.time,
            location: element.geometry,
            name: element.properties.title,
            source: this.dataSource,
            source_id: "" + element.properties.groupid,
            total_spot_number: element.properties.total,
            tariff_id: getUuidByString(this.dataSource + element.properties.tarif),
            parking_type: "on_street",
            zone_type: null,
            centroid: turf.centroid(element.geometry).geometry as IGeoCoordinatesPoint,
            active: true,
        };
    };

    private transformLocation = (element: any): IParkingLocation => {
        return {
            id: `${this.dataSource}-${element.properties.groupid}`.toLowerCase(),
            data_provider: "www.korid.cz",
            location: element.geometry,
            centroid: turf.centroid(element.geometry).geometry as IGeoCoordinatesPoint,
            total_spot_number: element.properties.total,
            source: this.dataSource,
            source_id: "" + element.properties.groupid,
        };
    };

    private transformMeansOfPayments = (data: string[]): IParkingTariffMeansOfPayment => ({
        accepts_payment_card: data.includes("paymentCard"),
        accepts_cash: data.includes("cash"),
    });

    private transformTariffElement = (element: any): IParkingTariff[] => {
        const tariff = [];

        const tariffDescription = {
            source: this.dataSource,
            last_updated: element.lastUpdated,
            payment_mode: element.paymentMode.join(","),
            payment_additional_description: element.paymentAdditionalDescription,
            free_of_charge: element.freeOfCharge,
            url_link_address: element.urlLinkAddress,
        };

        for (const chargeBandElement of element.chargeBand) {
            const chargeDescription = {
                tariff_id: getUuidByString(this.dataSource + chargeBandElement.chargeBandName),
                charge_band_name: chargeBandElement.chargeBandName,
                charge_currency: chargeBandElement.chargeCurrency,
                ...this.transformMeansOfPayments(chargeBandElement.acceptedMeansOfPayment || []),
            };

            for (const chargeElement of chargeBandElement.charge) {
                let timePeriod = chargeElement.timePeriod ? chargeElement.timePeriod : {};
                tariff.push({
                    ...tariffDescription,
                    ...chargeDescription,
                    charge: chargeElement.charge,
                    charge_type: chargeElement.chargeType,
                    charge_order_index: chargeElement.chargeOrderIndex,
                    charge_interval: chargeElement.chargeInterval,
                    max_iterations_of_charge: chargeElement.maxIterationsOfCharge,
                    min_iterations_of_charge: chargeElement.minIterationsOfCharge,
                    start_time_of_period: timePeriod.startTimeOfPeriod,
                    end_time_of_period: timePeriod.endTimeOfPeriod,
                });
            }
        }
        return tariff;
    };
}

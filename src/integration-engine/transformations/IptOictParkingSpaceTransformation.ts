import { IGenericFeature } from "#ie/datasources/interfaces/IGenericFeature";
import { getPointOnFeature } from "#ie/helpers/GeodataHelper";
import { IIptOictParkingSpacesProperties } from "#sch/datasources/iptoict/interfaces/IIptOictParkingSpacesProperties";
import { IParkingLocation } from "#sch/models/interfaces/IParkingLocation";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { TGeoCoordinates } from "@golemio/core/dist/output-gateway";

export class IptOictParkingSpaceTransformation extends AbstractTransformation<
    IGenericFeature<IIptOictParkingSpacesProperties>,
    IParkingLocation
> {
    public name: string = "IptOictParkingSpaceTransformation";

    constructor(private source: string) {
        super();
    }

    protected transformInternal = (element: IGenericFeature<IIptOictParkingSpacesProperties>) => {
        const transformed: IParkingLocation = {
            id: element.properties.id,
            source: this.source,
            source_id: element.properties.parking_id,
            data_provider: "manual",
            location: element.geometry as TGeoCoordinates,
            centroid: getPointOnFeature(element.geometry as TGeoCoordinates),
            total_spot_number: element.properties.capacity,
            special_access: element.properties.access,
        };

        return transformed;
    };
}

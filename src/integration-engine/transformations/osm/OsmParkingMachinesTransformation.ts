import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IParkingMachine } from "#sch/models/interfaces/IParkingMachine";
import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IGenericFeature } from "#ie/datasources/interfaces/IGenericFeature";
import { IOsmParkingMachinesProperties } from "#sch/datasources/osm/interfaces/IOsmParkingMachinesProperties";

export class OsmParkingMachinesTransformation extends AbstractTransformation<
    IGenericFeature<IOsmParkingMachinesProperties>,
    IParkingMachine
> {
    public name: string = "OsmParkingMachinesTransformation";

    constructor(private transformationDate: Date) {
        super();
    }

    transformInternal = (parkingMachine: IGenericFeature<IOsmParkingMachinesProperties>): IParkingMachine => {
        return {
            id: `${SourceEnum.OSM}-${parkingMachine.properties.osm_id}`,
            source: SourceEnum.OSM,
            sourceId: parkingMachine.properties.osm_id,
            code: null,
            type: "payment_machine",
            location: parkingMachine.geometry,
            validFrom: this.transformationDate,
            tariffId: null,
        };
    };
}

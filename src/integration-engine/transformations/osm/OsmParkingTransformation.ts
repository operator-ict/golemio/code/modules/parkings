import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IParking } from "#ie/ParkingInterface";
import { IGenericFeature } from "#ie/datasources/interfaces/IGenericFeature";
import { getPointOnFeature } from "#ie/helpers/GeodataHelper";
import { IOsmParkingProperties } from "#sch/datasources/osm/interfaces/IOsmParkingProperties";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { TGeoCoordinates } from "@golemio/core/dist/output-gateway";

export class OsmParkingTransformation extends AbstractTransformation<IGenericFeature<IOsmParkingProperties>, IParking> {
    public name: string = "OsmParkingTransformation";

    constructor(private transformationDate: Date) {
        super();
    }

    protected transformInternal = (element: IGenericFeature<IOsmParkingProperties>) => {
        const transformed: IParking = {
            id: `${SourceEnum.OSM}-${element.properties.osm_id}`,
            source: SourceEnum.OSM,
            source_id: element.properties.osm_id,
            data_provider: "www.openstreetmap.org",
            location: element.geometry as TGeoCoordinates,
            name: element.properties.name,
            parking_type: element.properties.parking_structure ? element.properties.parking_structure : "other",
            centroid: getPointOnFeature(element.geometry as TGeoCoordinates),
            covered: element.properties.covered ? element.properties.covered === "yes" : null,
            security: element.properties.supervised ? element.properties.supervised === "yes" : null,
            contact:
                element.properties.email || element.properties.phone || element.properties.website
                    ? {
                          email: element.properties.email ?? null,
                          phone: element.properties.phone ?? null,
                          website: element.properties.website ?? null,
                      }
                    : null,
            date_modified: this.transformationDate.toISOString(),
            total_spot_number: null,
            max_vehicle_dimensions: null,
            parking_policy: element.properties.parking_policy ?? null,
            active: true,
        };

        return transformed;
    };
}

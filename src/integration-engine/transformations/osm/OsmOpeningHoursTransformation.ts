import { SourceEnum } from "#helpers/constants/SourceEnum";
import { MaxStayParser } from "#helpers/osm/MaxStayParser";
import { OpeningHoursParser } from "#helpers/osm/OpeningHoursParser";
import { IGenericFeature } from "#ie/datasources/interfaces/IGenericFeature";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { IOsmParkingProperties } from "#sch/datasources/osm/interfaces/IOsmParkingProperties";
import { IOpeningHours } from "#sch/models/interfaces/openingHours/IOpeningHours";
import { IOpeningHoursPeriod } from "#sch/models/interfaces/openingHours/IOpeningHoursPeriod";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class OsmOpeningHoursTransformation extends AbstractTransformation<IGenericFeature<IOsmParkingProperties>, IOpeningHours> {
    public name: string = "OsmOpenHoursTransformation";

    constructor(@inject(ModuleContainerToken.OsmOpeningHoursParser) private openingHoursParser: OpeningHoursParser) {
        super();
    }

    protected transformInternal = (element: IGenericFeature<IOsmParkingProperties>) => {
        const periods = this.openingHoursParser.parse(element.properties.opening_hours);
        const maxStay = MaxStayParser.parse(element.properties.maxstay);
        if (periods !== null) {
            for (const period of periods) {
                if (maxStay !== undefined) {
                    period.maximum_duration = maxStay;
                }
            }
        }

        const transformed: IOpeningHours = {
            parking_id: `${SourceEnum.OSM}-${element.properties.osm_id}`,
            source: SourceEnum.OSM,
            valid_from: "1970-01-01T00:00:00.000Z",
            valid_to: null,
            periods_of_time: periods as IOpeningHoursPeriod[],
        };

        return transformed;
    };
}

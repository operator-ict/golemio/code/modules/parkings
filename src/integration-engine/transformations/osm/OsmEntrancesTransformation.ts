import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IGenericFeature } from "#ie/datasources/interfaces/IGenericFeature";
import { IOsmEntranceProperties } from "#sch/datasources/osm/interfaces/IOsmEntranceProperties";
import { IEntrance } from "#sch/models/interfaces/IEntrance";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";

export class OsmEntrancesTransformation extends AbstractTransformation<IGenericFeature<IOsmEntranceProperties>, IEntrance> {
    public name: string = "OsmParkingEntrancesTransformation";

    protected transformInternal = (element: IGenericFeature<IOsmEntranceProperties>) => {
        const transformed: IEntrance = {
            entrance_id: element.properties.osm_id,
            source: SourceEnum.OSM,
            parking_id: `${SourceEnum.OSM}-${element.properties.parking_id}`,
            level: element.properties.level,
            location: element.geometry as TGeoCoordinates,
            entry: element.properties?.entry ?? null,
            exit: element.properties?.exit ?? null,
            entrance_type: element.properties?.entrance_type ?? null,
            max_width: element.properties?.maxwidth ?? null,
            max_length: element.properties?.maxlength ?? null,
            max_weight: element.properties?.maxweight ?? null,
            max_height: element.properties?.maxheight ?? null,
        };

        return transformed;
    };
}

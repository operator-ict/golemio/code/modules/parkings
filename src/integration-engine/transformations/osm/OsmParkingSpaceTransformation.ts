import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IGenericFeature } from "#ie/datasources/interfaces/IGenericFeature";
import { getPointOnFeature } from "#ie/helpers/GeodataHelper";
import { IOsmParkingSpacesProperties } from "#sch/datasources/osm/interfaces/IOsmParkingSpacesProperties";
import { IParkingLocation } from "#sch/models/interfaces/IParkingLocation";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { TGeoCoordinates } from "@golemio/core/dist/output-gateway";

export class OsmParkingSpaceTransformation extends AbstractTransformation<
    IGenericFeature<IOsmParkingSpacesProperties>,
    IParkingLocation
> {
    public name: string = "OsmParkingSpaceTransformation";

    protected transformInternal = (element: IGenericFeature<IOsmParkingSpacesProperties>) => {
        const transformed: IParkingLocation = {
            id: `${SourceEnum.OSM}-${element.properties.parking_space_id}`,
            source: SourceEnum.OSM,
            source_id: element.properties.parking_id,
            data_provider: "www.openstreetmap.org",
            location: element.geometry as TGeoCoordinates,
            centroid: getPointOnFeature(element.geometry as TGeoCoordinates),
            total_spot_number: element.properties.capacity,
            special_access: element.properties.access ? [element.properties.access] : null,
        };

        return transformed;
    };
}

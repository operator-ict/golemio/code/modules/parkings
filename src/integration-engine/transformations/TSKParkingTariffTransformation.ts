import { IParkingTariff } from "#sch/models/interfaces/IParkingTariff";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { Parkings } from "#sch";
import getUuidByString from "uuid-by-string";
import { TskCalculationHelper } from "#ie/helpers/TskCalculationHelper";

const DayMap: Record<string, string> = {
    "1": "Mo",
    "2": "Tu",
    "3": "We",
    "4": "Th",
    "5": "Fr",
    "6": "Sa",
    "7": "Su", // 0 in input data
};

export class TSKParkingTariffTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly dataSource: string;

    constructor() {
        super();
        this.name = Parkings.tsk.name + "Data";
        this.dataSource = "tsk";
    }

    public transform = async (data: { tsk: Record<string, any>; lastUpdated: Date }): Promise<IParkingTariff[]> => {
        const { tsk, lastUpdated } = data;
        const tariffMap = tsk.reduce(
            (acc: any, item: any) => ({ ...acc, [item["CTARIF"]]: [...(acc[item["CTARIF"]] || []), item] }),
            {}
        );

        const dayAggregation = [];
        for (const key in tariffMap) {
            dayAggregation.push(this.aggregateByDay(tariffMap[key]));
        }

        const flattenHoliday = [];
        for (const key in dayAggregation) {
            flattenHoliday.push(this.flattenPayAtHoliday(dayAggregation[key]));
        }

        const result = [];
        for (const key in flattenHoliday) {
            result.push(...this.transformElement({ data: flattenHoliday[key], lastUpdated }));
        }

        return result;
    };

    private hasEqualDescription = (itemA: any, itemB: any): boolean =>
        itemA.TimeFrom === itemB.TimeFrom &&
        itemA.TimeTo === itemB.TimeTo &&
        itemA.PricePerHour == itemB.PricePerHour &&
        itemA.MaxParkingTime === itemB.MaxParkingTime &&
        itemA.MaxPrice === itemB.MaxPrice &&
        itemA.MinPrice === itemB.MinPrice;

    private aggregateByDay = (tariffMapItem: any) => {
        return tariffMapItem.reduce((acc: any, current: any) => {
            const ind = acc.findIndex((item: any) => {
                return item.PayAtHoliday === current.PayAtHoliday && this.hasEqualDescription(item, current);
            });
            const { Day, ...rest } = current;
            if (ind === -1) {
                acc.push({ ...rest, Day: [Day === "0" ? "7" : Day] });
            } else {
                acc[ind].Day.push(Day === "0" ? "7" : Day);
            }
            return acc;
        }, []);
    };

    private flattenPayAtHoliday = (tariffMapItem: any) => {
        return tariffMapItem.reduce((acc: any, current: any) => {
            const ind = acc.findIndex((item: any) => {
                return (
                    this.hasEqualDescription(item, current) &&
                    JSON.stringify(item.Day.sort()) === JSON.stringify(current.Day.sort())
                );
            });
            const { PayAtHoliday, Day, ...rest } = current;
            if (ind === -1) {
                acc.push({ ...rest, Day: Day.sort(), PayAtHoliday: [PayAtHoliday] });
            } else {
                acc[ind].PayAtHoliday.push(PayAtHoliday);
            }
            return acc;
        }, []);
    };

    private dayFormatter = (data: string[]): string => {
        let seqLength = 0;
        let dateString = `${DayMap[data[0]]}`;

        for (let i = 1; i < data.length; i++) {
            if (+data[i - 1] + 1 === +data[i] && data[i + 1]) {
                seqLength++;
                continue;
            }
            seqLength ? (dateString += `-${DayMap[data[i]]}`) : (dateString += `,${DayMap[data[i]]}`);
        }

        return dateString;
    };

    private openingHoursFormatter = (day: string, time: string, payAtHoliday: string[]) => {
        const payAtHolidayText = payAtHoliday.length === 1 ? (payAtHoliday[0] === "YES" ? "; PH only" : "; PH off") : "";
        return `${day} ${time}` + `${payAtHolidayText}`;
    };

    protected transformElement = (element: { data: any; lastUpdated: Date }): IParkingTariff[] => {
        const { lastUpdated, data } = element;
        const result: IParkingTariff[] = [];
        for (const item of data) {
            const dayString = this.dayFormatter(item.Day);
            const desc = {
                tariff_id: getUuidByString(`${this.dataSource}`.toUpperCase() + item.CTARIF),
                last_updated: lastUpdated.toISOString(),
                source: this.dataSource,
                payment_mode: "",
                payment_additional_description: "Parkování Praha",
                free_of_charge: item.PricePerHour == 0,
                charge_band_name: item.CTARIF,
                charge_currency: "CZK",
                start_time_of_period: this.openingHoursFormatter(dayString, item.TimeFrom, item.PayAtHoliday),
                end_time_of_period: this.openingHoursFormatter(dayString, item.TimeTo, item.PayAtHoliday),
            };

            result.push({
                ...desc,
                charge_interval: TskCalculationHelper.getChargeInterval(+item.MinPrice, +item.PricePerHour),
                max_iterations_of_charge: TskCalculationHelper.getMaxChargeIterations(
                    +item.MaxParkingTime,
                    +item.MinPrice,
                    +item.PricePerHour
                ),
                charge: TskCalculationHelper.getCharge(+item.MinPrice, +item.PricePerHour),
                charge_order_index: result.length,
                accepts_litacka: true,
            });

            if (+item.PricePerHour !== 0 && item.MaxPrice !== "0") {
                result.push({
                    ...desc,
                    charge: +item.MaxPrice,
                    charge_order_index: result.length,
                    charge_type: "maximum",
                    accepts_litacka: true,
                });
            }
        }
        return result;
    };
}

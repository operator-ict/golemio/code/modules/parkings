import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IParkingTariff } from "#sch/models/interfaces/IParkingTariff";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { IStaticParkingTariff } from "#sch/datasources/interfaces/IStaticParkingTariff";

export class StaticTariffsTransformation extends BaseTransformation implements ITransformation {
    name: string = "StaticTariffsTransformation";

    public transform = async (tariffs: IStaticParkingTariff[]): Promise<IParkingTariff[]> => {
        const result: IParkingTariff[] = [];

        for (const tariff of tariffs) {
            result.push(...this.transformElement(tariff));
        }
        return result;
    };

    protected transformElement = (tariff: IStaticParkingTariff): IParkingTariff[] => {
        const result: IParkingTariff[] = [];

        for (const charge of tariff.charges) {
            result.push({
                tariff_id: tariff.tariff_id,
                source: SourceEnum.Manual,
                last_updated: tariff.last_updated,
                payment_mode: tariff.payment_mode,
                payment_additional_description: tariff.payment_additional_description ?? undefined,
                free_of_charge: tariff.free_of_charge,
                url_link_address: tariff.url_link_address ?? undefined,
                charge_band_name: tariff.charge_band_name,
                charge_currency: tariff.charge_currency,
                charge: charge.charge,
                charge_type: charge.charge_type ?? undefined,
                charge_order_index: charge.charge_order_index,
                charge_interval: charge.charge_interval ?? undefined,
                max_iterations_of_charge: charge.max_iterations_of_charge ?? undefined,
                min_iterations_of_charge: charge.min_iterations_of_charge ?? undefined,
                start_time_of_period: charge.start_time_of_period ?? undefined,
                end_time_of_period: charge.end_time_of_period ?? undefined,
                allowed_vehicle_type: tariff.allowed_vehicle_type ?? undefined,
                allowed_fuel_type: tariff.allowed_fuel_type ?? undefined,
                accepts_litacka: tariff.payment_methods.includes("litacka"),
                accepts_cash: tariff.payment_methods.includes("cash"),
                accepts_payment_card: tariff.payment_methods.includes("card_offline"),
                accepts_mobile_payment: tariff.payment_methods.includes("mobile_app"),
            });
        }

        return result;
    };
}

export class TskParkingFilter {
    public filterUniqueParkings(data: any): Array<Record<string, any>> {
        const filtered = new Map();
        const invalidDuplicates: string[] = [];

        for (const parking of data) {
            if (filtered.has(parking.properties.tarifTab)) {
                let conflictingParking = filtered.get(parking.properties.tarifTab);
                if (!this.isSameParking(parking, conflictingParking)) {
                    filtered.delete(parking.properties.tarifTab);
                    invalidDuplicates.push(parking.properties.tarifTab);
                }
            } else if (!invalidDuplicates.includes(parking.properties.tarifTab)) {
                filtered.set(parking.properties.tarifTab, parking);
            }
        }
        return Array.from(filtered.values());
    }

    public isSameParking(a: Record<string, any>, b: Record<string, any>): boolean {
        const props = ["tarifTab", "ps_zps_celkem", "typZony", "platnostOd", "platnostDo", "ulice", "TARIF", "CTARIF"];
        for (const p of props) {
            if (a.properties[p] !== b.properties[p]) {
                return false;
            }
        }
        return true;
    }
}

import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IParkingMeasurements } from "#ie/ParkingInterface";
import { IBedrichovParking } from "#sch/datasources/interfaces/IBedrichovParking";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class BedrichovMeasurementsTransformation extends AbstractTransformation<IBedrichovParking, IParkingMeasurements> {
    public name = "BedrichovMeasurementsTransformation";
    protected actualDate: Date = new Date();

    constructor() {
        super();
    }

    protected transformInternal = (data: IBedrichovParking): IParkingMeasurements => {
        return {
            source: SourceEnum.Bedrichov,
            source_id: data.id,
            parking_id: `${SourceEnum.Bedrichov}-${data.id.toString()}`,
            total_spot_number: data.occupancy.overall.total,
            available_spot_number: data.occupancy.overall.free,
            occupied_spot_number:
                Number.isInteger(data.occupancy.overall.total) && Number.isInteger(data.occupancy.overall.free)
                    ? data.occupancy.overall.total - data.occupancy.overall.free
                    : null,
            date_modified: this.actualDate.toISOString(),
        };
    };
}

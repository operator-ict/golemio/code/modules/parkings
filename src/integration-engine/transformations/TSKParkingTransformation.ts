import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IPayment } from "#sch/models/interfaces/IPayment";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { Parkings } from "#sch";
import getUuidByString from "uuid-by-string";
import { IParking } from "#ie/ParkingInterface";
import { config } from "@golemio/core/dist/integration-engine/config";

const ZoneType = {
    RES: "zone_residential",
    MIX: "zone_mixed",
    VIS: "zone_visitors",
    OST: "zone_other",
    // FREE: "zone_free", // (placeholder pro Zóny neplaceného stání, které se budou integrovat v budoucnu)
};

export class TSKParkingTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly dataSource: string;
    public transformationDate: string;

    constructor(transformationDate: string) {
        super();
        this.name = Parkings.tsk.name + "Data";
        this.dataSource = SourceEnum.TSK;
        this.transformationDate = transformationDate;
    }

    public transform = async (data: Array<Record<string, any>>): Promise<{ parking: IParking[]; payment: IPayment[] }> => {
        const result: { parking: IParking[]; payment: IPayment[] } = {
            parking: [],
            payment: [],
        };

        for (const element of data) {
            const { parking, payment } = this.transformElement(element);
            result.parking.push(parking);
            result.payment.push(payment);
        }

        return result;
    };

    protected transformElement = (element: any): { parking: IParking; payment: IPayment } => {
        const id = `${this.dataSource}-${element.properties.tarifTab}`.toLowerCase();
        return {
            parking: {
                id,
                source: this.dataSource,
                source_id: "" + element.properties.tarifTab,
                data_provider: "www.tsk-praha.cz",
                location: element.geometry,
                total_spot_number: element.properties.ps_zps_celkem,
                name: element.properties.ulice,
                category: ZoneType[element.properties.typZony as keyof typeof ZoneType],
                tariff_id: getUuidByString(`${this.dataSource}`.toUpperCase() + element.properties.CTARIF),
                valid_from: element.properties.platnostOd,
                valid_to: element.properties.platnostDo,
                parking_type: "on_street",
                zone_type: ZoneType[element.properties.typZony as keyof typeof ZoneType],
                date_modified: this.transformationDate,
                active: true,
            },
            payment: {
                parking_id: id,
                source: this.dataSource,
                payment_web_url: config.PARKING_ZONES_PAYMENT_URL + "?shortname=" + element.properties.tarifTab,
            },
        };
    };
}

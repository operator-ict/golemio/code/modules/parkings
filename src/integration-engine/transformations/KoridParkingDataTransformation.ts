import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { Parkings } from "#sch";
import { IParkingMeasurements } from "#ie/ParkingInterface";

export class KoridParkingDataTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = Parkings.korid.name + "Data";
    }

    public transform = async (data: any): Promise<IParkingMeasurements[]> => {
        const results = [];
        const dateModified = new Date(data.time).toISOString();

        for (const elementKey in data.data) {
            if (data.data.hasOwnProperty(elementKey)) {
                results.push(
                    this.transformElement({
                        ...data.data[elementKey],
                        sourceId: elementKey,
                        time: dateModified,
                    })
                );
            }
        }
        return results;
    };

    protected transformElement = (element: any): IParkingMeasurements => {
        const sourceId = "" + element.sourceId;
        delete element.sourceId;

        return {
            // id: autoincrement
            source: "korid",
            source_id: sourceId,
            parking_id: `korid-${sourceId}`,
            available_spot_number: element.fr,
            closed_spot_number: element.cls,
            occupied_spot_number: element.occ,
            total_spot_number: element.tot,
            date_modified: element.time,
        };
    };
}

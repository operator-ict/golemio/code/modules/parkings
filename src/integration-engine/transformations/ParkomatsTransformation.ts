import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { Parkomats } from "#sch";
import { IParkomatDto } from "#sch/parkomats/models/interfaces/IParkomatDto";
import { DateTime } from "@golemio/core/dist/helpers";
import { IParkomatInput } from "#sch/parkomats/datasources/interfaces/IParkomatInput";

export class ParkomatsTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = Parkomats.name;
    }

    protected transformElement = async (data: IParkomatInput): Promise<IParkomatDto> => {
        const ticketBought = data.DateTime ? DateTime.fromISO(data.DateTime) : null;
        const validityFrom = data.DateFrom ? DateTime.fromISO(data.DateFrom) : null;
        const validityTo = data.DateTo ? DateTime.fromISO(data.DateTo) : null;

        const res: IParkomatDto = {
            channel: data.Channel,
            parking_zone: data.Section,
            price: data.Price,
            ticket_bought: ticketBought ? ticketBought.toISOString() : null,
            transaction_id: data.Id,
            validity_from: validityFrom ? validityFrom.toISOString() : null,
            validity_to: validityTo ? validityTo.toISOString() : null,
        };

        return res;
    };
}

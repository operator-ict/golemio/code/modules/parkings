import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { ITskParkingMachine } from "#sch/datasources/interfaces/ITskParkingMachine";
import { IParkingMachine } from "#sch/models/interfaces/IParkingMachine";
import { Geometry } from "@golemio/core/dist/shared/geojson";
import { wktToGeoJSON } from "@terraformer/wkt";
import { SourceEnum } from "#helpers/constants/SourceEnum";

export class TskParkingMachinesTransformation extends AbstractTransformation<ITskParkingMachine, IParkingMachine> {
    public name: string = "TskParkingMachinesTransformation";

    transformInternal = (tskParkingMachine: ITskParkingMachine): IParkingMachine => {
        return {
            id: `${SourceEnum.TSK_V2}-${tskParkingMachine.idParkMachine}`,
            source: SourceEnum.TSK_V2,
            sourceId: tskParkingMachine.idParkMachine,
            code: tskParkingMachine.code,
            type: tskParkingMachine.idStatus == 5 || tskParkingMachine.idStatus == 6 ? "info_box" : "payment_machine",
            location: wktToGeoJSON(tskParkingMachine.positionWKT) as Geometry,
            validFrom: new Date(tskParkingMachine.activeFrom),
            tariffId: tskParkingMachine.idTariff,
        };
    };
}

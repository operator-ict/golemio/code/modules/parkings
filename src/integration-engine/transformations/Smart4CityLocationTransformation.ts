import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IParking } from "#ie/ParkingInterface";
import { ISmart4CityLocation } from "#sch/datasources/interfaces/ISmart4CityLocation";
import { IParkingLocation } from "#sch/models/interfaces/IParkingLocation";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { GeoCoordinatesType, IGeoCoordinatesPoint } from "@golemio/core/dist/output-gateway";
import getUuidByString from "uuid-by-string";

interface ISmart4CityLocationTransformed extends IParking {
    parkingLocation: IParkingLocation;
}

export interface ISmart4CityLocationCollectionTransformed {
    parking: IParking[];
    parkingLocation: IParkingLocation[];
}

const getSmart4cityParkingType = (data: number | null) => {
    switch (data) {
        case 1:
            return "surface";
        case 2:
            return "on_street";
        default:
            return null;
    }
};

export class Smart4CityLocationTransformation extends AbstractTransformation<
    ISmart4CityLocation,
    ISmart4CityLocationTransformed
> {
    public name = "Smart4CityLocationTransformation";

    constructor(private code: string, private transformationDate: Date) {
        super();
    }

    protected transformInternal = (data: ISmart4CityLocation): ISmart4CityLocationTransformed => {
        const location: IGeoCoordinatesPoint = {
            type: GeoCoordinatesType.Point,
            coordinates: [data.longitude, data.latitude],
        };

        const uuid = getUuidByString(SourceEnum.Smart4City + data.id.toString());

        const parking: IParking = {
            id: `${SourceEnum.Smart4City}-${uuid}`,
            source: SourceEnum.Smart4City,
            source_id: uuid,
            data_provider: `www.smart4city.cz-${this.code}`,
            name: data.name,
            location,
            centroid: location,
            total_spot_number: data.capacity !== 0 ? data.capacity : null,
            date_modified: this.transformationDate.toISOString(),
            valid_from: this.transformationDate.toISOString(),
            parking_type: getSmart4cityParkingType(data.type),
            active: true,
        };

        return {
            ...parking,
            parkingLocation: this.transformLocation(parking),
        };
    };

    public transformCollection = (data: ISmart4CityLocation[]): ISmart4CityLocationCollectionTransformed => {
        const result: ISmart4CityLocationCollectionTransformed = {
            parking: [],
            parkingLocation: [],
        };

        for (const el of data) {
            if (!el.closed) {
                const { parkingLocation, ...parking } = this.transformElement(el);
                result.parking.push(parking);
                result.parkingLocation.push(parkingLocation);
            }
        }

        return result;
    };

    private transformLocation = (parking: IParking): IParkingLocation => {
        return {
            id: parking.id,
            source: parking.source,
            source_id: parking.source_id,
            data_provider: parking.data_provider,
            location: parking.location,
            centroid: parking.centroid,
            total_spot_number: parking.total_spot_number,
        };
    };
}

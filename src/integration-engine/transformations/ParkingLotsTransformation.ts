import { IPayment } from "#sch/models/interfaces/IPayment";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { Parkings } from "#sch";
import { IParking, IParkingMeasurements } from "#ie/ParkingInterface";
import { GeoCoordinatesType } from "@golemio/core/dist/output-gateway/Geo";
import { config } from "@golemio/core/dist/integration-engine/config";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IStaticParkingLotsGeoFeature } from "#sch/datasources/interfaces/IStaticParkingLotsGeo";
import { SourceEnum } from "#helpers/constants/SourceEnum";

const ParkingLotType = {
    true: "park_and_ride",
    false: "park_paid_private",
};

export interface IParkingLotsTransform {
    geo: IParking[];
    measurements: IParkingMeasurements[];
    payments: IPayment[];
}

export class ParkingLotsTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly dataSource: string;

    constructor() {
        super();
        this.name = Parkings.tsk.name + "Data";
        this.dataSource = SourceEnum.TSK;
    }

    public transform = async (data: any, customGeoFeatures?: IStaticParkingLotsGeoFeature[]): Promise<IParkingLotsTransform> => {
        const result: IParkingLotsTransform = { geo: [], measurements: [], payments: [] };
        for (const element of data) {
            const transformedData = this.transformElement(element);

            // "optional" geo location enhancement
            if (customGeoFeatures) {
                for (const customGeoFeature of customGeoFeatures) {
                    if (customGeoFeature.properties.id === transformedData.geo.id) {
                        transformedData.geo = {
                            ...transformedData.geo,
                            location: customGeoFeature.geometry,
                        };
                        break;
                    }
                }
            }

            result.geo.push(transformedData.geo);
            result.measurements.push(transformedData.measurements);
            if (transformedData.payments) {
                result.payments.push(transformedData.payments);
            }
        }
        return result;
    };

    protected transformElement = (element: any): { geo: IParking; measurements: IParkingMeasurements; payments?: IPayment } => {
        const paymentUrlShortname = this.getMPLAPaymentShortName(element.id);
        const parkingId = `${this.dataSource}-${element.id}`.toLowerCase();

        if (isNaN(parseInt(element.lastUpdated, 10))) {
            throw new GeneralError(`Could not parse date: ${element.lastUpdated}`, "ParkingLotsTransformation");
        }

        const dateModified = new Date(element.lastUpdated).toISOString();

        // location is always a Point, so no need to use turf for centroid
        return {
            geo: {
                id: parkingId,
                source: this.dataSource,
                source_id: "" + element.id,
                data_provider: "www.tsk-praha.cz",
                location: {
                    coordinates: [parseFloat(element.lng), parseFloat(element.lat)],
                    type: GeoCoordinatesType.Point,
                },
                total_spot_number: element.totalNumOfPlaces,
                name: element.name,
                category: ParkingLotType[element.pr as keyof typeof ParkingLotType],
                parking_type: ParkingLotType[element.pr as keyof typeof ParkingLotType],
                centroid: {
                    coordinates: [parseFloat(element.lng), parseFloat(element.lat)],
                    type: GeoCoordinatesType.Point,
                },
                date_modified: dateModified,
                active: true,
            },
            measurements: {
                source: this.dataSource,
                source_id: "" + element.id,
                parking_id: parkingId,
                available_spot_number: element.numOfFreePlaces,
                occupied_spot_number: element.numOfTakenPlaces,
                total_spot_number: element.totalNumOfPlaces,
                date_modified: dateModified,
            },
            ...(paymentUrlShortname && {
                payments: {
                    parking_id: parkingId,
                    source: this.dataSource,
                    payment_web_url: `${config.PARKINGS_PAYMENT_URL}?shortname=${paymentUrlShortname}`,
                },
            }),
        };
    };

    private getMPLAPaymentShortName = (id: number): string | null => {
        switch (id) {
            case 534002:
                return "139"; // Holešovice
            case 534004:
                return "142"; // Rajska Zahrada
            case 534007:
                return "141"; // Radotín
            case 534008:
                return "144"; // Zličín 1
            case 534009:
                return "145"; // Zličín 2
            case 534010:
                return "143"; // Skalka 2
            case 534011:
                return "147"; // Černý most
            case 534012:
                return "143"; // Skalka 1
            case 534014:
                return "121"; // Ládví
            case 534015:
                return "122"; // Depo Hostivař
            case 534016:
                return "123"; // Letňany
            case 534017:
                return "106"; // Westfield Chodov
            case 5340171:
                return "106"; // Chodov A
            case 5340172:
                return "106"; // Chodov E
            default:
                return null;
        }
    };
}

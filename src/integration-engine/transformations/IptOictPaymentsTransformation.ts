import { IIptOictPaymentsProperties } from "#sch/datasources/iptoict/interfaces/IIptOictPaymentsProperties";
import { IPayment } from "#sch/models/interfaces/IPayment";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";

export class IptOictPaymentsTransformation extends AbstractTransformation<IIptOictPaymentsProperties, IPayment> {
    public name: string = "IptOictPaymentsTransformation";

    constructor(private source: string) {
        super();
    }

    protected transformInternal = (element: IIptOictPaymentsProperties): IPayment => {
        return {
            parking_id: element.parking_id,
            source: this.source,
            payment_web_url: element.payment.web_url ?? null,
            payment_android_url: element.payment.android_url ?? null,
            payment_ios_url: element.payment.ios_url ?? null,
            payment_discovery_url: element.payment.discovery_url ?? null,
            reservation_type: element.reservation.reservation_type ?? null,
            reservation_web_url: element.reservation.web_url ?? null,
            reservation_android_url: element.reservation.android_url ?? null,
            reservation_ios_url: element.reservation.ios_url ?? null,
            reservation_discovery_url: element.reservation.discovery_url ?? null,
        };
    };
}

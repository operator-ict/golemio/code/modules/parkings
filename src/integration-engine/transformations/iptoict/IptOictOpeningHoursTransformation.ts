import { IGenericFeature } from "#ie/datasources/interfaces/IGenericFeature";
import { IIptOictParkingProperties } from "#sch/datasources/iptoict/interfaces/IIptOictParkingProperties";
import { IOpeningHours } from "#sch/models/interfaces/openingHours/IOpeningHours";
import { IOpeningHoursPeriod } from "#sch/models/interfaces/openingHours/IOpeningHoursPeriod";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class IptOictOpeningHoursTransformation extends AbstractTransformation<
    IGenericFeature<IIptOictParkingProperties>,
    IOpeningHours[]
> {
    public name: string = "IsphkOpeningHoursTransformation";

    constructor(private source: string) {
        super();
    }

    protected transformInternal = (element: IGenericFeature<IIptOictParkingProperties>) => {
        const result: IOpeningHours[] = [];

        if (element.properties.opening_hours) {
            for (const openingHour of element.properties.opening_hours) {
                const transformed: IOpeningHours = {
                    parking_id: `${this.source}-${element.properties.source_id}`,
                    source: this.source,
                    valid_from: openingHour.valid_from,
                    valid_to: openingHour.valid_to,
                    periods_of_time: openingHour.periods_of_time as IOpeningHoursPeriod[],
                } as IOpeningHours;
                result.push(transformed);
            }
        }

        return result;
    };
}

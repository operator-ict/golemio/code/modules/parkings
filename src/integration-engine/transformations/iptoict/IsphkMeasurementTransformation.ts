import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IIsphkMeasurementsItem } from "#sch/datasources/isphk/interfaces/IIsphkMeasurements";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IIsphkMeasurementSchema } from "./IIsphkMeasurementSchema";

export class IsphkMeasurementTransformation extends AbstractTransformation<IIsphkMeasurementsItem, IIsphkMeasurementSchema> {
    public name = "IsphkMeasurementTransformation";

    constructor(private transformationDate: Date) {
        super();
    }

    protected transformInternal = (data: IIsphkMeasurementsItem): IIsphkMeasurementSchema => {
        return {
            source: SourceEnum.Isphk,
            source_id: data.id,
            parking_id: `${SourceEnum.Isphk}-${data.id}`,
            total_spot_number: data.capacity,
            available_spot_number: data.free,
            occupied_spot_number: data.capacity - data.free,
            date_modified: this.transformationDate.toISOString(),
        };
    };
}

export interface IIsphkMeasurementSchema {
    source: string;
    source_id: string;
    available_spot_number: number;
    occupied_spot_number: number;
    total_spot_number: number;
    date_modified: string;
    parking_id: string;
}

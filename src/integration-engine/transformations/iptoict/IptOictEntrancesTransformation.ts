import { IGenericFeature } from "#ie/datasources/interfaces/IGenericFeature";
import { IIptOictEntrancesProperties } from "#sch/datasources/interfaces/IIptOictEntrancesProperties";
import { IEntrance } from "#sch/models/interfaces/IEntrance";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";

export class IptOictEntrancesTransformation extends AbstractTransformation<
    IGenericFeature<IIptOictEntrancesProperties>,
    IEntrance
> {
    public name: string = "IptOictEntrancesTransformation";

    constructor(private source: string) {
        super();
    }

    protected transformInternal = (element: IGenericFeature<IIptOictEntrancesProperties>): IEntrance => {
        return {
            entrance_id: element.properties.source_id,
            source: this.source,
            parking_id: `${this.source}-${element.properties.parking_id}`,
            location: element.geometry as TGeoCoordinates,
            entry: element.properties?.entry ?? null,
            exit: element.properties?.exit ?? null,
            entrance_type: element.properties?.entrance_type?.length ? element.properties?.entrance_type : null,
            level: element.properties.level,
            max_height: element.properties?.dimension?.max_height,
            max_width: element.properties?.dimension?.max_width ?? null,
            max_length: element.properties?.dimension?.max_length ?? null,
            max_weight: element.properties?.dimension?.max_weight ?? null,
        };
    };
}

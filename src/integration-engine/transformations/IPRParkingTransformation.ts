import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { Parkings } from "#sch";
import { IGeoCoordinatesMultiPolygon, IGeoCoordinatesPolygon } from "@golemio/core/dist/output-gateway/Geo";
import { IIPRParkingJsonFeature } from "#sch/datasources/interfaces/IIPRParkingJsonSchema";

export interface IIPRParkingElementTransformed {
    id: string;
    location: IGeoCoordinatesPolygon | IGeoCoordinatesMultiPolygon;
    total_spot_number: number;
}

interface IIPRParkingElementData extends IIPRParkingElementTransformed {
    source_id: string;
}

export interface IIPRParkingTransformed {
    [key: string]: IIPRParkingElementTransformed[];
}

export class IPRParkingTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = Parkings.ipr.name + "Data";
    }

    public transform = async (data: IIPRParkingJsonFeature[]): Promise<IIPRParkingTransformed> => {
        const result: IIPRParkingTransformed = {};
        for (const element of data) {
            const item = this.transformElement(element);

            if (result[item.source_id]) {
                result[item.source_id].push({
                    id: item.id,
                    location: item.location,
                    total_spot_number: item.total_spot_number,
                });
            } else {
                result[item.source_id] = [
                    {
                        id: item.id,
                        location: item.location,
                        total_spot_number: item.total_spot_number,
                    },
                ];
            }
        }
        return result;
    };

    protected transformElement = (element: IIPRParkingJsonFeature): IIPRParkingElementData => {
        return {
            id: element.properties.ZPS_ID,
            source_id: element.properties.TARIFTAB,
            location: element.geometry,
            total_spot_number: +element.properties.PS_ZPS,
        };
    };
}

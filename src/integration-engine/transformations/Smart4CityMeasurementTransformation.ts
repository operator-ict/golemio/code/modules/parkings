import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IParkingMeasurements } from "#ie/ParkingInterface";
import { ISmart4CityLocation } from "#sch/datasources/interfaces/ISmart4CityLocation";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import getUuidByString from "uuid-by-string";

export class Smart4CityMeasurementTransformation extends AbstractTransformation<ISmart4CityLocation, IParkingMeasurements> {
    public name = "Smart4CityLocationTransformation";

    constructor(private transformationDate: Date) {
        super();
    }

    protected transformInternal = (data: ISmart4CityLocation): IParkingMeasurements => {
        const uuid = getUuidByString(SourceEnum.Smart4City + data.id.toString());

        return {
            source: SourceEnum.Smart4City,
            source_id: uuid,
            parking_id: `${SourceEnum.Smart4City}-${uuid}`,
            total_spot_number: data.capacity,
            available_spot_number: data.correctionunoccupied,
            occupied_spot_number:
                Number.isInteger(data.capacity) && Number.isInteger(data.correctionunoccupied)
                    ? data.capacity - data.correctionunoccupied!
                    : null,
            date_modified: this.transformationDate.toISOString(),
        };
    };

    public transformArray = (data: ISmart4CityLocation[]): IParkingMeasurements[] => {
        const result: IParkingMeasurements[] = [];

        for (const el of data) {
            if (!el.closed && el.capacity && el.correctionunoccupied !== null) {
                result.push(this.transformElement(el));
            }
        }
        return result;
    };
}

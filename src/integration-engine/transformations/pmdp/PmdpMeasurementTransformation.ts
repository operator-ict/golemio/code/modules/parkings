import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IPmdpMeasurementSchema } from "#ie/transformations/pmdp/IPmdpMeasurementSchema";
import { IPmdpMeasurement } from "#sch/datasources/pmdp/interfaces/IPmdpMeasurement";
import { IParking } from "#sch/models/interfaces/IParking";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";

export class PmdpMeasurementTransformation extends AbstractTransformation<IPmdpMeasurement & IParking, IPmdpMeasurementSchema> {
    public name = "PmdpMeasurementTransformation";

    constructor(private transformationDate: Date) {
        super();
    }

    protected transformInternal = (data: IPmdpMeasurement & IParking): IPmdpMeasurementSchema => {
        return {
            source: SourceEnum.PMDP,
            source_id: data.source_id,
            parking_id: data.id,
            total_spot_number: data.total_spot_number,
            available_spot_number: Number(data.volno),
            occupied_spot_number: data.total_spot_number ? data.total_spot_number - Number(data.volno) : null,
            date_modified: this.transformationDate.toISOString(),
        };
    };
}

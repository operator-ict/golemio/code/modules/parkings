export interface IPmdpMeasurementSchema {
    source: string;
    source_id: string;
    available_spot_number: number;
    occupied_spot_number: number | null;
    total_spot_number: number | null;
    date_modified: string;
    parking_id: string;
}

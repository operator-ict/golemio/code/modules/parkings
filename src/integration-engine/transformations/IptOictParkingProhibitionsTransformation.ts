import { IptoictParkingProhibitions } from "#sch/datasources/iptoict/interfaces/IptOictParkingProhibitions";
import { IParkingProhibitions } from "#sch/models/interfaces/IParkingProhibitions";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";

export class IptOictParkingProhibitionsTransformation extends AbstractTransformation<
    IptoictParkingProhibitions,
    IParkingProhibitions
> {
    public name: string = "IptOictParkingProhibitionsTransformation";

    constructor(private source: string) {
        super();
    }

    protected transformInternal = (element: IptoictParkingProhibitions): IParkingProhibitions => {
        return {
            parking_id: `${this.source}-${element.parking_id}`,
            source: this.source,
            lpg: element.prohibited_access["lpg/cng"] ?? null,
            bus: element.prohibited_access.bus ?? null,
            truck: element.prohibited_access.truck ?? null,
            motorcycle: element.prohibited_access.motorcycle ?? null,
            bicycle: element.prohibited_access.bicycle ?? null,
            trailer: element.prohibited_access.trailer ?? null,
        };
    };
}

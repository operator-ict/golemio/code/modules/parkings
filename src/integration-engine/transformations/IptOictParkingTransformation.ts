import { IParking } from "#ie/ParkingInterface";
import { IGenericFeature } from "#ie/datasources/interfaces/IGenericFeature";
import { getPointOnFeature } from "#ie/helpers/GeodataHelper";
import { IIptOictParkingProperties, SupervisedEnum } from "#sch/datasources/iptoict/interfaces/IIptOictParkingProperties";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { TGeoCoordinates } from "@golemio/core/dist/output-gateway";

export class IptOictParkingTransformation extends AbstractTransformation<IGenericFeature<IIptOictParkingProperties>, IParking> {
    public name: string = "IptOictParkingTransformation";

    constructor(private source: string, private transformationDate: Date) {
        super();
    }

    protected transformInternal = (element: IGenericFeature<IIptOictParkingProperties>) => {
        const transformed: IParking = {
            id: `${this.source}-${element.properties.source_id}`,
            source: this.source,
            source_id: element.properties.source_id,
            data_provider: "manual",
            location: element.geometry as TGeoCoordinates,
            name: element.properties.name,
            parking_type: element.properties.parking_structure ? element.properties.parking_structure : "other",
            centroid: getPointOnFeature(element.geometry as TGeoCoordinates),
            covered: element.properties.covered,
            security: element.properties.supervised ? element.properties.supervised === SupervisedEnum.Yes : null,
            contact:
                element.properties.email || element.properties.phone || element.properties.website
                    ? {
                          email: element.properties.email ?? null,
                          phone: element.properties.phone ?? null,
                          website: element.properties.website ?? null,
                      }
                    : null,
            date_modified: this.transformationDate.toISOString(),
            total_spot_number: element.properties.capacity,
            max_vehicle_dimensions: null,
            parking_policy: element.properties.parking_policy ?? null,
            active: true,
        };

        return transformed;
    };
}

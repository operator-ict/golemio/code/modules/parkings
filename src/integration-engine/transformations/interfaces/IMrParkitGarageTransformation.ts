import { IParking, IParkingMeasurements, TTariffData } from "#ie/ParkingInterface";
import { IMrParkitGarageWithTariff } from "#sch/datasources/interfaces/IMrParkitGarage";
import { IParkingLocation } from "#sch/models/interfaces/IParkingLocation";
import { IParkingProhibitions } from "#sch/models/interfaces/IParkingProhibitions";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";

export interface IMrParkingTransformationDto extends IParking {
    parkingLocation: IParkingLocation;
    parkingMeasurement: IParkingMeasurements;
    tariffs: TTariffData[];
    parkingProhibitions: IParkingProhibitions;
}

export interface IMrParkitGarageTransformation
    extends AbstractTransformation<IMrParkitGarageWithTariff, IMrParkingTransformationDto> {}

import { SourceEnum } from "#helpers/constants/SourceEnum";
import {
    ITskParkingZoneTariff,
    ITskParkingZoneTariffDefinition,
    ITskParkingZoneTariffGroup,
} from "#sch/datasources/interfaces/ITskParkingZoneTariff";
import { IParkingTariff, IParkingTariffPeriod } from "#sch/models/interfaces/IParkingTariff";
import { DateTime, dateTime } from "@golemio/core/dist/helpers";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";

enum DaysInWeekEnum {
    "monday" = "Mo",
    "tuesday" = "Tu",
    "wednesday" = "We",
    "thursday" = "Th",
    "friday" = "Fr",
    "saturday" = "Sa",
    "sunday" = "Su",
}

type TDays = keyof typeof DaysInWeekEnum;

type ITariffCommonInfo = Pick<
    IParkingTariff,
    | "tariff_id"
    | "source"
    | "last_updated"
    | "payment_mode"
    | "free_of_charge"
    | "charge_band_name"
    | "accepts_mobile_payment"
    | "accepts_litacka"
    | "charge_currency"
    | "valid_from"
    | "valid_to"
>;

export class TskParkingZonesTariffsTransformation extends AbstractTransformation<ITskParkingZoneTariff, IParkingTariff[]> {
    public name: string = "TskParkingZonesTariffsTransformation";
    public orderIdx: number = 0;

    constructor(private transformationDate: Date) {
        super();
    }

    public transformInternal = (item: ITskParkingZoneTariff): IParkingTariff[] => {
        this.orderIdx = 0;
        let result: IParkingTariff[] = [];

        const tariffCommonInfo: ITariffCommonInfo = {
            tariff_id: item.id,
            source: SourceEnum.TSK_V2,
            last_updated: this.transformationDate.toISOString(),
            payment_mode: "pre_paid",
            free_of_charge: false,
            charge_band_name: item.name || "",
            accepts_mobile_payment: true,
            accepts_litacka: true,
            charge_currency: "czk",
        };

        for (const tg of item.tariffGroups || []) {
            for (const td of tg.tariffDefinitions || []) {
                const validityFrom = DateTime.max(
                    DateTime.fromISO(td.activeFrom, { timeZone: "Europe/Prague" }).toISOString(),
                    DateTime.fromISO(tg.activeFrom, { timeZone: "Europe/Prague" }).toISOString(),
                    DateTime.fromISO(item.activeFrom, { timeZone: "Europe/Prague" }).toISOString()
                );
                const validityTo = DateTime.min(
                    DateTime.fromISO(td.activeTo, { timeZone: "Europe/Prague" }).toISOString(),
                    DateTime.fromISO(tg.activeTo, { timeZone: "Europe/Prague" }).toISOString(),
                    DateTime.fromISO(item.activeTo, { timeZone: "Europe/Prague" }).toISOString()
                );

                tariffCommonInfo.valid_from = validityFrom.setTimeZone("Europe/Prague").toISOString();
                tariffCommonInfo.valid_to = validityTo.setTimeZone("Europe/Prague").toISOString();

                const periodsOfTime = this.getTariffPeriods(tg, td, item.maxPriceHoliday !== null);

                if (validityTo > dateTime(this.transformationDate)) {
                    if (!!td.minPriceItem) {
                        const chargeInfoMinimum = this.getChargeMinimums(td.minPriceItem, periodsOfTime, tariffCommonInfo);
                        result.push(...chargeInfoMinimum);
                    }

                    const charges = this.getChargeOther(td, item.maxHour, periodsOfTime, tariffCommonInfo);
                    result.push(...charges);

                    if (!!td.maxPriceItem) {
                        const chargeInfoMaximum = this.getChargeMaximums(td.maxPriceItem, periodsOfTime, tariffCommonInfo);
                        result.push(...chargeInfoMaximum);
                    }
                }
            }
        }

        result = this.addGlobalMinMax(item, result, tariffCommonInfo);

        return result;
    };

    private getChargeOther = (
        definition: ITskParkingZoneTariffDefinition,
        maxHour: number | null,
        periods: IParkingTariffPeriod[][],
        tariffCommonInfo: ITariffCommonInfo
    ): IParkingTariff[] =>
        periods.map((period) => ({
            ...tariffCommonInfo,
            charge_order_index: this.orderIdx++,
            charge: definition.pricePerHour / 60,
            charge_type: "other",
            charge_interval: 60,
            max_iterations_of_charge: definition.minuteTo - definition.minuteFrom,
            min_iterations_of_charge: 1,
            ...(maxHour && { maximum_duration_seconds: maxHour * 3600 }),
            periods_of_time: period,
        }));

    private getChargeMinimums = (
        price: number,
        periods: IParkingTariffPeriod[][],
        tariffCommonInfo: ITariffCommonInfo
    ): IParkingTariff[] =>
        periods.map((period) => ({
            ...tariffCommonInfo,
            charge_order_index: this.orderIdx++,
            charge: price,
            charge_type: "minimum",
            periods_of_time: period,
        }));

    private getChargeMaximums = (
        price: number,
        periods: IParkingTariffPeriod[][],
        tariffCommonInfo: ITariffCommonInfo
    ): IParkingTariff[] =>
        periods.map((period) => ({
            ...tariffCommonInfo,
            charge_order_index: this.orderIdx++,
            charge: price,
            charge_type: "maximum",
            periods_of_time: period,
        }));

    private getTariffPeriods = (
        group: ITskParkingZoneTariffGroup,
        definition: ITskParkingZoneTariffDefinition,
        hasHolidayTariff: boolean
    ): IParkingTariffPeriod[][] => {
        const result: IParkingTariffPeriod[] = [];

        for (const day of Object.keys(DaysInWeekEnum)) {
            if (group[day as TDays]) {
                result.push({
                    day_in_week: DaysInWeekEnum[day as TDays],
                    start: definition.timeFrom,
                    end: DateTime.fromFormat(definition.timeTo, "HH:mm:ss", { timeZone: "Europe/Prague" })
                        .subtract(1, "minutes")
                        .format("HH:mm:ss"),
                    ph: "PH_off",
                });
            }
        }

        if (hasHolidayTariff) {
            const holidayPeriods = result.map((el) => ({ ...el, ph: "PH_only" }));

            result.push(...holidayPeriods);
        }

        return [result];
    };

    private getAllPeriods(ph: "PH_off" | "PH_only"): IParkingTariffPeriod[] {
        return Object.keys(DaysInWeekEnum).map((day) => ({
            day_in_week: DaysInWeekEnum[day as TDays],
            start: "00:00:00",
            end: "23:59:00",
            ph: ph,
        }));
    }

    private addGlobalMinMax(
        item: ITskParkingZoneTariff,
        result: IParkingTariff[],
        tariffCommonInfo: ITariffCommonInfo
    ): IParkingTariff[] {
        if (!!item.maxPrice) {
            result.push({
                ...tariffCommonInfo,
                charge_order_index: this.orderIdx++,
                charge: item.maxPrice,
                charge_type: "maximum",
                periods_of_time: this.getAllPeriods("PH_off"),
            });
        }

        if (!!item.minPrice) {
            result.push({
                ...tariffCommonInfo,
                charge_order_index: this.orderIdx++,
                charge: item.minPrice,
                charge_type: "minimum",
                periods_of_time: this.getAllPeriods("PH_off"),
            });
        }

        if (!!item.maxPriceHoliday) {
            result.push({
                ...tariffCommonInfo,
                charge_order_index: this.orderIdx++,
                charge: item.maxPriceHoliday,
                charge_type: "maximum",
                periods_of_time: this.getAllPeriods("PH_only"),
            });
            if (!!item.minPrice) {
                result.push({
                    ...tariffCommonInfo,
                    charge_order_index: this.orderIdx++,
                    charge: item.minPrice,
                    charge_type: "minimum",
                    periods_of_time: this.getAllPeriods("PH_only"),
                });
            }
        }

        return result;
    }
}

import { IIptOictTariffsProperties } from "#sch/datasources/iptoict/interfaces/IIptOictTariffsProperties";
import { IParkingTariff } from "#sch/models/interfaces/IParkingTariff";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import getUuidByString from "uuid-by-string";

export class IptOictTariffsTransformation extends AbstractTransformation<IIptOictTariffsProperties, IParkingTariff[]> {
    public name: string = "IptOictTariffsTransformation";

    constructor(private source: string, private transformationDate: Date) {
        super();
    }

    protected transformInternal = (element: IIptOictTariffsProperties): IParkingTariff[] => {
        const tariffs: IParkingTariff[] = [];

        for (const chargeBands of element.charge_bands) {
            const bandCommon = {
                source: this.source,
                maximum_duration_seconds: chargeBands.maximum_duration,
                valid_from: chargeBands.valid_from ? new Date(chargeBands.valid_from) : null,
                valid_to: chargeBands.valid_to ? new Date(chargeBands.valid_to) : null,
                last_updated: chargeBands.last_modified_at_source
                    ? new Date(chargeBands.last_modified_at_source)
                    : this.transformationDate,
                free_of_charge: chargeBands.free_of_charge,
                url_link_address: chargeBands.url,
                payment_mode: chargeBands.payment_mode,
                payment_methods: chargeBands.payment_methods,
                charge_band_name: chargeBands.charge_band_name,
                accepts_payment_card: chargeBands.payment_methods.includes("card_offline"),
                accepts_cash: chargeBands.payment_methods.includes("cash"),
                accepts_mobile_payment: chargeBands.payment_methods.includes("mobile_app"),
                accepts_litacka: chargeBands.payment_methods.includes("litacka"),
            };

            for (const charge of chargeBands.charges) {
                const chargeItem = {
                    charge: parseInt(charge.charge),
                    charge_currency: "czk",
                    charge_type: charge.charge_type,
                    charge_order_index: charge.charge_order_index,
                    charge_interval: charge.charge_interval,
                    max_iterations_of_charge: charge.max_iterations_of_charge,
                    min_iterations_of_charge: charge.min_iterations_of_charge,
                    valid_from: charge.valid_from ? new Date(charge.valid_from) : null,
                    periods_of_time: charge.periods_of_time,
                };

                const tariffId = getUuidByString(`${this.source}`.toUpperCase() + `${JSON.stringify(element.parking_ids)}`);
                tariffs.push({ ...bandCommon, ...chargeItem, tariff_id: tariffId });
            }
        }

        return tariffs;
    };

    public transformParkingTariffsRelation = (
        data: IIptOictTariffsProperties[]
    ): Array<{ id: string; source: string; source_id: string; tariff_id: string }> => {
        const parkingTariffsIdPairs = [];
        for (const item of data) {
            const tariffId = getUuidByString(`${this.source}`.toUpperCase() + `${JSON.stringify(item.parking_ids)}`);
            for (const id of item.parking_ids) {
                parkingTariffsIdPairs.push({
                    id: `${this.source}-${id}`,
                    source: this.source,
                    source_id: id,
                    tariff_id: tariffId,
                });
            }
        }

        return parkingTariffsIdPairs;
    };
}

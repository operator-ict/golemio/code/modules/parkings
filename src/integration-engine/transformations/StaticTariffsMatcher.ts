import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { StaticTariffRelation } from "#sch/datasources/StaticTariffsJsonSchema";
import { IParking } from "#ie/ParkingInterface";

export class StaticTariffsMatcher {
    private readonly parkingsModel: ParkingsRepository;

    constructor(parkingsModel: ParkingsRepository) {
        this.parkingsModel = parkingsModel;
    }

    public async matchTariffRelations(tariffRelations: StaticTariffRelation[]): Promise<IParking[]> {
        const parkings: IParking[] = [];

        for (const tariffRelation of tariffRelations) {
            let parking = await this.parkingsModel.GetOne(tariffRelation.id);

            if (parking !== null && !parking.tariff_id) {
                parking.tariff_id = tariffRelation.tariff_id;
                parkings.push(parking);
            }
        }

        return parkings;
    }
}

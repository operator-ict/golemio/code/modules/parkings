import { IParking, IParkingMeasurements, ITariffPeriodElement, TTariffData } from "#ie/ParkingInterface";
import { IMrParkitGarageWithTariff } from "#sch/datasources/interfaces/IMrParkitGarage";
import { IParkingLocation } from "#sch/models/interfaces/IParkingLocation";
import { IParkingTariffPeriod } from "#sch/models/interfaces/IParkingTariff";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { GeoCoordinatesType, IGeoCoordinatesPoint } from "@golemio/core/dist/output-gateway";
import { IMrParkingTransformationDto, IMrParkitGarageTransformation } from "./interfaces/IMrParkitGarageTransformation";
import { SourceEnum } from "#helpers/constants/SourceEnum";

type TransformIn = IMrParkitGarageWithTariff;
type TransformOut = IMrParkingTransformationDto;

enum DaysInWeekEnum {
    "monday" = "Mo",
    "tuesday" = "Tu",
    "wednesday" = "We",
    "thursday" = "Th",
    "friday" = "Fr",
    "saturday" = "Sa",
    "sunday" = "Su",
}

enum OffWeekDays {
    "saturday" = "Sa",
    "sunday" = "Su",
}

type TDays = keyof typeof DaysInWeekEnum;

type TOffWeekDay = keyof typeof OffWeekDays;

type TPhType = keyof typeof PhTypes;

enum PhTypes {
    "off" = "ph_off",
    "only" = "ph_only",
}

export class MrParkitGarageTransformation
    extends AbstractTransformation<TransformIn, TransformOut>
    implements IMrParkitGarageTransformation
{
    public name = "MrParkitGarageTransformation";

    constructor(private transformationDate: Date) {
        super();
    }

    protected transformInternal = (data: TransformIn): TransformOut => {
        const location: IGeoCoordinatesPoint = {
            type: GeoCoordinatesType.Point,
            coordinates: [data.longitude, data.latitude],
        };

        const parking: IParking = {
            id: `mr_parkit-${data.id}`,
            source: SourceEnum.Mr_Parkit,
            source_id: data.id,
            data_provider: "www.mrparkit.com",
            name: data.name,
            location,
            centroid: location,
            total_spot_number: null,
            date_modified: this.transformationDate.toISOString(),
            valid_from: undefined,
            parking_type: "underground",
            address: {
                address_country: "Česko",
                address_formatted: data.address,
            },
            security: data.properties.benefits.includes("security_system") || data.properties.benefits.includes("cctv"),
            max_vehicle_dimensions: [
                data.maxVehicleDimensions.length / 100,
                data.maxVehicleDimensions.width / 100,
                data.maxVehicleDimensions.height / 100,
                0,
            ],
            active: true,
            tariff_id: data.id,
        };

        return {
            ...parking,
            parkingLocation: this.transformLocation(parking),
            parkingMeasurement: this.transformMeasurement(parking, data),
            tariffs: this.transformTariffElement(data),
            parkingProhibitions: this.transformParkingProhibitions(data),
        };
    };

    private transformLocation = (parking: IParking): IParkingLocation => {
        return {
            id: parking.id,
            source: parking.source,
            source_id: parking.source_id,
            data_provider: parking.data_provider,
            location: parking.location,
            centroid: parking.centroid,
            total_spot_number: parking.total_spot_number,
            address: parking.address,
            special_access: null,
        };
    };

    private transformMeasurement = (parking: IParking, data: TransformIn): IParkingMeasurements => {
        return {
            parking_id: parking.id,
            source: parking.source,
            source_id: parking.source_id,
            available_spot_number: data.free ? 1 : 0, // special case since we don't get occupancy data
            date_modified: this.transformationDate.toISOString(),
            occupied_spot_number: null,
            total_spot_number: null,
        };
    };

    private transformTariffElement = (element: IMrParkitGarageWithTariff): TTariffData[] => {
        const tariffCommonInfo = {
            charge_band_name: element.name,
            source: "mr_parkit",
            tariff_id: element.id,
            last_updated: this.transformationDate.toISOString(),
            free_of_charge: false,
            payment_mode: "pre_paid",
            charge_currency: "czk",
            valid_from: this.transformationDate.toISOString(),
        };

        const result = [
            {
                ...tariffCommonInfo,
                ...this.getHourlyTariffs(element),
            },
            {
                ...tariffCommonInfo,
                ...this.getWorkDayTariffs(element),
            },
            {
                ...tariffCommonInfo,
                ...this.getWeekendTariffs(element),
            },
        ];

        return result;
    };

    private getHourlyTariffs = (element: IMrParkitGarageWithTariff): ITariffPeriodElement => {
        return {
            charge: element.pricePerHour / 100,
            charge_type: "other",
            charge_order_index: 0,
            charge_interval: 3600,
            max_iterations_of_charge: 24,
            min_iterations_of_charge: element.minReservationDuration,
            periods_of_time: this.getWorkDayPeriodsOfTime(),
        };
    };

    private getWorkDayTariffs = (element: IMrParkitGarageWithTariff): ITariffPeriodElement => {
        return {
            charge: element.pricePerDay / 100,
            charge_type: "other",
            charge_order_index: 1,
            charge_interval: 86400,
            max_iterations_of_charge: element.maxReservationDuration / 24,
            min_iterations_of_charge: 0,
            periods_of_time: this.getWorkDayPeriodsOfTime(),
        };
    };

    private getWeekendTariffs = (element: IMrParkitGarageWithTariff): ITariffPeriodElement => {
        return {
            charge: element.pricePerWeekend / 100,
            charge_type: "other",
            charge_order_index: 2,
            charge_interval: 172800,
            max_iterations_of_charge: 1,
            min_iterations_of_charge: 0,
            periods_of_time: this.getWeekendPeriodsOfTime(),
        };
    };

    private transformParkingProhibitions = (element: IMrParkitGarageWithTariff) => {
        return {
            parking_id: `${SourceEnum.Mr_Parkit}-${element.id}`,
            source: SourceEnum.Mr_Parkit,
            lpg: element.properties.lpgCngAllowed,
            motorcycle: element.properties.motorbikesAllowed,
        };
    };

    private getWorkDayPeriodsOfTime = () => {
        const result: IParkingTariffPeriod[] = [];
        for (const day of Object.keys(DaysInWeekEnum)) {
            for (const ph of Object.keys(PhTypes)) {
                result.push({
                    day_in_week: DaysInWeekEnum[day as TDays],
                    start: "00:00",
                    end: "23:59",
                    ph: PhTypes[ph as TPhType],
                });
            }
        }
        return result;
    };

    private getWeekendPeriodsOfTime = () => {
        const result: IParkingTariffPeriod[] = [];
        for (const day of Object.keys(OffWeekDays)) {
            for (const ph of Object.keys(PhTypes)) {
                result.push({
                    day_in_week: OffWeekDays[day as TOffWeekDay],
                    start: "00:00",
                    end: "23:59",
                    ph: PhTypes[ph as TPhType],
                });
            }
        }
        return result;
    };
}

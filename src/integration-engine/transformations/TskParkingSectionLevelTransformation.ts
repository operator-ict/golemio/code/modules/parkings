import { SourceEnum } from "#helpers/constants/SourceEnum";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IParkingLocation } from "#ie/ParkingInterface";
import { ITskParkingSectionLevel } from "#sch/datasources/interfaces/ITskParkingSectionLevel";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { wktToGeoJSON } from "@terraformer/wkt";
import { TGeoCoordinates } from "@golemio/core/dist/output-gateway";
import { getPointOnFeature } from "#ie/helpers/GeodataHelper";

@injectable()
export class TskParkingSectionLevelTransformation extends AbstractTransformation<ITskParkingSectionLevel, IParkingLocation> {
    name: string = "TskParkingSectionLevelTransformation";

    constructor(private specialAccess?: string) {
        super();
    }

    protected transformInternal = (zone: ITskParkingSectionLevel): IParkingLocation => {
        const location = wktToGeoJSON(zone.borderWKT) as TGeoCoordinates;
        return {
            id: SourceEnum.TSK_V2 + "-" + zone.idSectionItem,
            data_provider: "www.tsk-praha.cz",
            location,
            centroid: getPointOnFeature(location),
            total_spot_number: zone.projectedCapacity,
            source: SourceEnum.TSK_V2,
            source_id: zone.idSection,
            special_access: this.specialAccess ? [this.specialAccess] : undefined,
        };
    };
}

/* ie/index.ts */
import { StaticTariffsWorker } from "#ie/workers/StaticTariffsWorker";
import { UpdateAddressWorker } from "#ie/workers/UpdateAddressWorker";
import { NewParkingsWorker } from "./workers/NewParkingsWorker";
import { TskParkingWorker } from "#ie/workers/TskParkingWorker";
import { ParkingSourcesWorker } from "#ie/workers/ParkingSourcesWorker";
import { ParkomatsWorker } from "./workers/ParkomatsWorker";

export * from "./transformations/KoridParkingDataTransformation";
export * from "./transformations/KoridParkingConfigTransformation";
export * from "./queueDefinitions";
export * from "./ZtpParkingZones/ZtpParkingZonesDataSourceFactory";
export * from "./ZtpParkingZones/ZtpParkingZonesTransformation";
export * from "./ZtpParkingZones/ZtpParkingZonesWorker";
export * from "./workers/StaticTariffsWorker";
export * from "./workers/TskParkingWorker";
export * from "./workers/ParkingSourcesWorker";
export * from "./workers/ParkomatsWorker";
export * from "./transformations/ParkomatsTransformation";

export const workers = [
    StaticTariffsWorker,
    UpdateAddressWorker,
    NewParkingsWorker,
    TskParkingWorker,
    ParkingSourcesWorker,
    ParkomatsWorker,
];

import { AbstractWorker } from "@golemio/core/dist/integration-engine";
import { UpdateStaticTariffsTask } from "#ie/workers/tasks/UpdateStaticTariffsTask";
import { StaticTariffsTransformation } from "#ie/transformations/StaticTariffsTransformation";
import { ParkingTariffsRepository } from "#ie/repositories/ParkingTariffsRepository";
import { StaticTariffsMatcher } from "#ie/transformations/StaticTariffsMatcher";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { StaticTariffsDataSourceFactory } from "#ie/datasources/StaticTariffsDataSourceFactory";
import { config } from "@golemio/core/dist/integration-engine/config";
import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingsSecondaryRepository } from "#ie/repositories/ParkingsSecondaryRepository";

export class StaticTariffsWorker extends AbstractWorker {
    protected readonly name: string = "StaticTariffsWorker";

    constructor() {
        super();
        const parkingRepository = ParkingsContainer.resolve<ParkingsRepository>(ModuleContainerToken.ParkingsRepository);
        const parkingSecondaryRepository = ParkingsContainer.resolve<ParkingsSecondaryRepository>(
            ModuleContainerToken.ParkingsSecondaryRepository
        );
        const parkingsModel = new ParkingsRepository();
        const task = new UpdateStaticTariffsTask(
            this.getQueuePrefix(),
            StaticTariffsDataSourceFactory.getDataSource(config.datasources.StaticParkingsSourceUrl),
            new StaticTariffsTransformation(),
            new ParkingTariffsRepository(parkingRepository, parkingSecondaryRepository),
            parkingsModel,
            new StaticTariffsMatcher(parkingsModel)
        );
        this.registerTask(task);
    }
}

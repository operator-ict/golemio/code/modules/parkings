import { AbstractWorker } from "@golemio/core/dist/integration-engine";
import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { SaveTskParkingMachinesTask } from "#ie/workers/tasks/SaveTskParkingMachinesTask";
import { SaveTskParkingSectionLevelTask } from "#ie/workers/tasks/SaveTskParkingSectionLevelTask";
import { SaveTskParkingSectionsTask } from "#ie/workers/tasks/SaveTskParkingSectionsTask";
import { TskParkingSectionLevelTransformation } from "#ie/transformations/TskParkingSectionLevelTransformation";
import { SaveTskParkingZonesTariffsTask } from "#ie/workers/tasks/SaveTskParkingZonesTariffsTask";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";

export class TskParkingWorker extends AbstractWorker {
    protected readonly name: string = "TskParkingWorker";

    constructor() {
        super();

        const saveTskParkingSectionsTask = new SaveTskParkingSectionsTask(
            this.getQueuePrefix(),
            ParkingsContainer.resolve(ModuleContainerToken.TskParkingSectionsDataSource),
            ParkingsContainer.resolve(ModuleContainerToken.ParkingsRepository),
            IntegrationEngineContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig)
        );

        const saveTskParkingSectionLevel251Task = new SaveTskParkingSectionLevelTask(
            "saveTskParkingSectionLevel251",
            this.getQueuePrefix(),
            ParkingsContainer.resolve(ModuleContainerToken.TskParkingSectionLevel251DataSource),
            new TskParkingSectionLevelTransformation(),
            ParkingsContainer.resolve(ModuleContainerToken.ParkingsLocationRepository),
            IntegrationEngineContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig)
        );

        const saveTskParkingSectionLevel253Task = new SaveTskParkingSectionLevelTask(
            "saveTskParkingSectionLevel253",
            this.getQueuePrefix(),
            ParkingsContainer.resolve(ModuleContainerToken.TskParkingSectionLevel253DataSource),
            new TskParkingSectionLevelTransformation("disabled"),
            ParkingsContainer.resolve(ModuleContainerToken.ParkingsLocationRepository),
            IntegrationEngineContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig)
        );

        const saveTskParkingMachinesTask = new SaveTskParkingMachinesTask(
            this.getQueuePrefix(),
            ParkingsContainer.resolve(ModuleContainerToken.TskParkingMachinesDataSource),
            ParkingsContainer.resolve(ModuleContainerToken.TskParkingMachinesTransformation),
            ParkingsContainer.resolve(ModuleContainerToken.ParkingMachinesRepository)
        );

        const saveTskParkingZonesTariffsTask = new SaveTskParkingZonesTariffsTask(
            this.getQueuePrefix(),
            ParkingsContainer.resolve(ModuleContainerToken.TskParkingZonesTariffsDataSource),
            ParkingsContainer.resolve(ModuleContainerToken.ParkingTariffsRepository)
        );

        this.registerTask(saveTskParkingSectionsTask);
        this.registerTask(saveTskParkingSectionLevel251Task);
        this.registerTask(saveTskParkingSectionLevel253Task);
        this.registerTask(saveTskParkingMachinesTask);
        this.registerTask(saveTskParkingZonesTariffsTask);
    }
}

import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { NewParkingsWorker } from "#ie/workers/NewParkingsWorker";
import { IIptOictTaskTypeInput } from "#ie/workers/schemas/IptOictTaskTypeSchema";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { NEW_PARKINGS_WORKER_NAME } from "../constants";

@injectable()
export class GenerateIptOictTariffsJobsTask extends AbstractEmptyTask {
    public readonly queueName = "generateIptOictTariffsJobs";
    public readonly queueTtl = 60 * 60 * 1000; // 1 hour

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(ModuleContainerToken.CachedParkingSourcesRepository)
        private cachedParkingSourcesRepository: CachedParkingSourcesRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(): Promise<void> {
        const sources = await this.cachedParkingSourcesRepository.getTariffsDataSourceSources({
            isRestrictedToOpenData: false,
        });

        const exchange = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");

        for (const source of sources) {
            const data: IIptOictTaskTypeInput = { source };
            await QueueManager.sendMessageToExchange(
                exchange + "." + NewParkingsWorker.workerName.toLowerCase(),
                "saveIptOictTariffsData",
                data
            );
        }
    }
}

import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { AverageOccupancyRepository } from "#ie/repositories/AverageOccupancyRepository";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";

@injectable()
export class CalculateAverageOccupancyTask extends AbstractEmptyTask {
    public readonly queueName = "calculateAverageOccupancy";
    public readonly queueTtl = 60 * 60 * 1000;

    constructor(
        @inject(ModuleContainerToken.AverageOccupancyRepository) private averageOccupancyRepository: AverageOccupancyRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(): Promise<void> {
        try {
            await this.averageOccupancyRepository.calculateAverageOccupancy();
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError("Error while calculating average occupancy", this.constructor.name, err);
        }
    }
}

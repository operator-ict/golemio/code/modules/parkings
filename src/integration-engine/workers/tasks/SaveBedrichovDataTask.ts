import { ParkingProvider } from "#ie/datasources/helpers/ParkingProviderEnum";
import { IParkingProviderDataSourceFactory } from "#ie/datasources/interfaces/IParkingProviderDataSourceFactory";
import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { BedrichovMeasurementsTransformation } from "#ie/transformations/BedrichovMeasurementsTransformation";
import { AbstractEmptyTask, PostgresModel } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { NEW_PARKINGS_WORKER_NAME } from "../constants";

@injectable()
export class SaveBedrichovDataTask extends AbstractEmptyTask {
    public readonly queueName = "saveBedrichovData";
    public readonly queueTtl = 60 * 1000; // 1min

    constructor(
        @inject(ModuleContainerToken.ParkingProviderDataSourceFactory)
        private dataSourceFactory: IParkingProviderDataSourceFactory,
        @inject(ModuleContainerToken.ParkingsMeasurementRepository)
        private readonly parkingsMeasurementsRepository: PostgresModel,
        @inject(ModuleContainerToken.ParkingsMeasurementsActualRepository)
        private readonly parkingsMeasurementsActualRepository: PostgresModel
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    protected async execute(): Promise<void> {
        try {
            const measurements = await this.dataSourceFactory.getDataSource(ParkingProvider.Bedrichov).getAll();
            const transformation = ParkingsContainer.resolve<BedrichovMeasurementsTransformation>(
                ModuleContainerToken.BedrichovMeasurementsTransformation
            );
            const transformedData = transformation.transformArray(measurements);
            await this.parkingsMeasurementsRepository.bulkSave(transformedData);
            await this.parkingsMeasurementsActualRepository.bulkSave(transformedData);
        } catch (err) {
            throw new GeneralError("Error while saving Bedrichov measurements data", this.constructor.name, err);
        }
    }
}

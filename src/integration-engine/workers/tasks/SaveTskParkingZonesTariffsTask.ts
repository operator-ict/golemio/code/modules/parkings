import { SourceEnum } from "#helpers/constants/SourceEnum";
import { IParkingTariff } from "#sch/models/interfaces/IParkingTariff";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { ITskParkingZoneTariff } from "#sch/datasources/interfaces/ITskParkingZoneTariff";
import { TskParkingZonesTariffsTransformation } from "#ie/transformations/TskParkingZonesTariffsTransformation";
import { ParkingTariffsRepository } from "#ie/repositories/ParkingTariffsRepository";

export class SaveTskParkingZonesTariffsTask extends AbstractEmptyTask {
    public queueName: string = "saveTskTariffs";

    constructor(
        queuePrefix: string,
        private dataSource: DataSource<ITskParkingZoneTariff[]>,
        private repository: ParkingTariffsRepository
    ) {
        super(queuePrefix);
    }

    protected async execute(): Promise<void> {
        const processingDate = new Date();
        const data: ITskParkingZoneTariff[] = await this.dataSource.getAll();
        const transformation = new TskParkingZonesTariffsTransformation(processingDate);
        const transformedData: IParkingTariff[][] = transformation.transformArray(data);
        const flatTransformedData: IParkingTariff[] = transformedData.flat();
        await this.repository.merge(flatTransformedData, SourceEnum.TSK_V2, processingDate);
    }
}

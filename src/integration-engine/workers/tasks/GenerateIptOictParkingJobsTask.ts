import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { NewParkingsWorker } from "#ie/workers/NewParkingsWorker";
import { IIptOictTaskTypeInput } from "#ie/workers/schemas/IptOictTaskTypeSchema";
import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { IConfiguration } from "@golemio/core/dist/integration-engine/config";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { NEW_PARKINGS_WORKER_NAME } from "../constants";

@injectable()
export class GenerateIptOictParkingJobsTask extends AbstractEmptyTask {
    public readonly queueName = "generateIptOictParkingJobs";
    public readonly queueTtl = 60 * 60 * 1000; // 1 hour

    constructor(
        @inject(ContainerToken.Config) private config: IConfiguration,
        @inject(ModuleContainerToken.CachedParkingSourcesRepository)
        private cachedParkingSourcesRepository: CachedParkingSourcesRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(): Promise<void> {
        const sources = await this.cachedParkingSourcesRepository.getParkingSpaceDataSourceSources({
            isRestrictedToOpenData: false,
        });

        for (const source of sources) {
            const data: IIptOictTaskTypeInput = { source };
            await QueueManager.sendMessageToExchange(
                this.config.RABBIT_EXCHANGE_NAME + "." + NewParkingsWorker.workerName.toLowerCase(),
                "saveIptOictParkingData",
                data
            );
        }
    }
}

import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { IGenericFeature } from "#ie/datasources/interfaces/IGenericFeature";
import { IptOictDataSource } from "#ie/datasources/iptoict/IptOictDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingsLocationRepository } from "#ie/repositories/ParkingsLocationRepository";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { IptOictParkingSpaceTransformation } from "#ie/transformations/IptOictParkingSpaceTransformation";
import { IptOictParkingTransformation } from "#ie/transformations/IptOictParkingTransformation";
import { IptOictOpeningHoursTransformation } from "#ie/transformations/iptoict/IptOictOpeningHoursTransformation";
import { IIptOictTaskTypeInput, IptOictTaskTypeValidationSchema } from "#ie/workers/schemas/IptOictTaskTypeSchema";
import { IIptOictParkingProperties } from "#sch/datasources/iptoict/interfaces/IIptOictParkingProperties";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { AbstractGolemioError, GeneralError, ValidationError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { NEW_PARKINGS_WORKER_NAME } from "../constants";
import { OpenHoursRepository } from "#ie/repositories/OpenHoursRepository";

@injectable()
export class SaveIptOictParkingDataTask extends AbstractTask<IIptOictTaskTypeInput> {
    public readonly queueName = "saveIptOictParkingData";
    public readonly queueTtl = 60 * 60 * 1000; // 1 hour
    public readonly schema = IptOictTaskTypeValidationSchema;

    constructor(
        @inject(ModuleContainerToken.IptOictDataSource) private iptOictDataSource: IptOictDataSource,
        @inject(ModuleContainerToken.ParkingsRepository) private parkingRepository: ParkingsRepository,
        @inject(ModuleContainerToken.ParkingsLocationRepository) private parkingLocationRepository: ParkingsLocationRepository,
        @inject(ModuleContainerToken.OpenHoursRepository) private openHoursRepository: OpenHoursRepository,
        @inject(ModuleContainerToken.CachedParkingSourcesRepository)
        private cachedParkingSourcesRepository: CachedParkingSourcesRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(data: IIptOictTaskTypeInput): Promise<void> {
        try {
            const jobConfig = await this.getSourceConfig(data.source);
            await this.updateParkingData(jobConfig.source, jobConfig.datasource_parking);
            await this.updateParkingLocations(jobConfig.source, jobConfig.datasource_locations);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError("Error while saving IPT parking data", this.constructor.name, err);
        }
    }

    private async updateParkingData(source: string, sourceUrl: string) {
        const processingDate = new Date();
        const data: Array<IGenericFeature<IIptOictParkingProperties>> = await this.iptOictDataSource.getParkingData(sourceUrl);
        const transformation = new IptOictParkingTransformation(source, processingDate);
        const transformedData = transformation.transformArray(data);
        await this.parkingRepository.saveActiveParkingsWithoutAddress(transformedData, source);

        const openingHoursTransformation = new IptOictOpeningHoursTransformation(source);
        const openingHours = data
            .flatMap((parking) => openingHoursTransformation.transformElement(parking))
            .filter((oh) => oh.periods_of_time !== null);

        await this.openHoursRepository.mergeOpenHours(openingHours, [source], processingDate);
    }

    private async updateParkingLocations(source: string, sourceUrl: string) {
        const data = await this.iptOictDataSource.getParkingLocationData(sourceUrl);
        const transformation = new IptOictParkingSpaceTransformation(source);
        const transformedData = transformation.transformArray(data);
        await this.parkingLocationRepository.saveWithoutAddress(transformedData);
    }

    private async getSourceConfig(source: string) {
        const sources = await this.cachedParkingSourcesRepository.getParkingSpaceDataSourceConfig({
            isRestrictedToOpenData: false,
        });
        if (!sources.length) {
            throw new GeneralError(`No sources found for IPT parking task`, this.constructor.name);
        }

        const config = sources.find((el) => el.source === source);
        if (!config) {
            throw new ValidationError(`Invalid source value: ${source}, must use one of: [${sources.join(", ")}]`);
        }
        return config;
    }
}

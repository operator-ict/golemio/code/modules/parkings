import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { ILogger } from "@golemio/core/dist/helpers";
import { ParkingSourcesRepository } from "#ie/repositories/ParkingSourcesRepository";
import { IParkingSource } from "#sch/models/interfaces/IParkingSource";
import { UpdateParkingSourceValidationSchema } from "#ie/workers/schemas/ParkingSourceSchema";
import { DataSourceIntegrationChecker } from "#ie/businessRules/DataSourceIntegrationChecker";

export class UpdateParkingSourceTask extends AbstractTask<IParkingSource> {
    public readonly queueName = "updateParkingSource";
    public readonly queueTtl = 3 * 60 * 60 * 1000; // 3h
    public readonly schema = UpdateParkingSourceValidationSchema;

    constructor(
        queuePrefix: string,
        private log: ILogger,
        private repository: ParkingSourcesRepository,
        private integrationChecker: DataSourceIntegrationChecker
    ) {
        super(queuePrefix);
    }

    protected async execute(data: IParkingSource) {
        try {
            this.integrationChecker.checkAllowedDataSources(data);
            await this.repository.save(data);
        } catch (err) {
            this.log.error(`Error while updating parking source: ${err}`);
        }
    }
}

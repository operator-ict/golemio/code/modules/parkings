import { IParking, IParkingMeasurements, TTariffData } from "#ie/ParkingInterface";
import { ParkingProvider } from "#ie/datasources/helpers/ParkingProviderEnum";
import { IParkingProviderDataSourceFactory } from "#ie/datasources/interfaces/IParkingProviderDataSourceFactory";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingsLocationRepository } from "#ie/repositories/ParkingsLocationRepository";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { MrParkitGarageTransformation } from "#ie/transformations/MrParkitGarageTransformation";
import { Parkings } from "#sch";
import { ParkingMeasurementsDtoSchema } from "#sch/datasources/ParkingMeasurementsDtoSchema";
import { IMrParkitGarageWithTariff } from "#sch/datasources/interfaces/IMrParkitGarage";
import { IParkingLocation } from "#sch/models/interfaces/IParkingLocation";
import { PostgresModel, QueueManager } from "@golemio/core/dist/integration-engine";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";
import {
    AbstractGolemioError,
    DatasourceError,
    GeneralError,
    TransformationError,
} from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { NEW_PARKINGS_WORKER_NAME } from "../constants";
import { ParkingTariffsRepository } from "#ie/repositories/ParkingTariffsRepository";
import { IParkingProhibitions } from "#sch/models/interfaces/IParkingProhibitions";
import { ParkingsProhibitionsRepository } from "#ie/repositories/ParkingsProhibitionsRepository";
import { UpdateAddressWorker } from "#ie/workers/UpdateAddressWorker";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { SourceEnum } from "#helpers/constants/SourceEnum";

@injectable()
export class SaveMrParkitDataTask extends AbstractEmptyTask {
    public readonly queueName = "saveMrParkitData";
    public readonly queueTtl = 15 * 60 * 1000; // 15min

    private measurementsRepository: PostgresModel;

    constructor(
        @inject(ModuleContainerToken.ParkingProviderDataSourceFactory)
        private dataSourceFactory: IParkingProviderDataSourceFactory,
        @inject(ModuleContainerToken.ParkingsRepository) private parkingRepository: ParkingsRepository,
        @inject(ModuleContainerToken.ParkingsLocationRepository) private parkingLocationRepository: ParkingsLocationRepository,
        @inject(ModuleContainerToken.ParkingTariffsRepository) private parkingsTariffsRepository: ParkingTariffsRepository,
        @inject(ModuleContainerToken.ParkingsProhibitionsRepository)
        private parkingsProhibitionsRepository: ParkingsProhibitionsRepository,
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig
    ) {
        super(NEW_PARKINGS_WORKER_NAME);

        this.measurementsRepository = new PostgresModel(
            Parkings.measurements.name + "Model",
            {
                outputSequelizeAttributes: Parkings.measurements.outputSequelizeAttributes,
                pgSchema: Parkings.pgSchema,
                pgTableName: Parkings.measurements.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Parkings.measurements.name + "PgModelValidator", ParkingMeasurementsDtoSchema)
        );
    }

    protected async execute(): Promise<void> {
        const dataSource = this.dataSourceFactory.getDataSource(ParkingProvider.MrParkit);
        let garageResult: IMrParkitGarageWithTariff[] = [];

        try {
            garageResult = await dataSource.getAll();
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new DatasourceError("Error while fetching MR.PARKIT garages", this.constructor.name, err);
        }

        const transformationDate = new Date();
        const garageTransformation = new MrParkitGarageTransformation(transformationDate);

        let transformedGarages: IParking[] = [];
        let transformedLocations: IParkingLocation[] = [];
        let transformedMeasurements: IParkingMeasurements[] = [];
        let transformedTariffs: TTariffData[] = [];
        let transformedParkingProhibitions: IParkingProhibitions[] = [];

        try {
            for (const garage of garageResult) {
                const { parkingLocation, parkingMeasurement, tariffs, parkingProhibitions, ...parking } =
                    garageTransformation.transformElement(garage);
                transformedGarages.push(parking);
                transformedLocations.push(parkingLocation);
                transformedMeasurements.push(parkingMeasurement);
                transformedTariffs.push(...tariffs);
                transformedParkingProhibitions.push(parkingProhibitions);
            }
        } catch (err) {
            throw new TransformationError("Error while transforming MR.PARKIT garages", this.constructor.name, err);
        }

        if (!transformedGarages.length) {
            return;
        }

        try {
            await this.measurementsRepository.bulkSave(transformedMeasurements);
            await this.parkingLocationRepository.saveWithoutAddress(transformedLocations);
            await this.parkingRepository.saveActiveParkingsWithoutAddress(transformedGarages, SourceEnum.Mr_Parkit);
            await this.parkingsTariffsRepository.bulkSave(transformedTariffs);

            await this.parkingsProhibitionsRepository.bulkSave(transformedParkingProhibitions);

            const exchange = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");

            await QueueManager.sendMessageToExchange(
                exchange + "." + UpdateAddressWorker.workerName.toLowerCase(),
                "updateMissingParkingsAddresses",
                {}
            );

            await QueueManager.sendMessageToExchange(
                exchange + "." + UpdateAddressWorker.workerName.toLowerCase(),
                "updateMissingParkingsLocationAddresses",
                {}
            );
        } catch (err) {
            throw new GeneralError("Error while saving MR.PARKIT data: " + err.message, this.constructor.name, err);
        }
    }
}

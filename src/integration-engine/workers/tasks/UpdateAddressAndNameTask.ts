import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { IUpdateAddressAndNameInput } from "#ie/workers/schemas/UpdateAddressAndNameSchema";
import { IUpdateAddressInput, UpdateAddressValidationSchema } from "#ie/workers/schemas/UpdateAddressSchema";
import { ILogger } from "@golemio/core/dist/helpers";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { GeocodeApi, IPostalAddress } from "@golemio/core/dist/integration-engine/helpers/GeocodeApi";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
@injectable()
export class UpdateAddressAndNameTask extends AbstractTask<IUpdateAddressInput> {
    public readonly queueName = "updateAddressAndName";
    public readonly queueTtl = 60 * 60 * 1000; // 1h
    public readonly schema = UpdateAddressValidationSchema;

    constructor(
        @inject(CoreToken.Logger) private log: ILogger,
        @inject(ContainerToken.GeocodeApi)
        private readonly geocodeApi: GeocodeApi,
        @inject(ModuleContainerToken.ParkingsRepository)
        private readonly repository: ParkingsRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }
    public async execute(data: IUpdateAddressAndNameInput) {
        const now = Date.now();

        try {
            const address = (await this.geocodeApi.getExtendedAddressFromPhoton(
                data.point.coordinates[1],
                data.point.coordinates[0]
            )) as IPostalAddress;
            const name = this.buildName(address, data);
            await this.repository.update(
                { address, name, address_updated_at: now },
                { where: { id: data.id }, fields: ["address", "name", "address_updated_at"] }
            );
        } catch (err) {
            this.log.error(
                // eslint-disable-next-line max-len
                `Error while updating address: ${data?.name} [${data?.point?.coordinates[1]},${data?.point?.coordinates[0]}] ${err}`
            );
            await this.postponeNextCheck(data.id);
        }
    }

    private buildName(address: IPostalAddress, data: IUpdateAddressAndNameInput): string {
        return `${data.name} ${address.street_address}, ${address.address_region}`;
    }

    // temporary solution to decrease the number of requests to the geocoding service. Should be solved in
    // https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/120
    private async postponeNextCheck(parkingId: string) {
        try {
            await this.repository.update(
                { address_updated_at: Date.now() },
                { where: { id: parkingId }, fields: ["address_updated_at"] }
            );
        } catch (err) {
            this.log.error(`Error while postponing next check for parking and name with id ${parkingId} ${err}`);
        }
    }
}

import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingsLocationRepository } from "#ie/repositories/ParkingsLocationRepository";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { UpdateAddressWorker } from "#ie/workers/UpdateAddressWorker";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class UpdateMissingParkingsLocationAddressesTask extends AbstractEmptyTask {
    public readonly queueName = "updateMissingParkingsLocationAddresses";
    public readonly queueTtl = 60 * 60 * 1000; // 60 minutes

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(ModuleContainerToken.ParkingsLocationRepository) private parkinglocationRepository: ParkingsLocationRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(): Promise<void> {
        const exchange = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");

        const parkings = await this.parkinglocationRepository.findWithoutAddress();

        for (const parking of parkings) {
            await QueueManager.sendMessageToExchange(
                exchange + "." + UpdateAddressWorker.workerName.toLowerCase(),
                "updateLocationAddress",
                {
                    id: parking.id,
                    point: parking.centroid,
                }
            );
        }
    }
}

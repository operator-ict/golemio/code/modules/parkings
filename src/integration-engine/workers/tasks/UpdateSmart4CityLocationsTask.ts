import { ParkingProvider } from "#ie/datasources/helpers/ParkingProviderEnum";
import { IParkingProviderDataSourceFactory } from "#ie/datasources/interfaces/IParkingProviderDataSourceFactory";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingsLocationRepository } from "#ie/repositories/ParkingsLocationRepository";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { Smart4CityLocationTransformation } from "#ie/transformations/Smart4CityLocationTransformation";
import { UpdateAddressWorker } from "#ie/workers/UpdateAddressWorker";
import { ISmart4CityLocation } from "#sch/datasources/interfaces/ISmart4CityLocation";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { IUpdateSmart4CityInput, UpdateSmart4CityValidationSchema } from "#ie/workers/schemas/UpdateSmart4CitySchema";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { AbstractGolemioError, DatasourceError } from "@golemio/core/dist/shared/golemio-errors";
import { GeneralError, TransformationError } from "@golemio/core/dist/shared/golemio-errors";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { SourceEnum } from "#helpers/constants/SourceEnum";

@injectable()
export class UpdateSmart4CityLocationsTask extends AbstractTask<IUpdateSmart4CityInput> {
    public readonly queueName = "updateSmart4CityLocations";
    public readonly queueTtl = 15 * 60 * 1000;
    public readonly schema = UpdateSmart4CityValidationSchema;

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(ModuleContainerToken.ParkingProviderDataSourceFactory)
        private dataSourceFactory: IParkingProviderDataSourceFactory,
        @inject(ModuleContainerToken.ParkingsRepository) private parkingRepository: ParkingsRepository,
        @inject(ModuleContainerToken.ParkingsLocationRepository) private parkingLocationRepository: ParkingsLocationRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(data: IUpdateSmart4CityInput): Promise<void> {
        let locations: ISmart4CityLocation[] = [];

        try {
            const dataSource = this.dataSourceFactory.getDataSource(ParkingProvider.Smart4CityLocation, data.code);
            locations = await dataSource.getAll();
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new DatasourceError("Error while fetching Smart4City locations", this.constructor.name, err);
        }

        let transformedData;

        try {
            const transformationDate = new Date();
            const transformation = new Smart4CityLocationTransformation(data.code, transformationDate);
            transformedData = transformation.transformCollection(locations);
        } catch (err) {
            throw new TransformationError("Error while transforming Smart4City locations", this.constructor.name, err);
        }

        if (!transformedData.parking.length) {
            return;
        }

        try {
            await this.parkingRepository.saveActiveParkingsWithoutAddress(
                transformedData.parking,
                SourceEnum.Smart4City,
                transformedData.parking[0].data_provider
            );
            await this.parkingLocationRepository.saveWithoutAddress(transformedData.parkingLocation);
        } catch (err) {
            throw new GeneralError("Error while saving Smart4City locations data", this.constructor.name, err);
        }

        const exchange = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");

        await QueueManager.sendMessageToExchange(
            exchange + "." + UpdateAddressWorker.workerName.toLowerCase(),
            "updateMissingParkingsAddresses",
            {}
        );

        await QueueManager.sendMessageToExchange(
            exchange + "." + UpdateAddressWorker.workerName.toLowerCase(),
            "updateMissingParkingsLocationAddresses",
            {}
        );
    }
}

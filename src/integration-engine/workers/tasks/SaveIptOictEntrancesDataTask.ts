import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingEntrancesRepository } from "#ie/repositories/ParkingEntrancesRepository";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { NEW_PARKINGS_WORKER_NAME } from "../constants";
import { IIptOictTaskTypeInput, IptOictTaskTypeValidationSchema } from "#ie/workers/schemas/IptOictTaskTypeSchema";
import { AbstractGolemioError, GeneralError, ValidationError } from "@golemio/core/dist/shared/golemio-errors";
import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { IptOictEntrancesTransformation } from "#ie/transformations/iptoict/IptOictEntrancesTransformation";
import { IptOictEntrancesDataSource } from "#ie/datasources/iptoict/IptOictEntrancesDataSource";

@injectable()
export class SaveIptOictEntrancesDataTask extends AbstractTask<IIptOictTaskTypeInput> {
    public readonly queueName = "saveIptOictEntrancesData";
    public readonly queueTtl = 15 * 60 * 1000; // 15min
    public readonly schema = IptOictTaskTypeValidationSchema;

    constructor(
        @inject(ModuleContainerToken.IptOictEntrancesDataSource) private datasource: IptOictEntrancesDataSource,
        @inject(ModuleContainerToken.ParkingEntrancesRepository)
        private parkingEntrancesRepository: ParkingEntrancesRepository,
        @inject(ModuleContainerToken.CachedParkingSourcesRepository)
        private cachedParkingSourcesRepository: CachedParkingSourcesRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    protected async execute(data: IIptOictTaskTypeInput): Promise<void> {
        try {
            const jobConfig = await this.getSourceConfig(data.source);
            await this.updateEntrancesData(jobConfig.source, jobConfig.datasource_entranecs);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError("Error while saving IPT entrances data", this.constructor.name, err);
        }
    }

    private async updateEntrancesData(source: string, sourceUrl: string) {
        const processingDate = new Date();
        const entrances = await this.datasource.getEntrancesData(sourceUrl);
        const transformation = new IptOictEntrancesTransformation(source);
        const transformedData = transformation.transformArray(entrances);
        await this.parkingEntrancesRepository.mergeEntrances(transformedData, [source], processingDate);
    }

    private async getSourceConfig(source: string) {
        const sources = await this.cachedParkingSourcesRepository.getParkingEntrancesDataSourceConfig({
            isRestrictedToOpenData: false,
        });
        if (!sources.length) {
            throw new GeneralError(`No sources found for IPT entrances task`, this.constructor.name);
        }

        const config = sources.find((el) => el.source === source);
        if (!config) {
            throw new ValidationError(`Invalid source value: ${source}, must use one of: [${sources.join(", ")}]`);
        }
        return config;
    }
}

import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { ITskParkingSection } from "#sch/datasources/interfaces/ITskParkingSection";
import { TskParkingSectionTransformation } from "#ie/transformations/TskParkingSectionTransformation";
import { UpdateAddressWorker } from "#ie/workers/UpdateAddressWorker";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { SourceEnum } from "#helpers/constants/SourceEnum";

export class SaveTskParkingSectionsTask extends AbstractEmptyTask {
    public readonly queueName = "saveTskParkingSections";

    constructor(
        queuePrefix: string,
        private dataSource: DataSource<ITskParkingSection[]>,
        private parkingsRepository: ParkingsRepository,
        private config: ISimpleConfig
    ) {
        super(queuePrefix);
    }

    public async execute(): Promise<void> {
        const sourceData: ITskParkingSection[] = await this.dataSource.getAll();
        const transformation = new TskParkingSectionTransformation(new Date());
        const transformedParkings = transformation.transformArray(sourceData);
        await this.parkingsRepository.saveActiveParkingsWithoutAddress(transformedParkings, SourceEnum.TSK_V2);

        const exchange = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");
        await QueueManager.sendMessageToExchange(
            exchange + "." + UpdateAddressWorker.workerName.toLowerCase(),
            "updateMissingParkingsAddresses",
            {}
        );
    }
}

import { SmartCityListDatasourceCache } from "#ie/datasources/smart4city/SmartCityListDatasourceCache";
import { ISmart4CityTaskTypeInput, Smart4CityTaskTypeValidationSchema } from "#ie/workers/schemas/Smart4CityTaskTypeSchema";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractGolemioError, DatasourceError } from "@golemio/core/dist/shared/golemio-errors";
import { ISmart4CityList } from "#sch/datasources/interfaces/ISmart4CityList";

export enum Smart4CityTaskType {
    locations = "locations",
    measurements = "measurements",
}

@injectable()
export class GenerateSmart4CityTaskListTask extends AbstractTask<ISmart4CityTaskTypeInput> {
    public readonly queueName = "generateSmart4CityTaskList";
    public readonly queueTtl = 3 * 60 * 60 * 1000; // 3h
    public readonly schema = Smart4CityTaskTypeValidationSchema;

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(ModuleContainerToken.SmartCityListDatasourceCache)
        private dataSource: SmartCityListDatasourceCache
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(data: ISmart4CityTaskTypeInput): Promise<void> {
        let sourceDataCityList: Array<Pick<ISmart4CityList, "code">> = [];

        try {
            sourceDataCityList = await this.dataSource.getAll();
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new DatasourceError("Error while fetching Smart4City list", this.constructor.name, err);
        }

        const exchange = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");

        for (const city of sourceDataCityList) {
            await QueueManager.sendMessageToExchange(
                exchange + "." + NEW_PARKINGS_WORKER_NAME.toLowerCase(),
                data.type === Smart4CityTaskType.locations ? "updateSmart4CityLocations" : "updateSmart4CityMeasurements",
                { code: city.code }
            );
        }
    }
}

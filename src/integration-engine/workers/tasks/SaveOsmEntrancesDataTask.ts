import { OsmEntrancesDataSource } from "#ie/datasources/osm/OsmEntrancesDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingEntrancesRepository } from "#ie/repositories/ParkingEntrancesRepository";
import { OsmEntrancesTransformation } from "#ie/transformations/osm/OsmEntrancesTransformation";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { NEW_PARKINGS_WORKER_NAME } from "../constants";
import { SourceEnum } from "#helpers/constants/SourceEnum";

@injectable()
export class SaveOsmEntrancesDataTask extends AbstractEmptyTask {
    public readonly queueName = "saveOsmEntrancesDataTask";
    public readonly queueTtl = 15 * 60 * 1000; // 15min

    constructor(
        @inject(ModuleContainerToken.OsmEntrancesDataSource) private osmEntrancesDataSource: OsmEntrancesDataSource,
        @inject(ModuleContainerToken.ParkingEntrancesRepository)
        private parkingEntrancesRepository: ParkingEntrancesRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    protected async execute(): Promise<void> {
        const processingDate = new Date();
        const entrances = await this.osmEntrancesDataSource.getOsmEntrances();
        const transformation = new OsmEntrancesTransformation();
        const transformedEntrances = transformation.transformArray(entrances);
        await this.parkingEntrancesRepository.mergeEntrances(transformedEntrances, [SourceEnum.OSM], processingDate);
    }
}

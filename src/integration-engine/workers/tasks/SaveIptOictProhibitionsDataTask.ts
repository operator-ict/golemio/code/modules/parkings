import { IptOictParkingsProhibitionsDataSource } from "#ie/datasources/iptoict/IptOictParkingsProhibitionsDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingsProhibitionsRepository } from "#ie/repositories/ParkingsProhibitionsRepository";
import { IptOictParkingProhibitionsTransformation } from "#ie/transformations/IptOictParkingProhibitionsTransformation";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { AbstractGolemioError, GeneralError, ValidationError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { NEW_PARKINGS_WORKER_NAME } from "../constants";
import { IIptOictTaskTypeInput, IptOictTaskTypeValidationSchema } from "#ie/workers/schemas/IptOictTaskTypeSchema";
import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";

@injectable()
export class SaveIptOictProhibitionsDataTask extends AbstractTask<IIptOictTaskTypeInput> {
    public readonly queueName = "saveIptOictProhibitionsDataTask";
    public readonly queueTtl = 60 * 60 * 1000; // 1 hour
    public readonly schema = IptOictTaskTypeValidationSchema;

    constructor(
        @inject(ModuleContainerToken.IptOictParkingsProhibitionsDataSource)
        private iptOictParkingsProhibitionsDataSource: IptOictParkingsProhibitionsDataSource,
        @inject(ModuleContainerToken.ParkingsProhibitionsRepository)
        private parkingsProhibitionsRepository: ParkingsProhibitionsRepository,
        @inject(ModuleContainerToken.CachedParkingSourcesRepository)
        private cachedParkingSourcesRepository: CachedParkingSourcesRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(taskInputData: IIptOictTaskTypeInput): Promise<void> {
        try {
            const jobConfig = await this.getSourceConfig(taskInputData.source);
            await this.updateProhibitionsData(jobConfig.source, jobConfig.datasource_prohibitions);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }
            throw new GeneralError("Error while saving parking prohibitions data", this.constructor.name, err);
        }
    }

    private async updateProhibitionsData(source: string, sourceUrl: string) {
        const data = await this.iptOictParkingsProhibitionsDataSource.getParkingsProhibitionsData(sourceUrl);
        const transformedData = new IptOictParkingProhibitionsTransformation(source).transformArray(data);
        await this.parkingsProhibitionsRepository.bulkSave(transformedData);
    }

    private async getSourceConfig(source: string) {
        const sources = await this.cachedParkingSourcesRepository.getParkingProhibitionsDataSourceConfig({
            isRestrictedToOpenData: false,
        });
        if (!sources.length) {
            throw new GeneralError(`No sources found for IPT prohibitions task`, this.constructor.name);
        }

        const config = sources.find((el) => el.source === source);
        if (!config) {
            throw new ValidationError(`Invalid source value: ${source}, must use one of: [${sources.join(", ")}]`);
        }
        return config;
    }
}

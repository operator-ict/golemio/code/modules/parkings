import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingsLocationRepository } from "#ie/repositories/ParkingsLocationRepository";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { IUpdateAddressInput, UpdateAddressValidationSchema } from "#ie/workers/schemas/UpdateAddressSchema";
import { ILogger } from "@golemio/core/dist/helpers";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { GeocodeApi } from "@golemio/core/dist/integration-engine/helpers/GeocodeApi";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class UpdateLocationAddressTask extends AbstractTask<IUpdateAddressInput> {
    public readonly queueName = "updateLocationAddress";
    public readonly queueTtl = 60 * 60 * 1000; // 1h
    public readonly schema = UpdateAddressValidationSchema;

    constructor(
        @inject(CoreToken.Logger) private log: ILogger,
        @inject(ContainerToken.GeocodeApi)
        private readonly geocodeApi: GeocodeApi,
        @inject(ModuleContainerToken.ParkingsLocationRepository)
        private readonly repository: ParkingsLocationRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }
    public async execute(data: IUpdateAddressInput) {
        const now = Date.now();

        try {
            const address = await this.geocodeApi.getExtendedAddressFromPhoton(
                data.point.coordinates[1],
                data.point.coordinates[0]
            );
            await this.repository.update(
                { address, address_updated_at: now },
                { where: { id: data.id }, fields: ["address", "address_updated_at"] }
            );
        } catch (err) {
            this.log.error(
                // eslint-disable-next-line max-len
                `Error while updating address: [${data?.point?.coordinates[1]},${data?.point?.coordinates[0]}] with id ${data?.id} ${err}`
            );
            await this.postponeNextCheck(data.id);
        }
    }

    // temporary solution to decrease the number of requests to the geocoding service. Should be solved in
    // https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/120
    private async postponeNextCheck(parkingLocationId: string) {
        try {
            await this.repository.update(
                { address_updated_at: Date.now() },
                { where: { id: parkingLocationId }, fields: ["address_updated_at"] }
            );
        } catch (err) {
            this.log.error(`Error while postponing next check for parking location with id ${parkingLocationId} ${err}`);
        }
    }
}

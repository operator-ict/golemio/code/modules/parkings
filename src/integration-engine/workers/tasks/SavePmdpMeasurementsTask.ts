import { CachedPmdpParkingRepository } from "#helpers/data-access/CachedPmdpParkingRepository";
import { PmdpMeasurementsDataSourceFactory } from "#ie/datasources/pmdp/PmdpMeasurementsDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { PmdpMeasurementTransformation } from "#ie/transformations/pmdp/PmdpMeasurementTransformation";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { IPmdpTaskTypeInput, PmdpTaskTypeValidationSchema } from "#ie/workers/schemas/PmdpTaskTypeSchema";
import { IPmdpMeasurement } from "#sch/datasources/pmdp/interfaces/IPmdpMeasurement";
import { AbstractTask, PostgresModel } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractGolemioError, DatasourceError, TransformationError } from "@golemio/core/dist/shared/golemio-errors";

@injectable()
export class SavePmdpMeasurementsTask extends AbstractTask<IPmdpTaskTypeInput> {
    public readonly queueName = "savePmdpMeasurements";
    public readonly queueTtl = 60 * 1000;
    public readonly schema = PmdpTaskTypeValidationSchema;

    constructor(
        @inject(ModuleContainerToken.ParkingsMeasurementRepository)
        private readonly parkingsMeasurementsRepository: PostgresModel,
        @inject(ModuleContainerToken.ParkingsMeasurementsActualRepository)
        private readonly parkingsMeasurementsActualRepository: PostgresModel,
        @inject(ModuleContainerToken.PmdpMeasurementsDataSourceFactory)
        private readonly PmdpMeasurementsDataSourceFactory: PmdpMeasurementsDataSourceFactory,
        @inject(ModuleContainerToken.CachedPmdpParkingRepository)
        private readonly cachedPmdpParkingRepository: CachedPmdpParkingRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(data: IPmdpTaskTypeInput): Promise<void> {
        let measurement: IPmdpMeasurement;
        try {
            measurement = await this.PmdpMeasurementsDataSourceFactory.getData(data.code);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new DatasourceError(`Error while fetching PMDP measurement (${data.code})`, this.constructor.name, err);
        }

        let transformedData;
        try {
            const parking = await this.cachedPmdpParkingRepository.getPmdpParking(data.code);
            const transformation = new PmdpMeasurementTransformation(new Date());
            transformedData = transformation.transformElement({ ...measurement, ...parking });
        } catch (err) {
            throw new TransformationError(`Error while transforming PMDP measurement (${data.code})`, this.constructor.name, err);
        }

        try {
            await this.parkingsMeasurementsRepository.bulkSave([transformedData]);
            await this.parkingsMeasurementsActualRepository.bulkSave([transformedData]);
        } catch (err) {
            throw new GeneralError(`Error while saving PMDP measurements data (${data.code})`, this.constructor.name, err);
        }
    }
}

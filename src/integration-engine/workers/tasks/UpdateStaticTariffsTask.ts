import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { StaticTariffsTransformation } from "#ie/transformations/StaticTariffsTransformation";
import { ParkingTariffsRepository } from "#ie/repositories/ParkingTariffsRepository";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { StaticTariffsJson } from "#sch/datasources/StaticTariffsJsonSchema";
import { IParking } from "#ie/ParkingInterface";
import { StaticTariffsMatcher } from "#ie/transformations/StaticTariffsMatcher";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";

export class UpdateStaticTariffsTask extends AbstractEmptyTask {
    public readonly queueName = "updateStaticTariffs";
    public readonly queueTtl = 60 * 60 * 1000; // 1 hour

    private dataSource: DataSource;
    private transformer: StaticTariffsTransformation;
    private parkingsTariffsModel: ParkingTariffsRepository;
    private matcher: StaticTariffsMatcher;
    private parkingsModel: ParkingsRepository;

    constructor(
        queuePrefix: string,
        dataSource: DataSource,
        transformer: StaticTariffsTransformation,
        parkingsTariffsModel: ParkingTariffsRepository,
        parkingsModel: ParkingsRepository,
        matcher: StaticTariffsMatcher
    ) {
        super(queuePrefix);
        this.dataSource = dataSource;
        this.transformer = transformer;
        this.parkingsTariffsModel = parkingsTariffsModel;
        this.parkingsModel = parkingsModel;
        this.matcher = matcher;
    }

    public async execute(): Promise<void> {
        try {
            const data: StaticTariffsJson = await this.dataSource.getAll();
            const transformedData = await this.transformer.transform(data.tariffs);
            await this.parkingsTariffsModel.save(transformedData);

            const matches: IParking[] = await this.matcher.matchTariffRelations(data.relations);
            await this.parkingsModel.save(matches);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError(
                    `Error while refreshing static parking tariffs: ${err.message}`,
                    this.constructor.name,
                    err
                );
            }
        }
    }
}

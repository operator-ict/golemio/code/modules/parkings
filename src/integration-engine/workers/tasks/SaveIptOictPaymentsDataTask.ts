import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { IptOictPaymentsDataSource } from "#ie/datasources/iptoict/IptOictPaymentsDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingPaymentsRepository } from "#ie/repositories/ParkingPaymentsRepository";
import { IptOictPaymentsTransformation } from "#ie/transformations/IptOictPaymentsTransformation";
import { IIptOictTaskTypeInput, IptOictTaskTypeValidationSchema } from "#ie/workers/schemas/IptOictTaskTypeSchema";
import { IIptOictPaymentsProperties } from "#sch/datasources/iptoict/interfaces/IIptOictPaymentsProperties";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { AbstractGolemioError, GeneralError, ValidationError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { NEW_PARKINGS_WORKER_NAME } from "../constants";

@injectable()
export class SaveIptOictPaymentsDataTask extends AbstractTask<IIptOictTaskTypeInput> {
    public readonly queueName = "saveIptOictPaymentsData";
    public readonly queueTtl = 60 * 60 * 1000; // 1 hour
    public readonly schema = IptOictTaskTypeValidationSchema;

    constructor(
        @inject(ModuleContainerToken.IptOictPaymentsDataSource) private iptOictPaymentsDataSource: IptOictPaymentsDataSource,
        @inject(ModuleContainerToken.ParkingPaymentsRepository) private parkingPaymentsRepository: ParkingPaymentsRepository,
        @inject(ModuleContainerToken.CachedParkingSourcesRepository)
        private cachedParkingSourcesRepository: CachedParkingSourcesRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(data: IIptOictTaskTypeInput): Promise<void> {
        try {
            const jobConfig = await this.getSourceConfig(data.source);
            await this.updatePaymentsData(jobConfig.source, jobConfig.datasource_payments);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError("Error while saving IPT payments data", this.constructor.name, err);
        }
    }

    private async updatePaymentsData(source: string, sourceUrl: string) {
        const processingDate = new Date();
        const data: IIptOictPaymentsProperties[] = await this.iptOictPaymentsDataSource.getPaymentsData(sourceUrl);
        const transformation = new IptOictPaymentsTransformation(source);
        const transformedData = transformation.transformArray(data);
        await this.parkingPaymentsRepository.updatePaymentsBySource(transformedData, source, processingDate);
    }

    private async getSourceConfig(source: string) {
        const sources = await this.cachedParkingSourcesRepository.getParkingPaymentsDataSourceConfig({
            isRestrictedToOpenData: false,
        });
        if (!sources.length) {
            throw new GeneralError(`No sources found for IPT payment task`, this.constructor.name);
        }

        const config = sources.find((el) => el.source === source);
        if (!config) {
            throw new ValidationError(`Invalid source value: ${source}, must use one of: [${sources.join(", ")}]`);
        }
        return config;
    }
}

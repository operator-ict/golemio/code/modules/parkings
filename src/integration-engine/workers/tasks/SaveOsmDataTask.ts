import { OsmDataSource } from "#ie/datasources/osm/OsmDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { OpenHoursRepository } from "#ie/repositories/OpenHoursRepository";
import { ParkingsLocationRepository } from "#ie/repositories/ParkingsLocationRepository";
import { ParkingsSecondaryRepository } from "#ie/repositories/ParkingsSecondaryRepository";
import { OsmOpeningHoursTransformation } from "#ie/transformations/osm/OsmOpeningHoursTransformation";
import { OsmParkingSpaceTransformation } from "#ie/transformations/osm/OsmParkingSpaceTransformation";
import { OsmParkingTransformation } from "#ie/transformations/osm/OsmParkingTransformation";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { NEW_PARKINGS_WORKER_NAME } from "../constants";
import { SourceEnum } from "#helpers/constants/SourceEnum";

@injectable()
export class SaveOsmDataTask extends AbstractEmptyTask {
    public readonly queueName = "saveOsmDataTask";
    public readonly queueTtl = 15 * 60 * 1000; // 15min

    constructor(
        @inject(ModuleContainerToken.OsmDataSource) private osmDataSource: OsmDataSource,
        @inject(ModuleContainerToken.ParkingsSecondaryRepository) private parkingSecondaryRepository: ParkingsSecondaryRepository,
        @inject(ModuleContainerToken.ParkingsLocationRepository) private parkingLocationRepository: ParkingsLocationRepository,
        @inject(ModuleContainerToken.OsmOpeningHoursTransformation)
        private osmOpeningHoursTransformation: OsmOpeningHoursTransformation,
        @inject(ModuleContainerToken.OpenHoursRepository) private openHoursRepository: OpenHoursRepository,
        @inject(CoreToken.Logger) private logger: ILogger
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    protected async execute(): Promise<void> {
        await this.updateParkingsData();
        await this.updateParkingsLocationData();
    }

    private async updateParkingsData(): Promise<void> {
        const processingDate = new Date();
        const parkings = await this.osmDataSource.getParkingsData();
        const transformation = new OsmParkingTransformation(new Date());
        const transformedParkings = transformation.transformArray(parkings);
        await this.parkingSecondaryRepository.saveActiveParkingsWithoutAddress(transformedParkings, SourceEnum.OSM);

        const openingHours = this.osmOpeningHoursTransformation
            .transformArray(parkings)
            .filter((oh) => oh.periods_of_time !== null);
        await this.openHoursRepository.mergeOpenHours(openingHours, [SourceEnum.OSM], processingDate);
    }

    private async updateParkingsLocationData(): Promise<void> {
        const parkingsLocation = await this.osmDataSource.getParkingsLocationData();
        const transformation = new OsmParkingSpaceTransformation();
        const transformedParkingsLocation = transformation.transformArray(parkingsLocation);
        await this.parkingLocationRepository.bulkSave(transformedParkingsLocation);
    }
}

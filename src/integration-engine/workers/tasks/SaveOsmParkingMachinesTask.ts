import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingMachinesModel } from "#sch/models/ParkingMachinesModel";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { NEW_PARKINGS_WORKER_NAME } from "../constants";
import { OsmParkingMachinesDataSource } from "#ie/datasources/osm/OsmParkingMachinesDataSource";
import { ParkingMachinesRepository } from "#ie/repositories/ParkingMachinesRepository";
import { OsmParkingMachinesTransformation } from "#ie/transformations/osm/OsmParkingMachinesTransformation";

@injectable()
export class SaveOsmParkingMachinesTask extends AbstractEmptyTask {
    public readonly queueName = "saveOsmParkingMachinesTask";
    public readonly queueTtl = 15 * 60 * 1000; // 15min

    constructor(
        @inject(ModuleContainerToken.OsmParkingMachinesDataSource)
        private dataSource: OsmParkingMachinesDataSource,
        @inject(ModuleContainerToken.ParkingMachinesRepository) private repository: ParkingMachinesRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    protected async execute(): Promise<void> {
        const transformationDate = new Date();

        const parkingMachines = await this.dataSource.getOsmParkingMachines();
        const transformation = new OsmParkingMachinesTransformation(transformationDate);
        const transformedData = transformation.transformArray(parkingMachines);
        await this.repository.bulkSave(transformedData, this.getAttributesToUpdate());
    }

    private getAttributesToUpdate(): string[] {
        return ParkingMachinesModel.attributeList.filter((key) => key !== "validFrom").concat("updated_at");
    }
}

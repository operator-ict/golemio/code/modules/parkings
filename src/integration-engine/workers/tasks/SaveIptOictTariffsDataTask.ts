import { SourceEnum } from "#helpers/constants/SourceEnum";
import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { IptOictTariffsDataSource } from "#ie/datasources/iptoict/IptOictTariffsDataSource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingTariffsRepository } from "#ie/repositories/ParkingTariffsRepository";
import { IptOictTariffsTransformation } from "#ie/transformations/IptOictTariffsTransformation";
import { IIptOictTaskTypeInput, IptOictTaskTypeValidationSchema } from "#ie/workers/schemas/IptOictTaskTypeSchema";
import { IIptOictTariffsProperties } from "#sch/datasources/iptoict/interfaces/IIptOictTariffsProperties";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { AbstractGolemioError, GeneralError, ValidationError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { NEW_PARKINGS_WORKER_NAME } from "../constants";

@injectable()
export class SaveIptOictTariffsDataTask extends AbstractTask<IIptOictTaskTypeInput> {
    public readonly queueName = "saveIptOictTariffsData";
    public readonly queueTtl = 60 * 60 * 1000; // 1 hour
    public readonly schema = IptOictTaskTypeValidationSchema;

    constructor(
        @inject(ModuleContainerToken.IptOictTariffsDataSource) private iptOictTariffsDataSource: IptOictTariffsDataSource,
        @inject(ModuleContainerToken.ParkingTariffsRepository) private parkingTariffsRepository: ParkingTariffsRepository,
        @inject(ModuleContainerToken.CachedParkingSourcesRepository)
        private cachedParkingSourcesRepository: CachedParkingSourcesRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(data: IIptOictTaskTypeInput): Promise<void> {
        try {
            const jobConfig = await this.getSourceConfig(data.source);
            await this.updateTariffsData(jobConfig.source as SourceEnum, jobConfig.datasource_tariffs);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError("Error while saving IPT tariffs data", this.constructor.name, err);
        }
    }

    private async updateTariffsData(source: SourceEnum, sourceUrl: string) {
        const processingDate = new Date();
        const data: IIptOictTariffsProperties[] = await this.iptOictTariffsDataSource.getTariffsData(sourceUrl);
        const transformation = new IptOictTariffsTransformation(source, processingDate);
        const transformedTariffData = transformation.transformArray(data);
        const transformedParkingData = transformation.transformParkingTariffsRelation(data);
        await this.parkingTariffsRepository.replaceTariffs(
            transformedTariffData.flat(),
            transformedParkingData,
            source,
            processingDate
        );
    }

    private async getSourceConfig(source: string) {
        const sources = await this.cachedParkingSourcesRepository.getParkingTariffDataSourceConfig({
            isRestrictedToOpenData: false,
        });
        if (!sources.length) {
            throw new GeneralError(`No sources found for IPT tariffs task`, this.constructor.name);
        }

        const config = sources.find((el) => el.source === source);
        if (!config) {
            throw new ValidationError(
                `Invalid source value: ${source}, must use one of: [${sources.map((el) => el.source).join(", ")}]`
            );
        }
        return config;
    }
}

import { ParkingProvider } from "#ie/datasources/helpers/ParkingProviderEnum";
import { IParkingProviderDataSourceFactory } from "#ie/datasources/interfaces/IParkingProviderDataSourceFactory";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { Smart4CityMeasurementTransformation } from "#ie/transformations/Smart4CityMeasurementTransformation";
import { Parkings } from "#sch";
import { ISmart4CityLocation } from "#sch/datasources/interfaces/ISmart4CityLocation";
import { ParkingMeasurementsDtoSchema } from "#sch/datasources/ParkingMeasurementsDtoSchema";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractTask, PostgresModel } from "@golemio/core/dist/integration-engine";
import { IUpdateSmart4CityInput, UpdateSmart4CityValidationSchema } from "#ie/workers/schemas/UpdateSmart4CitySchema";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { AbstractGolemioError, DatasourceError } from "@golemio/core/dist/shared/golemio-errors";
import { GeneralError, TransformationError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

@injectable()
export class UpdateSmart4CityMeasurementsTask extends AbstractTask<IUpdateSmart4CityInput> {
    public readonly queueName = "updateSmart4CityMeasurements";
    public readonly queueTtl = 15 * 60 * 1000;
    public readonly schema = UpdateSmart4CityValidationSchema;

    private measurementsRepository: PostgresModel;

    constructor(
        @inject(ModuleContainerToken.ParkingProviderDataSourceFactory)
        private dataSourceFactory: IParkingProviderDataSourceFactory
    ) {
        super(NEW_PARKINGS_WORKER_NAME);

        this.measurementsRepository = new PostgresModel(
            Parkings.measurements.name + "Model",
            {
                outputSequelizeAttributes: Parkings.measurements.outputSequelizeAttributes,
                pgSchema: Parkings.pgSchema,
                pgTableName: Parkings.measurements.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Parkings.measurements.name + "PgModelValidator", ParkingMeasurementsDtoSchema)
        );
    }

    public async execute(data: IUpdateSmart4CityInput): Promise<void> {
        let measurements: ISmart4CityLocation[] = [];

        try {
            const dataSource = this.dataSourceFactory.getDataSource(ParkingProvider.Smart4CityLocation, data.code);
            measurements = await dataSource.getAll();
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new DatasourceError("Error while fetching Smart4City measurements", this.constructor.name, err);
        }

        let transformedData;

        try {
            const transformationDate = new Date();
            const transformation = new Smart4CityMeasurementTransformation(transformationDate);
            transformedData = transformation.transformArray(measurements);
        } catch (err) {
            throw new TransformationError("Error while transforming Smart4City measurements", this.constructor.name, err);
        }

        if (!transformedData.length) {
            return;
        }

        try {
            await this.measurementsRepository.bulkSave(transformedData);
        } catch (err) {
            throw new GeneralError("Error while saving Smart4City measurements data", this.constructor.name, err);
        }
    }
}

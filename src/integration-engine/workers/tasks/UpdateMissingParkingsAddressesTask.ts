import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { UpdateAddressWorker } from "#ie/workers/UpdateAddressWorker";

@injectable()
export class UpdateMissingParkingsAddressesTask extends AbstractEmptyTask {
    public readonly queueName = "updateMissingParkingsAddresses";
    public readonly queueTtl = 60 * 60 * 1000; // 1 hour

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(ModuleContainerToken.ParkingsRepository) private parkingRepository: ParkingsRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(): Promise<void> {
        const exchange = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");

        const parkings = await this.parkingRepository.findWithoutAddress();

        for (const parking of parkings) {
            if (parking.source === "ipr") {
                await QueueManager.sendMessageToExchange(
                    exchange + "." + UpdateAddressWorker.workerName.toLowerCase(),
                    "updateAddressAndName",
                    {
                        id: parking.id,
                        point: parking.centroid,
                        name: parking.name,
                    }
                );
            } else {
                await QueueManager.sendMessageToExchange(
                    exchange + "." + UpdateAddressWorker.workerName.toLowerCase(),
                    "updateAddress",
                    {
                        id: parking.id,
                        point: parking.centroid,
                    }
                );
            }
        }
    }
}

import { ParkingsLocationRepository } from "#ie/repositories/ParkingsLocationRepository";
import { TskParkingSectionLevelTransformation } from "#ie/transformations/TskParkingSectionLevelTransformation";
import { UpdateAddressWorker } from "#ie/workers/UpdateAddressWorker";
import { ITskParkingSectionLevel } from "#sch/datasources/interfaces/ITskParkingSectionLevel";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";

export class SaveTskParkingSectionLevelTask extends AbstractEmptyTask {
    constructor(
        public readonly queueName: string,
        queuePrefix: string,
        private dataSource: DataSource<ITskParkingSectionLevel[]>,
        private transformation: TskParkingSectionLevelTransformation,
        private parkingsLocationRepository: ParkingsLocationRepository,
        private config: ISimpleConfig
    ) {
        super(queuePrefix);
    }

    public async execute(): Promise<void> {
        const sourceData: ITskParkingSectionLevel[] = await this.dataSource.getAll();
        const parkingLocations = this.transformation.transformArray(sourceData);
        await this.parkingsLocationRepository.saveWithoutAddress(parkingLocations);

        const exchange = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");
        await QueueManager.sendMessageToExchange(
            exchange + "." + UpdateAddressWorker.workerName.toLowerCase(),
            "updateMissingParkingsLocationAddresses",
            {}
        );
    }
}

import { DateTime, ILogger, dateTime } from "@golemio/core/dist/helpers";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { IParkomatMessage, IParkomatValidationSchema } from "../schemas/ParkomatSchema";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkomatsTransformation } from "#ie/transformations/ParkomatsTransformation";
import { ParkomatsRepository } from "#ie/repositories/ParkomatsRepository";
import { ParkomatsDataSource } from "#ie/datasources/parkomats/ParkomatsDataSource";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { Parkomats } from "#sch";

@injectable()
export class RefreshDataInDbTask extends AbstractTask<IParkomatMessage> {
    public readonly queueName = "refreshDataInDB";
    public readonly queueTtl = 59 * 60 * 1000; // 59 minutes
    public readonly schema = IParkomatValidationSchema;

    constructor(
        @inject(ModuleContainerToken.ParkomatsRepository)
        private model: ParkomatsRepository,
        @inject(ModuleContainerToken.ParkomatsTransformation)
        private transformation: ParkomatsTransformation,
        @inject(ModuleContainerToken.ParkomatsDataSource)
        private dataSource: ParkomatsDataSource,
        @inject(CoreToken.Logger)
        private log: ILogger
    ) {
        super(Parkomats.name.toLowerCase());
    }

    public execute = async (msg: IParkomatMessage): Promise<void> => {
        let from: DateTime;
        let to: DateTime;
        if (msg.from && msg.to) {
            from = dateTime(new Date(msg.from), { timeZone: "Europe/Prague" });
            to = dateTime(new Date(msg.to), { timeZone: "Europe/Prague" });
            this.log.debug(`Interval from: ${from} to ${to} was used.`);
        } else {
            const now = new Date();
            now.setHours(4, 0, 0, 0);
            to = dateTime(now).setTimeZone("Europe/Prague");
            from = to.clone();
            from.subtract(28, "hours");
        }

        const data = await this.dataSource.getParkomats(
            "from=" + from.format("yyyy-LL-dd'T'HH:mm:ss") + `&to=${to.format("yyyy-LL-dd'T'HH:mm:ss")}`
        );
        const transformedData = await this.transformation.transform(data);
        await this.model.bulkSave(transformedData);
    };
}

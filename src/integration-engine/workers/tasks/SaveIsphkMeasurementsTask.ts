import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { IsphkMeasurementTransformation } from "#ie/transformations/iptoict/IsphkMeasurementTransformation";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { IsphkMeasurementsSchema } from "#sch/datasources/isphk/IsphkMeasurementsSchema";
import { PostgresModel } from "@golemio/core/dist/integration-engine";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IIsphkMeasurements } from "#sch/datasources/isphk/interfaces/IIsphkMeasurements";

@injectable()
export class SaveIsphkMeasurementsTask extends AbstractTaskJsonSchema<IIsphkMeasurements> {
    public readonly queueName = "saveIsphkMeasurements";
    public readonly queueTtl = 15 * 60 * 1000;
    public readonly schema = new JSONSchemaValidator("SaveIsphkMeasurementsTaskValidation", IsphkMeasurementsSchema);

    constructor(
        @inject(ModuleContainerToken.ParkingsMeasurementRepository)
        private readonly parkingsMeasurementsRepository: PostgresModel,
        @inject(ModuleContainerToken.ParkingsMeasurementsActualRepository)
        private readonly parkingsMeasurementsActualRepository: PostgresModel
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(inputData: IIsphkMeasurements): Promise<void> {
        try {
            const isphkMeasurementTransformation = new IsphkMeasurementTransformation(new Date());
            const transformedData = isphkMeasurementTransformation.transformArray(inputData.data);
            await this.parkingsMeasurementsRepository.bulkSave(transformedData);
            await this.parkingsMeasurementsActualRepository.bulkSave(transformedData);
        } catch (err) {
            throw new GeneralError("Error while saving ISPHK measurements data", this.constructor.name, err);
        }
    }
}

import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { TskParkingMachinesTransformation } from "#ie/transformations/TskParkingMachinesTransformation";
import { ParkingMachinesRepository } from "#ie/repositories/ParkingMachinesRepository";
import { ITskParkingMachine } from "#sch/datasources/interfaces/ITskParkingMachine";
import { IParkingMachine } from "#sch/models/interfaces/IParkingMachine";

export class SaveTskParkingMachinesTask extends AbstractEmptyTask {
    public queueName: string = "saveTskParkingMachines";

    constructor(
        queuePrefix: string,
        private dataSource: DataSource<ITskParkingMachine[]>,
        private transformation: TskParkingMachinesTransformation,
        private repository: ParkingMachinesRepository
    ) {
        super(queuePrefix);
    }

    protected async execute(): Promise<void> {
        const data: ITskParkingMachine[] = await this.dataSource.getAll();
        const transformedData: IParkingMachine[] = this.transformation.transformArray(data);
        await this.repository.bulkSave(transformedData);
    }
}

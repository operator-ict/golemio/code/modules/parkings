import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingsLocationRepository } from "#ie/repositories/ParkingsLocationRepository";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { UpdateAddressWorker } from "#ie/workers/UpdateAddressWorker";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class UpdateAllParkingsLocationAdresses extends AbstractEmptyTask {
    public readonly queueName = "updateAllParkingsLocationAdresses";
    public readonly queueTtl = 30 * 60 * 1000; // 30 minutes

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(ModuleContainerToken.ParkingsLocationRepository) private parkinglocationRepository: ParkingsLocationRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(): Promise<void> {
        const batchSize = this.config.getValue<number>("module.parking.addresses.batchSize");
        const updateInterval = this.config.getValue<number>("module.parking.addresses.updateIntervalDays");
        const exchange = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");

        const parkings = await this.parkinglocationRepository.findWithOutdatedAddress(+batchSize, +updateInterval);

        for (const parking of parkings) {
            await QueueManager.sendMessageToExchange(
                exchange + "." + UpdateAddressWorker.workerName.toLowerCase(),
                "updateLocationAddress",
                {
                    id: parking.id,
                    point: parking.centroid,
                }
            );
        }
    }
}

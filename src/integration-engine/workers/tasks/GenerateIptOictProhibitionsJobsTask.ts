import { AbstractEmptyTask, IConfiguration, QueueManager } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { CachedParkingSourcesRepository } from "#helpers/data-access/CachedParkingSourcesRepository";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { IIptOictTaskTypeInput } from "#ie/workers/schemas/IptOictTaskTypeSchema";

@injectable()
export class GenerateIptOictProhibitionsJobsTask extends AbstractEmptyTask {
    public readonly queueName = "generateIptOictProhibitionsJobs";
    public readonly queueTtl = 60 * 60 * 1000; // 1 hour

    constructor(
        @inject(ContainerToken.Config) private config: IConfiguration,
        @inject(ModuleContainerToken.CachedParkingSourcesRepository)
        private cachedParkingSourcesRepository: CachedParkingSourcesRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(): Promise<void> {
        const sources = await this.cachedParkingSourcesRepository.getProhibitionsDataSourceSources({
            isRestrictedToOpenData: false,
        });

        for (const source of sources) {
            const data: IIptOictTaskTypeInput = { source };
            await QueueManager.sendMessageToExchange(
                this.config.RABBIT_EXCHANGE_NAME + "." + NEW_PARKINGS_WORKER_NAME.toLowerCase(),
                "saveIptOictProhibitionsDataTask",
                data
            );
        }
    }
}

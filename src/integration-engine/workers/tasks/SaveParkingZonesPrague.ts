import { SourceEnum } from "#helpers/constants/SourceEnum";
import { ITariffCheckerResult } from "#ie/businessRules/interfaces/ITariffCheckerResult";
import { ITariffInfo } from "#ie/businessRules/interfaces/ITariffInfo";
import TariffCheckerFactory, { TariffCheckerEnum } from "#ie/businessRules/TariffCheckerFactory";
import { mergeParkingZoneDataSets } from "#ie/helpers/LocationBuilderHelper";
import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingPaymentsRepository } from "#ie/repositories/ParkingPaymentsRepository";
import { ParkingsBusinessErrorsRepository } from "#ie/repositories/ParkingsBusinessErrorsRepository";
import { ParkingsLocationRepository } from "#ie/repositories/ParkingsLocationRepository";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { ParkingTariffsRepository } from "#ie/repositories/ParkingTariffsRepository";
import { IPRParkingTransformation } from "#ie/transformations/IPRParkingTransformation";
import { TSKParkingTariffTransformation } from "#ie/transformations/TSKParkingTariffTransformation";
import { TSKParkingTransformation } from "#ie/transformations/TSKParkingTransformation";
import { UpdateAddressWorker } from "#ie/workers/UpdateAddressWorker";
import { Parkings } from "#sch/index";
import { IBusinessError } from "#sch/models/interfaces/IBusinessError";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { IConfiguration } from "@golemio/core/dist/integration-engine/config";
import {
    CSVDataTypeStrategy,
    DataSource,
    FTPProtocolStrategy,
    IFile,
    IFTPSettings,
    JSONDataTypeStrategy,
    PaginatedHTTPProtocolStrategy,
} from "@golemio/core/dist/integration-engine/datasources";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import getUuidByString from "uuid-by-string";
import { dateTime } from "@golemio/core/dist/helpers";
import { TskParkingFilter } from "#ie/transformations/TskParkingFilter";

export class SaveParkingZonesPrague extends AbstractEmptyTask {
    public queueName: string = "saveParkingZonesPrague";

    private parkingsLocationModel: ParkingsLocationRepository;
    private dataSourceIPR: DataSource;
    private dataSourceTSK: DataSource;
    private dataSourceTariffTSK: DataSource;
    private iprTransformation: IPRParkingTransformation;
    private tskTariffTransformation: TSKParkingTariffTransformation;
    private readonly ftpTSKSettings: IFTPSettings;
    private parkingsModel: ParkingsRepository;
    private parkingsTariffsModel: ParkingTariffsRepository;
    private parkingPaymentRepository: ParkingPaymentsRepository;
    private parkingsBusinessErrorsRepository: ParkingsBusinessErrorsRepository;
    private config: IConfiguration;
    private tskParkingFilter: TskParkingFilter;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.config = ParkingsContainer.resolve<IConfiguration>(ContainerToken.Config);
        const strategyIPR = new JSONDataTypeStrategy({ resultsPath: "features" });
        strategyIPR.setFilter((item) => item.properties.TARIFTAB);
        this.dataSourceIPR = new DataSource(
            Parkings.ipr.name + "DataSource",
            new PaginatedHTTPProtocolStrategy({
                headers: {},
                method: "GET",
                url: this.config.datasources.ParkingZones,
            }),
            strategyIPR,
            new JSONSchemaValidator(Parkings.ipr.name + "DataSource", Parkings.ipr.datasourceJsonSchema)
        );
        this.ftpTSKSettings = {
            filename: "",
            path: this.config.datasources.TSKFTPParkingZonesPath,
            url: {
                host: this.config.datasources.TSKFTP.host,
                port: this.config.datasources.TSKFTP.port,
                user: this.config.datasources.TSKFTP.user,
                password: this.config.datasources.TSKFTP.password,
            },
            encoding: "utf8",
        };
        this.dataSourceTSK = new DataSource(
            Parkings.tsk.name + "DataSource",
            new FTPProtocolStrategy(this.ftpTSKSettings),
            new JSONDataTypeStrategy({ resultsPath: "features" }),
            new JSONSchemaValidator(Parkings.tsk.name + "DataSource", Parkings.tsk.datasourceGeoJsonSchema)
        );
        this.dataSourceTariffTSK = new DataSource(
            Parkings.tsk.name + "DataSource",
            new FTPProtocolStrategy(this.ftpTSKSettings),
            new CSVDataTypeStrategy({
                fastcsvParams: { delimiter: "\t", headers: true },
                subscribe: (json: any) => json,
            }),
            new JSONSchemaValidator(Parkings.tsk.name + "DataSource", Parkings.tsk.datasourceTariffJsonSchema)
        );
        this.iprTransformation = new IPRParkingTransformation();
        this.tskTariffTransformation = new TSKParkingTariffTransformation();
        this.parkingsLocationModel = new ParkingsLocationRepository();

        this.parkingsModel = ParkingsContainer.resolve<ParkingsRepository>(ModuleContainerToken.ParkingsRepository);
        this.parkingsTariffsModel = ParkingsContainer.resolve<ParkingTariffsRepository>(
            ModuleContainerToken.ParkingTariffsRepository
        );
        this.parkingPaymentRepository = ParkingsContainer.resolve<ParkingPaymentsRepository>(
            ModuleContainerToken.ParkingPaymentsRepository
        );
        this.parkingsBusinessErrorsRepository = ParkingsContainer.resolve<ParkingsBusinessErrorsRepository>(
            ModuleContainerToken.ParkingsBusinessErrorsRepository
        );
        this.tskParkingFilter = new TskParkingFilter();
    }

    /**
     *  Parking zones Prague queue worker method
     *  - store all data for parking zones in Prague.
     */
    protected execute = async (): Promise<void> => {
        const currentDate = new Date();
        await this.saveParkingZonesLocationsPrague(currentDate);
        await this.saveParkingZonesTariffsPrague(currentDate);
    };

    /**
     *  Parking zones Prague
     *  - store location data for parking zones in Prague.
     *  - merged TSK and IPR data sources
     */
    private saveParkingZonesLocationsPrague = async (currentDate: Date): Promise<void> => {
        const processingDate = new Date();
        this.ftpTSKSettings.filename = `Area_${dateTime(currentDate).format("yyyy-LL-dd")}.json`;
        this.dataSourceTSK.protocolStrategy.setConnectionSettings(this.ftpTSKSettings);
        const dataTSK: IFile = await this.dataSourceTSK.getAll();

        const filteredTSKRecords = this.tskParkingFilter.filterUniqueParkings(dataTSK.data);

        const transformedTSKData = await this.transformTskData(currentDate.toISOString(), filteredTSKRecords);

        const dataIPR = await this.dataSourceIPR.getAll();
        const transformedIPRData = await this.iprTransformation.transform(dataIPR);

        const result = await mergeParkingZoneDataSets(transformedTSKData.parking, transformedIPRData);
        await this.parkingsModel.saveActiveParkingsWithoutAddress(result.parking, SourceEnum.TSK);
        await this.parkingsLocationModel.saveWithoutAddress(result.locations);
        await this.parkingPaymentRepository.updatePaymentsBySource(transformedTSKData.payment, SourceEnum.TSK, processingDate);

        await QueueManager.sendMessageToExchange(
            this.config.RABBIT_EXCHANGE_NAME + "." + UpdateAddressWorker.workerName.toLowerCase(),
            "updateMissingParkingsAddresses",
            {}
        );

        await QueueManager.sendMessageToExchange(
            this.config.RABBIT_EXCHANGE_NAME + "." + UpdateAddressWorker.workerName.toLowerCase(),
            "updateMissingParkingsLocationAddresses",
            {}
        );
    };

    private async transformTskData(currentDate: string, dataTSK: Array<Record<string, any>>) {
        const tskTransformation = new TSKParkingTransformation(currentDate);
        return await tskTransformation.transform(dataTSK);
    }

    /**
     *  Parking zones Prague
     *  - store tariff data for parking zones in Prague.
     */
    private saveParkingZonesTariffsPrague = async (currentDate: Date): Promise<void> => {
        this.ftpTSKSettings.filename = `Tarif_${dateTime(currentDate).format("yyyy-LL-dd")}.tsv`;
        this.dataSourceTariffTSK.protocolStrategy.setConnectionSettings(this.ftpTSKSettings);
        const dataTSK: IFile = await this.dataSourceTariffTSK.getAll();
        const dataRecords = dataTSK.data as Record<string, any>;

        const { validTariffs, invalidResults } = this.checkBusinessRules(dataRecords);

        const transformedTSKData = await this.tskTariffTransformation.transform({
            tsk: validTariffs,
            lastUpdated: currentDate,
        });

        await this.parkingsTariffsModel.deleteByTariffIds(transformedTSKData);
        await this.parkingsTariffsModel.bulkSave(transformedTSKData);
        await this.saveBusinessErrors(invalidResults);
    };

    private checkBusinessRules(dataRecords: Record<string, any>) {
        const tariffChecker = TariffCheckerFactory.get(TariffCheckerEnum.TSK);

        const checkedTariffs = dataRecords?.map((tariff: any) => {
            return {
                checkerResult: tariffChecker.isTariffValid({
                    cTariff: +tariff["CTARIF"],
                    pricePerHour: +tariff["PricePerHour"],
                    maxParkingTime: +tariff["MaxParkingTime"],
                    maxPrice: +tariff["MaxPrice"],
                    minPrice: +tariff["MinPrice"],
                }),
                tariff: tariff as object,
            };
        });

        const listOfInvalidTariffIds = checkedTariffs
            .filter((checkedTariff: any) => !checkedTariff.checkerResult.isValid)
            .map((checkedTariff: any) => checkedTariff.tariff["CTARIF"]);

        const validTariffs = dataRecords.filter((tariff: any) => !listOfInvalidTariffIds.includes(tariff["CTARIF"]));

        const invalidResults = (
            checkedTariffs.map((checkedTariff: any) => checkedTariff.checkerResult) as Array<ITariffCheckerResult<ITariffInfo>>
        ).filter((element) => !element.isValid);

        return { validTariffs, invalidResults };
    }

    private async saveBusinessErrors(invalidResults: Array<ITariffCheckerResult<ITariffInfo>>) {
        const businessErrors = invalidResults.map((result) => {
            return {
                identifier: getUuidByString(`${result.failedInput?.cTariff}`),
                provider: "tsk",
                record_date: dateTime(new Date()).format("yyyy-LL-dd"),
                type: "tariff",
                detail: result.failedInput,
            } as IBusinessError;
        });

        if (businessErrors.length > 0) {
            await this.parkingsBusinessErrorsRepository.bulkSave(businessErrors);
        }
    }
}

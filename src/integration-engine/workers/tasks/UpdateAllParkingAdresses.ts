import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ParkingsRepository } from "#ie/repositories/ParkingsRepository";
import { NEW_PARKINGS_WORKER_NAME } from "#ie/workers/constants";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { UpdateAddressWorker } from "#ie/workers/UpdateAddressWorker";

@injectable()
export class UpdateAllParkingAdresses extends AbstractEmptyTask {
    public readonly queueName = "updateAllParkingAdresses";
    public readonly queueTtl = 30 * 60 * 1000; // 30 minutes

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(ModuleContainerToken.ParkingsRepository) private parkingRepository: ParkingsRepository
    ) {
        super(NEW_PARKINGS_WORKER_NAME);
    }

    public async execute(): Promise<void> {
        const batchSize = this.config.getValue<number>("module.parking.addresses.batchSize");
        const updateInterval = this.config.getValue<number>("module.parking.addresses.updateIntervalDays");
        const exchange = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");

        const parkings = await this.parkingRepository.findWithOutdatedAddress(+batchSize, +updateInterval);

        for (const parking of parkings) {
            if (parking.source === "ipr") {
                await QueueManager.sendMessageToExchange(
                    exchange + "." + UpdateAddressWorker.workerName.toLowerCase(),
                    "updateAddressAndName",
                    {
                        id: parking.id,
                        point: parking.centroid,
                        name: parking.name,
                    }
                );
            } else {
                await QueueManager.sendMessageToExchange(
                    exchange + "." + UpdateAddressWorker.workerName.toLowerCase(),
                    "updateAddress",
                    {
                        id: parking.id,
                        point: parking.centroid,
                    }
                );
            }
        }
    }
}

import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { AbstractWorker } from "@golemio/core/dist/integration-engine";
import { ITask } from "@golemio/core/dist/integration-engine/workers/interfaces/ITask";
import { NEW_PARKINGS_WORKER_NAME } from "./constants";
import { SaveParkingZonesPrague } from "./tasks/SaveParkingZonesPrague";

export class NewParkingsWorker extends AbstractWorker {
    static readonly workerName = NEW_PARKINGS_WORKER_NAME;
    public readonly name = NewParkingsWorker.workerName;

    constructor() {
        super();

        // Register tasks
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.SaveMrParkitDataTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.GenerateSmart4CityTaskListTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.UpdateSmart4CityLocationsTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.UpdateSmart4CityMeasurementsTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.CalculateAverageOccupancyTask));
        this.registerTask(new SaveParkingZonesPrague(this.getQueuePrefix()));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.GenerateIptOictParkingJobsTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.SaveIptOictParkingDataTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.GenerateIptOictPaymentJobsTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.SaveIptOictPaymentsDataTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.GenerateIptOictTariffsJobsTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.SaveIptOictTariffsDataTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.SaveOsmDataTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.SaveIsphkMeasurementsTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.SaveOsmEntrancesDataTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.GenerateIptOictProhibitionsJobsTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.SaveIptOictProhibitionsDataTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.SaveOsmParkingMachinesTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.GenerateIptOictEntrancesJobsTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.SaveIptOictEntrancesDataTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.SavePmdpMeasurementsTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.SaveBedrichovDataTask));
    }

    public registerTask = (task: ITask): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}

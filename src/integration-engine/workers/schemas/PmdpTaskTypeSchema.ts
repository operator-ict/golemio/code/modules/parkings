import { PmdpIdList } from "#ie/datasources/pmdp/PmdpMeasurementsDataSource";
import { IsEnum } from "@golemio/core/dist/shared/class-validator";

export interface IPmdpTaskTypeInput {
    code: string;
}

export class PmdpTaskTypeValidationSchema implements IPmdpTaskTypeInput {
    @IsEnum(PmdpIdList)
    code!: string;
}

import { IsString, ArrayMinSize, ArrayMaxSize, IsNumber } from "@golemio/core/dist/shared/class-validator";
import { Point } from "@golemio/core/dist/shared/geojson";

export class PointSchema implements Point {
    @IsString()
    type!: "Point";

    @ArrayMinSize(2)
    @ArrayMaxSize(2)
    @IsNumber({}, { each: true })
    coordinates!: number[];
}

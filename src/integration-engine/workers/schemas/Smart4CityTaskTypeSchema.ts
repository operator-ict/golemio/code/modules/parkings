import { IsEnum } from "@golemio/core/dist/shared/class-validator";

export interface ISmart4CityTaskTypeInput {
    type: string;
}

export class Smart4CityTaskTypeValidationSchema implements ISmart4CityTaskTypeInput {
    @IsEnum(["locations", "measurements"])
    type!: string;
}

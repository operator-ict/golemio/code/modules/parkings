import { IsString } from "@golemio/core/dist/shared/class-validator";

export interface IIptOictTaskTypeInput {
    source: string;
}

export class IptOictTaskTypeValidationSchema implements IIptOictTaskTypeInput {
    @IsString()
    source!: string;
}

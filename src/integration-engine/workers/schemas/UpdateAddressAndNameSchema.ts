import { IUpdateAddressInput } from "#ie/workers/schemas/UpdateAddressSchema";

export interface IUpdateAddressAndNameInput extends IUpdateAddressInput {
    name: string;
}

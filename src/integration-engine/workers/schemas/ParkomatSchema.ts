import { IsDateString, IsOptional } from "@golemio/core/dist/shared/class-validator";

export interface IParkomatMessage {
    from?: string;
    to?: string;
}

export class IParkomatValidationSchema implements IParkomatMessage {
    @IsDateString()
    @IsOptional()
    from!: string;
    @IsDateString()
    @IsOptional()
    to!: string;
}

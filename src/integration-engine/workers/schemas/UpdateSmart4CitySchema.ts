import { IsString } from "@golemio/core/dist/shared/class-validator";

export interface IUpdateSmart4CityInput {
    code: string;
}

export class UpdateSmart4CityValidationSchema implements IUpdateSmart4CityInput {
    @IsString()
    code!: string;
}

import { IsObject, IsString, ValidateNested } from "@golemio/core/dist/shared/class-validator";
import { Type } from "@golemio/core/dist/shared/class-transformer";
import { Point } from "@golemio/core/dist/shared/geojson";
import { PointSchema } from "#ie/workers/schemas/PointSchema";

export interface IUpdateAddressInput {
    id: string;
    point: Point;
}

export class UpdateAddressValidationSchema implements IUpdateAddressInput {
    @IsString()
    id!: string;

    @IsObject()
    @ValidateNested()
    @Type(() => PointSchema)
    point!: Point;
}

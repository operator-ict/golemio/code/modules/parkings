import {
    IsObject,
    IsString,
    IsBoolean,
    IsNotEmpty,
    IsOptional,
    IsUrl,
    ValidateIf,
} from "@golemio/core/dist/shared/class-validator";
import { IParkingSource, IParkingSourcePayment, IParkingSourceReservation } from "#sch/models/interfaces/IParkingSource";

class ParkingSourcePayment implements IParkingSourcePayment {
    @IsString()
    @IsNotEmpty()
    @ValidateIf((_, value) => value !== null)
    web_url!: string | null;

    @IsString()
    @IsNotEmpty()
    @ValidateIf((_, value) => value !== null)
    android_url!: string | null;

    @IsString()
    @IsNotEmpty()
    @ValidateIf((_, value) => value !== null)
    ios_url!: string | null;

    @IsString()
    @IsNotEmpty()
    @ValidateIf((_, value) => value !== null)
    discovery_url!: string | null;
}

class ParkingSourceReservation extends ParkingSourcePayment implements IParkingSourceReservation {
    @IsString()
    @IsNotEmpty()
    @ValidateIf((_, value) => value !== null)
    type!: string | null;
}

export class UpdateParkingSourceValidationSchema implements IParkingSource {
    @IsString()
    @IsNotEmpty()
    source!: string;

    @IsString()
    @IsNotEmpty()
    @ValidateIf((_, value) => value !== null)
    name!: string | null;

    @IsBoolean()
    open_data!: boolean;

    @IsBoolean()
    api_v3_allowed!: boolean;

    @IsBoolean()
    legacy_api_allowed!: boolean;

    @IsOptional()
    @IsObject()
    payment!: ParkingSourcePayment | null;

    @IsOptional()
    @IsObject()
    reservation!: ParkingSourceReservation | null;

    @IsOptional()
    @IsObject()
    contact!: object | null;

    @IsOptional()
    @IsUrl()
    datasource_parking!: string | null;

    @IsOptional()
    @IsUrl()
    datasource_locations!: string | null;

    @IsOptional()
    @IsUrl()
    datasource_payments!: string | null;

    @IsOptional()
    @IsUrl()
    datasource_entrances!: string | null;

    @IsOptional()
    @IsUrl()
    datasource_prohibitions!: string | null;

    @IsOptional()
    @IsUrl()
    datasource_tariffs!: string | null;
}

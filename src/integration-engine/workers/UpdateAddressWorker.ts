import { ParkingsContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { AbstractWorker } from "@golemio/core/dist/integration-engine";
import { ITask } from "@golemio/core/dist/integration-engine/workers/interfaces/ITask";

export class UpdateAddressWorker extends AbstractWorker {
    static readonly workerName = "Parkings";
    protected readonly name = UpdateAddressWorker.workerName;

    constructor() {
        super();
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.UpdateAddressTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.UpdateLocationAddressTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.UpdateAddressAndNameTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.UpdateAllParkingAdresses));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.UpdateAllParkingsLocationAdresses));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.UpdateMissingParkingAdressesTask));
        this.registerTask(ParkingsContainer.resolve<ITask>(ModuleContainerToken.UpdateMissingParkingsLocationAdressesTask));
    }

    public registerTask = (task: ITask): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}

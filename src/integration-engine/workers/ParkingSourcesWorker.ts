import { AbstractWorker } from "@golemio/core/dist/integration-engine";
import { UpdateParkingSourceTask } from "#ie/workers/tasks/UpdateParkingSourceTask";
import { ParkingSourcesRepository } from "#ie/repositories/ParkingSourcesRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc";
import { DataSourceIntegrationChecker } from "#ie/businessRules/DataSourceIntegrationChecker";

export class ParkingSourcesWorker extends AbstractWorker {
    protected readonly name: string = "ParkingSources";

    constructor() {
        super();
        const task = new UpdateParkingSourceTask(
            this.getQueuePrefix(),
            IntegrationEngineContainer.resolve<ILogger>(CoreToken.Logger),
            new ParkingSourcesRepository(),
            new DataSourceIntegrationChecker()
        );
        this.registerTask(task);
    }
}

import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { ParkingsContainer } from "#ie/ioc/Di";
import { RefreshDataInDbTask } from "./tasks/RefreshDataInDbTask";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";

export class ParkomatsWorker extends AbstractWorker {
    protected readonly name = "Parkomats";

    constructor() {
        super();
        this.registerTask(ParkingsContainer.resolve<RefreshDataInDbTask>(ModuleContainerToken.RefreshDataInDbTask));
    }
    public registerTask = (task: AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}

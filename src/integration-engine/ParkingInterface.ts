import { IContact } from "#sch/models/interfaces/IParking";
import { IParkingTariff, IParkingTariffPeriod } from "#sch/models/interfaces/IParkingTariff";
import { IPostalAddress } from "@golemio/core/dist/integration-engine";
import { IGeoCoordinatesPoint, TGeoCoordinates } from "@golemio/core/dist/output-gateway/Geo";

export interface IParking {
    id: string;
    source: string;
    source_id: string;
    data_provider: string;
    date_modified: string;
    location: TGeoCoordinates;
    address?: IPostalAddress;
    name: string | null;
    total_spot_number: number | null;
    category?: string;
    tariff_id?: string;
    valid_from?: string;
    valid_to?: string;
    parking_type: string | null;
    zone_type?: string | null;
    centroid?: IGeoCoordinatesPoint;
    security?: boolean | null;
    max_vehicle_dimensions?: number[] | null;
    covered?: boolean | null;
    contact?: IContact | null;
    parking_policy?: string | null;
    active: boolean;
}

export interface IParkingLocation {
    id: string;
    data_provider: string;
    location: TGeoCoordinates;
    address?: IPostalAddress;
    centroid?: IGeoCoordinatesPoint;
    total_spot_number: number | null;
    source: string;
    source_id: string;
    special_access?: string[];
}

export interface IParkingMeasurements {
    source: string;
    source_id: string;
    parking_id: string;
    available_spot_number: number | null;
    closed_spot_number?: number;
    occupied_spot_number: number | null;
    total_spot_number: number | null;
    date_modified: string;
}

export interface ITariffPeriodElement {
    charge: number;
    charge_type?: string;
    charge_order_index: number;
    charge_interval?: number;
    max_iterations_of_charge?: number;
    min_iterations_of_charge?: number;
    periods_of_time?: IParkingTariffPeriod[];
}

export type TTariffData = IParkingTariff & ITariffPeriodElement;

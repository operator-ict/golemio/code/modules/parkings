import { IPmdpMeasurement } from "#sch/datasources/pmdp/interfaces/IPmdpMeasurement";
import { PmdpMeasurementSchema } from "#sch/datasources/pmdp/PmdpMeasurementSchema";
import { DataSource, XMLDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import path from "path";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";

export enum PmdpIdList {
    pmdp_001 = "pmdp-pmdp_001",
    pmdp_002 = "pmdp-pmdp_002",
}

@injectable()
export class PmdpMeasurementsDataSourceFactory {
    private static DATASOURCE_NAME = "PmdpMeasurementsDataSource";

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getData(code: string): Promise<IPmdpMeasurement> {
        return new DataSource(
            PmdpMeasurementsDataSourceFactory.DATASOURCE_NAME,
            this.getProtocolStrategy(code),
            new XMLDataTypeStrategy({
                resultsPath: "ParkingInfo",
                xml2jsParams: { explicitArray: false, ignoreAttrs: true, trim: true, normalize: true, emptyTag: () => null },
            }),

            new JSONSchemaValidator(PmdpMeasurementsDataSourceFactory.DATASOURCE_NAME, PmdpMeasurementSchema)
        ).getAll();
    }

    private getProtocolStrategy(code: string): HTTPFetchProtocolStrategy {
        const url = new URL(this.config.getValue<string>("old.datasources.parking.pmdp.ApiBaseUrl"));
        url.pathname = path.join(url.pathname, this.getUrlFromCode(code));

        return new HTTPFetchProtocolStrategy({
            method: "GET",
            url: url.toString(),
            responseType: "text",
            headers: { "Content-Type": "text/xml" },
        });
    }

    private getUrlFromCode(code: string) {
        switch (code) {
            case PmdpIdList.pmdp_001:
                return "rychtarka.xml";
            case PmdpIdList.pmdp_002:
                return "jizdecka.xml";
            default:
                throw new Error("Unrecognized parking id");
        }
    }
}

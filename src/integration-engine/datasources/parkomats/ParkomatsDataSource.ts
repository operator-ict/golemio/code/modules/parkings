import { Parkomats } from "#sch";
import { ParkomatInputSchema } from "#sch/parkomats/datasources/ParkomatInputSchema";
import { DataSource, IConfiguration, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IGenericFeature } from "../interfaces/IGenericFeature";
import { IParkomatInput } from "#sch/parkomats/datasources/interfaces/IParkomatInput";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
@injectable()
export class ParkomatsDataSource {
    constructor(@inject(ContainerToken.Config) private config: IConfiguration) {}

    public async getParkomats(params: string): Promise<Array<IGenericFeature<IParkomatInput>>> {
        return new DataSource(
            Parkomats.name + "DataSource",
            new HTTPFetchProtocolStrategy({
                headers: {
                    authorization: this.config.datasources.TSKParkomatsToken,
                },
                method: "GET",
                url: this.config.datasources.TSKParkomats + "/parkingsessionshistory?" + params,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(Parkomats.name + "DataSource", ParkomatInputSchema)
        ).getAll();
    }
}

import { StaticTariffsJsonSchema } from "#sch/datasources/StaticTariffsJsonSchema";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class StaticTariffsDataSourceFactory {
    public static getDataSource(sourceUrl: string): DataSource {
        return new DataSource(
            "StaticTariffsDataSource",
            new HTTPFetchProtocolStrategy({
                headers: {},
                method: "GET",
                url: sourceUrl,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(StaticTariffsJsonSchema.name, StaticTariffsJsonSchema.jsonSchema)
        );
    }
}

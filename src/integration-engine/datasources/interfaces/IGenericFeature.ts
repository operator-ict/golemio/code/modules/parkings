import { Feature } from "@golemio/core/dist/shared/geojson";

export interface IGenericFeature<T extends object> extends Feature {
    properties: T;
}

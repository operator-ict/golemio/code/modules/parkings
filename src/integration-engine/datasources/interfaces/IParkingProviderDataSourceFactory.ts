import { IMrParkitGarageWithTariff } from "#sch/datasources/interfaces/IMrParkitGarage";
import { ISmart4CityLocation } from "#sch/datasources/interfaces/ISmart4CityLocation";
import { ParkingProvider } from "../helpers/ParkingProviderEnum";
import { IDataSourceProvider } from "./IDataSourceProvider";
import { ISmart4CityList } from "#sch/datasources/interfaces/ISmart4CityList";
import { IBedrichovParking } from "#sch/datasources/interfaces/IBedrichovParking";

export type DataSourceProviderDict = {
    [ParkingProvider.MrParkit]: IDataSourceProvider<IMrParkitGarageWithTariff[]>;
    [ParkingProvider.Smart4CityList]: IDataSourceProvider<ISmart4CityList[]>;
    [ParkingProvider.Smart4CityLocation]: IDataSourceProvider<ISmart4CityLocation[]>;
    [ParkingProvider.Bedrichov]: IDataSourceProvider<IBedrichovParking[]>;
};

export type DataSourceReturnType<T extends ParkingProvider> = ReturnType<DataSourceProviderDict[T]["getDataSource"]>;

export interface IParkingProviderDataSourceFactory {
    getDataSource<T extends ParkingProvider>(
        presetLogsProvider: T,
        ...params: Parameters<DataSourceProviderDict[T]["getDataSource"]>
    ): DataSourceReturnType<T>;
}

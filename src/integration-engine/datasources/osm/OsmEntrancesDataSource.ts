import { OsmSchemaGenerator } from "#sch/datasources/osm/FeatureCollectionSchemaGenerator";
import { IOsmEntranceProperties } from "#sch/datasources/osm/interfaces/IOsmEntranceProperties";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IGenericFeature } from "./../interfaces/IGenericFeature";

@injectable()
export class OsmEntrancesDataSource {
    private url: string;

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {
        this.url = this.config.getValue<string>("old.datasources.parking.osm.datasource.entrancesUrl");
    }

    public async getOsmEntrances(): Promise<Array<IGenericFeature<IOsmEntranceProperties>>> {
        return new DataSource<Array<IGenericFeature<IOsmEntranceProperties>>>(
            "OsmEntrancesDataSource",
            new HTTPFetchProtocolStrategy({
                method: "GET",
                url: this.url,
                headers: {
                    "Content-Type": "application/json",
                },
            }),
            new JSONDataTypeStrategy({ resultsPath: "features" }),
            new JSONSchemaValidator("OsmEntrancesDataSourceValidator", OsmSchemaGenerator.getEntranceSchema().properties.features)
        ).getAll();
    }
}

import { OsmSchemaGenerator } from "#sch/datasources/osm/FeatureCollectionSchemaGenerator";
import { IOsmParkingProperties } from "#sch/datasources/osm/interfaces/IOsmParkingProperties";
import { IOsmParkingSpacesProperties } from "#sch/datasources/osm/interfaces/IOsmParkingSpacesProperties";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IGenericFeature } from "../interfaces/IGenericFeature";

@injectable()
export class OsmDataSource {
    private parkingsUrl: string;
    private parkingsLocation: string;

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {
        this.parkingsUrl = this.config.getValue<string>("old.datasources.parking.osm.datasource.parkingUrl");
        this.parkingsLocation = this.config.getValue<string>("old.datasources.parking.osm.datasource.parkingLocationUrl");
    }

    public async getParkingsData(): Promise<Array<IGenericFeature<IOsmParkingProperties>>> {
        return new DataSource<Array<IGenericFeature<IOsmParkingProperties>>>(
            "ParkingOsmDataSource",
            new HTTPFetchProtocolStrategy({
                method: "GET",
                url: this.parkingsUrl,
                headers: {
                    "Content-Type": "application/json",
                },
            }),
            new JSONDataTypeStrategy({ resultsPath: "features" }),
            new JSONSchemaValidator("ParkingOsmDataSourceValidator", OsmSchemaGenerator.getParkingSchema().properties.features)
        ).getAll();
    }

    public async getParkingsLocationData(): Promise<Array<IGenericFeature<IOsmParkingSpacesProperties>>> {
        return new DataSource<Array<IGenericFeature<IOsmParkingSpacesProperties>>>(
            "ParkingSpacesOsmDataSource",
            new HTTPFetchProtocolStrategy({
                method: "GET",
                url: this.parkingsLocation,
                headers: {
                    "Content-Type": "application/json",
                },
            }),
            new JSONDataTypeStrategy({ resultsPath: "features" }),
            new JSONSchemaValidator(
                "ParkingOsmDataSourceValidator",
                OsmSchemaGenerator.getParkingSpacesSchema().properties.features
            )
        ).getAll();
    }
}

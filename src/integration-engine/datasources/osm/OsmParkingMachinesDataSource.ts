import { OsmSchemaGenerator } from "#sch/datasources/osm/FeatureCollectionSchemaGenerator";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IGenericFeature } from "./../interfaces/IGenericFeature";
import { IOsmParkingMachinesProperties } from "#sch/datasources/osm/interfaces/IOsmParkingMachinesProperties";

@injectable()
export class OsmParkingMachinesDataSource {
    private url: string;

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {
        this.url = this.config.getValue<string>("module.parking.osm.datasource.parkingMachinesUrl");
    }

    public async getOsmParkingMachines(): Promise<Array<IGenericFeature<IOsmParkingMachinesProperties>>> {
        return new DataSource<Array<IGenericFeature<IOsmParkingMachinesProperties>>>(
            "OsmParkingMachinesDataSource",
            new HTTPFetchProtocolStrategy({
                method: "GET",
                url: this.url,
                headers: {
                    "Content-Type": "application/json",
                },
            }),
            new JSONDataTypeStrategy({ resultsPath: "features" }),
            new JSONSchemaValidator(
                "OsmParkingMachinesDataSourceValidator",
                OsmSchemaGenerator.getParkingMachinesSchema().properties.features
            )
        ).getAll();
    }
}

import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { IMrParkitGarageWithTariff } from "#sch/datasources/interfaces/IMrParkitGarage";
import { ISmart4CityLocation } from "#sch/datasources/interfaces/ISmart4CityLocation";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ParkingProvider } from "./helpers/ParkingProviderEnum";
import { IDataSourceProvider } from "./interfaces/IDataSourceProvider";
import {
    DataSourceProviderDict,
    DataSourceReturnType,
    IParkingProviderDataSourceFactory,
} from "./interfaces/IParkingProviderDataSourceFactory";
import { ISmart4CityList } from "#sch/datasources/interfaces/ISmart4CityList";
import { IBedrichovParking } from "#sch/datasources/interfaces/IBedrichovParking";

@injectable()
export class ParkingProviderDataSourceFactory implements IParkingProviderDataSourceFactory {
    private readonly dataSourceProviderDict: DataSourceProviderDict;

    constructor(
        @inject(ModuleContainerToken.MrParkitDataSourceProvider)
        mrParkitDataSourceProvider: IDataSourceProvider<IMrParkitGarageWithTariff[]>,
        @inject(ModuleContainerToken.Smart4CityListDataSourceProvider)
        smart4CityListDataSourceProvider: IDataSourceProvider<ISmart4CityList[]>,
        @inject(ModuleContainerToken.Smart4CityLocationDataSourceProvider)
        smart4CityLocationDataSourceProvider: IDataSourceProvider<ISmart4CityLocation[]>,
        @inject(ModuleContainerToken.BedrichovDataSource)
        bedrichovDataSource: IDataSourceProvider<IBedrichovParking[]>
    ) {
        this.dataSourceProviderDict = {
            [ParkingProvider.MrParkit]: mrParkitDataSourceProvider,
            [ParkingProvider.Smart4CityList]: smart4CityListDataSourceProvider,
            [ParkingProvider.Smart4CityLocation]: smart4CityLocationDataSourceProvider,
            [ParkingProvider.Bedrichov]: bedrichovDataSource,
        };
    }

    public getDataSource<T extends ParkingProvider>(
        presetLogsProvider: T,
        ...params: Parameters<DataSourceProviderDict[T]["getDataSource"]>
    ): DataSourceReturnType<T> {
        return this.dataSourceProviderDict[presetLogsProvider].getDataSource(...params) as DataSourceReturnType<T>;
    }
}

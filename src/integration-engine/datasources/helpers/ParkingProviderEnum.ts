export enum ParkingProvider {
    MrParkit = "MrParkit",
    Smart4CityList = "Smart4CityList",
    Smart4CityLocation = "Smart4CityLocation",
    Bedrichov = "Bedrichov",
}

import { smart4CityListJsonSchema } from "#sch/datasources/Smart4CityListJsonSchema";
import { ISmart4CityList } from "#sch/datasources/interfaces/ISmart4CityList";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IDataSourceProvider } from "../interfaces/IDataSourceProvider";

@injectable()
export class Smart4CityListDataSourceProvider implements IDataSourceProvider<ISmart4CityList[]> {
    private static DATASOURCE_NAME = "Smart4CityListDataSource";

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getDataSource(): IDataSource<ISmart4CityList[]> {
        return new DataSource<ISmart4CityList[]>(
            Smart4CityListDataSourceProvider.DATASOURCE_NAME,
            this.getProtocolStrategy(),
            new JSONDataTypeStrategy({ resultsPath: "cities" }),
            new JSONSchemaValidator(Smart4CityListDataSourceProvider.DATASOURCE_NAME, smart4CityListJsonSchema)
        );
    }

    private getProtocolStrategy(): HTTPFetchProtocolStrategy {
        const url = new URL(this.config.getValue<string>("old.datasources.parking.Smart4City.ApiBaseUrl"));

        return new HTTPFetchProtocolStrategy({
            method: "GET",
            url: url.toString(),
            headers: { "Content-Type": "application/json" },
        });
    }
}

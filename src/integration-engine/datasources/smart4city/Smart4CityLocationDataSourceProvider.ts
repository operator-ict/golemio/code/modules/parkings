import { smart4CityLocationJsonSchema } from "#sch/datasources/Smart4CityLocationJsonSchema";
import { ISmart4CityLocation } from "#sch/datasources/interfaces/ISmart4CityLocation";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import path from "path";
import { IDataSourceProvider } from "../interfaces/IDataSourceProvider";

@injectable()
export class Smart4CityLocationDataSourceProvider implements IDataSourceProvider<ISmart4CityLocation[]> {
    private static DATASOURCE_NAME = "Smart4CityLocationDataSource";

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getDataSource(cityCode: string): IDataSource<ISmart4CityLocation[]> {
        return new DataSource<ISmart4CityLocation[]>(
            Smart4CityLocationDataSourceProvider.DATASOURCE_NAME,
            this.getProtocolStrategy(cityCode),
            new JSONDataTypeStrategy({ resultsPath: "parkinglocations" }),
            new JSONSchemaValidator(Smart4CityLocationDataSourceProvider.DATASOURCE_NAME, smart4CityLocationJsonSchema)
        );
    }

    private getProtocolStrategy(cityCode: string): HTTPFetchProtocolStrategy {
        const url = new URL(this.config.getValue<string>("old.datasources.parking.Smart4City.ApiBaseUrl"));
        url.pathname = path.join(url.pathname, cityCode, "/parkinglocations");

        return new HTTPFetchProtocolStrategy({
            method: "GET",
            url: url.toString(),
            headers: { "Content-Type": "application/json" },
        });
    }
}

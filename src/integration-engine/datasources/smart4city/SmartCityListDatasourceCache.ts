import { ParkingProvider } from "#ie/datasources/helpers/ParkingProviderEnum";
import {
    DataSourceReturnType,
    IParkingProviderDataSourceFactory,
} from "#ie/datasources/interfaces/IParkingProviderDataSourceFactory";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ISmart4CityList } from "#sch/datasources/interfaces/ISmart4CityList";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

const TWELVE_HOURS = 12 * 60 * 60 * 1000;

@injectable()
export class SmartCityListDatasourceCache {
    private data: Array<Pick<ISmart4CityList, "code">>;
    private dataSource: DataSourceReturnType<ParkingProvider.Smart4CityList>;
    private readonly updatedAt: number;

    constructor(
        @inject(ModuleContainerToken.ParkingProviderDataSourceFactory)
        private dataSourceFactory: IParkingProviderDataSourceFactory
    ) {
        this.dataSource = this.dataSourceFactory.getDataSource(ParkingProvider.Smart4CityList);
        this.data = [];
        this.updatedAt = new Date().getTime();
    }

    public async getAll() {
        const now = Date.now();
        if (!this.data.length || this.updatedAt + TWELVE_HOURS < now) {
            const response: ISmart4CityList[] = await this.dataSource.getAll();
            this.data = response.map((el) => ({ code: el.code }));
        }

        return this.data;
    }
}

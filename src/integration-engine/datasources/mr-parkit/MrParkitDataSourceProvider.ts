import { mrParkitJsonSchema } from "#sch/datasources/MrParkitJsonSchema";
import { IMrParkitGarageWithTariff } from "#sch/datasources/interfaces/IMrParkitGarage";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IDataSourceProvider } from "../interfaces/IDataSourceProvider";

type MrParkitDto = IMrParkitGarageWithTariff[];

@injectable()
export class MrParkitDataSourceProvider implements IDataSourceProvider<MrParkitDto> {
    private static DATASOURCE_NAME = "MrParkitDataSource";

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getDataSource(): IDataSource<MrParkitDto> {
        const url = this.config.getValue<string>("old.datasources.parking.mrParkit.datasource.url");
        const apiVersion = this.config.getValue<string>("old.datasources.parking.mrParkit.datasource.apiVersion");
        const apiKey = this.config.getValue<string>("old.datasources.parking.mrParkit.datasource.apiKey");

        return new DataSource<MrParkitDto>(
            MrParkitDataSourceProvider.DATASOURCE_NAME,
            this.getProtocolStrategy(url, apiVersion, apiKey),
            new JSONDataTypeStrategy({ resultsPath: "garages" }),
            new JSONSchemaValidator(MrParkitDataSourceProvider.DATASOURCE_NAME, mrParkitJsonSchema)
        );
    }

    private getProtocolStrategy(url: string, apiVersion: string, apiKey: string): HTTPFetchProtocolStrategy {
        return new HTTPFetchProtocolStrategy({
            method: "GET",
            url: url,
            headers: {
                "Content-Type": "application/json",
                "x-api-version": apiVersion,
                "x-api-key": apiKey,
            },
            timeoutInSeconds: 20,
        });
    }
}

import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { IptOictFeatureCollectionSchemaGenerator } from "#sch/datasources/iptoict/IptOictFeatureCollectionSchemaGenerator";

@injectable()
export class IptOictEntrancesDataSource {
    public getEntrancesData(sourceUrl: string) {
        return new DataSource(
            "IptOictParkingEntrancesDataSource",
            new HTTPFetchProtocolStrategy({
                method: "GET",
                url: sourceUrl,
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
            }),
            new JSONDataTypeStrategy({ resultsPath: "features" }),
            new JSONSchemaValidator(
                "IptOictParkingEntrancesSourceValidator",
                IptOictFeatureCollectionSchemaGenerator.getParkingEntrancesSchema().properties.features
            )
        ).getAll();
    }
}

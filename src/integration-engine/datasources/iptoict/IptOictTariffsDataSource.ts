import { iptOictTariffsSchema } from "#sch/datasources/iptoict/IptOictTariffsSchema";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";

@injectable()
export class IptOictTariffsDataSource {
    public getTariffsData(sourceUrl: string) {
        return new DataSource(
            "IptOictTariffsDataSource",
            new HTTPFetchProtocolStrategy({
                method: "GET",
                url: sourceUrl,
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator("IptOictTariffsDataSourceValidator", iptOictTariffsSchema)
        ).getAll();
    }
}

import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { iptOictPaymentsSchema } from "#sch/datasources/iptoict/IptOictPaymentsSchema";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";

@injectable()
export class IptOictPaymentsDataSource {
    public getPaymentsData(sourceUrl: string) {
        return new DataSource(
            "IptOictPaymentsDataSource",
            new HTTPFetchProtocolStrategy({
                method: "GET",
                url: sourceUrl,
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator("IptOictPaymentsDataSourceValidator", iptOictPaymentsSchema)
        ).getAll();
    }
}

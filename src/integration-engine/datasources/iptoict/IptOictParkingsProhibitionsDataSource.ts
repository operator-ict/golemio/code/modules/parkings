import { iptOictParkingProhibitionsSchema } from "#sch/datasources/iptoict/IptOictParkingProhibitionsSchema";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class IptOictParkingsProhibitionsDataSource {
    public getParkingsProhibitionsData(sourceUrl: string) {
        return new DataSource(
            "IptOictParkingsProhibitionsDataSource",
            new HTTPFetchProtocolStrategy({
                method: "GET",
                url: sourceUrl,
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator("IptOictParkingsProhibitionsDataSource", iptOictParkingProhibitionsSchema)
        ).getAll();
    }
}

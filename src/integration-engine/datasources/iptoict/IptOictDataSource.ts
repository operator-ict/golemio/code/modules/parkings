import { IptOictFeatureCollectionSchemaGenerator } from "#sch/datasources/iptoict/IptOictFeatureCollectionSchemaGenerator";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class IptOictDataSource {
    public getParkingData(sourceUrl: string) {
        return new DataSource(
            "IptOictParkingDataSource",
            new HTTPFetchProtocolStrategy({
                method: "GET",
                url: sourceUrl,
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
            }),
            new JSONDataTypeStrategy({ resultsPath: "features" }),
            new JSONSchemaValidator(
                "IptOictParkingDataSourceValidator",
                IptOictFeatureCollectionSchemaGenerator.getParkingSchema().properties.features
            )
        ).getAll();
    }

    public getParkingLocationData(sourceUrl: string) {
        return new DataSource(
            "IptOictParkingDataSource",
            new HTTPFetchProtocolStrategy({
                method: "GET",
                url: sourceUrl,
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
            }),
            new JSONDataTypeStrategy({ resultsPath: "features" }),
            new JSONSchemaValidator(
                "IptOictParkingDataSourceSourceValidator",
                IptOictFeatureCollectionSchemaGenerator.getParkingSpacesSchema().properties.features
            )
        ).getAll();
    }
}

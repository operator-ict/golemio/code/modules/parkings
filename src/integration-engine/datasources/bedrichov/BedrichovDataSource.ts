import { BedrichovParkingSchema } from "#sch/datasources/BedrichovParkingSchema";
import { IBedrichovParking } from "#sch/datasources/interfaces/IBedrichovParking";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class BedrichovDataSource {
    private static DATASOURCE_NAME = "BedrichovDataSource";

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getDataSource(): IDataSource<IBedrichovParking> {
        const url = this.config.getValue<string>("module.parking.bedrichov.datasource.url");
        const token = this.config.getValue<string>("module.parking.bedrichov.datasource.token");

        return new DataSource<IBedrichovParking>(
            BedrichovDataSource.DATASOURCE_NAME,
            this.getProtocolStrategy(url, token),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(BedrichovDataSource.DATASOURCE_NAME, BedrichovParkingSchema)
        );
    }

    private getProtocolStrategy(url: string, token: string): HTTPFetchProtocolStrategy {
        return new HTTPFetchProtocolStrategy({
            method: "POST",
            url: url,
            headers: {
                "Content-Type": "application/json",
                Authorization: `Token ${token}`,
            },
        });
    }
}

import { StaticParkingLotsGeoJsonSchema } from "#sch/datasources/StaticParkingLotsGeoJsonSchema";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class StaticParkingLotsGeoDataSourceFactory {
    public static getDataSource(sourceUrl: string): DataSource {
        return new DataSource(
            "StaticParkingLotsGeoDataSource",
            new HTTPFetchProtocolStrategy({
                headers: {},
                method: "GET",
                url: sourceUrl,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(StaticParkingLotsGeoJsonSchema.name, StaticParkingLotsGeoJsonSchema.jsonSchema)
        );
    }
}

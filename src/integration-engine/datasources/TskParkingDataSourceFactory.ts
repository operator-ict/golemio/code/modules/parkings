import { TskParkingMachinesSchema } from "#sch/datasources/TskParkingMachinesSchema";
import { TskParkingSectionLevelSchema } from "#sch/datasources/TskParkingSectionLevelSchema";
import { TskParkingSectionSchema } from "#sch/datasources/TskParkingSectionSchema";
import { TskParkingZoneTariffSchema } from "#sch/datasources/TskParkingZoneTariffSchema";
import { IConfiguration } from "@golemio/core/dist/integration-engine/config";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

interface IParkingEndpoints {
    [id: string]: {
        name: string;
        jsonSchema: any;
    };
}

@injectable()
export class TskParkingDataSourceFactory {
    private schemas: IParkingEndpoints = {
        parkingSections: TskParkingSectionSchema,
        parkingSectionLevel251: TskParkingSectionLevelSchema,
        parkingSectionLevel253: TskParkingSectionLevelSchema, // ZTP
        parkingMachines: TskParkingMachinesSchema,
        parkingTariffs: TskParkingZoneTariffSchema,
    };

    constructor(@inject(ContainerToken.Config) private config: IConfiguration) {}

    public getDataSource(endpoint: string): DataSource {
        const authToken = Buffer.from(
            this.config.datasources.parking.TskApiCredentials.user + ":" + this.config.datasources.parking.TskApiCredentials.pass
        ).toString("base64");

        return new DataSource(
            this.schemas[endpoint].name + "DataSource",
            new HTTPFetchProtocolStrategy({
                headers: {
                    Authorization: `Basic ${authToken}`,
                },
                method: "GET",
                url: this.config.datasources.parking.TskApiBaseUrl + this.config.datasources.parking.TskApiEndpoints[endpoint],
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(this.schemas[endpoint].name, this.schemas[endpoint].jsonSchema)
        );
    }
}

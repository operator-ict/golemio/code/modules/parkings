import { BaseController } from "@golemio/core/dist/input-gateway/controllers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { InputParkingSourceSchema } from "#sch/datasources/InputParkingSourceSchema";

export class ParkingSourcesController extends BaseController {
    public name!: string;
    protected validator!: JSONSchemaValidator;
    protected queuePrefix!: string;

    constructor() {
        super("ParkingSources", new JSONSchemaValidator("ParkingSource", InputParkingSourceSchema));
    }

    processData = async (inputData: any): Promise<void> => {
        try {
            await this.validator.Validate(inputData);
            const queue = "input." + this.queuePrefix + ".updateParkingSource";

            await this.sendMessageToExchange(queue, JSON.stringify(inputData), {
                persistent: true,
            });
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while validating input data.", this.name, err, 422);
            }
        }
    };
}

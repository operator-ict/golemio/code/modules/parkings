import { ParkingSourcesController } from "#ig/controllers/ParkingSourcesController";
import { checkContentTypeMiddleware } from "@golemio/core/dist/input-gateway/helpers";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";

export class ParkingSourcesRouter {
    public router: Router;

    constructor(private sourcesController: ParkingSourcesController) {
        this.router = Router();
        this.initRoutes();
    }

    private initRoutes = (): void => {
        this.router.post("/source", checkContentTypeMiddleware(["application/json"]), this.postParkingSource);
    };

    private postParkingSource = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.sourcesController.processData(req.body);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}

const parkingSourcesController = new ParkingSourcesController();

const parkingSourcesRouter = new ParkingSourcesRouter(parkingSourcesController).router;
export { parkingSourcesRouter };

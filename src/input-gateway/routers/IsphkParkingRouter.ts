import { IsphkParkingController } from "#ig/controllers/IsphkParkingController";
import { checkContentTypeMiddleware } from "@golemio/core/dist/input-gateway/helpers";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";

export class IsphkParkingRouter {
    public router: Router;

    constructor(private isphkParkingController: IsphkParkingController) {
        this.router = Router();
        this.initRoutes();
    }

    private initRoutes = (): void => {
        this.router.post("/measurements", checkContentTypeMiddleware(["application/json"]), this.postIsphkMeasurements);
    };

    private postIsphkMeasurements = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.isphkParkingController.processData(req.body);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}

const isphkParkingController = new IsphkParkingController();

const isphkParkingRouter = new IsphkParkingRouter(isphkParkingController).router;
export { isphkParkingRouter };

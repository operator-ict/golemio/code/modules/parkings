/* ig/korid-parkings/index.ts */
export * from "./KoridParkingsConfigController";
export * from "./KoridParkingsDataController";
export * from "./KoridParkingsRouter";

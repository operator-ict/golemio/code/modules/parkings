import { NumberOfRecordsEventData } from "@golemio/core/dist/helpers/logger/interfaces/INumberOfRecordsEventData";
import { LoggerEventType, checkContentTypeMiddleware, loggerEvents } from "@golemio/core/dist/input-gateway/helpers";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { KoridParkingsConfigController, KoridParkingsDataController } from "./";

export class KoridParkingsRouter {
    public router: Router;
    private configController: KoridParkingsConfigController;
    private dataController: KoridParkingsDataController;

    constructor() {
        this.router = Router();
        this.configController = new KoridParkingsConfigController();
        this.dataController = new KoridParkingsDataController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.post("/config", checkContentTypeMiddleware(["application/json"]), this.PostConfig);
        this.router.post("/data", checkContentTypeMiddleware(["application/json"]), this.PostData);
    };

    private PostConfig = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.configController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: req.body.geojson.features.length,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
    private PostData = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.dataController.processData(req.body);
            // logging number of records
            const dataToEmit: NumberOfRecordsEventData = {
                numberOfRecords: Object.keys(req.body.data).length,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}

const parkingsRouter = new KoridParkingsRouter().router;

export { parkingsRouter };

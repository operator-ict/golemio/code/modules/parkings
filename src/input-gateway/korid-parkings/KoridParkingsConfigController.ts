import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { BaseController } from "@golemio/core/dist/input-gateway/controllers";
import { Parkings } from "#sch/index";

export class KoridParkingsConfigController extends BaseController {
    public name!: string;
    protected validator!: JSONSchemaValidator;
    protected queuePrefix!: string;

    constructor() {
        super(Parkings.name, new JSONSchemaValidator(Parkings.korid.name, Parkings.korid.datasourceJsonConfSchema));
    }

    /**
     * Data processing
     */
    public processData = async (inputData: any): Promise<void> => {
        try {
            await this.validator.Validate(inputData); // throws an error

            await this.sendMessageToExchange("input." + this.queuePrefix + ".saveKoridConfToDB", JSON.stringify(inputData), {
                persistent: true,
            });
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while validating input data.", this.name, err, 422);
            }
        }
    };
}

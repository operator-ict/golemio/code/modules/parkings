/**
 *  Parsing strings based on https://wiki.openstreetmap.org/wiki/Key:opening_hours#Implementation
 *  Minimal implementation:
 * - "24/7"
 * - "09:00-22:00"
 * - "Mo-Sa 09:00-19:00; Su 10:00-18:00"
 * - "Mo-Fr 07:00-01:00"
 */

import { DaysInWeekEnum } from "#helpers/constants/periods/DaysInWeekEnum";
import { PhTypes } from "#helpers/constants/periods/PhTypesEnum";
import { IOpeningHoursPeriod } from "#sch/models/interfaces/openingHours/IOpeningHoursPeriod";

export class OpeningHoursParser {
    private readonly TWENTY_FOUR_SEVEN = "24/7";
    private readonly OSM_BASIC_FORMAT_REGEX =
        // eslint-disable-next-line max-len
        /(?<element>(?<days>(Mo|Tu|We|Th|Fr|Sa|Su)([-,]?(Mo|Tu|We|Th|Fr|Sa|Su))*)*[ ]?(?<timeSpan>\d{2}:\d{2}-\d{2}:\d{2},?){1,})(; )?/gs;
    private readonly OSM_FORMAT_REGEX =
        // eslint-disable-next-line max-len
        /^((?<element>(?<days>(Mo|Tu|We|Th|Fr|Sa|Su)([-,]?(Mo|Tu|We|Th|Fr|Sa|Su))*)*[ ]?(?<timeSpan>\d{2}:\d{2}-\d{2}:\d{2},?){1,})(; )?)+$/;
    private daysInWeek: string[];

    constructor() {
        this.daysInWeek = Object.values(DaysInWeekEnum);
    }

    public parse(openingHours: string | null): Array<Partial<IOpeningHoursPeriod>> | null {
        const periods: Array<Partial<IOpeningHoursPeriod>> = [];

        if (!openingHours) {
            return null;
        }

        if (openingHours === this.TWENTY_FOUR_SEVEN) {
            return this.generatePeriods(Object.values(DaysInWeekEnum), "00:00", "23:59", Object.values(PhTypes));
        }

        if (this.OSM_FORMAT_REGEX.test(openingHours) === false) {
            return null;
        }

        const result = openingHours.matchAll(this.OSM_BASIC_FORMAT_REGEX);

        for (const match of result) {
            const isDayRange = match.groups?.days?.includes("-");
            if (isDayRange) {
                const days = match.groups?.days?.split("-");
                const startDay = days![0] as DaysInWeekEnum;
                const endDay = days![1] as DaysInWeekEnum;
                //get all other days in between startDay and endDay
                const startDayIndex = this.daysInWeek.indexOf(startDay);
                const endDayIndex = this.daysInWeek.indexOf(endDay);
                const daysInBetween = this.daysInWeek.slice(startDayIndex, endDayIndex + 1);

                const timeSpan = match.groups?.timeSpan.split("-");
                const startTime = timeSpan![0];
                const endTime = timeSpan![1];

                periods.push(
                    ...this.generatePeriods(daysInBetween as DaysInWeekEnum[], startTime, endTime, Object.values(PhTypes))
                );
            } else {
                // single day or multiple days
                const days = match.groups?.days?.split(",") ?? Object.values(DaysInWeekEnum);
                const timeSpan = match.groups?.timeSpan.split("-");
                const startTime = timeSpan![0];
                const endTime = timeSpan![1];

                periods.push(...this.generatePeriods(days as DaysInWeekEnum[], startTime, endTime, Object.values(PhTypes)));
            }
        }

        return periods;
    }

    private generatePeriods = (
        weekDays: DaysInWeekEnum[],
        startTime: string,
        endTime: string,
        ph: PhTypes[]
    ): Array<Partial<IOpeningHoursPeriod>> => {
        const result: Array<Partial<IOpeningHoursPeriod>> = [];

        for (const day of weekDays) {
            for (const phType of ph) {
                if (startTime < endTime) {
                    result.push({ day_in_week: day.toString(), start: startTime, end: endTime, ph: phType });
                } else {
                    result.push({ day_in_week: day.toString(), start: startTime, end: "23:59", ph: phType });
                    result.push({ day_in_week: this.getNextDay(day), start: "00:00", end: endTime, ph: phType });
                }
            }
        }

        return result;
    };

    private getNextDay = (day: DaysInWeekEnum): DaysInWeekEnum => {
        const index = this.daysInWeek.indexOf(day);

        return this.daysInWeek[(index + 1) % 7] as DaysInWeekEnum;
    };
}

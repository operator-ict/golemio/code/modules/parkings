/**
 *  Parsing strings based on https://wiki.openstreetmap.org/wiki/Key:maxstay
 *  Minimal implementation:
 * - "unlimited", "none", "no"
 * - "90 minutes"
 * - "1 hour"; "5 hours", "1.5 hours"
 * - "1 day"; "2 days"
 */

export class MaxStayParser {
    public static parse(maxStay: string | null): number | null | undefined {
        if (!maxStay) {
            return null;
        }

        if (maxStay === "unlimited" || maxStay === "none" || maxStay === "no") {
            return undefined;
        }

        const maxStayMatch = maxStay.match(/^(\d+(\.\d+)?)\s*(minute|hour|day)s?$/);

        if (maxStayMatch) {
            const value = parseFloat(maxStayMatch[1]);
            const unit = maxStayMatch[3];

            if (unit === "minute") {
                return value;
            }

            if (unit === "hour") {
                return value * 60;
            }

            if (unit === "day") {
                return value * 60 * 24;
            }
        }

        return null;
    }
}

import { SourceEnum } from "#helpers/constants/SourceEnum";
import { Parkings } from "#sch";
import { IParking } from "#sch/models/interfaces/IParking";
import { ParkingModel } from "#sch/models/ParkingModel";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractCachedRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractCachedRepository";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { QueryTypes } from "@golemio/core/dist/shared/sequelize";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class CachedPmdpParkingRepository extends AbstractCachedRepository<IParking> {
    public schema: string = Parkings.pgSchema;
    public tableName: string = "parkings";

    constructor(connector: IDatabaseConnector, log: ILogger, cacheTTLInSeconds: number = 60 * 60) {
        super(connector, log, cacheTTLInSeconds);
    }

    public async getPmdpParking(id: string): Promise<IParking> {
        try {
            const parking = (await this.getAll()).find((el) => el.id === id);
            if (!parking) {
                throw new Error(`Id ${id} not found`);
            }
            return parking;
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                throw error;
            }
            throw new GeneralError(`Unable to get PMDP parking data`, CachedPmdpParkingRepository.name, error, 500);
        }
    }

    protected async getAllInternal(): Promise<IParking[]> {
        return await this.connector.getConnection().query<IParking>(
            `SELECT ${ParkingModel.attributeList.join(",")}
            FROM ${this.schema}.${this.tableName}
            WHERE source = '${SourceEnum.PMDP}'`,
            { type: QueryTypes.SELECT, raw: true }
        );
    }
}

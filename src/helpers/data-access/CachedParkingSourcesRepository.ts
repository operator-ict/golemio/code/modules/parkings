import { Parkings } from "#sch";
import { ParkingSourcesModel } from "#sch/models/ParkingSourcesModel";
import { IParkingSource, IParkingSourcePayment, IParkingSourceReservation } from "#sch/models/interfaces/IParkingSource";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractCachedRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractCachedRepository";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { QueryTypes } from "@golemio/core/dist/shared/sequelize";

type TParkingSourceDatasource = {
    source: string;
    datasource_parking: string;
    datasource_locations: string;
};

type TParkingPaymentsSourceDatasource = {
    source: string;
    datasource_payments: string;
};

export type TParkingPaymentsSourceConfig = {
    [key: string]: {
        payment: IParkingSourcePayment | null;
        reservation: IParkingSourceReservation | null;
    };
};

export type TParkingEntrancesSourceDatasourceConfig = {
    source: string;
    datasource_entranecs: string;
};

export type TParkingProhibitionsSourceDatasourceConfig = {
    source: string;
    datasource_prohibitions: string;
};

export type TParkingTariffsSourceDatasourceConfig = {
    source: string;
    datasource_tariffs: string;
};

export class CachedParkingSourcesRepository extends AbstractCachedRepository<IParkingSource> {
    public schema: string = Parkings.pgSchema;
    public tableName: string = "parking_sources";

    constructor(connector: IDatabaseConnector, log: ILogger, cacheTTLInSeconds: number = 60 * 5) {
        super(connector, log, cacheTTLInSeconds);
    }

    public async getV3Sources(options: { isRestrictedToOpenData: boolean }): Promise<string[]> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData))
            .filter((element) => element.api_v3_allowed)
            .map((element) => element.source);
    }

    public async getLegacySources(options: { isRestrictedToOpenData: boolean }): Promise<string[]> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData))
            .filter((element) => element.legacy_api_allowed)
            .map((element) => element.source);
    }

    public async getParkingSpaceDataSourceSources(options: { isRestrictedToOpenData: boolean }): Promise<string[]> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData))
            .filter((element) => element.datasource_parking && element.datasource_locations)
            .map((element) => element.source);
    }

    public async getParkingPaymentDataSourceSources(options: { isRestrictedToOpenData: boolean }): Promise<string[]> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData))
            .filter((element) => element.datasource_payments)
            .map((element) => element.source);
    }

    public async getEntrancesDataSourceSources(options: { isRestrictedToOpenData: boolean }): Promise<string[]> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData))
            .filter((element) => element.datasource_entrances)
            .map((element) => element.source);
    }

    public async getProhibitionsDataSourceSources(options: { isRestrictedToOpenData: boolean }): Promise<string[]> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData))
            .filter((element) => element.datasource_prohibitions)
            .map((element) => element.source);
    }

    public async getTariffsDataSourceSources(options: { isRestrictedToOpenData: boolean }): Promise<string[]> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData))
            .filter((element) => element.datasource_tariffs)
            .map((element) => element.source);
    }

    public async getParkingSpaceDataSourceConfig(options: {
        isRestrictedToOpenData: boolean;
    }): Promise<TParkingSourceDatasource[]> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData))
            .filter((element) => element.datasource_parking && element.datasource_locations)
            .map((element) => ({
                source: element.source,
                datasource_parking: element.datasource_parking!,
                datasource_locations: element.datasource_locations!,
            }));
    }

    public async getParkingPaymentsDataSourceConfig(options: {
        isRestrictedToOpenData: boolean;
    }): Promise<TParkingPaymentsSourceDatasource[]> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData))
            .filter((element) => element.datasource_payments)
            .map((element) => ({
                source: element.source,
                datasource_payments: element.datasource_payments!,
            }));
    }

    public async getParkingEntrancesDataSourceConfig(options: {
        isRestrictedToOpenData: boolean;
    }): Promise<TParkingEntrancesSourceDatasourceConfig[]> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData))
            .filter((element) => element.datasource_entrances)
            .map((element) => ({
                source: element.source,
                datasource_entranecs: element.datasource_entrances!,
            }));
    }

    public async getParkingProhibitionsDataSourceConfig(options: {
        isRestrictedToOpenData: boolean;
    }): Promise<TParkingProhibitionsSourceDatasourceConfig[]> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData))
            .filter((element) => element.datasource_prohibitions)
            .map((element) => ({
                source: element.source,
                datasource_prohibitions: element.datasource_prohibitions!,
            }));
    }

    public async getParkingPaymentsConfig(options: { isRestrictedToOpenData: boolean }): Promise<TParkingPaymentsSourceConfig> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData)).reduce(
            (result: TParkingPaymentsSourceConfig, el: IParkingSource) => {
                if (el.payment || el.reservation) {
                    result[el.source] = {
                        payment: el.payment!,
                        reservation: el.reservation!,
                    };
                }
                return result;
            },
            {}
        );
    }

    public async getParkingTariffDataSourceConfig(options: {
        isRestrictedToOpenData: boolean;
    }): Promise<TParkingTariffsSourceDatasourceConfig[]> {
        return (await this.getAllAllowed(options.isRestrictedToOpenData))
            .filter((element) => element.datasource_tariffs)
            .map((element) => ({
                source: element.source,
                datasource_tariffs: element.datasource_tariffs!,
            }));
    }

    private async getAllAllowed(isRestrictedToOpenData: boolean): Promise<IParkingSource[]> {
        try {
            return (await this.getAll()).filter((element) => this.filterOpenData(element, isRestrictedToOpenData));
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                throw error;
            }
            throw new GeneralError("Unable to load parking source table", CachedParkingSourcesRepository.name, error, 500);
        }
    }

    private filterOpenData(source: IParkingSource, isRestrictedToOpenData: boolean): boolean {
        if (!isRestrictedToOpenData) return true;
        return source.open_data;
    }

    protected async getAllInternal(): Promise<IParkingSource[]> {
        return await this.connector.getConnection().query<IParkingSource>(
            `SELECT ${ParkingSourcesModel.attributeList.join(",")}
            FROM ${this.schema}.${this.tableName}`,
            { type: QueryTypes.SELECT, raw: true }
        );
    }
}

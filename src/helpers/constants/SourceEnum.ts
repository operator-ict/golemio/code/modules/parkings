export enum SourceEnum {
    TSK = "tsk",
    TSK_V2 = "tsk_v2",
    IPR = "ipr",
    Smart4City = "smart4city",
    Korid = "korid",
    Mr_Parkit = "mr_parkit",
    Manual = "manual",
    OSM = "osm",
    Isphk = "isphk",
    PMDP = "pmdp",
    TestCases = "test_cases",
    Bedrichov = "bedrichov",
}

export function isSecondarySource(source: string) {
    return source.toLocaleLowerCase() === SourceEnum.OSM;
}

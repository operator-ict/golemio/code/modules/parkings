export enum ParkingPolicyEnum {
    ParkAndRide = "park_and_ride",
    KissAndRide = "kiss_and_ride",
    Commercial = "commercial",
    Zone = "zone",
    ParkSharing = "park_sharing",
    CustomerOnly = "customer_only",
}
